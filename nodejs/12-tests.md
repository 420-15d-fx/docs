# Plan de tests

## Des tests, pour quoi faire ?

::: tip Principal objectif des tests d’une API
- S'assurer que l'implémentation fonctionne correctement comme spécifiée dans le cahier des charges
- Pas de bogues !
::: 

- Assurance qualité.
- Amélioration continue.
- Réduction des risques.
- Sauve du temps.

## Processus

- Planifier les tests (création d’un plan de tests).
- Trouver un outils pour coder les tests (**Postman**, supertest, pytest, JMeter, mocha, Jasmine, RestAssured…).
- Créer un jeu de données permettant de simuler un environnement réel.
- Coder méticuleusement les tests en utilisant ce jeu de données.
- S’assurer de pouvoir effectuer les tests de façon automatique pour gagner du temps !


Actions de test de l'API

- Vérifier le **code d'état** HTTP correct.
  - Par exemple, la création d'une ressource doit renvoyer 201 CREATED et les demandes non autorisées doivent renvoyer 403 FORBIDDEN, etc.
- **Format** et **données** du body de la réponse
  - Vérifiez que le corps JSON est valide et que les noms, types et valeurs des champs sont corrects, y compris dans les réponses d'erreur.
- Vérifiez les **en-têtes de la réponse**. Les en-têtes du serveur HTTP ont des répercussions sur la sécurité et les performances.
- Vérifier l'intégrité des performances de base. Si une opération a été effectuée avec succès mais a pris un temps déraisonnable, le test échoue.
- **Autorisation** requise
- **Ordre** des données retournées
- Paramètres optionnels (query params)
- **Idempotence** (une opération a le même effet qu'on l'applique une ou plusieurs reprises)
- **Sans-danger** (une opération ne modifie pas la ressource)
- Code d’erreurs
- paramètres invalides (vide)
- paramètres invalides (mauvaise données)
- majuscule/minuscule
- Objet JSON avec trop ou pas assez d’attributs
- …

## Stratégie de tests
- Tests positifs de base (essentiels)
  - Teste les réponses normales attendues.
- Tests positifs : paramètres optionnels
  - Teste les réponses normales attendues avec des paramètres optionnels (query params).

- Tests négatifs : paramètres valides
  - Teste les cas d’opérations non-permises (exemple :  supprimer une configuration utilisateur sans autorisation ou créer une ressource qui existe déjà)
- Tests négatifs : paramètres invalides
  - Teste les cas contenants des paramètres invalides (mauvais format de body/entête, manque d’un paramètre obligatoire)

- Tests destructifs
  - Tests visant à mettre à l’épreuve la robustesse de l’API. On passe des valeurs null, des mauvais formats de données, des body immenses et autre pour tenter de faire planter l’application.

## Création d'un plan de test
- Après avoir établi la stratégie de tests, nous sommes prêts à créer le plan de tests.
- Voir le fichier [Cours18-ExemplePlanTest.xlsx](https://livecegepfxgqc.sharepoint.com/:x:/s/A23-42015DFX-00001/EcR7sRzT2HtBowQdTYavy-UBXVXMob1q54v_Fl6ybM_PuA?e=gfbgvh) pour un exemple de plan de tests.
  - Il contient une description de la stratégie de tests adoptée ainsi qu’un plan de tests détaillé.

## Implémenter les tests dans Postman
- Voir la démo en classe.
- Liens utiles
  - [​​​​​​​/test-scripts/](https://learning.postman.com/docs/writing-scripts/test-scripts/)
  - [/test-examples/](https://learning.postman.com/docs/writing-scripts/script-references/test-examples/)

  ## Jeux de données

Afin de s'assurer de pouvoir effectuer des tests de façon automatique pour gagner du temps, il peut être intéressant d'ajouter une route (disponible seulement en développement bien sûr) afin de réinitialiser notre base de données. Pour ce faire, vous pouvez créer une route spéciale qui videra la base de données et ajoutera de nouvelle données à parti d'un fichier

- fichier /conrollers/dbController.js

```js
"use strict";
​
// Récupère le modèle Article
const Article = require("../models/article");
​
//jeux de données
const articles = require("../seeds/articles");
​
let result = {};    
​
exports.seed = (req, res, next) =>{
​
    //On efface toute les articles de la bd
    Article.deleteMany()
        .then(_ =>{
            
            Article.insertMany(articles)    //Ajout du jeu de données article.
                .then(articles =>{
                    result.articles = articles;
                })
                .catch(err => {
                    if(!err.statusCode){
                        err.statusCode = 500;
                    }
                    next(err);
                });
        })
        .then( () =>{
            res.status(200).json(result); //Retour du résultat
        });
        
};
```

- fichier /seeds/articles.json

```json
[
  {
    "titre": "Premier article",
    "contenu": "Lorem ipsum dolor sit amet, consectetur adipiscing elit.",
    "createdAt": "2022-03-31T09:00:00.000Z",
    "updatedAt": "2022-03-31T09:00:00.000Z"
  },
  {
    "titre": "Deuxième article",
    "contenu": "Praesent eget lorem et neque elementum vestibulum.",
    "createdAt": "2022-03-30T10:12:00.000Z",
    "updatedAt": "2022-03-31T11:05:00.000Z"
  },
  
  //...
  
]
```

- fichier /routes/db.js

```js
const express = require("express");
​
const dbController = require("../controllers/dbController");
​
const router = express.Router();
​
router.get("/db/seed", dbController.seed );
​
module.exports = router;
```

Il est important que cette route ne soit accessible qu'en développement. Pour ce faire, vous pouvez utiliser la variable d'environnement NODE_ENV. Cette variable peut être définie dans le fichier package.json.
Exemple :

```json
"scripts": {
    "start": "NODE_ENV=production node app.js",
    "dev": "NODE_ENV=development nodemon app.js"
  },
```

Elle vaut `development` si vous avez lancé votre serveur avec la commande `npm run dev` et `production` si vous avez lancé votre serveur avec la commande `npm run start`.

- fichier app.js

```js 
// Importe les routes
const postRoutes = require("./routes/article");
const seed = require("./routes/db");
​
// Utilisation des routes en tant que middleware
app.use(postRoutes);
​
if (process.env.NODE_ENV == "development") { //Accessible seulement en developpement
    app.use(seed);
}
```

## Les variables de collection et d'environnement dans Postman

Les variables de collection dans Postman sont des variables qui peuvent être utilisées pour stocker des valeurs réutilisables pour une collection donnée. Elles peuvent être utilisées pour stocker des informations telles que des URL de base, des clés d'API, des identifiants, des jetons d'accès et d'autres informations similaires qui sont nécessaires pour effectuer des requêtes HTTP dans l'ensemble de la collection.

Ces variables sont particulièrement utiles lors des tests, car elles permettent stocker des valeurs qui peuvent être utilisées pour vérifier des conditions spécifiques dans les réponses de requêtes. Par exemple, vous pouvez stocker des variables pour les codes de réponse HTTP attendus, des shémas JSON attendues, le dernier Id créé par un test pour le réutiliser dans un autre test, etc. 

![Variables de collection](./img/test1.png)

La colonne "Initial value" contient la première valeur lors de la création d'une variable. La colonne "Current value" contient la valeur courante de la variable. C'est cette valeur qui sera utilisée et modifié par les tests.

## Réinitialisation des variables
Lors de l'exécution des tests, les valeurs des variables seront modifiée. Idéalement nous voulons pouvoir exécuter nos tests plusieurs fois et toujours sur le même jeu de données. C'est pourquoi il devient intéressant de créer une route `/db/seed` permettant de réinitialiser les données de la base de données.

De même, il devient nécessaire de réinitialiser les valeurs courantes des variables de collection avec leur valeur initiale. Malheureusement, Postman ne permet pas d'obtenir la valeur intitiale. Voici comment faire pour contourner ce problème.

Sur la requête `/db/seed`,  ajouter dans la section `Pre-request Script` le code suivant :

```js	
//Pour enregistrer les valeurs initiales dans la varialbe reset_values (à exécuter qu'une seule fois!)
pm.collectionVariables.set("reset_values",pm.collectionVariables.toJSON().values)
​
//Pour réinitialiser les valeurs courantes à partir des valeurs contenues dans la variable rest_values
pm.collectionVariables.get("reset_values").forEach((a)=>{
    if(a.key != "reset_values" ){
          pm.collectionVariables.set(a.key,a.value)}
    }
  
);
```

Ici, la ligne 2 permet de conserver toutes les valeurs courante dans une variable nommé rest_values. Cette ligne devra être **exécutée qu'une fois seulement** avant la première exécution de nos test. Par la suite vous devez la mettre en commentaire. **Il est donc important qu'avant la première exécution des tests, la valeurs courantes des variables soient égales aux valeures initiales**. 

La ligne 5 permet réinitialiser les valeurs courantes à partir des valeurs contenues dans la variable rest_values

## Sources

- https://practicalprogramming.fr/postman
- https://www.sisense.com/blog/rest-api-testing-strategy-what-exactly-should-you-test/