# Introduction à Node.js

[​​​​​​​Node.js](https://github.com/nodejs/node/) est un environnement bas niveau permettant l’exécution de JavaScript côté serveur ou sur n’importe quel environnement (Linux, Windows, Mac Os).

- Crée par [Ryan Dahl](https://en.wikipedia.org/wiki/Ryan_Dahl) en 2009.
- Utilise le moteur [JavaScript V8](https://fr.wikipedia.org/wiki/V8_%28moteur_JavaScript%29).
- https://fr.wikipedia.org/wiki/Node.js

## Installation

Node.js peut être installé de deux façons :

### 1 - Directement sur le site officiel
- https://nodejs.org/fr/
- Installation de la version LTS (long Term Support)
- Vérification du numéro de version dans un terminal pour node et npm

```bash
$ node -v
v18.13.0
$ npm -v
8.19.3
``` 


### 2- NVM (Node Version Manager)

NVM est un gestionnaire de version pour Node.js, il permet d’installer plusieurs versions de Node.js et de passer de l’une à l’autre très facilement.

- https://github.com/coreybutler/nvm-windows/releases
- Télécharger et exécuter **nvm-setup.exe** 

![nodejs](./img/node3.png){data-zoomable width=50%}

- Vérifier les versions de node.js installées

```bash
$ nvm list  
       v14.21.3
       v16.18.1
->     v18.12.1
```

- Installer la version LTS (long Term Support)

```bash
$ nvm install 18.13
Downloading and installing node v18.13.0...
Downloading https://nodejs.org/dist/v18.13.0/node-v18.13.0-linux-x64.tar.xz...
######################################## 100,0%
Computing checksum with sha256sum
Checksums matched!
Now using node v18.13.0 (npm v8.19.3)
```

- Utiliser une version en particulier

```bash
$ nvm use 18
Now using node v18.13.0 (npm v8.19.3)
```

- Vérifier la version de node et npm

```bash
$ node -v
v18.13.0
$ npm -v
8.19.3
``` 

## JavaScript coté serveur

![nodejs](./img/js_cote_serveur.gif)


## Synchrone vs Asynchrone

Node.js fonctionne en programmation :
- événementielle, 
- non bloquante (asynchrone),
- à fil unique (mono-thread) --> très efficace en terme de mémoire.
  - Moins de consommation mémoire : Pas besoin de gérer plusieurs threads ni des mécanismes complexes de synchronisation.
  - Performances élevées en I/O : Les opérations lourdes (ex : lecture de fichiers, requêtes API) ne bloquent pas l'exécution du programme.

![nodejs](./img/node2.png)

## Que peut faire Node.js ?
- générer du contenu de page dynamique,
- créer, ouvrir, lire, écrire, supprimer et fermer des fichiers sur le serveur,
- collecter les données des formulaires,
- ajouter, supprimer, modifier des données dans une base de données,
- …
- Voir la liste des modules livrés avec node.js https://nodejs.org/api/

## Exécution de code

Pour exécuter du code JavaScript, il faut enregistrer le code dans un fichier avec l’extension **.js** et l’exécuter avec la commande **node**.

```bash
$ node monfichier.js
```

### REPL (Read Eval Print Loop)

Il est également possible d’exécuter du code JavaScript directement dans un terminal avec la commande **node**.

```bash
$ node
Welcome to Node.js v18.13.0.
Type ".help" for more information.
> 2+2
4
> console.log("Salut gang!")
Salut gang!
```
*Ctrl + d pour quitter*

Peut être utilisé :
  - pour des tests,
  - exécution du code immédiate,
  - ne s’enregistre nulle part !


## Pratique

::: tip Pratique #1
- Créer un fichier demo1.js contenant le code suivant :
```js
const fs = require('fs');
fs.writeFileSync('demo1.txt', 'salut depuis Node.js');
```
- Exécuter ce fichier :
```bash
node demo1.js
```

- Vérifier que le fichier demo1.txt a bien été créé.
:::


::: info Demos sur Gitlab
Voir le fichier `d1-function.js` pour les exemples.

Les fichiers sont disponibles sur le dépôt GitLab :
https://gitlab.com/420-15d-fx/demos-nodejs
:::