# Documentation

## Une documentation, pour quoi faire ?
- Améliore l'expérience des développeurs qui utilisent notre API.
- Diminue le temps consacré à l'intégration des nouveaux utilisateurs.
- Permet également d'identifier les bogues et les problèmes dans l'architecture de l'API.
- Diminue le temps (et les maux de tête !) passé à comprendre le fonctionnement de l'API et à déchiffrer les erreurs inattendues lors de son utilisation.

![Documentation](./img/doc1.png)

## Swagger
Il existe plusieurs solutions pour « automatiser » la création de la documentation : Redoc, DapperDox, WidderShins…

Voir une [liste ici](https://nordicapis.com/7-open-source-openapi-documentation-generators/).

Pour ce cours nous utiliserons Swagger

« [Swagger](https://swagger.io/) est un cadre logiciel open-source soutenu par un vaste écosystème d'outils qui aident les développeurs à concevoir, construire, documenter et consommer des services web REST »

## Swagger UI
Swagger fournit un ensemble d’outils pour concevoir des API et améliorer le travail avec les services Web.
Parmi ces outils, nous utiliserons [Swagger UI](https://swagger.io/tools/swagger-ui/) qui permet de visualiser et d'interagir avec les ressources API.

Voir l’exemple du [Petstore](https://petstore.swagger.io/).

![Swagger UI](./img/doc2.png){ data-zoomable width=40%}

## swagger-ui-express
Pour utiliser Swagger UI avec Express.js :
- Installer la librairie swagger-ui-express pour utiliser Swagger UI dans une application Express.

`npm install --save swagger-ui-express`

- Configurer le fichiers `app.js`
  
```js
const swaggerUi = require('swagger-ui-express');
const swaggerDocument = require('./swagger.json');
app.use('/api-docs', swaggerUi.serve, swaggerUi.setup(swaggerDocument));
```	

- Pour permettre de faire des requêtes à notre API et de créer automatiquement le json à mettre dans le fichier `swagger.json` nous devons créer un fichier `swagger.json` à la racine du projet.
Attention, ce fichier doit être un json vide.

![Swagger UI](./img/doc3.png){ data-zoomable width=30%}

## Inspecteur Swagger

Swagger Inspector vous permettra de faire des requêtes à notre API et de créer automatiquement le json à mettre dans le fichier swagger.json ce qui permet de gagner un temps de développement précieux.
- Créez-vous un compte [Swagger](https://swagger.io/tools/swaggerhub-explore/)
- Connectez-vous à votre compte
- Démarrez votre API
- Faite des requêtes sur votre API à partir de l’inspecteur Swagger de la même façon que dans Postman: https://explore.swaggerhub.com/
- Vous aurez probablement besoin d’installer l’extension [Swagger UI](https://addons.mozilla.org/en-US/firefox/addon/swagger-inspector-ff-extension/) dans votre navigateur afin que l’inspecteur soit capable de rejoindre votre API qui est à l’adresse `http://localhost:3000`
- Sélectionnez les requêtes que vous souhaitez prendre en compte pour votre documentation.
- Sélectionner `OAS 3.0` puis cliquez sur le bouton `Create Api definition`.

![Swagger UI](./img/doc4.png){ data-zoomable width=40%}

- Si tout est correct, vous pourrez **lancer le Hub** (SWAGGERHUB)
- À partir du Hub, on peut **exporter le fichier au format JSON**. Et l’intégrer dans notre projet, dans le fichier `swagger.json`.

![Swagger UI](./img/doc5.png){ data-zoomable width=40%}

## Affichage de la documentation

Se rendre à l’adresse `http://localhost:3000/api-docs`.

![Swagger UI](./img/doc6.png){ data-zoomable width=40%}

## Sources & En savoir Plus

- https://medium.com/evodeck/why-you-should-write-api-documentation-2bca47574753
- ​​​​​​​Autre exemple en utilisant la librairie swagger-jsdoc avec un système d’annotation pour décrire l’API
  - ​​​​​​​https://dev.to/kabartolo/how-to-document-an-express-api-with-swagger-ui-and-jsdoc-50do ​​​​​​​