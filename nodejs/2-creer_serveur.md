# Créer un serveur

Node.js étant un interpréteur JavaScript côté serveur, il faut coder nous même le serveur qui sera à l’écoute d’un port.
- Le serveur node.js va écouter certains événements sur le port 3000. Il « tourne » constamment.
- Le navigateur envoi un événement sur le port écouté par le serveur node.js : `http://localhost:3000`
- Le serveur node.js retourne du contenu au navigateur.
- Utilisation du module `http` qui permet de travailler avec les **requêtes** et les **réponses** http.

## Exemple de création d'un serveur

​​​​La fonction de callback de `http.createServer((req, res) => {..}` prend deux paramètres :

```js
const server = http.createServer((req, res) => {
```

- `req` --> Requête.
  - `req.url`
  - `req.method`
  - `req.headers`
  - ...
- `res` --> Réponse. Objet que l’on va renvoyer à la page.

- Voir le code du fichier [demo2.js](https://gitlab.com/420-15d-fx/demos-nodejs/-/blob/main/demo2.js?ref_type=heads)
- Voir le code du fichier [demo3.js](https://gitlab.com/420-15d-fx/demos-nodejs/-/blob/main/demo3.js?ref_type=heads)

## Exemple pour l'envoi d'une réponse au client

Pour envoyer une réponse, on utilise le 2ème argument (`res`) passé au callback de `http.createServer()`. 

Cet objet `http.ServerResponse` est créé par le serveur HTTP.

Exemple :

```js
res.setHeader('Content-Type', 'text/html; charset=utf-8');
res.write('Salut gang!');
res.end();
```

::: info Demos
Les fichiers sont disponibles sur le dépôt GitLab :https://gitlab.com/420-15d-fx/demos-nodejs

- Voir le code du fichier`demo4.js`
- Voir les méthodes de [http.ServerResponse](https://nodejs.org/dist/latest-v14.x/docs/api/http.html#http_class_http_serverresponse)

:::



## Pratique

::: tip Pratique #2
- Créer un fichier avec le code nécessaire pour faire un serveur.
- Mettre le code html suivant dans une variable puis affichez le quand une requête est reçu par le serveur à l'url "/".
```html
<!DOCTYPE html>
<html lang="fr">
<head>
<title>Saisir un message</title>
</head>
<body>
<form action="/message" method="POST">
<input type="text" name="message"><button type="submit">Soumettre</button>
</form>
</body>
</html>
```
​
- Afficher une page avec inscrit « Merci » quand on clique sur le bouton « Soumettre »
- Si l'url n'est pas la bonne alors on retourne un code de statut 404 sinon on retourne le code 200.
- Solution dans `demo5.js`

## Streams et Buffers

Il est important de comprendre les **streams** et les **buffers** pour comprendre comment fonctionne Node.js.

### Streams

Les streams sont des flux de données en continu qui permettent de lire ou d'écrire des données par petits morceaux plutôt que de les charger en mémoire en une seule fois. Ils sont utilisés pour manipuler des données de manière asynchrone, ce qui les rend idéaux pour le traitement de données en streaming, comme la lecture de fichiers volumineux, les requêtes HTTP, etc.

### Buffers

Les buffers sont des zones de mémoire tampon temporaires utilisées pour stocker des données binaires brutes. Ils sont particulièrement utiles lorsqu'il s'agit de manipuler des données brutes, telles que des fichiers binaires, des données réseau ou d'autres données qui ne sont pas des chaînes de caractères textuelles.

Les données entrantes sont envoyés sous forme de flux (stream).
Le flux est « découpé » en petits morceaux (chunk) d’une certaine taille mémoire (buffer)
Le serveur va traiter chaque chunk de manière asynchrone.

![stream](./img/node4.png){data-zoomable width=50%}

L'objet de requête est utilisé pour gérer les blocs de données.

```js
request.on('eventName',callback)
```

- `eventName` : C'est le nom de l'événement qui s'est déclenché
- `callback` : il s'agit de la fonction de rappel, c'est-à-dire du gestionnaire d'événements de l'événement particulier.

```js
const body = [];
​
req.on('data', (chunk) => {
  
            // Accumulation des chunks
            body.push(chunk);
            console.log(body)
        });
​
 req.on('end', () => {
  
            // conversion des chunks en données significative
            const parsedBody = Buffer.concat(body).toString();
            const message = parsedBody.split('=')[1];
              
            // Printing the data
            console.log(message);
        });
```

## Pratique

::: tip Pratique #3
- Créer un formulaire avec un champ texte et un bouton.
- Quand le formulaire est envoyé, le contenu du champ texte doit être affiché dans la page
- Voir `demo6.js` ​​​​​​​pour solution
:::