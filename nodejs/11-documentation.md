# Documentation

## Une documentation, pour quoi faire ?
- Améliore l'expérience des développeurs qui utilisent notre API.
- Diminue le temps consacré à l'intégration des nouveaux utilisateurs.
- Permet également d'identifier les bogues et les problèmes dans l'architecture de l'API.
- Diminue le temps (et les maux de tête !) passé à comprendre le fonctionnement de l'API et à déchiffrer les erreurs inattendues lors de son utilisation.

![Documentation](./img/doc1.png)

Il existe plusieurs solutions pour « automatiser » la création de la documentation : Redoc, DapperDox, WidderShins…

Voir une [liste ici](https://nordicapis.com/7-open-source-openapi-documentation-generators/).

Pour ce cours nous utiliserons Postman

## Postman

Sur le nom de votre collection, faire un clic droit et choisir "View Documentation".

![Postman](./img/doc7.png)

La liste des requêtes de la collection s'affiche. Vous pouvez ajouter des descriptions à chaque requête.

![Postman](./img/doc8.png)

Cliquer ensuite sur "Publish" dans le menu du haut à droite pour publier la documentation.

Votre navigateur s'ouvre et vous pouvez accéder à un écran de configuration.

![Postman](./img/doc9.png)

Vous pouvez choisir un nom pour votre documentation, une description, un logo, etc.

Cliquez ensuite sur "Publish" pour publier la documentation.

![Postman](./img/doc10.png)

Le lien vers la documentation est alors disponible.

![Postman](./img/doc11.png)

Vous obtenez des exemples de requêtes et de réponses. Cependant les exemples de réponse sont vides comme vous pouvez le constater. Il est noté "No response body
This request doesn't return any response body".

Afin de pouvoir afficher des exemples de réponses, il faut ajouter des exemples dans les requêtes.

Retournez dans Postman et cliquez sur une requête. Faire "send" pour "save as example"

![Postman](./img/doc12.png)

Cette requête est alors ajoutée dans la liste des exemples de requêtes.

La documentation se met à jour automatiquement.

![Postman](./img/doc13.png)




## Sources & En savoir Plus

- https://medium.com/evodeck/why-you-should-write-api-documentation-2bca47574753