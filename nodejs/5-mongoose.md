# Express.js - Base de données

![Express.js - Base de données](./img/mongoose.png){ data-zoomable width=50% }

## Installation de MongoDB

- https://www.mongodb.com/try/download/community ​​​​​​​
- Il est également possible de créer une base de données hébergée. Il faut cependant s’inscrire sur https://www.mongodb.com/try

## Mongoose
- Simplifie l’utilisation de MongoDB dans un projet Node.js
- Object-Document Mapping Library (ODM).
- [Documentation](https://mongoosejs.com/docs/guide.html)

## Installation de Mongoose

```bash
npm install mongoose
```

## Connexion à la base de données

```js
import mongoose from 'mongoose';
const port = 3000;
// ...
(async () => {
    try {
        await mongoose.connect('mongodb://127.0.0.1:27017/nom_de_la_base_de_donnees');

        app.listen(port, () => {
            console.log(`Serveur à l'écoute sur : http://localhost:${port}`);
        });
    } catch (err) {
        console.error("Erreur de connexion à MongoDB :", err);
    }
})();
```

::: warning Attention !
 S’assurer que votre serveur MongoDB est bien démarré sur votre machine.
:::

## Schéma
- Mongoose définit la forme des documents grâce à un schéma.
- Chaque schéma correspond à une collection MongoDB.
- Cela correspond aux **modèles**
- Ceux-ci seront déclarés dans le dossier `models`

[Documentation](https://mongoosejs.com/docs/guide.html#definition)

Exemple de schéma pour un Article ​​​​​​​

```js
import mongoose from 'mongoose';

const Schema = mongoose.Schema;
const articleSchema = new Schema(
  {
    titre: { // nom de la propriété
      type: String, // type de la propriété
      required: true // propriété requise ou non
    },
    contenu: {
      type: String,
      required: true
    }
  },
  // Options - timestamps permet d'ajouter automatiquement les champs createdAt et updatedAt
  { timestamps: true }
);
​
const Article = model('Article', articleSchema);
export default Article;
```

## Requêtes (Queries)

- Mongoose fournit des méthodes pour les opérations de CRUD (Create, Read, Update, Delete).
- Voir [la documentation](https://mongoosejs.com/docs/api/query.html)

  - Model.deleteMany()
  - Model.deleteOne()
  - Model.find()
  - Model.findById()
  - Model.findByIdAndDelete()
  - Model.findByIdAndUpdate()
  - Model.findOne()
  - Model.findOneAndDelete()
  - Model.findOneAndReplace()
  - Model.findOneAndUpdate()
  - Model.replaceOne()
  - Model.updateMany()
  - Model.updateOne() ​​​​​​​

### Exemple de requête - Récupère tous les articles

```js
import Article from '../models/article';
// ...

// GET /articles
export async function getArticles(req, res) {
  try {
    const articles = await Article.find();
    res.json(articles);
  } catch (err) {
    console.log(err);
  }
};
```


:::danger async/await
Vous pouvez observer que les contrôleurs ont été déclarés avec le mot-clé `async` et les requêtes sont effectuées avec le mot-clé `await`. 

En effet, les méthodes de Mongoose retournent des promesses. Donc, pour attendre la résolution de la promesse, on utilise le mot-clé `await`.

Cela permet d'écrire le code de manière plus lisible en remplaçant .then() et .catch() par une syntaxe plus naturelle : 
  
  ```js
  Article.find().then(articles => {
    res.json(articles);
  }).catch(err => {
    console.log(err);
  });
  ```

  par 

  ```js
  try {
    const articles = await Article.find();
    res.json(articles);
  } catch (err) {
    console.log(err);
  }
  ```



- `async` permet de déclarer une fonction asynchrone.
- `await` permet d'attendre la résolution d'une promesse.
:::

### Exemple de requête - Récupère un article

```js
import Article from '../models/article';
// ...

// GET /article/:id
export async function getArticle(req, res) {
  try {
    const article = await Article.findById(req.params.id);
    res.json(article);
  } catch (err) {
    console.log(err);
  }
};
```

### Exemple de requête - Créer un article

```js
import Article from '../models/article';

// ...

// POST /article
export async function createArticle(req, res) {
  try {
    // Crée un nouvel article avec les informations du body
    const article = new Article({
      titre: req.body.titre,
      contenu: req.body.contenu
    });
    // Enregistre l'article dans la base de données
    // Utilisation de la méthode save() qui retourne une promesse
    const result = await article.save();
    // Retourne le résultat au format JSON
    res.json(result);
  } catch (err) {
    console.log(err);
  }
};
```

## Pratique 10

::: tip Pratique #10
- À partir de la pratique 9
- Créer un dossier nommé `models`
- Créer à l'intérieur du dossier `models` un fichier nommé `article.js` et ajouter le code pour créer le schéma mongoose pour un article.
- Modifier le code du fichier `articleController.js` :
  - Modifier la méthode `createArticle` pour permettre d'ajouter un article dans la base de données.
  - Modifier la méthode `getArticles` pour permettre l'affichage des articles provenant de la base de données.
  - Modifier la méthode` getArticle` pour permettre d'afficher le détail d'un article provenant de la base de données.
- Ajouter la possibilité de mettre à jour un article un utilisation la même vue que pour la création.
- ​Ajouter la possibilité de supprimer un article 
- Voir `demo9-mongoose-hbs` pour solution.
:::

::: info
Les démos sont disponibles dans le dépôt Gitlab : https://gitlab.com/420-15d-fx/demo-express
:::

## Validation

- Mongoose permet de valider les données avant de les enregistrer dans la base de données.
- Voir https://mongoosejs.com/docs/validation.html

### Exemple de validation avec message d'erreur personnalisé

::: details Voir le code

```js
import mongoose from 'mongoose';
import { Schema, model } from 'mongoose';

const articleSchema = new Schema(
  {
    titre: {
      type: String,
      required: [true, 'Le titre est requis'],
      minlength: [5, 'Le titre doit contenir au moins 5 caractères'],
      maxlength: [50, 'Le titre doit contenir au plus 50 caractères']
    },
    contenu: {
      type: String,
      required: [true, 'Le contenu est requis']
    }
  },
  { timestamps: true }
);

const Article = model('Article', articleSchema);
export default Article;
```
:::

### Exemple de validation personnalisée

La validation personnalisée est déclarée en passant une fonction de validation.

::: details Voir le code

```js
import mongoose from 'mongoose';

const Schema = mongoose.Schema;

const userSchema = new Schema({
  phone: {
    type: String,
    validate: {
      validator: function(v) {
        return /\d{3}-\d{3}-\d{4}/.test(v); // true ou false si le numéro de téléphone est valide ou non
      },
      message: props => `${props.value} n'est pas un numéro de téléphone valide!`
    },
    required: [true, 'Le numéro de téléphone est requis']
  },
  // ... autres champs du schéma
});
```
:::

### Exemple de validation personnalisée sur un vecteur

::: details Voir le code

```js
import mongoose from 'mongoose';

const Schema = mongoose.Schema;

// Définition du schéma du modèle
const myModelSchema = new Schema({
  monVecteur: {
    type: [String], // Un vecteur de chaînes de caractères
    validate: {
      validator: function(vector) {
        // Vérifie si chaque valeur a une longueur d'au moins cinq caractères
        return vector.every(value => value.length >= 5);
      },
      message: props => `La longueur de chaque valeur dans le vecteur doit être d'au moins cinq caractères.`
    },
    required: [true, 'Le vecteur est requis']
  },
    // ... autres champs du schéma
});
```
:::


### Exemple de validation personnalisée avec une fonction asynchrone

Il est également possible de déclarer une fonction asynchrone pour la validation.

::: details Voir le code

```js
import mongoose from 'mongoose';

const userSchema = new mongoose.Schema({
    nom: {
        type: String,
        required: true,
        validate: {
            // retourne une promesse
            validator: async function (value) {
                // Ici, on peut faire une requête à la base de données ou une autre opération asynchrone
                const existingUser = await mongoose.models.User.findOne({ nom: value });
                // retourne true si l'utilisateur n'existe pas
                return !existingUser; // La validation échoue si l'utilisateur avec ce nom existe déjà
            },
            message: 'Ce nom d\'utilisateur est déjà utilisé.'
        }
    },
    // ... autres champs du schéma
});
```
:::

## Utiliser des relations

- Mongoose permet de définir des relations entre les collections.
- Voir https://mongoosejs.com/docs/populate.html

### Exemple de relation pour ajouter plusieurs catégories à un article

1. Créer un schéma pour Categorie.
2. Dans le schéma de Article, on ajoute Categories :

::: details Voir le code

```js
import mongoose from 'mongoose';
const Schema = mongoose.Schema;

const articleSchema = new Schema(
  {
    titre: {
      //...
    },
    contenu: {
      //...
    },
    // Ajout de la relation, remarquez que le type est un tableau
    // car un article peut avoir plusieurs catégories
    categories: [
      {
        type: Schema.Types.ObjectId,
        ref: 'Categorie'
      }
    ]
  },
  //...
);
```



![Exemple de relation](./img/mongoose1.png){ data-zoomable width=25% }

Pour l’enregistrement, on ajoute le tableau contenant les ids des categories qui proviendraient par exemple d’un sélecteur multiple dans le formulaire.​​​​​​​

```js
  const article = new Article({
    titre: req.body.titre,
    contenu: req.body.contenu,
    categories: req.body.categories
  });
```

:::

### Exemple de relation pour ajouter plusieurs objets catégories à un article

1. Créer un schéma pour Categorie.
2. Dans le schéma de Article, on ajoute Categories :

::: details Voir le code

```js
import mongoose from 'mongoose';
const Schema = mongoose.Schema;

const articleSchema = new Schema(
  {
    titre: {
      //...
    },
    contenu: {
      //...
    },
    // Ici le type de catégorie sera un vecteur d'objet respectant le schéma categorie
    categories: {
        type : [categorieSchema],
      }

  },
  //...
);
```



<!-- ![Exemple de relation](./img/mongoose2.png){ data-zoomable width=25% } -->

Pour l’enregistrement, on ajoute le tableau contenant les ids des categories qui proviendraient par exemple d’un sélecteur multiple dans le formulaire.​​​​​​​

```js
  const article = new Article({
    titre: req.body.titre,
    contenu: req.body.contenu,
    categories: req.body.categories
  });
```

:::



### Exemple de relation pour ajouter une seule catégorie à un article

1. Créer un schéma pour Categorie.
2. Dans le schéma de Article, on ajoute Categorie :

::: details Voir le code

```js
import mongoose from 'mongoose';
const Schema = mongoose.Schema;

const articleSchema = new Schema(
  {
    titre: {
      //...
    },
    contenu: {
      //...
    },
    // Ajout de la relation, remarquez que le type est un objet
    // car un article peut avoir une seule catégorie
    categorie: {
      type: Schema.Types.ObjectId,
      ref: 'Categorie'
    }
  },
  //...
);
```

Pour l’enregistrement, on ajoute l’id de la categorie qui proviendrait par exemple d’un sélecteur dans le formulaire.​​​​​​​

```js
  const article = new Article({
    titre: req.body.titre,
    contenu: req.body.contenu,
    categorie: req.body.categorie
  });
```
:::



## Peupler les documents référencés dans une collection.

Lorsque vous avez un schéma avec un champ qui fait référence à un autre schéma et que vous récupérez des documents à partir de la collection, le champ de référence ne contient généralement que l'ID du document référencé.

En utilisant la fonction `populate()`, vous pouvez remplacer cet ID par le document complet référencé.

Cela permet de récupérer facilement toutes les informations nécessaires pour un document, même si ces informations sont stockées dans une autre collection de la base de données.

La fonction `populate()` est très utile pour éviter les requêtes multiples à la base de données et pour améliorer les performances de votre application.

Voici un exemple pour afficher un utilisateur avec les détails des articles.

```js
import User from '../models/user';

export async function getUser(req, res, next) {
    const userId =  req.params.userId;
    try {
        const user = await User.findById(userId).populate("articles");
        res.json(user);
    } catch (err) {
        next(err);
    }
};
```

voici un exemple pour obtenir le détails des enfants et petits enfants d'un parent. 

```js
import User from '../models/user';
import Article from '../models/article';
import Categorie from '../models/categorie';

export async function getUser(req, res, next) {
    const userId =  req.params.userId;
    try {
        const user = await User.findById(userId)
            .populate({
                path : "articles",  //nom de la propriété dans l'objet parent
                model : Article,
                populate : {
                    path : "categories", //nom de la propriété dans l'objet parent
                    model : Categorie,
                }
            });
        res.json(user);
    } catch (err) {
        next(err);
    }
};
```

### Exemple de requête - Récupère tous les articles avec les catégories qui ont une date de création supérieure à 2023-01-01

```js
import Article from '../models/article';
// ...

// GET /articles
export async function getArticles(req, res, next) {
  try {
    const articles = await Article.find({
      'categories.createdAt': { $gt: new Date('2023-01-01') }
    }).populate('categories');
    res.json(articles);
  } catch (err) {
    console.log(err);
  }
};
```

## Supprimer les documents référencés dans une collection.

Lorsque vous supprimez un document, vous devez également supprimer les documents référencés dans les autres collections.

Pour cela, vous pouvez utiliser la fonction `deleteMany()`.

```js
import Article from '../models/article';
import Categorie from '../models/categorie';

exports.deleteArticle = async (req, res, next) => {
    const articleId =  req.params.articleId;
    try {
        const article = await Article.findById(articleId);
        if (!article) {
            const error = new Error("Article introuvable");
            error.statusCode = 404;
            throw error;
        }
        // Supprime l'article
        await Article.deleteOne({ _id: articleId });
        // Supprime les catégories associées à l'article
        await Categorie.deleteMany({ _id: { $in: article.categories } });
        res.json({ message: "Article supprimé" });
    } catch (err) {
        next(err);
    }
};
```

## Pratique 11

::: tip Pratique #11
- Ajouter la possibilité de créer des catégories.
- Modifier les formulaire pour la création et la modification d'un article pour ajouter la possibilité sélectionner plusieurs catégories.
- Modifier les méthodes `createArticle` et `updateArticle`.
- Modifier la page de détail d'un article pour afficher ses catégories.
- Voir demo10-mongoose-relations​​​​​​​.
:::

## Sources

- https://expressjs.com/en/4x/api.html
- https://mongoosejs.com/
