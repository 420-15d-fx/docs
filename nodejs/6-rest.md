# Service Web et API REST

## Service Web

![Service Web](./img/rest1.png){ data-zoomable width=50% }

Un Service Web est un système de communication informatique qui permet à des applications de différentes plateformes et langages de programmation de communiquer entre elles via Internet ou un réseau local.

Les Services Web permettent à des applications de s'échanger des données structurées (texte, JSON, XML, etc.) en utilisant des protocoles de communication standard tels que HTTP et SOAP.

Ceux-ci sont basés sur des standards ouverts tels que XML, SOAP, WSDL et UDDI, ce qui les rend indépendants des plateformes et des langages de programmation.

Les Services Web sont également compatibles avec les architectures SOA (Service-Oriented Architecture) qui permettent de créer des systèmes modulaires et flexibles en utilisant des services indépendants.

Exemples de service Web :
- Cotes boursières.
- Taux de change.
- Taux d'intérêts.
- Validations : adresse de courriel, adresse postale en fonction du code postal, etc.
- Correction de l'orthographe et suggestions.
- Librairie.
- Réseaux sociaux.
- Recherche sur Internet.
- Envoi de SMS.
- Traduction.
- Statistiques
- Données météorologiques

## API REST (REpresentational State Transfer)

Une API REST est une interface de programmation qui permet à des applications de communiquer entre elles via Internet ou un réseau local en utilisant le protocole HTTP.

Un API REST est un ensemble de règles et de conventions utilisées pour concevoir des interfaces de programmation d'applications (API) Web.

Les API REST permettent à différents systèmes informatiques de communiquer entre eux en utilisant des requêtes HTTP (GET, POST, PUT, PATCH et DELETE) pour accéder et manipuler des ressources (par exemple des données ou des fonctionnalités) sur un serveur distant.

Six contraintes architecturales définissent un système REST 

- **Architecture client-serveur** : L'API REST doit être conçue en utilisant une architecture client-serveur. Cela signifie que le serveur fournit les ressources et les données, tandis que le client fait les demandes et manipule les données.

- **Stateless** *sans état* : La communication client-serveur s'effectue sans conservation de l'état de la session de communication, ce qui signifie que chaque requête contient toutes les informations nécessaires pour comprendre la demande sans avoir besoin de contexte supplémentaire. L'état de l'application est donc stocké uniquement sur le client et non sur le serveur.

- **Cacheable** *mise en cache* : Les API REST doivent être conçues pour être apte à être mise en cache . Cela signifie que les clients doivent pouvoir stocker en cache les réponses du serveur pour des requêtes ultérieures afin de réduire la latence et d'améliorer la performance.

- **Interface uniforme** : L'API REST doit utiliser une interface uniforme pour accéder aux ressources. Cela signifie que toutes les ressources sont accessibles via des URLs uniques (routes), et que les opérations pour accéder et manipuler ces ressources sont standardisées à l'aide des méthodes HTTP (GET, POST, PUT, PATCH etDELETE).

- **Layered System** *Système à plusieurs niveaux* : L'architecture REST doit être conçue en utilisant un système en couches. Cela signifie que les clients ne doivent pas savoir si une ressource est située sur le serveur d'origine ou sur un serveur tiers, car l'API REST cache ces détails.

- **Code on demand** *Code à la demande* (optionnel) : L'API REST peut inclure du code exécutable envoyé du serveur au client sous forme d'applets ou de scripts. Cela permet une plus grande flexibilité, mais n'est pas toujours nécessaire pour toutes les API REST.

[Source](https://fr.wikipedia.org/wiki/Representational_state_transfer)

### Les API REST basées sur HTTP sont définies par :

- Un URI de base, comme http://api.example.com/collection/
- Des méthodes HTTP standards (par ex. : GET, POST, PUT, PATCH et DELETE)
- Un type de média ([type MIME](https://fr.wikipedia.org/wiki/Type_de_m%C3%A9dias)). Nous utiliserons le type JSON.
- Les routes (ou endpoints) sont les URL utilisées pour faire les requêtes à une API.

[Source](https://fr.wikipedia.org/wiki/Representational_state_transfer#Appliqu%C3%A9_aux_services_web)

## Routes

Les routes (ou endpoints) sont les URL utilisées pour faire les requêtes à une API.

Exemple de route :

`api.openweathermap.org/data/2.5/weather?q={city_name}&appid={API_key}`

- `city_name` et `API_key` représente des variables passées en paramètres.
- On trouve également cette notation `:city_name`.


[Voici une petite liste d’API « gratuites »](https://github.com/public-apis/public-apis)

## Méthodes HTTP

| Méthode | CRUD | Description |
| --- | --- | --- |
| GET | Read | Récupérer une ressource |
| POST | Create | Créer une ressource |
| PUT | Update | Mettre à jour une ressource |
| PATCH | Update | Mettre à jour une partie d'une ressource |
| DELETE | Delete | Supprimer une ressource |

**GET**

- Utilisée pour récupérer une représentation d'une ressource  à partir d'une URI. Le serveur répond ensuite en renvoyant une représentation de la ressource demandée, qui peut être un fichier texte, une image, un document JSON ou XML, ou tout autre type de données.
- Le format de la ressource est spécifié dans l'en-tête de la réponse.
- Retourne une représentation de la ressource créée selon le format spécifié et le code de statut 200 (OK).
- Idempotente : Elle a le même effet qu'on l'applique une ou plusieurs reprises. 
- Sécuritaire : Elle ne modifie pas la ressource. 

**POST**

- Utilisées pour envoyer des données à un serveur afin de créer une nouvelle ressource ou de mettre à jour une ressource existante en ajoutant une information à celle-ci. Elle est utilisée pour effectuer des opérations qui changent l'état des ressources existantes.
- Le client envoie des données dans le corps de la requête au format JSON. Le serveur traite ensuite ces données pour créer une nouvelle ressource ou ajouter un élément à une ressource existante.
- Retourne une représentation de la ressource crée dans le corps de la réponse au format JSON et le code de statut 201 (Created)
- Retroune L'emplacement de la nouvelle ressource dans l'en-tête HTTP "Location".
- N'est pas idempotent. L’envoi de la même requête à plusieurs reprises va créer plusieurs ressources.

**PUT** 

- Utilisée pour mettre à jour une ressource existante ou créer une nouvelle ressource sur un serveur.
  - Pour créer une nouvelle ressource avec la méthode PUT, l'URI de la ressource doit être connu à l'avance et inclus dans la requête. Cela signifie que l'identifiant unique de la ressource doit être inclus dans l'URI de la requête, afin que le serveur sache où placer la nouvelle ressource.
- La représentation complète de la ressource à modifiée doit être envoyé dans le corps de la requête.
- Retourne une représentation de la ressource modifée selon le format spécifié et le code de statut 200 (OK).
- Idempotente

**​​​​​​​PATCH**

- Utilisée pour mettre à jour partiellement une ressource existante sur un serveur. Contrairement à la méthode PUT, qui remplace complètement la ressource existante par une nouvelle version, la méthode PATCH permet de ne mettre à jour que les parties de la ressource qui ont été spécifiées dans la requête.
- Les données à modifier sont envoyées dans le corps de la requête.
- Retourne une représentation de la ressource modifée selon le format spécifié et le code de statut 200 (OK).
- Non-idempotente

**DELETE**

- Utiliser pour supprimer une ressource existante sur un serveur. Lorsqu'un client envoie une requête DELETE à un serveur pour une ressource donnée, le serveur supprime la ressource correspondante.
- Ne retourne aucun contenu avec le code de statut 204 (No content).
- Idempotente.
- Ce n'est pas une erreur de supprimer une ressource qui n'existe pas.

## Code de réponse

| Code | Description | Signification |
| --- | --- | --- |
| 200 | OK | Requête traitée avec succès: une représentation de la ressource est renvoyée dans le corps |
| 201 | Created | Requête traitée avec succès et création d’une ressource |
| 204 | No Content | Requête traitée avec succès mais pas d’information à renvoyer |
| 303 | See Other | La réponse à cette requête est ailleurs. Voir l'en-tête HTTP « Location » pour l'emplacement (URI) de la ressource concernée. |
| 304 | Not Modified | La ressource n’a pas été modifiée depuis la dernière requête |
| 307 | Temporary Redirect | Requête non traitée, car ce n'est pas le bon URI. Voir l'en-tête HTTP « Location » pour le nouvel URI et resoumettre la requête à cet URI. |
| 400 | Bad Request | La syntaxe de la requête est erronée |
| 401 | Unauthorized | Une authentification est nécessaire pour accéder à la ressource |
| 403 | Forbidden | Le serveur a compris la requête, mais refuse de l'exécuter. Contrairement à l'erreur 401, l'authentification ne fera aucune différence. |
| 404 | Not Found | La ressource n'existe pas. |
| 405 | Method Not Allowed | Méthode de requête HTTP non autorisée. |
| 406 | Not Acceptable | Toutes les réponses possibles sont refusées; il n'est pas possible de produire les données dans le format désiré. |
| 409 | Conflict | La requête est valide, mais ne peut pas être complétée en raison d'une sorte de non-concordance. Par exemple, lorsqu'une ressource existe déjà.|
| 418 | I'm a teapot 😀 | Signifie « Je suis une théière » informe que le serveur refuse de préparer du café, car il s'agit d'une [théière](https://www.google.com/teapot/) |
| 422 | Unprocessable Entity | La requête est bien formée mais que le serveur est incapable de la traiter car elle contient des erreurs sémantiques ou ne remplit pas certaines conditions (Erreurs de validation ou de logique)  |
| 500 | Internal Server Error | Erreur interne du serveur. |

## Node.js et API

Jusqu’à présent, notre serveur Node.js envoi les données dans un template avec :
  
  ```js
  res.render();
  ```

Maintenant, nous allons envoyer les données au format JSON pour utiliser notre serveur comme une API :

```js
// Retourne les données au format JSON avec le code de statut 200 (OK)
res.json({ articles: articles });
​
// Retourne les données et un message au format JSON avec le code de statut 201 (Created)
res.status(201).json({
      message: 'Article créé avec succès !',
      article: article
    });
```

### Location
L'en-tête HTTP Location est un en-tête de réponse de la famille des codes de statut HTTP. Il est utilisé pour fournir au client l'adresse d'une ressource éventuellement créée par la requête. Son utilisation est obligatoire dans les réponses 201 (Created).
```js
res.location("/article/" + article.id);
res.status(201).json(article);
```

### middleware `express.json()`

Pour pouvoir parser le corps des requêtes, on utilise le middleware `express.json()`.
https://expressjs.com/fr/api.html#express.json

```js
// Dans app.js
// Parse le corps des requêtes au format JSON pour pouvoir les utiliser dans les routes avec req.body.
// Doit être avant les routes.
app.use(express.json()); 
```

## Convention de format de réponse

Dans le développement d’API REST, il est essentiel de maintenir une **cohérence** dans le format des réponses, que ce soit pour les requêtes réussies (**succès**) ou pour les requêtes qui rencontrent des problèmes (**erreurs**). OpenAI, comme d’autres grandes plateformes (Google, GitHub, Stripe), adopte des conventions strictes pour structurer les réponses en JSON.

L’objectif de ces conventions est d’assurer une **interopérabilité**, une **clarté** et une **facilité de gestion des erreurs** pour les développeurs utilisant l’API.

Dans le cadre du cours, nous utiliserons les convention d'**OpenAI**

###  Format de réponse en cas de succès (HTTP 200, 201, etc.)
Une requête réussie doit inclure :

- **status** : Code HTTP de succès (200, 201, etc.).
- **message** : Description du succès de la requête.
- **data** : Contenu retourné par l’API.
- **path** : L’URL concernée par la requête.
- **timestamp** : Date et heure de la réponse pour faciliter le débogage.

**Exemple d’une réponse de succès (200 OK)**

```json
{
  "status": 200,
  "message": "Requête traitée avec succès.",
  "data": {
    "id": "12345",
    "nom": "John Doe",
    "courriel": "johndoe@exemple.com"
  },
  "path": "/users/12345",
  "timestamp": "2024-03-04T12:34:56.789Z"
}

```

### Format de réponse en cas d’erreur (HTTP 400, 404, 500, etc.)

Une erreur doit inclure :

- **status** : Code HTTP correspondant à l’erreur (400, 404, 500, etc.).
- **error** : Type d’erreur (ex: "Bad Request", "Not Found", "Internal Server Error").
- **message** : Description détaillée de l’erreur.
- **path** : L’URL où l’erreur s’est produite.
- **timestamp** : Date et heure de l’erreur pour faciliter le suivi.

**Exemple d’une erreur 400 Bad Request (Requête invalide)**

```json
{
  "status": 404,
  "error": "Not Found",
  "message": "L'utilisateur avec l'ID 12345 n'existe pas.",
  "path": "/users/12345",
  "timestamp": "2024-03-04T12:50:30.987Z"
}


```

## Démo

Voir le dossier `demo11-rest` et le fichier `demo11-rest.postman_collection.json` qu'il faut importer dans Postman pour avoir des exemples de requêtes et de tests.