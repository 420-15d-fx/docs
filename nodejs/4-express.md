# Express.js
https://expressjs.com/

Express.js est un Micro-framework utiliser pour créer des applications Web basées sur Node.js
- Présent dans les modules NPM.
- Fournit des outils pour aller plus vite dans la création d'applications Node.js.
- Alternatives : Adonis.js, Koa, Sails.js, …

## Installation

```bash
npm install express --save
```

## Utilisation
Pour utiliser Express.js, il s'agit d'importer le module en utilisant la commande `require('express')`

```js
const express = require('express');
​
const app = express();
​
const port = 3000;
​
// code …
​
app.listen(port, () => {
	console.log(`Le serveur écoute sur http://localhost:${port}`);
});

```

Ici, `app.listen` lance l'exécution du serveur sur le port 3000.

### Pratique #1

::: tip Pratique
- Créer un dossier nommé `blogue` 
- Créer un fichier package.json (`npm init`)
- Installer Express.js
- Ajouter un fichier nommé `app.js` qui exécute un serveur sur le port 3000
- Modifier le fichier `package.json` pour ajouter la commande `"start" : "nodemon app.js"`
- Exécuter la commande et vérifier si serveur fonctionne.
:::

::: info
Les démos sont disponibles dans le dépôt Gitlab : https://gitlab.com/420-15d-fx/demo-express
:::
## Middleware
[Documentation](https://devdocs.io/express/guide/using-middleware)

Une application Express est essentiellement une série d'appels de fonction de middleware. 

Les fonctions middleware sont des fonctions qui ont accès à l'objet de requête (`req`), à l'objet de réponse (`res`) et à la fonction middleware suivante dans le cycle requête-réponse de l'application.

La prochaine fonction middleware est généralement désignée par une variable nommée `next`.

Les fonctions du middleware peuvent effectuer les tâches suivantes :
- Exécuter n'importe quel code.
- Apportez des modifications à la requête et aux objets de réponse.
- Terminer le cycle requête-réponse.
- Appelez la fonction middleware suivante dans la pile.

Si la fonction middleware actuelle ne termine pas le cycle requête-réponse, elle doit appeler next() pour passer le contrôle à la fonction middleware suivante. Sinon, la demande sera laissée en suspens.
Cela permet de diviser le code en plusieurs blocs ou morceaux au lieu d'avoir une énorme fonction qui fait tout.

```js
app.use((req, res, next) => { ...}
```

![middleware](./img/express1.png){data-zoomable width=50%}

### Pratique #2

::: tip Pratique
- Ajouter dans le fichier `app.js` un middleware qui set l'en-tête `http` (setHeader) de la réponse (res) et qui appelle le middleware suivant.
- Dans le middleware suivant, écrire un titre html `<h1>Salut depuis Express!</h1>` et retourner la réponse au client.
- Voir solution `demo1-middleware` dans le dépôt Gitlab https://gitlab.com/420-15d-fx/demo-express
:::

## Utiliser des routes
`app.use()` peut prendre une route (URL) comme premier paramètre. 

Attention pour les routes en utilisant `use()`, **l'ordre est important**. Il faut mettre les routes les plus spécifiques en premier et les plus générales à la fin.

En effet, Express.js analyse le début de l'URL puis exécute le premier middleware qui correspond. C'est pour cela que l'on préferera utiliser `app.get()` ou `app.post()` pour les routes.

La méthode `app.use()` répond à toutes les méthodes (`GET`, `POST`, `PUT`, …)

On peut également utiliser :
- `app.get()`
- `app.post()`
- `app.put()`
- `app.delete()`

​​​​​​​​​​​​​​Afin de filtrer les méthodes auxquelles on souhaite « répondre ».

Exemples :
```js
app.use('/', (req, res, next) => {
  // S'exécute quelque soit la méthode (GET, POST, PUT, …) et quelque soit l'url
  // Ex.  /, /categories, /articles, …
}

app.use('/article', (req, res, next) => {
  // S'exécute quelque soit la méthode (GET, POST, PUT, …) et seulement si l'url commence par /article
  // Ex. /article, /article/1, /article/2, …
}

app.get('/article', (req, res, next) => {
  // S'exécute seulement si la méthode est GET et seulement si l'url est /article
}

app.post('/article', (req, res, next) => {
  // S'exécute seulement si la méthode est POST et seulement si l'url est /article
}
```

### Pratique #3

::: tip Pratique
- Dans le fichier app.js, modifier le premier middleware qui répond à la route "/" afin qu'il spécifie l'en-tête http le html suivant :

```js
const debutHtml = `<html lang="fr">
<head>
<title>Blogue</title>
</head>
<nav style="background-color:black;">
  <h1 style="text-align:center; color:white">Mon supre blogue</h1>
</nav>
<body>`;
```

- Ajouter la route `/articles` pour un `get` seulement, qu'il écrive le titre "Liste des articles" puis qu'il appelle le middleware suivant.
- Ajouter ensuite un middleware qui répond à la route `/`, qui écrit le html suivant et qui renvoi la requête au client.

```js
const finHtml = `</body>
</html>`;
```

- Tester les routes http.
- voir `demo2-routes` pour solution
:::

## Récupérer les données d'un formulaire
Les données provenant d'un formulaire, peuvent être récupérées en accédant à la propriété `body` de la requête: `req.body`.

De plus, pour être en mesure d'analyser le corps `body` de la requête, on doit utiliser un `parser`.

```js
app.use(express.urlencoded({
    extended: false
}));
```

Si un champ de formulaire a l'attribut `name="titre"`, on peut récupérer la valeur du champ avec `req.body.titre`.

### Pratique #4

::: tip Pratique
- À partir de la pratique #3, ajouter la route `/articles/new` pour un `get` seulement, qui écrit le html suivant puis qui appelle le middleware suivant :

```js
const formulaire = `<h2>Ajouter un article</h2>
<form action="/articles/new" method="POST">
	<label for="titre">Titre de l'article :</label>
	<input type="text" id="titre" name="titre" required>
	<button type="submit">Ajouter</button>
</form>
<br>
<a href="/articles">Retour à la liste des articles</a>`;
```
- Ajouter la route `/articles/new` pour un `post` seulement qui obtient le titre de l'article et l'ajoute à la liste des articles nommmées `articles` , écrit un message de confirmation suivant puis appelle le middleware suivant:

```js
const message = `
<h1>L'article a bien été ajouté.</h1>
<a href="/articles">Retour à la liste des articles</a>`;
```

- Modifier le middleware `/articles` pour afficher les articles contenus dans la liste `articles`.
- Tester les routes http.
- Voir `demo3-form-data` pour la solution.
:::



## Gestion des erreurs 404

Voici un exemple d’un middleware pour gérer les pages 404.

Peut-on le placer n’importe où dans le code ?

```js
app.use((req, res, next) => {
  res.setHeader("Content-Type", "text/html; charset=utf-8");
  res.status(404).send('<h1>Page introuvable !</h1>');
});
```

::: details Réponse
Il faut placer ce middleware à la fin du code, après toutes les autres routes.
:::

## Utilisation de vues

Il est possible de renvoyer des fichiers HTML (vues) en utilisant la méthode `sendFile()`.

Par exemple, nous pourrions vouloir renvoyer le fichier `index.html` qui se trouve dans le répertoire `views​​​​​​​`:

```js	
res.sendFile(path.join(__dirname, './', 'views', 'index.html'));
```

`path.join()` permet de créer le chemin d'accès correct pour trouver le fichier HTML.

`__dirname` est une variable d'environnement dans node.js qui vous indique le chemin absolu du répertoire contenant le fichier en cours d'exécution.

### Pratique #5

::: tip Pratique
- Créer la structure de dossier et fichier suivantes : 

```markdown
📂 demo4-vue-html
│── 📂 views                   # Dossier contenant les fichiers HTML
│   ├── 📄 index.html          # Page principale affichant la liste des articles
│   ├── 📄 add-post.html       # Page permettant l'ajout d'un article
│── 📄 app.js                  # Fichier principal du serveur Express
│── 📄 package.json            # Fichier de configuration du projet Node.js

```
- Ajouter le html suivant dans le fichier `/views/index.html` : 

```html
<!DOCTYPE html>
<html lang="fr">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Accueil</title>
</head>

<body>
    <header>
        <nav>
            <ul>
                <li><a href="/">Accueil</a></li>
                <li><a href="/article/new">Ajout d'article</a></li>
            </ul>
        </nav>
    </header>

    <main>
        <h1>Mon blogue</h1>
        <p>Liste des articles...</p>
    </main>
</body>

</html>
```
- Ajouter le html suivant dans le fichier `views/add-post.html` :

```js
<!DOCTYPE html>
<html lang="fr">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Ajout d'article</title>
</head>

<body>
    <header>
        <nav>
            <ul>
                <li><a href="/">Accueil</a></li>
                <li><a href="/article/new">Ajout d'article</a></li>
            </ul>
        </nav>
    </header>

    <main>
        <form action="" method="POST">
            <input type="text" name="titre">
            <button type="submit">Ajouter</button>
        </form>
    </main>
</body>

</html>
```
- Dans le fichier `app.js` :

  - Ajouter la route `/articles/new` pour un `get` seulement qui spécifie le l'en-tête et retourn le fichier `/views/add-post.html`.
  - Ajouter la route `/articles/new` pour un `post` seulement qui obtient le titre de l'article et l'ajoute à la liste des articles nommmées `articles` , et redirige vers la page d'accueil en utilisant l'inscruction suivante : 

  ```js
    res.redirect('/');
  ```
  - Ajouter la route `/` pour un `get` seulement qui spécifie le l'en-tête et retourn le fichier `/views/index.html`.
  - Ajouter un middleware pour la gestion des erreurs 404.

- Tester les routes http.
- Voir` demo4-vue-html` pour la solution.
:::

::: warning Remarque
Vous remarquerez que, dans l'exemple précédent, bien qu'il soit possible de retourner une vue (fichier HTML), il n'est pas possible d'injecter des variables dans ces fichiers. Il est donc impossible d'afficher, par exemple, la liste des articles sur la page d'accueil.

Pour ce faire, il est nécessaire d'utiliser un moteur de template.
:::

## Template HTML

Express.js offre plusieurs moteurs de template :

En voici trois avec un exemple de `syntaxe utilisée pour afficher une variable `name`:

- [EJS](https://ejs.co/): `<p><%= name %></p>`
- [PUG (jade)](https://pugjs.org/api/getting-started.html): `p #{name}`
- [Handlebars (hbs)](https://handlebarsjs.com/): `<p>{{name}}</p>`


Voici par exemple l'installation de Handelbars :

```bash
npm install hbs --save
```


## Fichiers statiques

Pour utiliser des fichiers statiques comme du css par exemple. On doit utiliser le middlerware `express.static`.

Voici un exemple spécifiant que les fichiers statiques se trouvent dans le sous-dossier "public":

```js
app.use(express.static(path.join(__dirname, 'public')));
```

### Pratique #6

::: tip Pratique


Créer la structure de dossiers et fichiers suivante : 

```makdown
📂 demo5-template-hml-handlebars
│── 📂 public                   # Dossier contenant les fichiers statics
│   ├── 📄 main.css             # Mise en page du site
│── 📂 views                    # Dossier contenant les vues Handlebars
│── │── 📂 partials             # Dossier contenant les vues Handlebars
│   │   ├── 📄 footer.hbs       # Footer du site web
│   │   ├── 📄 head.hbs         # Header du site web
│   │   ├── 📄 navigation.hbs   # Navigation du site web
│   ├── 📄 index.hbs            # Page principale affichant la liste des articles
│   ├── 📄 form.hbs             # Page du formulaire pour ajouter un article
│   ├── 📄 404.hbs              # Page d'erreur 404
│── 📄 app.js                   # Fichier principal Express
│── 📄 package.json             # Fichier de configuration Node.js
```
- installer BootStrap 

```bash
npm i bootstrap@5.3.3
```
- Ajouter le code suivant dans le ficher `/public/main.css`

```css
/* Applique un modèle flexbox pour toute la page */
html, body {
    height: 100%;
    margin: 0;
    display: flex;
    flex-direction: column;
}

/* Main prend tout l’espace disponible si besoin */
main {
    flex: 1;
}

```

- Ajouter le code suivant dans le fichier `/views/partials/head.hbs` :

```html
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>{{title}}</title>

     <!-- Bootstrap CSS (Chargé depuis node_modules) -->
    <link rel="stylesheet" href="/bootstrap/css/bootstrap.min.css">
    
    <link rel="stylesheet" href="/css/main.css">
</head>

```

- Ajouter le code suivant dans le fichier `/views/partials/navigation.hbs` :

```html
    <header class="d-flex flex-wrap justify-content-center py-3 mb-4 border-bottom">
      <a href="/" class="d-flex align-items-center mb-3 mb-md-0 me-md-auto link-body-emphasis text-decoration-none">
        <svg class="bi me-2" width="40" height="32"><use xlink:href="#bootstrap"/></svg>
        <span class="fs-4">Mon blogue</span>
      </a>

      <ul class="nav nav-pills">
        <li class="nav-item"><a href="/" class="nav-link active" aria-current="page">Accueil</a></li>
        <li class="nav-item"><a href="/articles/new" class="nav-link">Ajouter d'article</a></li>
      </ul>
    </header>

```

- Ajouter le code suivant dans le fichier `/views/partials/footer.hbs` :

```html
    <footer class="bg-dark text-light text-center py-3 mt-4">
        <div class="container">
            <p class="mb-0">© 2024 Mon Blog - Tous droits réservés.</p>
        </div>
    </footer>

    <!-- Bootstrap JS (Local) -->
    <script src="/bootstrap/js/bootstrap.bundle.min.js"></script>

    </body>
</html>

```

- Ajouter le code suivant dans le fichier `/views/index.hbs` :

```html
{{> head}} <!-- Inclure le header -->
<body>
    {{> navigation}} <!-- Inclure la navigation -->
    <main class="container mt-4">
        <h1 class="text-center mb-4">Mon super blog</h1>
      

        {{#if articles.length}}
            <div class="row">
                {{#each articles}}
                    <div class="col-md-4 mb-4">
                        <div class="card shadow-sm">
                            <div class="card-body">
                                <h5 class="card-title">{{this.titre}}</h5>
                                <p class="card-text">Un aperçu de l'article...</p>
                                <a href="#" class="btn btn-primary">Voir</a>
                            </div>
                        </div>
                    </div>
                {{/each}}
            </div>
        {{else}}
            <p class="text-center text-muted">Aucun article disponible.</p>
        {{/if}}

        <div class="text-center mt-4">
            <a href="/articles/new" class="btn btn-success">Ajouter un article</a>
        </div>
    </main>
{{> footer}} <!-- Inclure le footer -->


```

- Ajouter le code suivant dans le fichier `/views/form.hbs` :

```html
{{> head}} <!-- Inclure le header -->
<body>
    {{> navigation}} <!-- Inclure la navigation -->
    <main class="container mt-5">
        <div class="row justify-content-center">
            <div class="col-md-8 col-lg-6">
                <h2 class="text-center">Ajouter un article</h2>

                <form action="/articles/new" method="POST" class="bg-light p-4 rounded shadow">
                    <div class="mb-3">
                        <label for="titre" class="form-label">Titre de l'article :</label>
                        <input type="text" id="titre" name="titre" class="form-control" required>
                    </div>

                    <button type="submit" class="btn btn-primary w-100">
                        Créer
                    </button>
                </form>

                <div class="text-center mt-3">
                    <a href="/" class="btn btn-outline-secondary">Retour à l'accueil</a>
                </div>
            </div>
        </div>
    </main>
{{> footer}} <!-- Inclure le footer -->

```

- Installer le module `express`
- Installer le module `hbs`

- Ajouter le code suivant dans le fichier `app.js`

```js
"use strict";

const express = require("express");
const path = require("path");
const hbs = require("hbs"); // Importer hbs

const app = express();
const port = 3000;

const articles =[{titre: 'Article 1'}, {titre: 'Article 2'}];

// Définir hbs comme moteur de template
app.set("view engine", "hbs");

// Déclarer le dossier views qui contient les templates
app.set("views", path.join(__dirname, "views"));

// Enregistrer les partials
hbs.registerPartials(path.join(__dirname, "views", "partials"));

// Middleware pour l'affichage des fichiers statiques 
app.use(express.static(path.join(__dirname, 'public')));

// Servir Bootstrap depuis node_modules
app.use('/bootstrap', express.static(__dirname + '/node_modules/bootstrap/dist'));

// Middleware pour parser les requêtes POST
app.use(express.urlencoded({ extended: false }));



// Démarrage du serveur
app.listen(port, () => {
  console.log(`Serveur en écoute sur http://localhost:${port}`);
});

```
- Ajouter la route `"/"` en `"get"` seulement, permettant d'afficher la vue `index.hbs` avec la liste des articles :

```js
// Route pour afficher la page d'accueil
app.get("/", (req, res) => {
    res.render("index",               //nom de la vue retournée
    { title: "Accueil", articles });  //Objet passé à la vue
  });
```

- Ajouter la route `"/articles/new"` en `"get"` seulement, permettant d'afficher la vue `form.hbs`.
- Ajouter la route `"/articles/new"` en `"post"` seulement, permettant d'ajouter l'article à la liste des articles et de regiriger vers la page d'accueil.
- Ajouter un middlerware permettant la gestion des erreurs 404 qui affiche la vue` 404.hbs`.
- Tester les routes http.
- Voir `demo5-template-hml-hbs` pour la solution.

:::


:::warning Note
L'objectif du cours étant de créer une API avec Node.js, nous ne nous attarderons pas davantage sur les vues et les templates car nous ne les utiliserons pas.

Notre application retournant des données au format JSON, nous n'avons pas besoin de vues.
:::

## Retourner des données au format JSON

Pour retourner des données JSON, on utilise la méthode `json()`.

```js
const articles = [ { "id": 1, "titre": "Article 1" }, { "id": 2, "titre": "Article 2" } ]

app.get('/api/articles', (req, res, next) => {
  res.json(articles);
});

```

On peut également retourner un code de statut avec la méthode `status()`.

```js
app.get('/api/articles', (req, res, next) => {
  res.status(200).json(articles);
});
```

*La status 200 est retourné par défaut, il n'est donc pas nécessaire de le spécifier.*


## Router

La fonction `express.Router()` est utilisée pour créer un nouvel objet routeur lorsque vous souhaitez l'utiliser dans votre programme pour gérer les requêtes. 

Exemple pour un fichier `routes.js`:

```js
const express = require('express');

//Définition d'un objet routeur
const router = express.Router();
   
router.get('/', function (req, res, next) {
    console.log("Le routage fonctionne!");
    res.end();
})
​
router.get('/articles', function (req, res, next) {
    console.log("Le routage vers la page des articles");
    res.end();
})
// Export des routes pour utilisation dans app.js
exports.routes = router;

```

Exemple pour le fichier `app.js`:

```js
const express = require('express');
const app = express();

// Utilisation des routes
app.use(routes.routes);
```


Il est également possible d'ajouter un préfixe aux routes

```js
app.use('/admin', routes.routes);
```

Toutes les routes définies dans `routes.js` seront préfixées par `/admin`.


### Pratique #7

::: tip Pratique

À partir de la pratique #6 : 

- Créer le dossier nommé `routes` et ajouter les fichiers suivants à l'intérieur :

```makdown
📂 demo6-router-hml
│── 📂 public                   
│── 📂 routes                   
│   ├── 📄 admin.js             # Fichier contenant les routes de la section administrateur
│   ├── 📄 articles.js          # Fichier contenant les routes pour l'affichage des articles
│── 📂 views                    
│── 📄 app.js                  
│── 📄 package.json             
```
- Déplacer les middleware du fichier `app.js` concernant la création d'un article dans le fichier `/routes/admin.js` :

```js

"use strict";

const express = require('express');

//Définition de l'objet router
const router = express.Router();

// N'ayant pas encore de base de données, on défini quelques titre d'articles
const articles =[{titre: 'Article 1'}, {titre: 'Article 2'}];

// /admin/articles/new => GET
router.get("/articles/new", (req, res) => {
    res.render("form", { title: "Ajouter un article" });
});

// /admin/articles/new => POST
router.post("/articles/new", (req, res) => {
  console.log(req.body);
  articles.push(req.body); //Ajout de l'article
  console.log(`Article ajouté : ${req.body.titre}`);
  res.redirect("/");
});

exports.routes = router; // Export des routes 
exports.articles = articles; // Export des articles 

```
- Déplacer le middleware du fichier `app.js` permettant l'affichage des articles dans le fichier `/routes/articles.js` :

```js
"use strict";

const express = require('express');

//Définition de l'objet router
const router = express.Router();

// Importe les articles
const { articles } = require('./admin');

// Route pour afficher la page d'accueil
router.get("/", (req, res) => {
    res.render("index", { title: "Accueil", articles });
  });

  // Export des routes 
exports.routes = router;


```

- Modifier le fichier `app.js` pour utiliser le **router** :

```js
"use strict";

const express = require("express");
const path = require("path");
const hbs = require("hbs"); // Importer `hbs`
const app = express();
const port = 3000;


// Importe les routes
const adminRoutes = require('./routes/admin');
const articlesRoutes = require('./routes/articles');

// Définir `hbs` comme moteur de template
app.set("view engine", "hbs");

// Déclarer le dossier views qui contient les templates
app.set("views", path.join(__dirname, "views"));

// Enregistrer les partials
hbs.registerPartials(path.join(__dirname, "views", "partials"));

// Servir Bootstrap depuis node_modules
app.use('/bootstrap', express.static(__dirname + '/node_modules/bootstrap/dist'));


// Middleware pour l'affichage des fichiers statiques 
app.use(express.static(path.join(__dirname, 'public')));

// Middleware pour parser les requêtes POST
app.use(express.urlencoded({ extended: false }));


// Utilisation des routes en tant que middleware
// route /admin
app.use('/admin', adminRoutes.routes);
// route /
app.use('/', articlesRoutes.routes);

// Middleware pour gérer les erreurs 404
app.use((req, res) => {
    res.status(404).render("404", { title: "Page non trouvée" });
});

// Démarrage du serveur
app.listen(port, () => {
  console.log(`Serveur en écoute sur http://localhost:${port}`);
});

- Modifier les liens dans les vues pour la section admin.
- Tester les routes http.
- Voir `demo6-router-hbs` pour la solution.

```
:::
## Modèle MVC (Modèle - Vue - Contrôleur)

![MVC](./img/express5.png){data-zoomable width=50%}

Restructuration du code pour ajouter les **contrôleurs** afin de se rapprocher du modèle **MVC**.

Pour mieux diviser notre code et respecter le modèle MVC nous pouvons créer des contrôleurs dans lesquels se trouvera les méthodes utilisées par les différentes routes.

Par convention, les contrôleurs se trouvent dans un dossier nommé `controllers`. 

Voici un exemple de contrôleur pour un gestion d'articles :

Fichier /controllers/articlesController.js

```js
//Méthode du contrôleur pour la vue du formulaire de création d'un article
exports.getCreateArticle = (req, res, next) => {
    res.render("form", { title: "Ajouter un article" });
};


//Méthode du contrôleur pour l'ajout d'un article quand le formulaire est soumis
exports.postCreateArticle = (req, res, next) => {
  articles.push(req.body); //Ajout de l'article
  res.redirect("/");
};
```


Une fois le contrôleur créé, il faut modifier les routes pour appeler les méthodes du contrôleur.

```js
const express = require('express');
const router = express.Router();

//Importation du controller pour l'appel des méthodes
const adminController = require('../controllers/articleController.js');

// /articles/new => GET
router.get('/articles/new', adminController.getCreateArticle);

// /articles/new => POST
router.post('/articles/new', adminController.postCreateArticle);

// Export des routes 
exports.routes = router;
```

### Pratique 8

::: tip Pratique

À partir de la pratique #7 : 

- Créer le dossier nommé `controllers` et ajouter les fichiers suivants à l'intérieur :

```makdown
📂 demo6-router-hml
│── 📂 controllers                   
│   ├── 📄 adminController.js             # Fichier les middleware pour la création d'un article
│   ├── 📄 articlesController.js          # Fichier middleware pour l'affichage des articles
│   ├── 📄 errorController.js             # Fichier middleware pour l'affichage des erreurs
│── 📂 public                   
│── 📂 routes                   
│── 📂 views                    
│── 📄 app.js                  
│── 📄 package.json             
```

- Déplacer les middlewares qui se trouve dans le fichier `/routes/admin.js` dans le fichier `/controllers/adminController.js` en les affectant à des variables qui seront exportées:

```js
// N'ayant pas encore de base de données, on défini quelques titre d'articles
const articles =[{titre: 'Article 1'}, {titre: 'Article 2'}];

//Méthode du contrôleur pour la vue du formulaire de création d'un article
exports.getCreateArticle = (req, res, next) => {
    res.render("form", { title: "Ajouter un article" });
};


//Méthode du contrôleur pour l'ajout d'un article quand le formulaire est soumis
exports.postCreateArticle = (req, res, next) => {
  console.log(req.body);
  articles.push(req.body); //Ajout de l'article
  console.log(`Article ajouté : ${req.body.titre}`);
  res.redirect("/");
};

//Exportation des articles
exports.articles = articles;
```


- Déplacer le middlewares qui se trouve dans le fichier `/routes/articles.js` dans le fichier `/controllers/articlesController.js` en l'affectant à une variable qui sera exportée:

```js
// Importe les articles
const { articles } = require('./adminController');

// Route pour afficher la page d'accueil
exports.getArticles = (req, res, next) => {
    res.render("index", { title: "Accueil", articles });
  };

```


- Déplacer le middlewares qui se trouve dans le fichier `app.js` pour la gestion des erreurs 404 dans le fichier `/controllers/errorController.js` en l'affectant à une variable qui sera exportée:

```js
// Importe les articles
const { articles } = require('./adminController');

// Route pour afficher la page d'accueil
exports.getArticles = (req, res, next) => {
    res.render("index", { title: "Accueil", articles });
  };

```

- Modifier le fichier `/routes/admin.js` pour utiliser le contrôleur :

```js
"use strict";

const express = require('express');

//Définition de l'objet router
const router = express.Router();


//Importation du controller pour l'appel des méthodes
const adminController = require('../controllers/adminController.js');

// /admin/articles/new => GET
router.get('/articles/new', adminController.getCreateArticle);

// /admin/articles/new => POST
router.post('/articles/new', adminController.postCreateArticle);

// Export des routes 
exports.routes = router;

```


- Modifier le fichier `/routes/aticles.js` pour utiliser le contrôleur :

```js
"use strict";

const express = require('express');

//Définition de l'objet router
const router = express.Router();

//Importation du contrôleur pour l'appel des méthodes
const articleController = require('../controllers/articlesController.js');

// / => GET
router.get('/', articleController.getArticles);

// Export des routes 
exports.routes = router;

```

- Modifier le fichier app.js pour utilise le contrôleur :

```js
"use strict";

const express = require("express");
const path = require("path");
const hbs = require("hbs"); // Importer `hbs`
const app = express();
const port = 3000;


// Importe les routes
const adminRoutes = require('./routes/admin');
const articlesRoutes = require('./routes/articles');

//Imporation du controller pour la gestion des erreurs 404
const errorController = require('./controllers/errorController');

// Définir `hbs` comme moteur de template
app.set("view engine", "hbs");

// Déclarer le dossier views qui contient les templates
app.set("views", path.join(__dirname, "views"));

// Enregistrer les partials
hbs.registerPartials(path.join(__dirname, "views", "partials"));


// Middleware pour l'affichage des fichiers statiques 
app.use(express.static(path.join(__dirname, 'public')));

// Servir Bootstrap depuis node_modules
app.use('/bootstrap', express.static(__dirname + '/node_modules/bootstrap/dist'));

// Middleware pour parser les requêtes POST
app.use(express.urlencoded({ extended: false }));


// Utilisation des routes en tant que middleware
// route /admin
app.use('/admin', adminRoutes.routes);
// route /
app.use('/', articlesRoutes.routes);

//Gestion des erreurs 404
app.use(errorController.get404);

// Middleware pour gérer les erreurs 404
app.use((req, res) => {
    res.status(404).render("404", { title: "Page non trouvée" });
});

// Démarrage du serveur
app.listen(port, () => {
  console.log(`Serveur en écoute sur http://localhost:${port}`);
});

- Tester les routes http.
- Voir `demo7-controllers-hbs` pour la solution.

```



## Extraire les parmètres de l' URL

Exemple d'URL : http://localhost:3000/articles/1?test=5

```js
// Exemple de route avec récupération des paramètres de l'URL
router.get('/articles/:id', testController.getPost);

// Contrôleur testController
exports.getArticle = (req, res, next) => {
  console.log(req.params.id); // 1
  console.log(req.query); // { test: '5' }
  console.log(req.query.test); // 5
  res.end();
});
```
:::


### Pratique #9

::: tip Pratique

À partir de la pratique #8 :

- Créer la vue `/views/details.hbs` permettant d'afficher les détails d'un article : 

```html
{{> head}} <!-- Inclure le header -->
<body>
    {{> navigation}} <!-- Inclure la navigation -->
    <main class="container mt-5">
        <div class="row justify-content-center">
            <div class="col-md-8 col-lg-6">
                <div class="card shadow">
                    <div class="card-body">
                        <h2 class="card-title text-center">{{article.titre}}</h2>
                        
                        <div class="mt-3">
                            <p class="card-text">{{article.contenu}}</p>
                        </div>

                        <div class="d-flex justify-content-between mt-4">
                            <a href="/" class="btn btn-outline-secondary">Retour aux articles</a>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </main>
</body>
{{> footer}} <!-- Inclure le footer -->


```
- Ajouter la méthode `getArticle` au fichier `articlesController.js` qui permettra d'afficher la vue `details` avec l'article correspondant au numéro passé en paramètre dans l'URL. La titre de la page sera le titre de l'article.
- Dans le fichier `adminController.js`:
  - modifier la constante `articles` afin qu'elle contienne un tableau d'objets avec un `id` et un `titre`.
  - modifier la méthode `postCreateArticle` pour ajouter un `id` à l'article lors de sa création.
- Ajouter la route suivante au fichier `articles.js` permettant d'afficher le détail d'un article:
  - `router.get('/articles/:articleId', artidcleController.getArticle);`
- Modifier le liens `voir`des articles dans la page d'accueil afin de rediriger vers la page de détails.
- Tester les routes http.
- Voir `demo8-params-url-hbs` pour la solution.
:::

## Sources

- https://expressjs.com/en/4x/api.html