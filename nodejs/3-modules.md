# Notions de modules

## Modules intégrés
- Node.js dispose de modules « intégrés », voir https://nodejs.org/api
- Nous avons déjà utilisé http et fs par exemple.

## NPM (Node Package Manager)
- La communauté met aussi a disposition des modules facilement installables en utilisant le gestionnaire de paquet NPM. https://www.npmjs.com/
- NPM est installé automatiquement avec node.js.
- Les modules sont installés dans le dossier `node_modules`.
- La liste des modules installés ainsi que la version requise sont consignées dans le fichier `package.json`
- On utilise les modules grâce à la commande `require()`, ou `import`.

Pour créer un nouveau package, on utilise la commande suivante:​​​​​​​

```bash
npm init
```

Il faut ensuite répondre aux questions posées.

Exemple d'un nouveau fichier `package.json`: 

```js
{
  "name": "demo7",
  "version": "1.0.0",
  "description": "",
  "main": "index.js",
  "scripts": {
    "test": "echo \"Error: no test specified\" && exit 1"
  },
  "author": "",
  "license": "ISC"
}
```

## Installation d'un module

la commande `npm install` est utilisé pour installer un module.

Exemple d'installation du module `nodemon`:

```bash	
npm install nodemon
```

Les fichiers du package installé sont copiés dans le dossier `node_modules` et le fichier `package.json` est modifié et le fichier `package-lock.json` est créé.

 Exemple de contenu du fichier `package.json` après l'installation du module `nodemon`:

```js
{
  "name": "demo7",
  "version": "1.0.0",
  "main": "app.js",
  "scripts": {
    "test": "echo \"Error: no test specified\" && exit 1"
  },
  "author": "",
  "license": "ISC",
  "description": "",
  "dependencies": {
    "nodemon": "^3.1.9"
  }
}
```

`package-lock.json` est un fichier généré automatiquement lors de l'installation des dépendances d'un projet Node.js. Il sert à verrouiller de manière explicite les versions exactes des dépendances installées, ce qui garantit la reproductibilité et la cohérence des versions entre différents environnements de développement et de production.

Ce fichier enregistre les versions précises de toutes les dépendances du projet, y compris les dépendances indirectes (dépendances des dépendances). Cela signifie que si une version spécifique d'une dépendance est installée, elle sera enregistrée dans `package-lock.json`, même si une plage de versions plus large est spécifiée dans `package.json`

## Gestion des versions des paquets dans `package.json`

Les numéros de version des paquets sont définis selon le standard SemVer (Semantic Versioning). Ce standard définit un numéro de version selon le format suivant : MAJEUR.MINEUR.PATCH

On peut spécifier une version exacte ou une plage de version.

Les symboles suivants peuvent être utilisés pour définir une plage de versions :

- 1.2.3 => version exacte
- ~1.2.3 => équivalent à >=1.2.3 <1.3.0
- ^1.2.3 => équivalent à >=1.2.3 <2.0.0​​​​​​​



## Commandes NPM

| <div style="width:340px">Commande</div> | Description |
|---|---|
| `npm init` | Initialisation de npm avec création de "package.json" |
| `npm install`	| Installe tous les modules qui sont dans package.json |
| `npm install *module* --save` | Installe un module avec ajout dans package.json |
|  `npm install *module* -g` | Installe un module globalement sur la machine |
| `npm install *module* –save-dev` | Installe un module avec ajout dans package.json dans la partie devDependencies (module nécessaire seulement pour le développement) |
| `npm uninstall *module*` | Désinstallation d’un module |
| `npm uninstall *module* -g` | Désinstallation globale d’un module |
| `npm ls` |  Afficher la liste des modules pour un projet |
| `npm ls –g` |  Afficher la liste des modules installer gloabalement sur la machine |
| `npm update` | Mise à jour de tous les modules |
| `npm update *module*`  | Mise à jour d'un module spécifique |
|` npm run *nom_du_script*` |	Exécution d'un script |
| `npm audit` | Vérifier les vulnérabilités des dépendances |




:::
## Pratique

::: tip Pratique #4
- Créer un dossier nommé demo7
- À l'intérieur de ce dossier, exécuter la commande `npm init`
- Dans le fichier package.json, ​​​​​​ajouter la script (commande) `"start" :  "node app.js"`
- Exécuter la commande `npm run start` et voir ce qui se passe. (CTRL + c pour arrêter l'exécution)
- Créer un fichier nommé `app.js` avec le code pour :
  - créer un serveur node.js
  - Afficher une page html contentant le titre "Bienvenue sur mon blogue"
- Exécuter la commande `npm run start`
- Installer le module `nodemon`
  - Voir différence `--save` et `--save-dev`
- Voir numéro de version des modules​​​​​​​
- Voir `package.json` et `package-lock.json`
- Installer `nodemon` globalement
- Vérifier l'installation de nodemon : `nodemon -v`
- Modifier la commande start afin d'utiliser nodemon avec le fichier app.js au lieu de node : `nodemon app.js`
- Exécuter la commande `npm run start`
- Modifier le fichier `app.js` et voir se ce qui se passe.

La solution se trouve dans se trouve dans `Demo7`

---



Si ça ne fonctionne pas : Erreur : « l’exécution de scripts est désactivée sur ce système ».
- **Sur PC**
  - Ouvrir une console en mode **administrateur** : on démarre **powershell** et on fait un set:
```shell
C:> set-executionpolicy unrestricted
```	
- **Sur Mac**

```shell
sudo npm install -g nodemon
npm config set prefix /usr/local
```
:::


::: info Installation en production

Lorsque tu exécutes :
```bash
npm install --production
``` 

Seuls les dependencies seront installés.
- Les devDependencies (comme nodemon) ne seront pas installés en production.
- C'est pourquoi il est recommandé d’installer nodemon avec --save-dev, car en production, on utilise directement node server.js et non nodemon.
:::


## Module export
Node.js permet de découper notre application sous forme de module. Les variables créées dans chaque module auront une portée locale.
Il est ensuite possible d'inclure les modules avec l'instruction `require('./fichier.js')`.

Pour être utilisé, le module doit exporter des données en utilisants l'instruction module.exports

**fichier math.js​​​​​​​**

```js
"use strict";
​
const additionner = (x, y) => {
  return x + y;
}
​
const soustraire = (x, y) => {
  return x - y;
}
​
module.exports = {
  "additionner" : additionner,
  "soustraire" : soustraire
};
```

**fichier app.js**

```js
"use strict";
​
const math = require('./math');
​
console.log(math.additionner(2,2));
console.log(math.soustraire(4,2));
```

## Pratique

::: tip Pratique #5
- créer un dossier nommé `demo8`
- Reprendre le fichier `app.js` dans le dossier `demo7` et mettre le code qui est dans `http.createServer` dans un module `routes.js` à part.
- Vous devriez avoir l’arborescence suivante :

```markdown
📂 demo8
├── app.js
└── routes.js
```

- Modifier le fichier `app.js` afin qu'il importe le module `route.js` afin que la méthode `http.createServer` optienne en paramètre le contenu qui se trouve dans `routes.js`. Ex. `http.createServer(routes)`
- Exécuter `nodemon app.js` afin de vérifier si tout fonctionne.

La solution se trouve dans `demo8`
::: 

## module d’analyse d’URL « url »

Le module « url » fournit des utilitaires pour la résolution et l'analyse des URL : https://nodejs.org/dist/latest-v14.x/docs/api/url.html

Voir `demo9`

```js	
"use strict";

const http = require('http');
const url = require('url');

const server = http.createServer((req, res) => {

  const q = url.parse(req.url, true);

  console.log('host', req.headers.host); // localhost:3000
  console.log('href : ', q.href); // /index.html?post=15&cat=chien
  console.log('pathname : ', q.pathname); // /index.html
  console.log('search : ', q.search); // ?post=15&cat=chien 

  const qdata = q.query; // { post: '15', cat: 'chien' }
  console.log('post : ', qdata.post); // 15 
  console.log('cat : ', qdata.cat); // chien 
  res.statusCode = 200;
  res.end();
});

server.listen(3000, () => {
  console.log('Node.js est à l\'écoute sur http://localhost:%s ', server.address().port);
  });

```

## Sources

- https://nodejs.org/dist/latest-v18.x/docs/api/
- https://nodejs.org/en/docs/guides/anatomy-of-an-http-transaction/
- https://www.codeheroes.fr/2017/10/05/demystifions-boucle-devenement-event-loop-de-node-js/