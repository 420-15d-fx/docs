# HATEOAS (Hypertext As The Engine Of Application State)

HATEOAS est un principe de conception d'API qui permet aux clients de découvrir de manière dynamique les ressources d'une API et les actions disponibles sur ces ressources.

En d'autres termes, HATEOAS permet à une API de fournir des liens hypertextes (hyperliens) dans ses réponses, de sorte que les clients puissent naviguer facilement entre les différentes ressources et actions associées à l'API, sans avoir à connaître à l'avance la structure exacte de l'API.

Le principe de HATEOAS est basé sur le fait que les clients peuvent découvrir dynamiquement les ressources et les actions disponibles en suivant les liens hypertextes fournis par l'API. Cela rend les API plus flexibles et évolutives, car les développeurs peuvent ajouter de nouvelles fonctionnalités à l'API sans affecter les clients existants.

## HATEOAS - le dernier niveau de maturité d'une architecture REST

**Niveau 0** : L’API ne comporte qu’un seul endpoint.

**Niveau 1** : Plusieurs ressources atteignables via une URI en POST seulement.

:::tip Niveau 2
**Niveau utilisé jusqu'à présent** : Notion de verbes et de codes HTTP
	GET, POST, DELETE…

Code de réponse 1xx, 2xx, 3xx, 4xx, 5xx
:::

::: warning Niveau 3
**HATEOAS** : Les requêtes sont les mêmes qu’au niveau précédent, mais les réponses sont enrichies avec des liens hypermédias.
:::

![HATEOAS](./img/hateoas1.png){ data-zoomable }
Niveaux de maturité des applications REST selon [Leonard Richardson](https://martinfowler.com/articles/richardsonMaturityModel.html)

--- 
## Découplage client - serveur

Le « client » n’a plus besoin de connaître les URI pour faire des requêtes au serveur.
Les liens sont maintenant dans les réponses envoyées par le serveur.

Exemple de réponse pour un compte bancaire via la requête :

`GET /accounts/1`

```sh
{
  "balance": {
    "montant": 100,
    "devise": "CAN"
  },
  "_links": {
    "self": { "uri": "/accounts/1" },
    "factures": { "uri": "/accounts/1/factures" },
    "deposer": { "uri": "/accounts/1/deposer" },
    "retirer": { "uri": "/accounts/1/retirer" },
    "fermeture": { "uri": "/accounts/1/fermeture" }
  }
}
```

L’objet **_Links** contient toutes les informations nécessaires pour trouver les ressources relatives à ce compte.

![HATEOAS](./img/hateoas2.png){ data-zoomable }

## Conventions
Pour le moment il n’existe pas vraiment de convention officielle pour l'écriture des liens. Cependant on peut se baser sur ces spécifications : [HAL](https://en.wikipedia.org/wiki/Hypertext_Application_Language) ou [JSON-API](https://jsonapi.org/).

### Exemple avec HAL
```sh
{
  "_links": {
    "self": { "href": "/orders/523" },
    "warehouse": { "href": "/warehouse/56" },
    "invoice": { "href": "/invoices/873" }
  }
}
```
`self` permet de retrouver le lien vers la ressource qui a permis d’obtenir cette réponse.

### Exemple avec JSON-API

```sh
{
  "links": {
    "self": "http://example.com/articles",
    "next": "http://example.com/articles?page[offset]=2",
    "last": "http://example.com/articles?page[offset]=10"
  }
}
```

### Exemples d’API qui implémentent HATEOAS

- API Github [/Organizations](https://docs.github.com/en/rest/orgs)
- Paypal [/v2/checkout/orders](https://developer.paypal.com/docs/api/orders/v2/#orders_create)

## Exemple de réponse HATEOS pour une collection

```js
const url_base = process.env.URL + ":" + process.env.PORT;
​
//Exemple HATOES pour une collection
exports.getArticles = (req, res, next) => {
    Article.find()
        .then(articles => {
​
            const articlesHATEOS = articles.map(article =>{
​
                return{
​
                    ...article._doc, //Permet d'étaler l'objet Mongoose pour lui ajouter la propriété _links.
                    _links : [{
                        self: {
                            method: "GET",
                            href: url_base + req.url + "/" + article._id.toString()
                        }
                    },
                    {
                        update: {
                            method: "PUT",
                            href: url_base + req.url + "/" + article._id.toString()
                        }
                    },
                    {
                        delete: {
                            method: "DELETE",
                            href: url_base + req.url + "/" + article._id.toString()
                        }
                    }
                    ]
                };
​
            });
            res.json(articlesHATEOS);
        })
        .catch(err => {
            next(err);
        });
};
```

Quand vous accédez à un document MongoDB via Mongoose, le document est un objet JavaScript qui possède des propriétés supplémentaires ajoutées par Mongoose, telles que _id, __v (version), etc. Ces propriétés supplémentaires ne sont pas directement définies dans l'objet retourné par MongoDB, mais plutôt ajoutées par Mongoose lorsqu'il construit l'objet.

`article._doc` est la partie de l'objet Mongoose article qui contient réellement les données du document telles qu'elles sont stockées dans la base de données. En utilisant `...article._doc`, vous prenez toutes les propriétés de l'objet _doc et les étalez dans un nouvel objet. Il est égalment possible d'utiliser `article.toJSON()` qui fera la même chose. 

## Utilisation de la librairie [express-hateoas-links](https://github.com/orca-scan/express-hateoas-links)

```shell

npm install --save express-hateoas-links

```


Cette librairie permet de simplifier la syntaxe pour l'ajout de l'information HATEOAS.

`fichier app.js`
```js
const hateoasLinker = require("express-hateoas-links");
​
// remplace le res.json standard avec la nouvelle version
// qui prend en charge les liens HATEOAS
app.use(hateoasLinker); 
```

`fichier articleController.js`

```js	
//...
​
​
//Exemple d'utilisation de la library express-hateoas-links
res.status(200).json(article,[
  {
    self: {
      method: "GET",
      href: url_base + req.url + "/" + article._id.toString()
    }
  },
  {
    update: {
      method: "PUT",
      href: url_base + req.url + "/" + article._id.toString()
    }
  },
  {
    delete: {
      method: "DELETE",
      href: url_base + req.url + "/" + article._id.toString()
    }
  }
​
]);
​
//...
```	

---

::: tip Pratique 
Compléter le code du dossier `demo13-exercice1-hateoas` afin d’ajouter les liens HATEOAS dans le fichier `gameController.js`

Un joueur et un monstre ont 100 points de vie au début du jeu.

Chaque attaque inflige une perte comprise entre 3 et 10 points de vie à l’adversaire.

Un joueur peut choisir soit de faire une attaque, soit de récupérer 10 points de vie si son niveau est inférieur à 50.

Les attaques se succèdent entre le joueur et le monstre. Quand un niveau de vie est inférieur à 0, le jeu s’arrête.

Vérifier ensuite le fonctionnement dans Postman

:::

## Sources

- https://martinfowler.com/articles/richardsonMaturityModel.html
- https://putaindecode.io/articles/hateoas-le-graal-des-developpeurs-d-api/
- https://blog.octo.com/transformez-votre-api-web-en-une-api-hypermedia/
- https://youtu.be/7qqzqse1hgc
