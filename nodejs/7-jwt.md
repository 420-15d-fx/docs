# Authentification - JWT

Dans un système REST, il n’y a pas de conservation de l’état de la session (stateless).
Chaque requête doit contenir tout ce qui est nécessaire pour être « comprise » par le serveur.
Deux solutions pour s’authentifier :
- Envoyer un nom utilisateur et un mot de passe avec chaque requête.
  - Le serveur doit vérifier l’authentification à chaque réception de requête.
  - Le serveur d’authentification est parfois différent du serveur qui gère l’API.
- Utiliser un « token » fournit par le système d’authentification, et compréhensible par le serveur qui gère l’API.
  - JWT (JSON Web Token)
  - [Documentation](https://datatracker.ietf.org/doc/html/rfc7519)

## Fonctionnement

![JWT](./img/jwt1.png){ data-zoomable width=50% }

## Qu’est-ce que le JWT

Un token JWT se compose de trois parties séparées par des points :
- L'en-tête est un JSON encodé en base64 qui décrit le type de token et l'algorithme utilisé pour la signature.
- Le payload, est un JSON encodé en base64 qui contient les informations à transmettre. Ce JSON peut contenir des clefs prédéfinies (appelées claims) qui permettent de donner des informations supplémentaires sur le token (date d'expiration, cible, sujet…).
-  La signature permet de s'assurer de l'authenticité du token. Cette signature est générée à partir des deux premières clefs avec un algorithme particulier et avec une clef secrète.

![JWT](./img/jwt2.png){ data-zoomable width=50% }

Voir https://jwt.io/ pour décoder un JWT.

## Envoi du token JWT
- Le token JWT est passé dans chaque requête pour accéder à une ressource protégée.
- Typiquement on l’insère dans le Header avec la clé Authorization et le type Bearer.

![JWT](./img/jwt3.png){ data-zoomable width=25% }
![JWT](./img/jwt4.png){ data-zoomable width=50% }

### Exemple

![JWT](./img/jwt5.png){ data-zoomable width=50% }

## Avantages / inconvénients

|Avantages | Inconvénients |
|--|--|
|Facile à mettre en place.| Le JWT token reste valide même si l’utilisateur se déconnecte ou supprime son compte. | 
|La vérification de l’authenticité peut se faire de manière autonome grâce à la clé secrète.| Il faut stocker le JWT token côté client. |
|Des [librairies](https://jwt.io/libraries) sont disponibles pour de nombreux langages.|Il expire seulement quand sa date d’expiration est dépassée. |

## Utilisation avec Express.js
Pour utliser le token JWT avec Express.js :

```javascript
npm install --save jsonwebtoken
```

```javascript
const jwt = require('jsonwebtoken');
//...
// Création du token
const token = jwt.sign(
  {
    // Payload
    email: email
  },
  process.env.SECRET_JWT,
  { expiresIn: '1h' }
);
//...
// Vérification d'un token
decodedToken = jwt.verify(token, process.env.SECRET_JWT);

```

Ici, `process.env.SECRET_JWT` indique que la clé secrète se trouve dans le fichier `.env`.

## Ajout de middleware pour les routes

Pour les routes que l’on souhaite protéger avec l’authentification, on peut ajouter un middleware qui vérifie le JWT :

```javascript
// importe le middleware
const isAuth = require('../middleware/is-auth’);
...
// utilise le middleware avant d’executer le controlleur
router.post('/post/', isAuth, postsController.createPost);
```

### Exemple de middleware `is-auth.js` :

```javascript

const jwt = require('jsonwebtoken');
const dotenv = require('dotenv');
dotenv.config();

	/** Vérifie si la requête a un token JWT valide */

module.exports = (req, res, next) => {
	// Récupère le jeton depuis l'en-tête Authorization de la requête
  const authHeader = req.get('Authorization');

	// Vérifie si l'en-tête Authorization est présent
  if (!authHeader) {
    return res.status(401).json({ error: 'Non authentifié.' });
  }

	// Récupère le jeton JWT
  const token = authHeader.split(' ')[1];
  let decodedToken;

  try {
	  // Vérifie le jeton et récupére les données associées
    decodedToken = jwt.verify(token, process.env.SECRET_JWT);
		// Ajoute les données associées à l'objet de requête pour utilisation ultérieure
    req.user = decodedToken;
    next();
  } catch (err) {
    err.statusCode = 401;
    return next(err);
  }
};

```

## Variables d'environnement
- Rassembler toutes les variables d’environnement dans un fichier.
- Créer un fichier `.env` à la racine du projet.
- Ajouter les variables, exemple :​​​​​​​​​​​​​​

```bash
SECRET_JWT=maphrasesupersecrete
```

- Ajouter ce fichier dans le `.gitignore`
- ​​​​​​​Créer une copie du fichier `.env` nommée `.env.example` qui contient  les variables avec de fausses valeur. Ce fichier ne doit pas faire partie du `.gitignore`. Il servira de modèle pour les autres développeurs.
- Installer la librairie `dotenv`.
- Configuration : 

```javascript
const dotenv = require('dotenv');
dotenv.config();
```

- Utilisation :

```javascript
process.env.SECRET_JWT
```

## Hachage des données avec bcrypt

Le module `bcrypt` est un module de chiffrement de mot de passe pour Node.js.

Il permet de stocker les mots de passe de manière sécurisée en les hachant de manière irréversible, de sorte qu'ils ne puissent pas être facilement récupérés en cas de violation de la sécurité.

`bcrypt` utilise l'algorithme de hachage Blowfish pour chiffrer les mots de passe. Il utilise également une technique de salage (salt) pour renforcer la sécurité du hachage en ajoutant une chaîne aléatoire de caractères à chaque mot de passe avant de le hacher.

Pour hacher un mot de passe :​​​

```javascript
const bcrypt = require('bcrypt');
​
const motDePasse = 'monMotDePasse123';
const saltRounds = 10; //Nombre de tours de chiffrement
​
// Hachage du mot de passe avec une promesse
bcrypt.hash(motDePasse, saltRounds)
  .then(hash => {
    // Affichage du mot de passe haché
    console.log('Mot de passe haché :', hash);
​
  })
  .catch(err => {
    console.error(err);
  });
```


Exemple de comparaison de mot de passe 

```javascript
const bcrypt = require('bcrypt');
​
// Hash du mot de passe à partir d'une chaîne de caractères (pourrait provenir de la bd par exemple)
const motDePasseHash = '$2b$10$qvkvz1Ng.IwQ2Z4b4c4kiuVWh9sxGDXOXHsx7zbwwOxD.MPgx7OIm';
​
// Mot de passe fourni par l'utilisateur
const motDePasse = 'monMotDePasse123';
​
// Comparaison du mot de passe fourni avec le hash stocké
bcrypt.compare(motDePasse, motDePasseHash)
  .then(res => {
    if (res) {
      console.log('Mot de passe correct');
    } else {
      console.log('Mot de passe incorrect');
    }
  })
  .catch(err => {
    console.error(err);
  });
```

::: tip Pratique
- À partir de la démo `demo11-rest`
- Ajouter un modèle `User` avec les propriétés :
  - email
  - name
  - password
- Ajouter au nouveau contrôleur nommé `authController` avec les méthodes suivantes :
  - `signup` : Permet de créer un utilisateur à partir de son email, name et password. Vous devez hacher le mot de passe à l'aide du module `bcrypt`.
  - `login` : Vérifie si le courriel et le mot de passe de l'utilisateur sont valides. Si oui, alors crée un token JWT contenant le email, name et id de l'utilisateur et on le retourne avec la réponse.
- Ajouter les routes suivantes dans le fichier `auth.js`
  - /signup
  - /login
- Créer un fichier à la racine du projet nommé `.env`. Ajouter la clé suivante dans le fichier :
  - `SECRET_JWT=maphrasesupersecrete`
- Installer le module `dotenv`
- Créer un dossier nommé `middleware` 
- Ajouter à l'intérieur de ce dossier un fichier nommé `isAuth.js`
  - Écrire le code permettant à ce middleware de vérifier la validité d'un token JWT.
- Ajouter le middleware `isAuth` aux routes suivante pour valider l'authentification d'un utilisateur :
  - /post => POST
  - /post/postId => PUT
  - /post/postId => DELETE
- Dans Postman, tester les routes suivantes.
  - /signup
  - /login
- Dans Postman, tester les routes suivantes en ajoutant le token JWT dans le header de la requête avec la clé `Authorization` et la valeur `Bearer <token>` :
  - /post => POST
  - /post/postId => PUT
  - /post/postId => DELETE
- Voir la démo `demo12-jwt` pour la solution
:::
