# Gestion des erreurs

## Lancer une erreur

Pour les erreurs qui se produisent dans le code synchrone, ou si on souhaite « lever » une erreur, Express les « attrape » et les traite avec `throw`.

Par exemple si un utilisateur n'est pas authentifié :

```javascript
if (!decodedToken) {
  const error = new Error('Non authentifié.');
  error.statusCode = 401;
  throw error;
}
```

Pour les erreurs renvoyées par les fonctions **asynchrones**, on les transmet à la fonction `next()`, où Express les attrapera et les traitera.

Par exemple :

```javascript
try {
  const article = await Article.findById(postId);
  if (!article) {
    const erreur = new Error("L'article n'existe pas");
    erreur.statusCode = 404;
    throw erreur;
  }
} catch (err) {
  next(err);
}
```

## Le gestionnaire d'erreurs intégré
- Express est livré avec un gestionnaire d'erreurs intégré qui prend en charge toutes les erreurs qui pourraient être rencontrées dans l'application.
- Cette fonction middleware de gestion des erreurs par défaut est ajoutée à la fin de la pile de fonctions middleware.
- Si une erreur est passée à `next()` et qu’elle n’est pas traitée par un gestionnaire d'erreurs personnalisé, elle sera traitée par le gestionnaire d'erreurs intégré.
- L'erreur sera écrite au client avec la trace de la pile.
- La trace de la pile n'est pas incluse dans l'environnement de production.

Voici un exemple de middleware ajouté à la fin du fichier app.js pour gérer les erreurs :

```javascript
app.use(function (err, req, res, next) {
    
  // Si l'erreur est de type CastError c'est qu'on a tenté de convertir un id invalide
  if (err.kind === 'ObjectId' && err.name === 'CastError') {
    err.statusCode = 404;
    err.message= `L'id n'existe pas: ${err.message}`;
  }

  // Si l'erreur est de type ValidationError, c'est qu'on a tenté de créer une ressource invalide
  if (err.name === 'ValidationError') {
      err.message= `Erreur de validation: ${err.message}`,
      err.statusCode = 400;
  }

  if (!err.statusCode) err.statusCode = 500; //Si aucun code de status est fourni alors en retourne le code 500
  res.status(err.statusCode).json({ 
    message: err.message,   //message d'erreur
    statusCode: err.statusCode  //code de statut
  });
});
```

- On notera le paramètre supplémentaire `err` dans les paramètres de la fonction.
- On peut adapter le json qui est envoyé. Dans cet exemple on envoi le message et le code de statut.

## Sources
- https://expressjs.com/en/guide/error-handling.html