# CORS

Le « Cross-origin resource sharing » (CORS) est un mécanisme permettant à un navigateur d'accéder à des ressources provenant d'un serveur situé sur une origine différente de celle du site courant en ajoutant des en-têtes HTTP.

Lorsqu'un navigateur effectue une requête HTTP multi-origine, il demande une ressource provenant d'un domaine, d'un protocole ou d'un port différents de ceux de la page actuelle.

Par exemple, une page hébergée sur `http://domaine-a.com` peut charger des ressources telles qu'une image depuis `http://domaine-b.com`.

Bien que de nombreuses pages web chargent des ressources à partir de domaines distincts, les requêtes HTTP multi-origine sont restreintes par des considérations de sécurité. Ainsi, les API telles que `XMLHttpRequest` et `Fetch` suivent la règle d'origine unique, autorisant les requêtes uniquement vers la même origine, sauf si des en-têtes CORS sont utilisés pour lever ces restrictions.

![CORS](./img/cors1.png){ data-zoomable width=50% }

![CORS](./img/cors2.jpg){ data-zoomable width=50% }
[source](https://blog.4d.com/support-of-cross-origin-resource-sharing-cors/)

## Démo 

https://codepen.io/gduquerroy/pen/poryXdo

```js
fetch("http://localhost:3000/")
.then(res => res.json())
.then(data => console.log(data))
.catch(err => console.log(err.message))
```


Erreur si le CORS n’est pas configuré :

![CORS](./img/cors3.png){ data-zoomable width=50% }

## Requête de pré-vérification
- Une requête de pré-vérification cross-origin CORS est une requête de vérification faite pour contrôler si le protocole CORS est autorisé.
- C'est une requête utilisant la méthode `OPTIONS` qui utilise trois en-têtes HTTP : La méthode `Access-Control-Request-Method`, les en-têtes `Access-Control-Request-Headers` et les origines `Access-Control-Allow-Origin`.
- Une requête de pré-vérification est automatiquement envoyée par le navigateur quand cela est nécessaire.
- Afin de configurer les CORS, nous devons utiliser un middleware en spécifiant les origines et les méthodes et les en-têtes permises.

Exemple pour le site https://dbdiagram.io :

![CORS](./img/cors4.png){ data-zoomable width=75% }

## Middleware pour CORS

```js
app.use((req, res, next) => {
  res.setHeader('Access-Control-Allow-Origin', '*');
  // res.setHeader('Access-Control-Allow-Origin', 'https://cdpn.io');

  res.setHeader(
    'Access-Control-Allow-Methods',
    'OPTIONS, GET, POST, PUT, PATCH, DELETE'
  );
  res.setHeader('Access-Control-Allow-Headers', 'Content-Type, Authorization');
  next();
});
```


## Access-Control-Allow-Origin

Indique quels domaines sont autorisés à accéder à une ressource donnée.

Les valeurs possible pour `Access-Control-Allow-Origin` sont :
- `*` : Autorise l'accès à la ressource à partir de n'importe quelle origine. Cela peut être utilisé pour les API publiques ou pour autoriser un nombre inconnu d'origines.
- `<origin>` : Autorise l'accès à la ressource à partir d'une origine spécifique. L'origine doit être spécifiée sous la forme d'un nom de domaine complet (par exemple, `https://www.example.com`) ou d'une adresse IP.
- `null` : Autorise l'accès à la ressource à partir de pages web stockées localement (`file://`). Cette option est souvent utilisée pour les tests locaux.

ll est important de noter que la configuration de l'en-tête `Access-Control-Allow-Origin` peut affecter la sécurité de l'API. 

L'utilisation de l'option `*` peut permettre à n'importe quel domaine d'accéder à l'API, ce qui peut poser des problèmes de sécurité.

## Access-Control-Allow-Methods

Indique les méthodes HTTP autorisées pour une ressource donnée.
Les valeurs possible pour `Access-Control-Allow-Methods` sont :
- `GET` : Autorise la récupération d'une ressource.
- `POST` : Autorise l'envoi d'une nouvelle entité à une ressource.
- `PUT` : Autorise la modification d'une ressource existante.
- `DELETE` : Autorise la suppression d'une ressource existante.
- `OPTIONS` : Autorise l'obtention des méthodes HTTP autorisées pour une ressource.
- `HEAD` : Autorise l'obtention des en-têtes HTTP d'une ressource sans obtenir son contenu.
- `PATCH` : Autorise la modification partielle d'une ressource.

Il est important de noter que la liste des méthodes HTTP autorisées peut varier en fonction de la configuration de l'API. Il est recommandé de n'autoriser que les méthodes HTTP nécessaires à l'API pour des raisons de sécurité. Il est important de spécifier les méthodes HTTP autorisées pour chaque ressource afin de garantir que les utilisateurs peuvent interagir avec l'API de manière sécurisée et appropriée. 

## Access-Control-Allow-Headers

Indiquer les en-têtes HTTP personnalisés qui peuvent être utilisés dans une requête CORS.

Les valeurs possibles pour l'en-tête `Access-Control-Allow-Headers` sont : 
- `*` : Autorise tous les en-têtes. Cela est souvent utilisé pour les API publiques ou les API qui autorisent un nombre inconnu d'en-têtes personnalisés.
- `Origin` : Autorise la requête à spécifier l'origine du site web à partir duquel la requête est envoyée.
- `Content-Type` : Autorise la requête à spécifier le type de contenu de la demande. Cela est souvent utilisé pour les demandes POST et PUT.
- `Accept` : Autorise la requête à spécifier les types de contenu acceptables pour la réponse.
- `Authorization` : Autorise la requête à spécifier les informations d'authentification nécessaires pour accéder à une ressource.
- `X-Requested-With` : Autorise la requête à spécifier le type de requête Ajax (XMLHttpRequest, Fetch, etc.).
- `X-Forwarded-For` : Autorise la requête à spécifier une liste d'adresses IP pour lesquelles la demande a été acheminée.
- `If-Modified-Since` : Autorise la requête à spécifier la date à laquelle la ressource a été modifiée pour vérifier si elle a été mise à jour depuis la dernière requête.

Il est important de noter que l'utilisation de l'en-tête `Access-Control-Allow-Headers` doit être utilisée avec prudence pour des raisons de sécurité. Il est recommandé de n'autoriser que les en-têtes spécifiques nécessaires à la demande et d'éviter l'utilisation de l'en-tête `*` sauf si cela est absolument nécessaire.


## Librairie CORS

Il est également possible d’utiliser la librairie cors afin de profiter d’options supplémentaires.

Voir les exemples dans la [documentation](https://github.com/expressjs/cors).

--- 
::: tip Pratique
- À partir de `demo11-rest`.
- Ajouter un middleware  dans le fichier app.js permettant d'ajouter à l'en-tête de la réponse les éléments suivants :
  - `Access-Control-Allow-Origin`, pour n'importe quelle origine
  - `Access-Control-Allow-Methods`, pour les méthodes de l'API
  - `Access-Control-Allow-Headers`, pour autoriser la requête à spécifier les informations d'authentification nécessaires pour accéder à une ressource.
- Tester votre API : https://codepen.io/gduquerroy/pen/poryXdo avec l'url http://localhost:3000/posts
:::

## Sources

- https://developer.mozilla.org/fr/docs/Web/HTTP/CORS
- Librairie => https://github.com/expressjs/cors
- https://flaviocopes.com/express-cors/ ​​​​​​​​​​​​​​