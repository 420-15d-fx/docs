---
# https://vitepress.vuejs.org/reference/default-theme-home-page
layout: home


hero:
  name: "Techniques de l'informatique"
  text: "420-15D-FX Applications Web et sécurité"
  # tagline: My great project tagline
  # actions:
  #   - theme: brand
  #     text: Django
  #     link: /django/install
    # - theme: alt
    #   text: API Examples
    #   link: /api-examples

features:
  - title: ASP.NET Core
    details: Accés aux cours.
    link: /aspnet/presentation
 # - title: Django
 #   # icon: 
 #     # src: "/django/img/django-logo-positive.svg"
 #   details: Accés aux cours.
 #   link: /django/presentation
  - title: Javascript
    details: Accés aux cours.
    link: "/javascript/1-rappels"
  - title: Express.js
    # icon: 
    #   src: "https://upload.wikimedia.org/wikipedia/commons/c/c9/Microsoft_Office_Teams_%282018%E2%80%93present%29.svg"
    details: Accés aux cours.
    link:  /nodejs/1-intro
  - title: Vue.js
    details: Accés aux cours.
    link: 
---

