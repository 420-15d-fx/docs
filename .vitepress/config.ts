import { SearchPlugin } from "vitepress-plugin-search";
import { defineConfig } from 'vitepress'


var options = {
  previewLength: 62,
  buttonLabel: "Recherche",
  placeholder: "Recherche docs",
  language: "fr",
  allow: [],
  ignore: [],
};

// https://vitepress.vuejs.org/reference/site-config
export default defineConfig({
  head: [
    ['link', { rel: 'icon', href: 'https://www.cegepgarneau.ca/img/site-icons/coast-228x228.png' }]
  ],
  title: "420-15D-FX",
  description: "Node.js",
  // outDir: '../public',
  base: "/docs/",
  ignoreDeadLinks: true,
  themeConfig: {
    // https://vitepress.vuejs.org/reference/default-theme-config
    aside: true,
    outline : {
      level : [2,4]
    },
    nav: [
      { text: 'Accueil', link: '/' },
      // { text: 'Examples', link: '/markdown-examples' }
    ],
    sidebar: [
      /*{
        text: 'Django',
        collapsed: true,
        items: [
          { text: 'Présentation de Django', link: 'django/presentation' },
          { text: 'Installation de Django', link: 'django/install' },
          { text: 'Création d\'une application', link: 'django/apps' },
          { text: 'Première vue et urls', link: 'django/premiere_vue' },
          { text: 'Template', link: 'django/templates' },
          { text: 'Base de données', link: 'django/base_de_donnees' },
          { text: 'Le site d\'administration', link: 'django/le_site_d_administration' },
          { text: 'ORM', link: 'django/orm' },
          { text: 'Gestion des erreurs', link: 'django/gestion_des_erreurs' },
          { text: 'Les formulaires', link: 'django/forms' },
          { text: 'Authentification', link: 'django/auth' },
          { text: 'Message Flash', link: 'django/message_flash' },
          { text: 'Téléversement de fichier', link: 'django/televersement_fichier' },
          { text: 'En vrac', link: 'django/en_vrac' }
        ],
      },*/
      {
        text: 'ASP.NET Core',
        collapsed: true,
        items: [
          { text: 'Présentation', link: 'aspnet/presentation' },
          { text: 'Installation', link: 'aspnet/installation' },
          { text: 'Architecture MVC', link: 'aspnet/architecture' },
          { text: 'Projet', link: 'aspnet/projet' },
          { text: 'Templating', link: 'aspnet/templating' },
          { text: 'Routing', link: 'aspnet/routing' },
          { text: 'Razor', link: 'aspnet/razor' },
          { text: 'Formulaires', link: 'aspnet/forms' },
          { text: 'Validation des données', link: 'aspnet/validation' },
          { text: 'Base de données', link: 'aspnet/data_base' },
          { text: 'LINQ', link: 'aspnet/linq' },
          { text: 'Areas', link: 'aspnet/areas' },
          { text: 'Scaffolding', link: 'aspnet/scaffolding' },
          { text: 'Authentification', link: 'aspnet/authentication' },
          { text: 'Autorisation', link: 'aspnet/autorization' },
          { text: 'Téléversement de fichiers', link: 'aspnet/file_upload' },
          { text: 'Utilisation d\'une API', link: 'aspnet/api' },
          { text: 'Gestion des erreurs', link: 'aspnet/error_pages' },
          { text: 'Jeux d\'enregistrements', link: 'aspnet/seeds' },
          { text: 'Démonstrations', link: 'aspnet/demos' },
          { text: 'Exercices', link: 'aspnet/exercices' },
          


        ]
      },

      {
        text: 'Javascript - Approfondissement',
        collapsed: true,
        items: [
          { text: 'Extensions et rappels', link: '/javascript/1-rappels' },
          { text: 'Fonctions et fonctions fléchées', link: '/javascript/2-fonctions' },
          { text: 'Portée lexicale', link: '/javascript/3-portee_lexicale' },
          { text: 'IIFE (Immediately Invoked Function Expression)', link: '/javascript/4-iife' },
          { text: 'Fonctions anonymes', link: '/javascript/5-fn_anonymes' },
          { text: 'Concept de levage (Hoisting)', link: '/javascript/6-hoisting' },
          { text: 'this', link: '/javascript/7-this' },
          { text: 'Fermetures lexicales (closures)', link: '/javascript/8-closures' },
          { text: 'Fonction de rappel (Callback)', link: '/javascript/9-callback' },
          { text: 'Rest et Spread opérateurs', link: '/javascript/10-rest-spread' },
          { text: 'Affecter par décomposition', link: '/javascript/11-decomposition' },
          { text: 'Les promesses (promises)', link: '/javascript/12-promises' },
          { text: 'Stack / Heap', link: '/javascript/13-stack-heap' },
          { text: 'Debug', link: '/javascript/14-debug' },
          { text: 'Sources', link: '/javascript/15-sources' },
        ]
      },
      {
        text: 'Node.js',
        collapsed: false,
        items: [
          { text: 'Introduction - Installation', link: '/nodejs/1-intro' },
          { text: 'Créer un serveur', link: '/nodejs/2-creer_serveur' },
          { text: 'Notion de modules', link: '/nodejs/3-modules' },
          {
            text: 'Express.js',
            collapsed: true,
            items: [
              { text: 'Introduction', link: '/nodejs/4-express' },
              { text: 'Base de données', link: '/nodejs/5-mongoose' },
              { text: 'REST', link: '/nodejs/6-rest' },
              { text: 'Authentification - JWT', link: '/nodejs/7-jwt' },
              { text: 'CORS', link: '/nodejs/8-cors' },
              { text: 'Gestion des erreurs', link: '/nodejs/9-erreurs' },
              { text: 'HATEOAS', link: '/nodejs/10-Hateoas' },
              { text: 'Documentation', link: '/nodejs/11-documentation' },
              { text: 'Les tests', link: '/nodejs/12-tests' },
            ]
          },
          
        ]
      },
    /*  {
        text: 'Vue.js',
        collapsed: false,
        items: [
          { text: 'Introduction', link: '/vuejs/1-intro' },
          { text: 'Création d\'un projet', link: '/vuejs/2-projet' },
          { text: 'Composant', link: '/vuejs/3-composant' },
          { text: 'Formulaire', link: '/vuejs/4-form' },
          { text: 'Requête http', link: '/vuejs/5-http' },
          { text: 'Cycle de vie', link: '/vuejs/6-lifecycle' },
          { text: 'Les routes', link: '/vuejs/7-routes' },
          { text: 'Garde de navigation', link: '/vuejs/8-guard' },
          { text: 'Authentification', link: '/vuejs/9-auth' },
          { text: 'Pinia', link: '/vuejs/10-pinia' },
          { text: 'Hébergement', link: '/vuejs/11-hebergement' },
          { text: 'SSL', link: '/vuejs/12-ssl' },
          { text: 'Variables d\'environnment', link: '/vuejs/13-variables_environnement' },
        ]
      }*/
    ],
    footer: {
      message: '.',
      copyright: ''
    },
    returnToTopLabel: 'Retour en haut',
    outlineTitle: 'Sur cette page',
    socialLinks: [
      // { icon: 'github', link: 'https://gitlab.com/aec3' }
    ]
  },
  vite: { plugins: [SearchPlugin(options)] }
})
