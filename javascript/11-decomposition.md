# Affecter par décomposition

::: info Demos sur Gitlab
Voir le fichier `d10-destructuring.js` pour les exemples.

Les fichiers sont disponibles sur le dépôt GitLab :
https://gitlab.com/420-15d-fx/demos-js
:::


[Voir la documentation](https://developer.mozilla.org/fr/docs/Web/JavaScript/Reference/Operators/Destructuring_assignment)

L'affectation par décomposition est une expression JavaScript qui permet de décomposer des données d'un tableau ou d'un objet grâce à une syntaxe dont la forme ressemble à la structure du tableau ou de l'objet.

```javascript
let a, b, reste;
[a, b] = [10, 20];
console.log(a); // 10
console.log(b); // 20
​
[a, b, ...reste] = [10, 20, 30, 40, 50];
console.log(reste); // [30,40,50]
```


## Pratique #6

:::tip Décomposition
- Déclarer un objet "personne" avec trois propriétés (nom, age et adresse), dont la propriété "adresse" qui contient elle-même trois propriétés (rue, ville et province). Ensuite, vous devez utiliser l'affectation par décomposition pour assigner les propriétés "nom", "ville" et "province" de l'objet "personne" à trois variables distinctes.
- Affichez les variables  à la console
:::