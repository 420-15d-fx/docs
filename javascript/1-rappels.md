# Javascript - Approfondissement

## Extensions conseillées pour VSCode

### EsLint

- [ESLint](https://marketplace.visualstudio.com/items?itemName=dbaeumer.vscode-eslint)

- Exécutez également la commande suivante dans le terminal pour installer Eslint globalement sur votre machine.

```bash
npm install -g eslint

```

#### Configuration de ESlint

Dans la palette de commande de Vscode, recherchez `settings.json​​​​​​​`
​​​​​​​

![settings.json](./img/index.png){data-zoomable width=40%}


Et ajoutez  le code suivant pour l'exécution de Eslint lors de la sauvegarde d'un fichier

```json
"editor.codeActionsOnSave": {
        "source.fixAll.eslint": true    
      },
"eslint.validate": ["javascript"],

```



- Pour utiliser Eslint dans *tous* vos fichiers JavaScript, vous devez vous créer une fichier `.eslintrc.json` sous `C:/utilisateur/{votre_nom_utilisateur} `et exécuter la commande `eslint --init` et répondre aux questions.
- Voir tutoriel : https://www.stanleyulili.com/javascript/how-to-set-up-eslint-globally-with-vscode/
- Pour une installation locale dans un projet
  - ​​​​​​​Creér un fichier `package.json` avec la commande `npm init`
  - Créer un fichier `.eslintrc.json` avec la commande `eslint --init` et répondre aux questions.​​​​​​​


### Code Runner

Code Runner permet d'exécuter du code dans n'importe quel langage de programmation, y compris JavaScript, directement dans VSCode.

- [Code Runner](https://marketplace.visualstudio.com/items?itemName=formulahendry.code-runner)

### JSDoc Generator

JSDoc Generator permet de générer automatiquement des commentaires JSDoc pour vos fonctions.

- [JSDoc Generator](https://marketplace.visualstudio.com/items?itemName=crystal-spider.jsdoc-generator)

## Javascript - Rappels


|![js](./img/rappels1.png)|![js](./img/rappels2.png)|![js](./img/rappels3.png)|
|--|--|--|
| Pas d'affectation de type explicite                            | Les données peuvent être organisées en objets logiques | Fonctionne dans un navigateur et directement sur un pc / serveur |
| Les types de données peuvent être changés dynamiquement  | Types primitifs et de référence                           | Peut effectuer une grande variété de tâches                         |

Dans le cadre de ce cours, nous utiliserons la version ECMAScript Edition 8 (ES8/ES2017).

