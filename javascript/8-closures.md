# Fermetures lexicales (closure)

::: info Demos sur Gitlab
Voir les fichiers suivants pour des exemples : 
- `d7a-closure.js`
- `d7b-closure.js`
- `d7c-closure.js`
- `d7d-closure.js`

Les fichiers sont disponibles sur le dépôt GitLab :
https://gitlab.com/420-15d-fx/demos-js
:::

La closure, c'est quand une fonction "se souvient" de sa portée lexicale même lorsque la fonction est exécutée en dehors de cette portée.Cela permet à une fonction d'accéder aux variables de son contexte lexical, même après que le contexte d'exécution dans lequel elle a été créée ait disparu. Une closure se produit lorsque vous définissez une fonction à l'intérieur d'une autre fonction et que la fonction interne accède aux variables de la fonction externe.

## Accès aux variables externes

Une closure donne à une fonction interne accès non seulement aux variables de son propre scope mais aussi aux variables des fonctions externes et globales, selon où elle a été déclarée. Cela permet de créer des fonctions qui maintiennent un accès privé à une variable ou à un environnement, même après que la fonction externe ait terminé son exécution.

## Conservation de l'état

Les closures sont souvent utilisées pour conserver l'état entre les exécutions d'une fonction. Par exemple, une fonction peut retourner une autre fonction qui, à chaque appel, manipule des variables de l'environnement lexical de la première fonction. Cela permet de créer des fonctions avec des états privés.



## Pratique #4

:::tip Closures
En utilisant les closures, créer une fonction compteur() qui permet d’ajouter ou de soustraire 1 de la façon suivante :

```javascript 
let compte = compteur();
compte.plus();
compte.moins(); 
```

:::
