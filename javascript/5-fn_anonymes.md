# Fonctions anonymes

::: info Demos sur Gitlab
Voir le fichier `d4-function-anonyme.js​​​​​​​` pour les exemples.

Les fichiers sont disponibles sur le dépôt GitLab :
https://gitlab.com/420-15d-fx/demos-js
:::

Les fonctions anonymes (également appelée fonction lambda ou fonction sans nom) sont, comme leur nom l'indique, des fonctions qui ne portent pas de nom.

Elles peuvent être assignées à une variable :

```javascript
let addAnonyme = function(a, b) {
  return a + b;
}
​
//Équivalent à : 
let addAnonyme = (a, b) => a + b;
```

Ou utilisées comme paramètres d’une fonction :​​​​​​​

```javascript
// Déclaration d'une fonction qui prend une fonction en paramètre
function faireQuelqueChose(callback) {
  console.log("Je fais quelque chose !");
  callback();
}
​
// Appel de la fonction avec une fonction anonyme comme callback
faireQuelqueChose(function() {
  console.log("Je suis une fonction anonyme utilisée comme callback !");
});
```

​​​​​​​Dans cet exemple, la fonction `faireQuelqueChose` prend une fonction en paramètre appelée `callback`. Nous passons une fonction anonyme en tant que callback. Cette fonction anonyme est exécutée lorsque la fonction `faireQuelqueChose` est terminée.


## Pratique #3

:::tip Fonction anonyme
- Écrire une fonction anonyme qui prend deux entiers en paramètre et renvoie
  - un nombre négatif si le premier entier est inférieur au deuxième
  - un nombre positif si le premier entier est supérieur au deuxième
  - zéro si les deux entiers sont égaux.
- Écrire une fonction `trierTableau` qui prend un tableau d'entier en paramètre et une fonction de comparaison.
- Utiliser la fonction `trierTableau` pour  trier le tableau [5, 8, 1, 3, 9, 2] en ordre croissant.
- Utiliser la fonction `trierTableau`, mais cette fois-ci pour trier le tableau [5, 8, 1, 3, 9, 2] en ordre décroissant.