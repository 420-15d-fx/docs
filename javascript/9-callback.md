# Fonction de rappel (Callback)

::: info Demos sur Gitlab
Voir le fichier `d8a-callback,js` et `d8b-callback,js` pour les exemples.

Les fichiers sont disponibles sur le dépôt GitLab :
https://gitlab.com/420-15d-fx/demos-js
:::

Une fonction de rappel (callback en anglais) est une fonction qui est passée comme argument à une autre fonction, et qui est exécutée par cette dernière à un moment ultérieur.

En d'autres termes, lorsqu'une fonction est appelée, elle peut prendre une autre fonction en tant qu'argument, et cette fonction sera exécutée plus tard, lorsque l'action initiale sera terminée. La fonction de rappel est généralement utilisée pour gérer des événements asynchrones tels que le chargement de données, la réponse à des requêtes HTTP ou la manipulation de fichiers.

Voici un exemple :

```javascript
function addition(a, b, callback) {
  const result = a + b;
  callback(result);
}

function afficherResultat(result) {
  console.log("Le résultat est : " + result);
}

addition(2, 3, afficherResultat); // affichera "Le résultat est : 5"
```