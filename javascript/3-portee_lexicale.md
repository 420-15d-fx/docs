# Portée lexicale

::: info Demos sur Gitlab
Voir le fichier `d2-portee-lexicale.js` pour les exemples.

Les fichiers sont disponibles sur le dépôt GitLab :
https://gitlab.com/420-15d-fx/demos-js
:::

En JavaScript, les déclarations de variables peuvent être faites à l'aide de trois mots clés différents: `var`, `let` et `const`.
Chacun de ces mots clés a une portée lexicale différente, ce qui signifie qu'il affecte la manière dont la variable est accessible à différentes parties de votre code.

## `var` - `let` - `const`

### `var`

La déclaration de variable `var` est une déclaration de portée globale ou de fonction. Cela signifie qu'une variable déclarée avec `var` est accessible depuis n'importe où dans la fonction ou le fichier JavaScript où elle a été déclarée.​​​​​​​

Voici un exemple :

```javascript
function foo() {
  var x = 10;
  console.log(x); // 10
}
​
foo();
console.log(x); // erreur - x n'est pas défini
```

### `let`

La déclaration de variable `let` est une déclaration de portée de bloc. Cela signifie qu'une variable déclarée avec `let` est accessible uniquement dans le bloc où elle a été déclarée. 

Un bloc est simplement une paire d'accolades qui entoure une partie du code, comme une boucle ou une instruction conditionnelle.

Voici un exemple :

```javascript
function foo() {
  if (true) {
    let x = 10;
    console.log(x); // 10
  }
  console.log(x); // erreur - x n'est pas défini
}
​
foo();
```


::: warning `let` vs `var`
“D'une certaine façon `let` fonctionne comme `var`, la seule différence dans cette analogie est que `let` fonctionne avec les **portées de bloc** et `var` avec les **portées des fonctions**”
:::

### `const`

La déclaration de variable `const` est également une déclaration de portée de bloc, mais elle ne permet pas de réassigner la variable. Cela signifie qu'une variable déclarée avec `const` ne peut être affectée qu'une seule fois, lors de sa déclaration.
voir le fichier `d2-portee-lexicale.js` pour les exemples.

## Variable `undefined` et `null`

![js](./img/variable.png){data-zoomable width=50%}

### `undefined`

Une variable déclarée mais non initialisée est `undefined`.

Une fonction sans instruction `return` renvoie undefined par défaut.

Lorsque vous accédez à une propriété qui n'existe pas dans un objet, la valeur renvoyée est `undefined`.

```javascript
let variableNonInitialisee;
console.log(variableNonInitialisee); // Affiche undefined

function fonctionSansReturn() {}
console.log(fonctionSansReturn()); // Affiche undefined

const objet = {};
console.log(objet.proprieteInexistante); // Affiche undefined
```

### `null`

`null` est une valeur intentionnelle que l'on peut affecter à une variable pour indiquer explicitement l'absence de valeur ou la valeur nulle.

C'est généralement utilisé lorsqu'on veut réinitialiser une variable qui contenait précédemment une valeur.

```javascript
let variableNulle = null;
console.log(variableNulle); // Affiche null
```


