# Concept de levage (Hoisting)

::: info Demos sur Gitlab
Voir le fichier `d5-hoisting.js​​​​​​​​​​​​​` pour les exemples.

Les fichiers sont disponibles sur le dépôt GitLab :
https://gitlab.com/420-15d-fx/demos-js
:::


Le "hoisting" est un comportement particulier de JavaScript qui permet à certaines déclarations de variables et de fonctions d'être "levées" ou "remontées" au sommet de leur portée respective avant l'exécution du code.

Lors de l’exécution d’un script, le moteur Javascript lit une première fois l’ensemble du code, c’est la phase de compilation. Ensuite il exécute les instructions. Pendant la phase de compilation, les déclarations de variables et de fonctions sont mises en mémoire.

C’est comme si la déclaration était "remontées" au début du code. En réalité les déclarations ne sont pas placée au début du code mais elles sont mises en mémoire avant l’exécution.

Exemple de l’interprétation du code par le moteur Javascript :

![Hoisting](./img/hoisting1.png)

C’est la même chose pour les fonctions.

Les deux codes ci-dessous fonctionnent correctement :

![Hoisting](./img/hoisting2.png)

Par contre, si la fonction est assignée à une variable, on aura une erreur

![Hoisting](./img/hoisting3.png)

En effet, voici comment Javascript interprète le code

![Hoisting](./img/hoisting4.png)


