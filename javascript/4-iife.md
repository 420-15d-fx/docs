# IIFE (Immediately Invoked Function Expression)

::: info Demos sur Gitlab
Voir le fichier `d3-iife.js` pour les exemples.

Les fichiers sont disponibles sur le dépôt GitLab :
https://gitlab.com/420-15d-fx/demos-js
:::

“IIFE (Immediately Invoked Function Expression) (Expression de fonction invoquée immédiatement) est une fonction JavaScript qui est exécutée dès qu'elle est définie.” 

Voici un exemple d'IIFE :

```javascript
(function () {
  // code
})();
```

On utilise souvent cette notation pour éviter des conflits de portée de variables, car toutes les variables et fonctions déclarée à l'intérieur d'une IIFE sont limitées à la portée de la fonction.

Une des utilisations courantes de l'IIFE est de créer un contexte de portée pour les variables et fonctions définies à l'intérieur de la fonction, évitant ainsi les conflits de noms avec d'autres parties du code. 

Par exemple :​​​​​​​​​​​​​​

```javascript
(function() {
  var a = 1;
  function b() {
    console.log(a);
  }
  b(); // Affiche 1
})();
​
// A ce stade, la variable 'a' et la fonction 'b' ne sont plus accessibles
```

​​​​​​​Il est possible de passer des paramètres en utilisant les dernières parenthèses. Dans cet exemple, on passe “Cégep” et “Garneau” en arguments.

```javascript
let result = (function (a, b) { 
  return a + " " + b; 
})("Cégep", "Garneau");
​
// Crée immédiatement la sortie: 
console.log(result); // "Cégep Garneau"
```


## Pratique #2
::: tip IIFE
- Définir une IIFE qui prend en paramètre deux nombres et renvoie leur somme.
- Vérifier que le résultat est correct.
:::

