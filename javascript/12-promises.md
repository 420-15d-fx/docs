# Les promesses (promise)

::: info Demos sur Gitlab
Voir les fichiers:
- `d11a-promise.js`
- `d11b-promise-dans-fonction`
- `d11c-promise-enchainée`
- `d11d-promise-fetch`

Les fichiers sont disponibles sur le dépôt GitLab :
https://gitlab.com/420-15d-fx/demos-js
:::

[Voir la documentation](https://developer.mozilla.org/fr/docs/Web/JavaScript/Reference/Global_Objects/Promise)

L'objet `Promise` (pour « promesse ») est utilisé pour réaliser des traitements de façon asynchrone. Une promesse représente une valeur qui peut être disponible maintenant, dans le futur voire jamais.

Lorsque vous exécutez une tâche asynchrone, comme l'envoi d'une requête AJAX ou l'accès à une base de données, il est possible que le résultat ne soit pas immédiatement disponible. Dans ce cas, au lieu de bloquer l'exécution du code jusqu'à ce que le résultat soit disponible, vous pouvez utiliser une Promise pour traiter le résultat de manière asynchrone.

Une Promise est créée en appelant la fonction `Promise`, qui prend une fonction de rappel en argument. Cette fonction de rappel prend deux arguments, `resolve` et `reject`, qui sont des fonctions qui doivent être appelées pour signaler si la promesse est résolue avec succès ou rejetée avec une raison d'échec.

Voici un exemple

```javascript
obtenirData()
  .then(function(data) {
    // Traitement des données récupérées ici
  })
  .catch(function(error) {
    // Traitement de l'erreur ici
  });
  ```
​
Une promesse peut également être chaînée pour exécuter plusieurs opérations asynchrones les unes après les autres.

Par exemple, supposons que nous avons une fonction `obtenirData` qui récupère des données, et une fonction `traiterData` qui traite ces données.

Nous pouvons les chaîner comme suit :

```javascript
obtenirData()
  .then(traiterData)
  .then(function(result) { // result est le résultat de traiterData
    // Traitement du résultat ici
  })
  .catch(function(error) {
    // Traitement de l'erreur ici
  });
```

Dans cet exemple, `obtenirData` récupère les données, `traiterData` les traite, puis le résultat est traité par une autre fonction de rappel passée à `then`.

Si l'une des Promises est rejetée, elle sera traitée par la fonction de rappel passée à `catch`.



## Fetch()

(Documentation)[https://developer.mozilla.org/fr/docs/Web/API/Fetch_API/Using_Fetch]

`fetch()` est une méthode JavaScript qui permet d'envoyer des requêtes réseau. Elle est basée sur les promesses, ce qui la rend idéale pour les opérations asynchrones.

Voici un exemple simple d'utilisation de `fetch()` pour récupérer des données à partir d'une URL :

```javascript
fetch('https://api.example.com/data')
  .then(response => response.json()) // response.json() renvoie une Promise qui contient les données JSON, elle est traitée par le prochain then()
  .then(data => console.log(data)) // data contient les données récupérées sous forme d'objet JSON
  .catch(error => console.error(error)); // En cas d'erreur, on affiche l'erreur dans la console
```

Dans cet exemple, `fetch()` envoie une requête à l'URL spécifiée, puis traite la réponse en tant qu'objet JSON. Les données sont ensuite affichées dans la console.

`fetch()` prend en charge plusieurs options pour personnaliser la requête, telles que la méthode HTTP, les en-têtes, le corps de la requête, etc.

Exemple :

```javascript
fetch('https://api.example.com/data', {
  method: 'POST',
  headers: {
    'Content-Type': 'application/json' // En-tête de la requête indiquant que le corps est au format JSON
  },
  body: JSON.stringify({ key: 'value' }) // Corps de la requête, converti en JSON
})
  .then(response => response.json()) // response contient la réponse de la requête, on la convertit en JSON
  .then(data => console.log(data)) // data contient les données récupérées sous forme d'objet JSON
  .catch(error => console.error(error)); // En cas d'erreur, on affiche l'erreur dans la console
```


## Async/Await

`async` et `await` sont des fonctionnalités introduites dans ES2017 (ES8) qui permettent d'écrire du code asynchrone de manière plus lisible et compréhensible par rapport aux promesses (`Promise.then().catch()`) ou aux callbacks.

Bien que les promesses améliorent la lisibilité, elles peuvent devenir compliquées lorsqu'il y a plusieurs opérations enchaînées.

L'utilisation de async/await simplifie ce type de code en lui donnant une apparence synchrone tout en restant asynchrone.

## Déclarer une fonction `async`

Une fonction définie avec le mot-clé `async` retourne toujours une promesse.

```javascript
async function maFonction() {
    return "Bonjour, Async!";
}

maFonction().then(console.log); // Affiche "Bonjour, Async!"
```
Même si nous retournons une simple chaîne, la fonction renvoie automatiquement une promesse.


## Utiliser `await` pour attendre une promesse

Le mot-clé `await` est utilisé uniquement à l'intérieur d'une fonction `async` pour attendre la résolution d'une promesse.

```javascript

async function fetchData() {
    try {
        let response = await fetch('https://jsonplaceholder.typicode.com/todos/1'); //Attend que la requête soit terminée.
        let data = await response.json();  //Attend que la réponse soit convertie en JSON.
        console.log(data);
    } catch (error) {
        console.error("Erreur :", error);
    }
}

fetchData();
```

## Exécuter plusieurs requêtes avec `Promise.all`

Si on doit exécuter plusieurs appels asynchrones en parallèle, on peut utiliser `Promise.all()` :

```javascript
async function fetchMultipleTodos() {
    try {
        let [todo1, todo2] = await Promise.all([
            fetch('https://jsonplaceholder.typicode.com/todos/1').then(res => res.json()),
            fetch('https://jsonplaceholder.typicode.com/todos/2').then(res => res.json())
        ]);
        
        console.log(todo1, todo2);
    } catch (error) {
        console.error("Erreur lors de la récupération :", error);
    }
}

fetchMultipleTodos();
```

Ici, **les deux requêtes sont exécutées en parallèle**, ce qui est plus efficace que d'attendre que l'une se termine avant de commencer l'autre.

## Limitations et Précautions du `await`

- `await` bloque l'exécution de la fonction async jusqu'à ce que la promesse soit résolue. Si on enchaîne plusieurs await dans une boucle, cela peut ralentir le programme.

- `await` ne peut pas être utilisé en dehors d'une fonction async. Pour l'utiliser dans un contexte global, il faut encapsuler dans une fonction :

```javascript
(async () => {
    let data = await fetch('https://jsonplaceholder.typicode.com/todos/1').then(res => res.json());
    console.log(data);
})();
```


## Pratique #7

:::tip Promesse

- Créez une fonction getCatFact() qui renvoie une Promise.
- À l'intérieur de cette fonction, utilisez fetch pour récupérer les données de l'API (https://catfact.ninja/fact).
- Utilisez json() pour convertir la réponse en un objet JSON.
- Une fois les données converties, Afficher la phrase contenu dans le résultat à la console.
:::