# Source & En savoir plus

- https://developer.mozilla.org/fr/docs/Glossary/Hoisting
- https://developer.mozilla.org/fr/docs/Web/JavaScript/Guide/Functions
- https://developer.mozilla.org/fr/docs/Web/JavaScript/Reference/Functions
- https://developer.mozilla.org/fr/docs/Web/JavaScript/Closures
- https://developer.mozilla.org/fr/docs/Web/JavaScript/Reference/Operators/Destructuring_assignment
- https://grafikart.fr/tutoriels/promise-async-await-875
- https://developer.mozilla.org/fr/docs/Web/JavaScript/Reference/Global_Objects/Promise https://javascript.info/ ​​​​​​​