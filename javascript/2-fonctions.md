# Fonctions et fonctions fléchées

::: info Demos sur Gitlab
Voir le fichier `d1-function.js` pour les exemples.

Les fichiers sont disponibles sur le dépôt GitLab :
https://gitlab.com/420-15d-fx/demos-js
:::

## Fonctions

![js](./img/function.png){data-zoomable width=50%}

Les fonctions en JavaScript peuvent être définies de plusieurs façons, mais la syntaxe de base est la suivante :

```javascript
function nomDeLaFonction(paramètre1, paramètre2, ...) {
  // bloc de code à exécuter
  return valeurDeRetour;
}
```

Par exemple, voici une fonction qui calcule la somme de deux nombres :

```javascript
function addition(nombre1, nombre2) {
  return nombre1 + nombre2;
}
```

On peut appeler cette fonction de la manière suivante :

```javascript
let resultat = addition(2, 3);
console.log(resultat); // Affiche 5
```

## Fonction fléchée

Les fonctions fléchées sont une autre façon de définir des fonctions en JavaScript.

Elles ont une syntaxe plus concise et peuvent rendre votre code plus lisible.

Voici un exemple de fonction fléchée qui calcule la somme de deux nombres :

```javascript
const addition = (nombre1, nombre2) => {
  return nombre1 + nombre2;
}
```

La fonction fléchée est définie avec la syntaxe suivante : 

```javascript
(paramètre1, paramètre2, ...) => { /* bloc de code à exécuter */ }.
```

Elle peut avoir un ou plusieurs paramètres, et le corps de la fonction est délimité par des accolades.

On peut appeler cette fonction de la même manière que la fonction précédente :

```javascript
let resultat = addition(2, 3);
console.log(resultat); // Affiche 5
```

La principale différence entre une fonction et une fonction fléchée en JavaScript est la façon dont elles gèrent le contexte. Dans une fonction classique, le mot-clé `this` est défini par l'objet qui appelle la fonction. Dans une fonction fléchée, le contexte est hérité du contexte parent, ce qui peut être utile dans certaines situations.

Par exemple, voici une fonction classique qui définit un objet personne avec une méthode `saluer`&nbsp;:

```javascript
const personne = {
  nom: "Aminata",
  saluer: function() {
    console.log("Bonjour, je m'appelle ${this.nom}.");
  }
}
​
personne.saluer(); // Affiche "Bonjour, je m'appelle Aminata."
```

Maintenant, voici la même fonction définie comme une fonction fléchée :

```javascript
const personne = {
  nom: "Aminata",
  saluer: () => {
    console.log("Bonjour, je m'appelle ${this.nom}.");
  }
}
​
personne.saluer(); // Affiche "Bonjour, je m'appelle undefined."
```

Dans ce cas, la fonction fléchée ne définit pas correctement le contexte, et le résultat est `undefined`.

Pour cette raison, il est souvent préférable d'utiliser des fonctions classiques si vous devez accéder à l'objet `this` dans votre fonction.

## Fonction fléchée raccourcie 

Il est possible de raccourcir une fonction fléchée lorsque celle-ci ne contient qu'un seul paramètre ou aucun paramètre. Soit la fonction suivant permettant calculé la valeur au carré d'un nombre :

```javascript
function carre(x) {
  return x * x;
}
```

Celle-ci peut être raccourcie de la manière suivante :

```javascript
const carre = x => x * x;
​
console.log(carre(2)) //Affiche 4
```

De même, la fonction sans paramètre suivante :

```javascript
function dateActuelle() {
  return new Date();
}
```

Pourrait être raccourcie de la manière suivante :

```javascript
const dateActuelle = () => new Date();
​
console.log(dateActuelle()); //Affiche la date du jour
```

:::tip _ (underscore)


L'utilisation de l'underscore `_` est une convention dans JavaScript et d'autres langages pour indiquer qu'un paramètre est ignoré ou non pertinent dans le contexte. C'est une convention pour dire "ce paramètre est là, mais je ne m'en soucie pas".

Par exemple, la fonction `forEach` de JavaScript prend une fonction de rappel qui prend trois paramètres : l'élément, l'index et le tableau. Si vous souhaitez seulement avoir l'index qui est en deuxième position, vous pouvez utiliser l'underscore pour ignorer les autres paramètres :

```javascript
// Un tableau avec des valeurs
const nombres = [10, 20, 30];

// On utilise une fonction de rappel où l'élément est ignoré avec "_"
nombres.forEach((_, index) => {
  console.log(`Valeur à l'index ${index}`);
});

// Affiche :
// Valeur à l'index 0
// Valeur à l'index 1
// Valeur à l'index 2

:::


## Pratique #1
:::tip Fonction
- Écrivez une fonction classique qui prend un tableau de nombres en paramètre et retourne la somme de tous les nombres.
- Transformez cette fonction en une fonction fléchée en utilisant une boucle `for...of`.
- Testez votre fonction en appelant `somme([1, 2, 3, 4])`. Vous devriez obtenir `10`.
- Écrivez une autre fonction classique qui prend un tableau de chaînes de caractères en paramètre et retourne le nombre de caractères total de toutes les chaînes de caractères.
- Transformez cette fonction en une fonction fléchée en utilisant une boucle `for...of`.
- Testez votre fonction en appelant `longueur(["hello", "world"])`. Vous devriez obtenir `10`.
:::


## Fonctions natives

Voici une liste des fonctions natives les plus utilisées en Javascript :

https://waytolearnx.com/2019/09/liste-des-fonctions-javascript.html