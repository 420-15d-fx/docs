# Rest et Spread opérateurs

::: info Demos sur Gitlab
Voir le fichier `d9-spread-rest-operateurs.js` pour les exemples.

Les fichiers sont disponibles sur le dépôt GitLab :
https://gitlab.com/420-15d-fx/demos-js
:::

## Opérateur Spread
L'opérateur Spread est représenté par trois points `...`.

Il peut être utilisé pour décomposer un tableau, un objet ou une chaîne de caractères en éléments individuels.

Par exemple, supposons que nous avons un tableau d'éléments que nous voulons ajouter à un autre tableau. Nous pouvons utiliser l'opérateur `spread` pour décomposer le premier tableau en éléments individuels et les ajouter au deuxième tableau de la manière suivante :

```javascript
const tableau1 = [1, 2, 3];
const tableau2 = [4, 5, ...tableau1];
console.log(tableau2); // [4, 5, 1, 2, 3]
```

## Le paramètre Rest

Le paramètre Rest permet de rassembler les arguments d'une fonction en un seul tableau. Il est représenté par trois points `...`.

Par exemple, supposons que nous voulions écrire une fonction qui prend un nombre variable d'arguments et renvoie la somme des carrés de ces arguments. Voici à quoi cela pourrait ressembler :

```javascript
function sommeDesCarres(...nombres) {
  let somme = 0;
  for (const nombre of nombres) {
    somme += nombre * nombre;
  }
  return somme;
}
​
console.log(sommeDesCarres(1, 2, 3, 4)); // 30
```

## Pratique #5

:::tip Rest et Spread
- Écrire une fonction `trierTableaux` qui prend en paramètre 2 tableaux d'entiers et qui retourne un seul tableau trié en ordre croissant. La fonction doit utiliser l'opérateur Spread.
- Tester la fonction avec les tableaux suivants : [4, 2, 1] et [3, 5, 6].
- Écrire une fonction qui prend en entrée un nombre variable de chaînes de caractères, et qui renvoie la chaîne de caractères la plus longue parmi celles-ci.
- Tester la fonction en lui passant les paramètres suivants : 'chien', 'chat', 'oiseau', 'souris'.
:::