# Pile (Stack) vs Tas (Heap)

En JavaScript, les termes "heap" et "stack" se réfèrent à deux zones de mémoire différentes utilisées pour stocker des données.

Le "stack" (pile) est une zone de mémoire réservée pour le stockage temporaire des variables créées dans une fonction. Lorsqu'une fonction est appelée, une nouvelle zone de mémoire est créée pour stocker toutes les variables locales de cette fonction. 

Lorsque la fonction se termine, la zone de mémoire est libérée. Les variables créées dans une fonction sont généralement stockées dans la pile.

Le "heap" (tas) est une zone de mémoire réservée pour le stockage des objets créés dynamiquement, tels que les objets créés avec l'opérateur "new". Les objets créés dans le tas peuvent être référencés par plusieurs parties du code et doivent donc être gérés avec soin pour éviter les fuites de mémoire.

---

![Stack vs Heap](./img/stack1.png){data-zoomable width=50%}

---
Type primitif vs Type référence

![Stack vs Heap](./img/stack2.png){data-zoomable width=50%}

---
Javascript Runtime

![Stack vs Heap](./img/stack3.png){data-zoomable width=50%}

Liens intéressants:

- Reference vs Primitive Values/ Types => https://www.youtube.com/watch?v=9ooYYRLdg_g
- Article => https://academind.com/tutorials/reference-vs-primitive-values
- What is the difference between the stack and the heap? => https://www.quora.com/What-is-the-difference-between-the-stack-and-the-heap


## Moteur Javascript + API

Exemple pour l’exécution dans un navigateur.

Le « core » de Javascript fonctionne sur un seul fil d’exécution (single thread).

Le navigateur ou l’environnement d’exécution (comme Node.js) implémente des Web API qui peuvent être utilisées par JavaScript pour interagir avec des fonctionnalités externes (comme le DOM, les requêtes XHR, les timers (setTimeout, setInterval), la géolocalisation, etc.). Certaines de ces API, comme les requêtes réseau ou les timers, sont asynchrones et ne bloquent pas le fil d'exécution principal.

::: tip Web API
JavaScript ne définit que les structures de base comme les objets, les boucles, et les fonctions standards. Mais pour interagir avec l'environnement dans lequel il s'exécute (comme un navigateur), JavaScript s'appuie sur des Web API fournies par le moteur du navigateur.
Le navigateur fournit des fonctionnalités comme setTimeout, le DOM, les Web Workers, les Fetch API, etc. pour que JavaScript puisse interagir avec des aspects du Web ou de l'environnement d'exécution.

Exemples de Web API :
- DOM (Document Object Model)
- Fetch API (pour effectuer des requêtes réseau)
- Web Workers (pour exécuter des scripts en arrière-plan)
- Geolocation API (pour obtenir la position géographique de l'utilisateur)
- Canvas API (pour dessiner des graphiques et des animations)
- Web Storage (pour stocker des données localement dans le navigateur)
- Web Audio API (pour manipuler des données audio)
- WebRTC (pour la communication en temps réel)
- Service Workers (pour les applications web progressives)
- Notifications API (pour afficher des notifications)

:::

![Javascript Runtime](./img/runtime.png){data-zoomable width=50%}

Voir le fichier `d12-bloquant_non-bloquant.js` pour un exemple de code bloquant et non bloquant.

## Pratique #8

::: tip Moteur Javascript + API
Voir le fonctionnement à la « loupe » :
- [Exemple 1 Loupe](http://latentflip.com/loupe/?code=JC5vbignYnV0dG9uJywgJ2NsaWNrJywgZnVuY3Rpb24gb25DbGljaygpIHsKICAgIHNldFRpbWVvdXQoZnVuY3Rpb24gdGltZXIoKSB7CiAgICAgICAgY29uc29sZS5sb2coJ1lvdSBjbGlja2VkIHRoZSBidXR0b24hJyk7ICAgIAogICAgfSwgMjAwMCk7Cn0pOwoKY29uc29sZS5sb2coIkhpISIpOwoKc2V0VGltZW91dChmdW5jdGlvbiB0aW1lb3V0KCkgewogICAgY29uc29sZS5sb2coIkNsaWNrIHRoZSBidXR0b24hIik7Cn0sIDUwMDApOwoKY29uc29sZS5sb2coIldlbGNvbWUgdG8gbG91cGUuIik7!!!PGJ1dHRvbj5DbGljayBtZSE8L2J1dHRvbj4%3D)
- [Exemple 2 Loupe](http://latentflip.com/loupe/?code=ZnVuY3Rpb24gcHJpbnRIZWxsbygpIHsNCiAgICBjb25zb2xlLmxvZygnSGVsbG8gZnJvbSBiYXonKTsNCn0NCg0KZnVuY3Rpb24gYmF6KCkgew0KICAgIHNldFRpbWVvdXQocHJpbnRIZWxsbywgMzAwMCk7DQp9DQoNCmZ1bmN0aW9uIGJhcigpIHsNCiAgICBiYXooKTsNCiAgICBsZXQgYSA9IDE7DQogICAgY29uc29sZS5sb2coYSk7DQp9DQoNCmZ1bmN0aW9uIGZvbygpIHsNCiAgICBiYXIoKTsNCiAgICBsZXQgYiA9IDI7DQogICAgY29uc29sZS5sb2coYik7DQp9DQoNCmZvbygpOw%3D%3D!!!PGJ1dHRvbj5DbGljayBtZSE8L2J1dHRvbj4%3D)
- [Exemple 3 Loupe](http://latentflip.com/loupe/?code=DQoNCmZvciAobGV0IGkgPSAwOyBpIDwgNTsgaSsrKXsNCiAgICBzZXRUaW1lb3V0KGZ1bmN0aW9uIHRpbWVyKCkgew0KICAgICAgICBjb25zb2xlLmxvZygnaSA9ICcsIGkpOyAgICANCiAgICB9LCAyMDAwKTsgIA0KfQ0KDQo%3D!!!PGJ1dHRvbj5DbGljayBtZSE8L2J1dHRvbj4%3D)
- [Exemple 4 Loupe​​​​​​​​​​​​​​](http://latentflip.com/loupe/?code=Ly8gY29kZSBibG9xdWFudAovLyBMZSBwcm9ncmFtbWUgbmUgcGV1dCByaWVuIGZhaXJlIGQnYXV0cmUgCi8vIHRhbnQgcXVlIGNldHRlIGJvdWNsZSBmb3JFYWNoIG4nYSBwYXMgdGVybWlu6S4KWzEsIDIsIDMsIDRdLmZvckVhY2goZnVuY3Rpb24gKGkpIHsKICBjb25zb2xlLmxvZygnaScsIGkpCn0pCgoKLy8gY29kZSBub24gYmxvcXVhbnQKZnVuY3Rpb24gYXN5bmNGb3JFYWNoKGFycmF5LCBjYikgewogIGFycmF5LmZvckVhY2goZnVuY3Rpb24gKGopIHsKICAgIHNldFRpbWVvdXQoZnVuY3Rpb24gKCkgewogICAgICBjYihqKTsKICAgIH0sIDEwMDApOwogIH0pOwp9Cgphc3luY0ZvckVhY2goWzEsIDIsIDMsIDRdLCBmdW5jdGlvbiAoaikgewogIGogKj0gajsKICBjb25zb2xlLmxvZygnaicsIGopCn0pCiAKIAo%3D!!!PGJ1dHRvbj5DbGljayBtZSE8L2J1dHRvbj4%3D)
:::