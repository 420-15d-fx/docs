# this

::: info Demos sur Gitlab
Voir le fichier `d6-this​​​​​​​​​​​​​` pour les exemples.

Les fichiers sont disponibles sur le dépôt GitLab :
https://gitlab.com/420-15d-fx/demos-js
:::

le mot-clé `this` se réfère à l'objet actuel qui est en cours d'exécution. 

Cependant, la valeur de `this` peut varier en fonction du contexte dans lequel il est utilisé:
- Utilisation de `this` seul : Lorsque `this` est utilisé seul, la valeur de `this` dépend du contexte global dans lequel le code est exécuté. En général, dans le navigateur, `this` fait référence à l'objet `window`, qui représente la fenêtre du navigateur.

```javascript
console.log(this); // affiche l'objet "window" dans le navigateur
```

Exemple: on peut utiliser `this` pour accéder à des propriétés de l'objet global `window`:

```javascript
this.location // accède à la propriété "location" de l'objet "window"
this.document // accède à la propriété "document" de l'objet "window"
this.innerWidth // accède à la propriété "innerWidth" de l'objet "window"
this.innerHeight // accède à la propriété "innerHeight" de l'objet "window"
this.alert("Hello, world!") // affiche une boîte de dialogue avec le message "Hello, world!"
```

- Utilisation de `this` dans une méthode : Lorsque `this` est utilisé dans une méthode d'un objet, la valeur de `this` fait référence à l'objet qui appelle cette méthode.

```javascript
const obj = {
  nom: "Alice",
  saluer() {
    console.log(`Bonjour, je m'appelle ${this.nom}`);
  }
};
​
obj.saluer(); // affiche "Bonjour, je m'appelle Alice"
```

- Utilisation de `this` dans une fonction : Lorsque `this` est utilisé dans une fonction régulière, la valeur de `this` dépend de la façon dont la fonction est appelée. Si la fonction est appelée en tant que fonction globale, la valeur de `this` sera l'objet "window". Si la fonction est appelée en tant que méthode d'un objet, la valeur de `this` sera l'objet qui appelle la méthode. Dans une fonction flèchée, `this` fait référence à l'objet parent.
  
```javascript
function maFonction() {
  console.log(this);
}
​
maFonction(); // affiche l'objet "window" dans le navigateur
​
const obj = {
  nom: "Alice",
  saluer: function() {
    console.log(`Bonjour, je m'appelle ${this.nom}`);
  }
};
​
obj.saluer(); // affiche "Bonjour, je m'appelle Alice"
​
​
const obj2 = {
  nom: "Alice",
  saluer: ()=> {
    console.log(`Bonjour, je m'appelle ${this.nom}`);
    // affiche "Bonjour, je m'appelle undefined", car this est l'objet "window" dans le navigateur
  }
};
​
​
obj2.saluer(); // affiche l'objet "window" dans le navigateur
```
​
- Utilisation de `this` dans une fonction en[ mode strict](https://developer.mozilla.org/fr/docs/Web/JavaScript/Reference/Strict_mode) : En mode strict, la valeur de `this` dans une fonction régulière sera `undefined` si la fonction est appelée en tant que fonction globale, et fera référence à l'objet qui appelle la méthode si elle est appelée en tant que méthode d'un objet.

```javascript
'use strict';
​
function maFonction() {
  console.log(this);
}
​
maFonction(); // affiche "undefined" en mode strict
​
const obj = {
  nom: "Alice",
  saluer: function() {
    console.log(`Bonjour, je m'appelle ${this.nom}`);
  }
};
​
obj.saluer(); // affiche "Bonjour, je m'appelle Alice"
```


- Utilisation de `this` dans un événement : Lorsque `this` est utilisé dans un gestionnaire d'événement, la valeur de `this` fera référence à l'élément DOM qui a déclenché l'événement.

```html
<button id="myButton">Cliquez ici</button>
​
<script>
const button = document.getElementById("myButton");
​
button.addEventListener("click", function() {
  console.log(this); // affiche l'élément "button"
});
</script>
```
