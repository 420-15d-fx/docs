# Formulaire

[Documentation](https://fr.vuejs.org/guide/essentials/forms.html)

En Vue.js, les formulaires fonctionnent de manière similaire à ceux en HTML traditionnel, mais avec quelques différences.

Pour commencer, les valeurs des éléments de formulaire sont liées à l'état du composant. Cela signifie que lorsqu'un utilisateur entre des données dans un champ de formulaire, la valeur est automatiquement mise à jour dans l'état du composant.

En plus de lier les éléments de formulaire à l'état du composant, Vue.js offre également des fonctionnalités pour valider les données de formulaire avant de les soumettre. On peut utiliser des directives telles que `v-if` et `v-show` pour afficher des messages d'erreur ou de confirmation en fonction de l'état de validation du formulaire.

Pour gérer la soumission d'un formulaire, on peut utiliser la directive `v-on:submit` pour écouter l'événement de soumission du formulaire. On peut également ajouter le modificateur `.prevent` afin de ne pas recharger la page quand on soumet le formulaire.

On utilise la directive `v-model` pour lier les éléments de formulaire à l'état du composant (`data`). On peut utiliser `v-model` avec les éléments de formulaire suivants :

- `<input>`, `<select>`, `<textarea>`, `<checkbox>`, `<radio>`, ...

## Exemple

```js
<template>
  <form v-on:submit.prevent="submitForm">
    <label for="name">Nom: </label>
    <input name="name" type="text" v-model="name" />
    <button type="submit">Soumettre</button>
  </form>
</template>

data() {
  return {
    name: "",
  }
},
  methods: {
    submitForm() {
      console.log('Form submitted!')
    },
  },
```


## v-model : modificateurs

On peut utiliser les modificateurs `.trim` et `.number` avec `v-model` pour modifier le comportement de la liaison de données.

- `.number` : convertit la valeur de l'élément de formulaire en nombre avant de la stocker dans l'état du composant

```html
<input v-model.number="age" type="number">
```
- `.trim` : supprime les espaces blancs de début et de fin de la valeur de l'élément de formulaire avant de la stocker dans l'état du composant

```html
<input v-model.trim="nom" type="text">
```

Voir `demo10-form` pour un exemple.

## Validation des données

Voici un exemple de validation d'un formulaire avant son envoi avec Vue.js :

```js
<template>
  <form @submit.prevent="submitForm">
    <div>
      <label for="name">Nom :</label>
      <input type="text" id="name" v-model="name">
      <span v-if="nameError" class="error">{{ nameError }}</span>
    </div>
    <div>
      <label for="email">Email :</label>
      <input type="email" id="email" v-model="email">
      <span v-if="emailError" class="error">{{ emailError }}</span>
    </div>
    <div>
      <button type="submit">Envoyer</button>
    </div>
  </form>
</template>
​
<script>
export default {
  data() {
    return {
      name: "",
      email: "",
      nameError: "",
      emailError: "",
    };
  },
  methods: {
    validateForm() {
      let isValid = true;
      this.nameError = "";
      this.emailError = "";
      
      if (!this.name) {
        this.nameError = "Veuillez entrer votre nom";
        isValid = false;
      } 
      
      if (!this.email) {
        this.emailError = "Veuillez entrer votre adresse courriel";
        isValid = false;
      } 
      else if (!this.isValidEmail(this.email)) {
        this.emailError = "Veuillez entrer une adresse courriel valide";
        isValid = false;
      } 
      
      return isValid;
    },
    submitForm() {
      if (this.validateForm()) {
        // Envoyer le formulaire
        console.log("Formulaire envoyé");
      }
    },
    isValidEmail(email) {
      // Vérifier si l'adresse e-mail est valide
      //  Utiliser une expression régulière pour vérifier si l'adresse e-mail est valide
      // Cette fonction est juste un exemple simplifié
      return email.includes("@");
    },
  },
};
</script>
​
<style>
.error {
  color: red;
}
</style>

```

## Bibliothèques permettant de faire de la validation
- [vuelidate](https://github.com/vuelidate/vuelidate)
- [VeeValidate](https://vee-validate.logaretm.com/v4/)



