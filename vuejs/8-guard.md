# Gardes de navigation (Guards)

Les gardes de navigation sont des fonctions qui permettent de contrôler la navigation entre les routes. Elles sont utiles pour empêcher l'accès à certaines routes si l'utilisateur n'est pas authentifié, par exemple.

## « Guard » pour toutes les routes

`beforeEach` est un « guard » qui s'exécute avant chaque navigation. Il est utile pour vérifier si l'utilisateur est authentifié avant de lui permettre d'accéder à une route.

`afterEach` est un « guard » qui s'exécute après chaque navigation. Il est utile pour effectuer des actions après que l'utilisateur ait accédé à une route.

Ces deux « guards » sont définis sur l'objet `router` de Vue.js.

Elles prennent toutes deux une fonction en paramètre qui reçoit les paramètres suivants :

- `to` : l'objet `Route` de la route vers laquelle l'utilisateur navigue
- `from` : l'objet `Route` de la route depuis laquelle l'utilisateur navigue
- `next` : une fonction qui permet de continuer la navigation vers la route demandée. Si next n'est pas appelé ou est appelé avec un argument `false`, **la navigation sera annulée**.


```js
const router = new VueRouter({
  routes: [
    // ...
  ]
})

router.beforeEach((to, from, next) => {
  if (to.name !== 'login' && !isAuthenticated) {
    next({ name: 'login' })
  } else {
    next()
  }

});

router.afterEach((to, from) => {
    // quelque chose à faire après chaque navigation
});
```

## « Guard » pour une route spécifique

On peut définir un « guard » pour une route spécifique en utilisant la propriété `beforeEnter` de l'objet `Route` de la route.

```js
const router = new VueRouter({
  routes: [
    {
      path: '/profile',
      name: 'profile',
      component: Profile,
      beforeEnter: (to, from, next) => {
        if (to.name !== 'login' && !isAuthenticated) {
          next({ name: 'login' })
        } else {
          next()
        }
      }
    }
  ]
})
```

## Component Guards

On peut définir des « guards » dans un **composant**  en utilisant les propriétés `beforeRouteEnter`, `beforeRouteUpdate` et `beforeRouteLeave` du composant.

- `beforeRouteEnter` : appelé avant que le composant soit rendu. Ne peut pas accéder à `this`.
- `beforeRouteUpdate` : appelé lorsque le composant est déjà rendu et que la route change. Accès à `this`.
- `beforeRouteLeave` : appelé lorsque le composant est déjà rendu et que l'on quitte la route. Accès à `this`.

Les trois méthodes reçoivent les paramètres suivants :

- `to` : la route vers laquelle l'utilisateur navigue
- `from` : la route à partir de laquelle l'utilisateur navigue.
- `next` : une fonction qui permet de continuer la navigation vers la route demandée. Si next n'est pas appelé ou est appelé avec un argument `false`, **la navigation sera annulée**.

Les propriétés disponibles pour `to` et `from` sont les suivantes :
- `path`: le chemin de la route de destination en tant que chaîne de caractères
- `name`: le nom de la route de destination en tant que chaîne de caractères
- `params`: un objet contenant les paramètres de la route de destination
- `query`: un objet contenant les paramètres de requête de la route de destination
- `hash`: la partie du fragment d'URL de la route de destination

```js
const Profile = {
  template: `...`,
  beforeRouteEnter (to, from, next) {
    // appelé avant que le composant soit rendu
    // ne peut pas accéder à `this`
    // exemple : vérifier si l'utilisateur est authentifié
  },
  beforeRouteUpdate (to, from, next) {
    // appelé lorsque le composant est déjà rendu
    // et que la route change
    // accès à `this`
    // exemple : réinitialiser les données du composant
  },
  beforeRouteLeave (to, from, next) {
    // appelé lorsque le composant est déjà rendu
    // et que l'on quitte la route
    // accès à `this`
    // Utile quand l’utilisateur veut quitter une page et
    // qu’il n’a pas terminé de remplir un formulaire,
    // on peut l’avertir…
  }
}
```

## Meta
La propriété `meta` de l'objet `router` permet de stocker des métadonnées supplémentaires sur les routes. 

Les métadonnées sont des informations qui ne sont pas directement liées à la navigation ou à l'affichage de la route, mais qui peuvent être utiles pour d'autres fonctionnalités de l'application.

Par exemple, on peut utiliser la propriété `meta` pour stocker des informations sur l'accès à une route, comme les autorisations requises pour y accéder. On peut également utiliser meta pour stocker des informations sur la page, comme le titre de la page, ou des informations de suivi de l'analyse.

```js
const router = createRouter({
  routes: [
    {
      path: '/dashboard',
      component: Dashboard,
      meta: {
        requiresAuth: true
      }
    },
    {
      path: '/login',
      component: Login
    }
  ]
});
​
router.beforeEach((to, from, next) => {
  // Vérifier si la route requiert une authentification
  if (to.meta.requiresAuth) {
    // Vérifier si l'utilisateur est authentifié
    if (auth.isAuthenticated()) {
      // Continuer vers la route souhaitée
      next();
    } else {
      // Rediriger vers la page de connexion
      next('/login');
    }
  } else {
    // Si la route ne requiert pas d'authentification, continuer vers la route souhaitée
    next();
  }
});
```

