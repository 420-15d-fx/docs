# Requête http

Pour faire des requêtes http, on peut utiliser `fetch` qui est disponible dans les navigateurs récents.

`fetch()` retourne une [promesse](https://developer.mozilla.org/fr/docs/Web/JavaScript/Reference/Global_Objects/Promise) !


## GET : récupérer des données

```js
fetch('https://jsonplaceholder.typicode.com/users')
  .then(response => response.json())
  .then(data => {
    console.log(data)
  })
  .catch(err => {
    console.log(err)
  })
```

`data` contient les données json retournées par `response.json()`

Avec l'objet `response`, on peut vérifier si `response.ok` est `true`, ou `response.status = 200`


## POST : envoyer des données

Pour une requête POST, on doit spécifier 
- la méthode `POST`
- le type de contenu avec l'entête `Content-type: application/json; charset=UTF-8`
- un `body` avec les données à envoyer au format json. Utilisez `JSON.stringify()` si vous avez un objet javascript

```js
fetch('https://jsonplaceholder.typicode.com/posts', {
  method: 'POST',
  body: JSON.stringify({
    title: 'foo',
    body: 'bar',
    userId: 1
  }),
  headers: {
    'Content-type': 'application/json; charset=UTF-8',
  },
})
  .then((response) => response.json())
  .then((json) => console.log(json));
```
## PATCH : modifier des données

```js
fetch('https://jsonplaceholder.typicode.com/posts/1', {
  method: 'PATCH',
  body: JSON.stringify({
    title: 'foo',
  }),
  headers: {
    'Content-type': 'application/json; charset=UTF-8',
  },
})
  .then((response) => response.json())
  .then((json) => console.log(json));
```

## DELETE : supprimer des données

Pour une requête DELETE, on doit spécifier
- la méthode `DELETE`

```js
fetch('https://jsonplaceholder.typicode.com/posts/1', {
  method: 'DELETE',
})
  .then((response) => response.json())
  .then((json) => console.log(json));
```

## Alternatives à fetch

- [Axios](https://axios-http.com/docs/intro)
- [Alova](https://alova.js.org/)

## Démo

Voir la démo `demo11-http`