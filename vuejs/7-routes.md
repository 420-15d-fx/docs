# Les routes

Les applications Vue.js sont généralement des applications à page unique (SPA Single Page Application).

::: info ⏰
Cela ne signifie pas que l’application comporte une seule page !

Cela signifie que le navigateur charge seulement un fichier HTML, un fichier Javascript et un fichier pour le CSS.
:::


- L’application est gérée entièrement coté navigateur. 
- Les requêtes au serveur permettent d’obtenir des données au format JSON afin d’alimenter les vues.
- Cependant l’application peut avoir plusieurs pages (un peu comme pour une application mobile).

Le routage dans Vue.js consiste à définir les différentes URL qui seront accessibles dans l'application, et à associer chacune de ces URL à un composant Vue spécifique. Lorsque l'utilisateur accède à une URL particulière, le composant associé sera chargé et affiché dans la zone de contenu prévue à cet effet.


- Pour gérer les pages et les routes, nous utilisons la bibliothèque vue-router.

[Documentation](https://router.vuejs.org/)

## Installation

https://router.vuejs.org/installation.html#installation

### Dans un projet existant :

```bash
npm install vue-router@4
```

Puis configurer le fichier `main.js`

```js{3,7}
import { createApp } from 'vue'
import App from './App.vue'
import router from './router' // <== importer le routeur

const app = createApp(App)

app.use(router) // <== utiliser le routeur
app.mount('#app')
```

### Avec l’installateur de Vue 


- Répondre `Yes` pour « Add Vue Router for Single Page Application development? »
- Configure automatiquement les fichiers du projet et crée automatiquement le fichier de configuration des routes dans `src/router/index.js`

![npm init vue](./img/route1.png)


## Création de vues

Une vue (view) est un élément d'interface utilisateur qui représente une partie de l'application. Une vue peut être un composant, une page, un widget ou même une section spécifique d'une page.

Les vues dans Vue.js sont généralement définies à l'aide de templates HTML, qui peuvent être rendus dynamiquement en fonction de l'état de l'application et des données fournies par le composant parent. Les vues sont placées dans le dossier `src/views` de l'application.

Ici, l'application contient deux vues qui seront chargées suivant la route demandée.

![vues](./img/route2.png)

## Configurer les routes
La configuration des routes se fait dans le fichier `router/index.js`

import permet l'imporation des composants à utiliser
createWebHistory est utilisé pour créer un objet d'historique de navigation qui permet à l'application de suivre l'historique de navigation de l'utilisateur et de gérer la navigation de l'application.
routes définit le composant à charger selon le chemin

```js
import { createRouter, createWebHistory } from 'vue-router'
import HomeView from '../views/HomeView.vue' // <== importer le composant

const router = createRouter({
  history: createWebHistory(), // <== créer l'historique de navigation
  routes: [ // <== définir les routes
    {
      path: '/',
      name: 'home',
      component: HomeView // <== définir le composant à charger
      // pour la route '/' 
    },
    {
      path: '/about',
      name: 'about',
      component: () => import('../views/AboutView.vue') // <== le code de ce composant 
      // ne sera chargé qu'au moment où la route sera visitée pour la première fois,
      // optimisant ainsi les performances en évitant de charger toutes les composants en une seule fois.
    }
  ]
})

export default router
```

## router-view

Pour afficher les composants selon la route demandée, il faut ajouter le composant `router-view` dans le composant `App.vue`

```js
<template>
    ...
    <RouterView /> <!-- <== afficher le composant selon la route demandée -->
    ...
</template>


```

Les composants `HomeView` et `AboutView` seront affichés dans la zone de contenu prévue à cet effet, à la place du composant `RouterView`.

## Créer des liens

Pour créer des liens vers les différentes routes, on utilise la balise `<RouterLink>`.

L'attribut `to` remplace `href`.

```js
<template>
  <nav>
    <RouterLink to="/">Home</RouterLink>
    <RouterLink to="/about">About</RouterLink>
  </nav>
</template>

```

Voici le rendu HTML

![router-link](./img/route3.png){data-zoomable width=80%}

Les classes `router-link-exact-active` et `router-link-active` sont automatiquement ajoutées sur le lien actif.

## Nom d'une route

Lors de la création d'un lien, il est possible d'utiliser le nom d'une route au lieu de son url. C'est d'ailleurs ce qui est recommandé de faire, car cela évite de devoir modifier le code si jamais l'url de la route est modifiée. 

```js
{
  path: '/about',
  name: 'About',
  component: About
},
```

Créer un lien avec une route nommée :

```js
<RouterLink :to="{ name: 'About' }">À propos</RouterLink>
```

## Passer des données dans les routes

- Dans la définition de la route, on peut ajouter des segments de chemin dynamiques précédés d'un double point `:`.

```js
{
  path: '/user/:id', // <== :id est un paramètre dynamique
  name: 'User',
  component: User
}
```

- Ajouter une ou plusieurs données dans le lien.

```js
<router-link :to="'/articles/' + article.id ">…
```
`to` est maintenant une expression JavaScript qui sera évaluée.

- Dans le composant, on peut récupérer la valeur du paramètre avec `this.$route.params`

```js
created() {
  const id = this.$route.params.id
}
```

Voici d'autres propriétés utiles de l'objet router
- `$route.path`: une chaîne de caractères qui représente l'URL de la route actuelle.
- `$route.query`: un objet contenant les paramètres de requête de l'URL de la route actuelle (par exemple, ?page=1&sort=name).
- `$route.hash`: une chaîne de caractères qui représente la partie de hachage de l'URL de la route actuelle (par exemple, #section1).
- `$route.fullPath`: une chaîne de caractères qui représente l'URL complète de la route actuelle, y compris les paramètres de requête et la partie de hachage.
- `$route.name`: le nom de la route actuelle (si elle a été nommée).
- `$route.meta`: un objet contenant les métadonnées associées à la route actuelle (par exemple, les autorisations requises).

## Utiliser des paramètres de requêtes

- Dans la définition de la route, on peut ajouter des paramètres de requêtes précédés d'un point d'interrogation `?`.

```js
<router-link :to="{ name: 'Articles', query: { userId: article.userId }}">
```

- Dans le composant, on peut récupérer la valeur du paramètre avec `this.$route.query`

```js
created() {
  const userId = this.$route.query.userId
}
```

Dans la `demo12-route`, cela permet de filtrer les articles écrit par un utilisateur en particulier en cliquant sur #1 par exemple.

Par défaut, dans ce cas précis, ça ne va pas fonctionner car la route demandées (/articles?userId=1) est la même que la route sur laquelle nous sommes déjà (/articles) et Vue ne va pas refaire le rendu de cette page.

![route4](./img/route4.png)

Pour contourner ce problème, il faut mettre en place un watcher sur $route.

Ce watcher permettra de « surveiller » la route et quand elle change, on va forcer une mise à jour des données de la page.

Voir le fichier Articles.vue de la `demo12-route`.

```js
watch: {
  $route() {
    const userId = this.$route.query.userId
    this.getArticles(userId)
  }
},
```

## Navigation programmée

[Documentation](https://router.vuejs.org/guide/essentials/navigation.html)

Il est possible d'utiliser le router dans le code javascript pour naviguer entre les pages de notre application . Pour ce faire, on peut utiliser les méthodes de l'objet `router` pour naviguer entre les différentes vues de l'application. 

Par exemple, la méthode `push()` permet de naviguer vers une nouvelle route en ajoutant une nouvelle entrée dans l'historique de navigation du navigateur.

```js
<button @click="goToHome">Je veux aller à l'accueil</button>
​
methods: {
  goToHome() {
    this.$router.push('/')
  }
}
```

​
Charger une route en utilisant le nom  et en passant un paramètre :

```js
this.$router.push({ name: 'UnArticle', params: { id: 4 } })
```

Il existe d'autres méthodes permettant la navigation : 
- `$router.forward()` permet de naviguer vers l'avant dans l'historique de navigation de l'application. Cela équivaut à cliquer sur le bouton "Suivant" dans le navigateur.
- `$router.back()` permet de naviguer vers l'arrière dans l'historique de navigation de l'application. Cela équivaut à cliquer sur le bouton "Précédent" dans le navigateur.
- `$router.go(-1)` permet de naviguer d'un certain nombre de pages en arrière dans l'historique de navigation de l'application. Dans cet exemple, le nombre de pages en arrière est spécifié comme "-1", ce qui signifie qu'il s'agit de la page précédente dans l'historique de navigation. Si vous voulez naviguer de deux pages en arrière, vous pouvez spécifier "-2" comme argument. De même, si vous voulez naviguer en avant dans l'historique de navigation, vous pouvez spécifier un nombre positif comme argument.

## Page 404

- Dans le tableau des routes, ajout d’une route pour gérer les erreurs 404.
- Cette route « attrape tout » doit obligatoirement se trouver **à la fin** du tableau des routes.


Par exemple, pour rediriger toutes les pages qui n'ont pas de route définie vers le composant `NotFound.vue`, on peut ajouter la route générique suivante :

```js 
const router = createRouter({
  routes: [
    // autres routes définies ici
    { path: '/:notFound(.*)', component: NotFound },
  ],
});
```
Dans cet exemple, le `:notFound(.*)` est un paramètre dynamique qui va correspondre à toutes les routes qui n'ont pas été définies auparavant.

La valeur de ce paramètre sera passée au composant `NotFound.vue` via la propriété `$route.params.notFound`. 

## Styles
Nous savons que lors de la création de lien, les classes `router-link-exact-active` et `router-link-active` sont automatiquement ajoutées sur le lien actif.
- `router-link-exact-active` permet de styliser le lien quand on est sur la page qui correspond au lien
- `router-link-active` permet de styliser le lien quand on est sur la page qui correspond au lien et sur les pages enfants de ce lien.
Exemple: 
- ​​​​​​​/articles
- /articles/1


Il est possible de changer le nom de ces classes dans la configuration du router :

```js
const router = createRouter({
  history: createWebHistory(),
  routes: [
    //définition des routes ici
  ],
  linkActiveClass: 'active',
  linkExactActiveClass: 'exact',
})
```

## Défilement de la page

le *scrollBehavior* est une option de configuration pour les objets `Router`. Elle permet de personnaliser le comportement du défilement de la page lorsqu'une navigation est effectuée.

Par défaut, lorsque l'utilisateur navigue d'une page à une autre, la nouvelle page s'ouvre en haut de la fenêtre, même si la précédente avait été *scrollée*. Cela peut être gênant pour l'utilisateur car il doit chercher à nouveau l'endroit où il était.

Avec *scrollBehavior*, il est possible de définir le comportement du défilement de manière à ce que la page ouvre directement à l'endroit où l'utilisateur se trouvait avant la navigation.

```js
const router = new createRouter({
  routes: [
    //définition des routes ici
  ],
  scrollBehavior(to, from, savedPosition) {
    if (savedPosition) {
      return savedPosition;
    } else {
      return { x: 0, y: 0 };
    }
  }
})
```


Dans cet exemple, le *scrollBehavior* est défini avec une fonction qui reçoit trois arguments : `to`, `from` et `savedPosition`. 
- `to` représente la route de destination,
- `from` représente la route d'origine,
- `savedPosition` représente la position du scroll enregistrée pour cette route.

Dans la fonction, on vérifie si une position de scroll a été enregistrée pour la route actuelle. Si c'est le cas, on la renvoie. Sinon, on renvoie un nouvel objet avec les coordonnées `x` et `y` à `0`, pour que la page s'ouvre en haut.

Ainsi, avec cet exemple, la page s'ouvrira à l'endroit où l'utilisateur se trouvait avant la navigation.

## Sources

- https://router.vuejs.org/
- https://vueschool.io/lessons/introduction-to-vue-router-4