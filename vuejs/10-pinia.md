# Pinia

## Introduction

Pinia est une bibliothèque qui permet de gérer l’état d’une application Vue.js.

Jusqu’à présent, nous utilisions les "props" et les "emits" pour faire communiquer les composants entre eux.

Mais cela devient vite compliqué quand on a beaucoup de composants.

![Pinia](./img/pinia2.png){ data-zoomable width=50% }

Avec Pinia, on peut centraliser l’état de toutes les données d’une application dans un ou plusieurs stores.

![Pinia](./img/pinia4.png){ data-zoomable width=50% }

[Sources images](https://www.vuemastery.com/courses/pinia-fundamentals/fundamentals-what-is-pinia/)

## Installation

- Pour un projet existant :
```bash
npm install pinia
```

- Pour un nouveau projet, vous pouvez répondre `Yes` à la question `Add Pinia for state management?` 

![Pinia](./img/pinia1.png)

## Configuration

Fichier `/src/main.js`

```js{4,11}
import './assets/main.css'

import { createApp } from 'vue'
import { createPinia } from 'pinia'

import App from './App.vue'
import router from './router'

const app = createApp(App)

app.use(createPinia())
app.use(router)

app.mount('#app')

```

## Création d’un store

Les fichiers de store sont créés dans le dossier `/src/stores`.

Il est recommandé de créer un fichier par store. 

Par exemple, pour un store `counter`, on crée le fichier `/src/stores/counter.js`.

Pour un store `user`, on crée le fichier `/src/stores/user.js`

Cela permet de séparer les données de l’application en plusieurs fichiers.


Exemple de fichier `/src/stores/counter.js` :

```js
import { defineStore } from 'pinia'

export const useCounterStore = defineStore({
  id: 'counter',
  state: () => ({ 
    count: 0,
  }),
  getters: {
    doubleCount: (state) => state.count * 2,
  },
  actions: { 
    increment() {
      this.count++
    },
  },
})
```

- `id` : `counter` est le nom du store, doit être unique dans toute l’application.
- `state` : État initial du store, c’est un objet. 
- `getters` :  Les getters sont des méthodes qui permettent de récupérer des données du state et qui sont mises en cache. Elles sont mises à jour uniquement si le state change. Un peu comme les computed dans les composants Elles reçoivent le state en paramètre et retournent une valeur
- `actions` : Les actions sont des méthodes qui modifient le state, elles peuvent être asynchrones. 


## Utilisation d’un store

Dans un composant, il faut importer la fonction `useCounterStore()` qui permet de récupérer le store.

```vue
<template>
  <div>
    <p>Count: {{ store.count }}</p>
    <p>Double count: {{ store.doubleCount }}</p>
    <button @click="store.increment()">Increment</button>
  </div>
</template>

<script>
import { useCounterStore } from './stores/counter.js'

export default {
  data() {
    return {
    }
  },
  created () {
    this.store = useCounterStore() // Le store est maintenant disponible dans le composant
  },
}
</script>
```

- `store.count` : accès à la propriété `count` du store `counter`.
Cela est possible grâce à la fonction `useCounterStore()` qui permet de récupérer le store `counter` dans le composant.
- `store.doubleCount` : accès à la propriété `doubleCount` du store `counter` qui est un getter.
- `store.increment()` : appel de la méthode `increment()` du store `counter`.


Dans les outils de développement de votre navigateur, si vous avez installé l’extension [Vue.js devtools](https://devtools.vuejs.org/guide/installation.html), vous pouvez voir les stores et leur contenu.

![Pinia](./img/pinia3.png){ data-zoomable width=70% }

Voir la `demo14-pinia`.

## Exemple de store pour l'autentification

```js
import { defineStore } from 'pinia'
import { jwt-decode } from 'jwt-decode'

export const useAuthStore = defineStore({
  id: 'AuthStore',
  state: () => ({ 
    token: null,
  }),
  getters: {
    isAuthenticated(state) {
      // vérifie si le token existe et n'est pas expiré
      if (state.token) {
        const decoded = jwt_decode(state.token)
        const now = Date.now() / 1000
        if (decoded.exp < now) {
          return false
        } else {
          return true
        }
      } else {
        return false
      }
    },
  },
  actions: { 
    // login avec courriel et mot de passe
    async login(login, password) {
      const response = await fetch('http://monapi.com/login', {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json'
        },
        body: JSON.stringify({
          login,
          password
        })
      })
      if (response.ok) {
        const data = await response.json()
        this.token = data.token // On stocke le token dans le store
      } else {
        throw new Error('Impossible de se connecter')
      }
    },
    logout() {
      // On supprime le token du store
      this.token = null
    },
  },
})
```

## Utilisation du store d'authentification

```vue

<template>
  <div>
    <p v-if="isAuthenticated">Vous êtes connecté</p>
    <button v-if="isAuthenticated" @click="logout()">Se déconnecter</button>
    <p v-else>Vous n'êtes pas connecté</p>

    <form @submit.prevent="login()">
      <input type="text" v-model="login">
      <input type="password" v-model="password">
      <button type="submit">Se connecter</button>
    </form>
  </div>
</template>

<script>
import { useAuthStore } from './stores/auth.js'

export default {
  data() {
    return {
      login: '',
      password: '',
    }
  },
  computed: {
    isAuthenticated() {
      return this.store.isAuthenticated
    }
  },
  created () {
    this.store = useAuthStore() // Le store est maintenant disponible dans le composant
  },
  methods: {
    login() {
      try {
        this.store.login(this.login, this.password)
      } catch (error) {
        console.log(error.message)
      }
    },
    logout() {
      this.store.logout()
    },
  }
}
</script>
```

