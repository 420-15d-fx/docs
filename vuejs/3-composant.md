# Composant
[Documentation](https://fr.vuejs.org/guide/essentials/component-basics.html)

Un composant (`component`) est une partie de l'interface utilisateur qui peut être réutilisée et encapsulée dans une instance Vue. Les composants sont utilisés pour structurer une application en petits blocs autonomes qui peuvent être facilement maintenus et réutilisés.

Un composant Vue.js se compose généralement de trois parties :
- **Template** : définit la structure HTML du composant,
- **Données** : fournissent les informations dynamiques qui seront rendues dans le template
- **Logique JavaScript** : gère les interactions utilisateur et les données du composant.

Un composant Vue.js peut également avoir des propriétés (`props`) qui lui permettent de recevoir des données de son parent. Cela facilite la communication entre les composants et permet de réutiliser des composants dans différents contextes.

Les composants Vue.js sont très flexibles et peuvent être utilisés pour créer des interfaces utilisateur simples ou complexes. Ils permettent également une meilleure organisation du code et une meilleure maintenabilité de l'application.

![Composant](./img/composant1.png){ data-zoomable width=50% }

## Création d'un composant
Reprenons l'exemple de la documentation pour créer un bouton qui compte le nombre de clics.

- 1. Créer un fichier nommé `boutonCompteur.vue` dans le dossier `components` 


Dans ce template, nous définissons un bouton qui affiche le nombre de clics. 

La directive `@click` permet de définir un gestionnaire d'événements qui incrémente la valeur de la propriété `count` à chaque fois que l'utilisateur clique sur le bouton.


- 2. Dans le fichier `App.vue`, nous ajoutons le composant `boutonCompteur` dans la liste des composants :

::: code-group

```js [/components/boutonCompteur.vue]
<template>
  <button @click="count++" >Compteur {{ count }}</button>
</template>

<script>
export default {
  data() {
    return {
      count: 0,
    };
  },
};
</script>
```

```js [App.vue]
<template>
  <h1>Titre</h1>
  <boutonCompteur />
</template>

<script>
// Import du composant BoutonCompteur
import boutonCompteur from './components/boutonCompteur.vue';

export default {
  // Ajout du composant BoutonCompteur dans la liste des composants
  components: {
    boutonCompteur,
  }
};
</script>
```

:::

Voir la `demo7-composant` 

<script setup>
import BoutonCompteur from './components/BoutonCompteur.vue'
import FiltreDemo8 from './components/FiltreDemo8.vue'
</script>
<BoutonCompteur />



(Voir les scripts npm)


## Composant et communication 

### Communication entre le composant parent et le composant enfant.

[Documentation](https://fr.vuejs.org/guide/components/props.html)

Dans le composant parent, nous pouvons transmettre des données au composant enfant en utilisant les `props`. 

- Exemple de communication avec données statiques :

::: code-group

```js [App.vue]
<amis-contacts 
    name='Aminata'
    phone-number='0123456789'
    email='Aminata@garneau.ca'
></amis-contacts>
```

```js [amisContacts.vue]  
<template>
  <ul>
    <li> Nom: {{ name }} </li>
    <li> Téléphone: {{ phoneNumber }} </li>
    <li> Courriel: {{ email }} </li>
  </ul>
</template>

export default {
  name: 'ContactAmis',
  props: [
    name,
    phoneNumber,
    email
  ],
}
```

:::

Voir `demo7-props`



- Exemple de communication avec données dynamiques :

::: code-group

```js [App.vue]
<amis-contacts
    v-for='(contact, index) in contacts'
    :key='index'
    :name='contact.name'
    :phone-number='contact.phoneNumber'
    :email='contact.email'
></amis-contacts>
```

```js [amisContacts.vue]
<template>
  <ul>
    <li> Nom: {{ name }} </li>
    <li> Téléphone: {{ phoneNumber }} </li>
    <li> Courriel: {{ email }} </li>
  </ul>
</template>

export default {
  name: 'ContactAmis',
  props: [ name, phoneNumber, email ].
}
```
:::

Voir `demo7-props-dynamiques`

::: warning 🎃 Important 🎃
Les props dans le composant enfant ne doivent pas être modifiées, cela donne une erreur.

C’est le composant parent qui doit gérer les données.

Tous les props forment une liaison unidirectionnelle entre la propriété enfant et la propriété parent : lorsque la propriété parent est mise à jour, elle descendra vers l'enfant, mais pas l'inverse. 


👉🏾 Solution : copier la valeur `props` dans les `data` du composant.

👉🏾 Inconvénient : le composant parent n’est pas « informé » de la modification.

---

![Composant](./img/composant3.png){ data-zoomable width=40%}

---

[Voir la démo](https://play.vuejs.org/#eNqlVtty2zYQ/RWUmU7tjkXq5tRmFTeOk840D2mnzuSJLxAJkohBgAVAWa5HL/0bf4d+rAuAN1m07ExGlwEWu4uzd9x7l2Xpryrihd5Ck6JkWJOLiCO0yAlOiLRrs5tcXBZULQJY2OOgd76oWMOHgWkUC65xrJWjIcRxQd5EHijgWOPIa+hlLjgZ8apYEgnn48l0Nj99/cvZeccS4syIzqcdiRSYsk7d2wxLTnDlx63m4CVornGcH8TiPntQDAk9xqKMsgEkIAUIUrwSkmojrWVFdmEuAuu9RdBzP2xVLGmpYU2LUkiNjPevaktQKkWBIs8P+lQTxsj71UiTtZVJSIorptG9uceYHYIQBDzyTgwlFqCaE65V6FjQzi2WZ2P/E/Dz0XHDJImuJEf3G7jLcZjVImghG/j6jhlLfnZCS7EeKfov5VkIawmJMwISSG0Md64L5vhSuBvcVVB2Z7B+FEoDWKQwVyNFJE0bkaVI7pxIgWVGeYjGFo0maz3CjGZAicE0IttLbL724OQ4Ebcgh6blGp3BT2ZLfDQ+QfXXn74+tjqbG2aSFAhXWlhqbYbECa3AgZNxaeyBRMJJYu2cALvjxPFNJkXFE8hFJmSIXs0ny7OzmT2tSbc5JMjTJiB0SxOdh+h8/GMNaj2qSfOxu8maaQNRO9878bSC9E9p5n9VgkORW/Mjz4SeMiL/LDUVXEVemwGRhxkTtx8tzWSrTQArk5P4ZoD+Va0NLfL+kgRitIIkbM80uI5ACM3xh+tPYFnvsBBJxYD7wOHfRAlWGYyO7R04EWD3+CzaP2yNgNc/qw9rTbhqjLLl1qZx5EGJXB0wvYM782dWDrwKXnxcZgPdcnpxf29rDG02CMGaqt/rske/geojtzsG5VD/3mYDHXTqZJeV1oKjtzGj8Y3pESLLGHlPNPQWwHfxfvtgl4vAcR6SgltAwt21y18xtBrRFDgTp/lSki9U0aUJQdMwGa1XsFZaCp5dfN4+sO2DbY+hSS1LbJjATnvyyfZNML3WE7SKhlRu/8uGdUGHfamOK1FJSQkb1GPb8r6m51rtobZZh99kQp19pRRl1zkdX72BIr4rYXutJSRlnaumcf5TUUmSnQJyqVkPIOfH79IDPtyTd2pfJm99920IdhUEAfqCGakktEKJku0DeFMjRfsVwX8iSsO5sueUUyfacewBeCcEI5i3COoghSjFTO3acGhmdeKPamBXETQDp7AdcdBxic5F0kV8p0y7m+Agp8rfuwC9QT8Mn9g7Ov+1hdzptHI99zWqOlKt4pmBjGAalCQBCnSDgem5P7sYVXpkZUPEIUHr0fct4/P5wfXkzGvQGTjN5LVTDuAzumtBn+cF03kM3GP/1PzDb+JPB5Awkur2+TDtvVDgKQP+mDboB6Z661f4WGTGwe7l4vr2rvf900aX0R8iCs8VCKvTXkll1JeCto5x5hn1MB9pgl6lqXkaDb41ZvP5+akL3N5bo3OHPzMQeo+WXownEF3zM5EejnLPsjAXK9tr6m0q4kqNVi7VH5G7LbRWuiL16+zge+lp0wcQzw8h7j2UNv8DcEI5LA==)

:::

### Validation des props

[Documentation](https://fr.vuejs.org/guide/components/props.html#prop-validation)

- Vérification du type.
- Paramètres requis, true/false
- Possibilité d’ajouter une valeur par défaut.
- Possibilité d’ajouter une validation (`validator`) avec une fonction qui retourne true ou false.

- Si les conditions requises ne sont pas validées, message dans la console JavaScript du navigateur.
- Utile quand on développe un composant destiné à être utilisé par les autres.

```js
props: {
  myProp: {
    type: Number,
    required: false,
    default: 0,
    validator: function (value) {
      return Number.isInteger(value) && value >= 0 && value <= 10
    }
  }
}
```
​

Dans cet exemple, `myProp` est définie comme une `prop` de type `Number` requise et dont la valeur par défaut est 0. Le validator est une fonction qui prend la valeur de `prop` en entrée et renvoie `true` si la valeur est un entier compris entre 0 et 10, et `false` sinon. Si une valeur invalide est passée à `myProp`, une erreur sera déclenchée.



### Communication entre le composant enfant et le composant parent.

[Documentation](https://fr.vuejs.org/guide/components/events.html)

Pour communiquer des données d'un composant enfant à son parent, Vue.js fournit un système d'événements personnalisés.

![Composant](./img/composant4.png){ data-zoomable width=30%}

Dans le composant enfant, on émet un événement

```js
this.$emit('toggle-favorite', this.id);
```

Dans le parent, mise en place d’un écouteur d’événement avec `v-on` (ou `@`) qui exécute la méthode `toggleFavoris`.

```js
<amis-contacts
    v-for='(contact, index) in contacts'
    :key='index'
    :name='contact.name'
    :phone-number='contact.phoneNumber'
    :email='contact.email'
    @toggle-favorite='toggleFavoris'
></amis-contacts>
```

```js
methods: {
  toggleFavoris(id) {
    const amisFind = this.contactAmis.find(amis => amis.id === id)
    amisFind.isFavorite = !amisFind.isFavorite
  }
}
```

Voir `demo7-props-dynamiques`

## Pratique

::: tip Objectif
Utilisez la `demo7-props-dynamiques` pour ajouter un composant `AmisHeader.vue`.

Le header doit afficher le nombre de favoris.

Le mot « favori » doit être au singulier ou au pluriel suivant le nombre de favori.

![Composant](./img/composant5.png){ width=294}
:::

## Filter des données

Voir la `demo8-filtre`

<FiltreDemo8 />

## Démos

 [Démos sur Gitlab](https://gitlab.com/15d/420-15d-demo-vue)