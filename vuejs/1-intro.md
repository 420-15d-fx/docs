# Vue.js - Introduction

- Vue (ou Vue.js) est un framework JavaScript front-end à code source ouvert.
- Permet de construire des interfaces utilisateur et des applications web monopages.
- Contrairement à d'autres projets JavaScript populaires, Vue n'est pas soutenu par une grande entreprise comme React (Facebook) ou Angular (Google).
- Vue a été écrit à l'origine par Evan You et la [communauté](https://vuejs.org/v2/guide/team.html) open-source.


Principaux « concurrents » :

- [Angular](https://angular.io/) (Google)
- [React](https://reactjs.org/) (Facebook)
- [Svelte](https://svelte.dev/) (Rich Harris)

Vue.js est souvent comparé à d'autres frameworks tels que Angular et React, mais il se distingue par sa taille plus petite, sa simplicité et sa flexibilité.

Vue.js utilise une approche basée sur les composants, où les différents éléments de l'interface utilisateur sont encapsulés dans des composants réutilisables. Ces composants peuvent être imbriqués les uns dans les autres pour construire des interfaces complexes.

Exemples de sites réalisés avec Vue.js

- https://gitlab.com
- https://laracasts.com
- https://www.behance.net

Évolution du nombre d’étoile sur Github (2014-2024)

https://star-history.com/#facebook/react&vuejs/vue&angular/angular

![Vue.js](./img/intro1.png){data-zoomable width=40%}

## Configuration et installation

Il y a deux façons principales d’utiliser Vue :

- dans un projet Node.js, installation avec npm. Pour des SPA (Single Page-Application), Vue contrôle l’ensemble de l’interface graphique.

![Vue.js](./img/intro2.png)

- directement injecté dans un fichier HTML statique avec un lien vers un CDN (content delivery network). Permet d’utiliser Vue sur une partie d’un site seulement, ou sur quelques pages. Nous utiliserons cette méthode dans un premier temps.​​​​​​​

![Vue.js](./img/intro3.png)

[Documentation](https://fr.vuejs.org/guide/quick-start.html)

## VS Code
Extensions utiles :
- [Volar](https://marketplace.visualstudio.com/items?itemName=Vue.volar)
- [Prettier](https://marketplace.visualstudio.com/items?itemName=esbenp.prettier-vscode)

Pour formatter le code, configurer Prettier par défaut :

![Vue.js](./img/intro4.png)

Raccourcis clavier :

![Vue.js](./img/intro5.png)

## Documentation

- [Documentation officielle](https://fr.vuejs.org/guide/introduction.html)

Préférence d'API :

Il existe maintenant deux façons d'utiliser Vue :

- Options API
- Composition API

https://fr.vuejs.org/guide/introduction.html#api-styles

Dans le cadre de ce cours, nous utiliserons l'API par options.

Vous pouvez sélectionner la version de la documentation qui correspond à la version de l'API que vous préférez.

![Vue.js](./img/intro6.png)


## Utilisation avec un CDN

Dans un premier temps, nous utiliserons Vue.js avec un CDN (Content Delivery Network).

[Documentation](https://fr.vuejs.org/guide/quick-start.html#using-vue-from-cdn)

```html{9,11-12}
<!DOCTYPE html>
<html lang="fr">
  <head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>Démo1</title>
  </head>
  <body>
    <div id="app"></div>
    
    <script src="https://unpkg.com/vue@3/dist/vue.global.js"></script>
    <script src="app.js" defer></script>
  </body>
</html>
```


La `div` principale où sera injecté le code généré par le framework doit avoir l'id `app`.  

Les deux scripts suivants doivent être inclus : 
- `vue.global.js` : Contient le code pour les fonctionnalités de vue.js
- `app.js` : Contient le code de votre application

### Instance de vue

Dans le fichier `app.js` :

```js
Vue.createApp(options).mount('#app');
```

- Une instance Vue est la racine de l’application. Elle est créée en passant un objet `options`. 
Comme son nom l'indique, cet objet possède une variété de propriétés optionnelles qui donnent à l'instance la possibilité de stocker des données et d'effectuer des actions.

- L'instance Vue est ensuite insérée (`mount`) dans un élément de notre choix, formant une relation entre l'instance et cette partie du DOM. En d'autres termes, nous activons Vue sur la div ayant l'id `app` en définissant `#app` comme l'élément dans lequel notre instance est insérée.


## `data` - Données réactives

L’instance de Vue utilise la propriété `data` pour les données réactive. Elle doit retourner un objet qui contient les données.

Les données réactives sont des données qui peuvent être modifiées et qui vont mettre à jour automatiquement l’interface graphique.

Pour afficher la valeur de `message` dans une balise `p`, nous pouvons utiliser l'interpolation de texte, en utilisant la syntaxe « Mustache » (les doubles accolades).


### Démo 1 - Données réactives

<iframe height="300" style="width: 100%;" scrolling="no" title="vue_demo1" src="https://codepen.io/gduquerroy/embed/gOqLVwG?default-tab=js%2Cresult&editable=true" frameborder="no" loading="lazy" allowtransparency="true" allowfullscreen="true">
  See the Pen <a href="https://codepen.io/gduquerroy/pen/gOqLVwG">
  vue_demo1</a> by Gilles (<a href="https://codepen.io/gduquerroy">@gduquerroy</a>)
  on <a href="https://codepen.io">CodePen</a>.
</iframe>

Pour que le HTML soit interprété, nous devons utiliser l'attribut `v-html` dans la balise dans laquelle sera insérée le code.

<iframe height="300" style="width: 100%;" scrolling="no" title="Untitled" src="https://codepen.io/gduquerroy/embed/XWONveW?default-tab=js%2Cresult&editable=true" frameborder="no" loading="lazy" allowtransparency="true" allowfullscreen="true">
  See the Pen <a href="https://codepen.io/gduquerroy/pen/XWONveW">
  Untitled</a> by Gilles (<a href="https://codepen.io/gduquerroy">@gduquerroy</a>)
  on <a href="https://codepen.io">CodePen</a>.
</iframe>


## Liaison d'attributs (Attribute Binding)
Pour lier dynamiquement les attributs HTML à des valeurs de données dans l'instance de Vue, il faut utiliser la directive v-bind. On peut utiliser le raccourci deux-points `:` 

```js
data: {
  lien: "http://maboulangerie.ca"
}
```
Le code suivant **ne fonctionne pas**, il faut utiliser la directive `v-bind` quand on veut lier une propriété de l'instance de Vue à un attribut HTML.

```html
<a href="{{ lien }}"></a>` <!-- Ne fonctionne pas -->
```

Ce code fonctionne :
```html
<a v-bind:href="lien"></a>
<!-- ou -->
<a :href="lien"></a>
```

Dans cet exemple, la valeur de lien sera automatiquement mise à jour dans l'élément a chaque fois que la propriété lien de l'instance de Vue est modifiée.

Cela est valable pour tout les attributs : `src`, `alt`, `class`, `disabled`…

[Documentation](https://fr.vuejs.org/api/built-in-directives.html#v-bind)

​​​​​​​Exemples :

```html
<img :src="imageSrc" />
<img :src="'/path/to/images/' + fileName" />
​
<!-- Exemple avec des class -->
<div :class="{ red: isRed }"></div> <!-- Ajoute la class red si isRed est true -->
<div :class="[classA, classB]"></div> <!-- Ajoute les classes classA et classB -->
<div :class="[classA, { classB: isB, classC: isC }]"></div> <!-- Ajoute la class classA et classB si isB est true et classC si isC est true -->
​
<!--Exemple pour des styles -->
<div :style="{ fontSize: size + 'px' }"></div> <!-- Ajoute la propriété font-size avec la valeur de size -->
<div :style="[styleObjectA, styleObjectB]"></div> <!-- Ajoute les styles de styleObjectA et styleObjectB -->
​
<!-- Activer ou désactiver un bouton -->
<button :disabled="isButtonDisabled">Clique pas</button> <!-- Désactive le bouton si isButtonDisabled est true -->
```


## Les méthodes

L’option `methods` permet de définir des fonctions qui seront exécutées à certains moments dans l’application.

<iframe height="300" style="width: 100%;" scrolling="no" title="vue_demo3" src="https://codepen.io/gduquerroy/embed/GRzrKOz?default-tab=js%2Cresult&editable=true" frameborder="no" loading="lazy" allowtransparency="true" allowfullscreen="true">
  See the Pen <a href="https://codepen.io/gduquerroy/pen/GRzrKOz">
  vue_demo3</a> by Gilles (<a href="https://codepen.io/gduquerroy">@gduquerroy</a>)
  on <a href="https://codepen.io">CodePen</a>.
</iframe>

On utilise cette fonction dans le HTML avec les « Moustaches »

```html
<h2>{{ randomText() }}</h2>  
```

Pour utiliser les « data » dans les fonctions, il faut utiliser `this`.

```js{4,5,12,14}
const app = Vue.createApp({
  data() {
    return {
      text1: "Apprendre Vue.js",
      text2: "Je maîtrise Vue.js",
    };
  },
  methods: {
    randomText() {
      const number = Math.random();
      if (number > 0.5) {
        return this.text1;
      } else {  
        return this.text2;
      }
    },
  }, 
});
​
app.mount("#app");
​
```

## Pratique 1
::: tip ⏰
Utilisez les fichiers du dossier `pratique1` du [dépôt Gitlab des démos](https://gitlab.com/420-15d-fx/demos-vuejs), ou utiliser l'iframe ci-dessous.
- Complétez les fichiers `app.js` et `index.html` afin de remplacer les textes en majuscules par des données provenant de `app.js`.
- Ajoutez également une url pour l’image.
- Le champ input doit être automatiquement​​​​​​​ pré-rempli avec le prix. C'est cette valeur qui déterminer prix à afficher avec et sans taxes.

![Vue.js](./img/intro7.png)

<iframe height="300" style="width: 100%;" scrolling="no" title="vue_pratique1" src="https://codepen.io/gduquerroy/embed/XWOpapL?default-tab=js%2Cresult&editable=true" frameborder="no" loading="lazy" allowtransparency="true" allowfullscreen="true">
  See the Pen <a href="https://codepen.io/gduquerroy/pen/XWOpapL">
  vue_pratique1</a> by Gilles (<a href="https://codepen.io/gduquerroy">@gduquerroy</a>)
  on <a href="https://codepen.io">CodePen</a>.
</iframe>

:::

## Événements

Les événements dans Vue.js sont utilisés pour déclencher des actions dans une application en réponse à des actions de l'utilisateur, comme un clic sur un bouton ou la saisie d'une valeur dans un formulaire. 

Un événement peut être capturé avec la directive `v-on`.

Il est également possible d'utiliser un raccourci en utilisation l'arobase `@` : 

```js
methods: {
  ajoutTodo() {
    this.todos.push(this.todoSaisi);
  }
}
```

```html
<button v-on:click="ajoutTodo">
<!-- ou -->
<button @click="ajoutTodo">
```

- [Liste des événements relatifs à la souris](https://developer.mozilla.org/fr/docs/Web/API/Element#%C3%A9v%C3%A8nements_relatifs_%C3%A0_la_souris)
- [Liste des événements relatifs au clavier](https://developer.mozilla.org/fr/docs/Web/API/Element#%C3%A9v%C3%A8nements_relatifs_au_clavier)


Autres exemples (avec l'ajout d'un argument) :

```js
methods: {
  remove(index) {   // index est un argument
    this.todosArr.splice(index, 1);
  }
}
```

```html
<button @click="remove(index)"> <!-- index est un argument -->
​
<!-- Événement submit  -->
<form @submit="ajoutTodo"> <!-- ajoutTodo est une méthode qui est appelée lors de la soumission du formulaire -->
​
<!-- Événement au survol de la souris -->
<img @mouseover="showImage"> <!-- showImage est une méthode qui est appelée lors du survol de l'image -->
```



### Modificateur d'événements

[Documentation](https://fr.vuejs.org/guide/essentials/event-handling.html)

Les modificateurs d'événement sont des fonctionnalités de Vue.js qui permettent de modifier le comportement par défaut des événements dans une application Vue.js.

Voici quelques exemples de modificateurs d'événement :

- `prevent` : Empêche le comportement par défaut de l'événement. Par exemple, l'envoi d'un formulaire peut être empêché en utilisant ce modificateur sur l'événement submit.
```html
<form v-on:submit.prevent="faireQuelqueChose">
  ...
</form>
```

- `stop` : Arrête la propagation de l'événement vers les éléments parents. Par exemple, si un bouton est contenu dans une `div` avec un autre écouteur d'événement, l'utilisation de ce modificateur empêchera l'événement de se propager à la `div`.

```html
<button v-on:click.stop="doSomething">Cliquez-moi</button>
```

- `capture` : Écoute l'événement lorsqu'il est déclenché sur un élément parent avant qu'il ne se propage aux éléments enfants.

```html
<div v-on:click.capture="doSomething">
  <button>Cliquez-moi</button>
</div>
```

- `self` : Écoute l'événement uniquement lorsque l'élément lui-même est cliqué et non lorsque l'événement est propagé à partir d'un élément enfant.

```html
<div v-on:click.self="doSomething">
  <button>Cliquez-moi</button>
</div>
```

- `once` Permet d'écouter un événement une seule fois, après quoi l'écouteur est supprimé.

```html
<img @mouseover.once="showImage">
```

Il existe également des modificateur d'événement pour les touches.

[Documentation](https://fr.vuejs.org/guide/essentials/event-handling.html#key-modifiers)
​​​​​​​
```html
<!--Faire appel à `submit()` uniquement quand le code de la touche est `Enter`-->
<input v-on:keyup.enter="submit">

<!-- Détecte la touche « page-down » -->
<input @keyup.page-down="onPageDown" />
```

Alias pour les touches les plus courantes :​​​​​​​​​​​​​​

![Vue.js](./img/intro8.png){ data-zoomable width=60%}


### Objet Event

Dans un input, l’objet `event` est automatiquement envoyé à la méthode.
​​​​​​​
```html
<input type="text" v-on:input="setName">
```

```js
data: {
  name : "",
},
methods: {
  setName(event) {
    this.name = event.target.value;
  },
}
```

Si on a besoin de passer un argument supplémentaire, on utilisera `$event` pour passer l’objet événement.

```html
<input type="text" v-on:input="setName($event, 'Cegep')">
```

```js
setName(event, lastName) {
  this.name = `${event.target.value} ${lastName}`
},
```

Voir la [documentation sur la gestion des événements](https://fr.vuejs.org/guide/essentials/event-handling.html#method-handlers) 

### Démo événements

<iframe height="300" style="width: 100%;" scrolling="no" title="vue_demo4_v-model" src="https://codepen.io/gduquerroy/embed/oNmBGyd?default-tab=js%2Cresult&editable=true" frameborder="no" loading="lazy" allowtransparency="true" allowfullscreen="true">
  See the Pen <a href="https://codepen.io/gduquerroy/pen/oNmBGyd">
  vue_demo4_v-model</a> by Gilles (<a href="https://codepen.io/gduquerroy">@gduquerroy</a>)
  on <a href="https://codepen.io">CodePen</a>.
</iframe>

## Conditions

### `v-if` - `v-else`

Utilisée pour conditionner le rendu d'un élément complet, ce qui signifie que si la condition est fausse, l'élément ne sera pas du tout rendu dans le DOM. Par conséquent, v-if a un impact sur les performances, car il nécessite de détruire et recréer l'élément chaque fois que la condition change.
documentation

```html
<p v-if="enStock">{{ produit }}</p>
<p v-else-if="enVente">...</p>
<p v-else>...</p
​
<p v-if="a === 1">{{ produit }}</p>
```

Ici, `enStock` et `enVente` doivent être des booléens.

### `v-show`

Fonctionne comme le `v-if` mais cache l’élélement en ajoutant la propritété css `display:none`. L'élément est toujours présent dans DOM.

```html
<p v-show="afficheProduitDetails">...</p>
```

En résumé, si vous avez besoin de conditionner l'affichage d'un élément qui est peu souvent modifié, `v-if` est généralement la meilleure option.

Si vous avez besoin de conditionner la visibilité d'un élément qui est souvent modifié, `v-show` peut être une meilleure option en termes de performances.

## Iterateur

### `v-for`

[Documentation](https://fr.vuejs.org/guide/essentials/list.html)

Pour boucler sur un tableau ou un objet. Il est conseillé d’utiliser l’attribut `:key` avec u` index unique pour de meilleures performances

```js
data: {
  todos: ["manger", "dormir", "jouer"]
}
```

```html
<li v-for="(todo, index) in todos" :key="index">
  {{ todo }} {{ index }}
</li>
```

## Liaison à double sens (Two-Way Binding)

### `v-model`

La liaison à double sens (two-way binding) est un concept clé de Vue.js qui permet de synchroniser automatiquement les données entre le modèle (data) et l'interface utilisateur (UI) en temps réel, sans avoir besoin de gérer manuellement la mise à jour des deux.

Ainsi, chaque fois qu'un nouvel élément est saisi dans l'entrée, les données changent. Et chaque fois que les données changent, tout ce qui utilise ces données sera mis à jour.

```html
<div>
  <label for="username">Nom d'utilisateur :</label>
  <input type="text" id="username" v-model="username" />
  <p>Bienvenue, {{ username }} !</p>
</div>
```

```js
data() {
    return {
      username: ''
    }
  }
```

<iframe height="300" style="width: 100%;" scrolling="no" title="vue_demo2" src="https://codepen.io/gduquerroy/embed/abXpLVy?default-tab=js%2Cresult&editable=true" frameborder="no" loading="lazy" allowtransparency="true" allowfullscreen="true">
  See the Pen <a href="https://codepen.io/gduquerroy/pen/abXpLVy">
  vue_demo2</a> by Gilles (<a href="https://codepen.io/gduquerroy">@gduquerroy</a>)
  on <a href="https://codepen.io">CodePen</a>.
</iframe>

### Démo `v-model` - `for` - `if`

<iframe height="600" style="width: 100%;" scrolling="no" title="vue_demo5_events" src="https://codepen.io/gduquerroy/embed/YzBNrOJ?default-tab=js%2Cresult&editable=true" frameborder="no" loading="lazy" allowtransparency="true" allowfullscreen="true">
  See the Pen <a href="https://codepen.io/gduquerroy/pen/YzBNrOJ">
  vue_demo5_events</a> by Gilles (<a href="https://codepen.io/gduquerroy">@gduquerroy</a>)
  on <a href="https://codepen.io">CodePen</a>.
</iframe>


## Pratique 2

::: tip ⏰
Créer un système de liste de courses avec :
- Un champ pour ajouter un produit
- Deux boutons pour incrémenter ou décrémenter le nombre de produit
- Un bouton pour supprimer
- S’il n’y a pas de produit dans la liste, afficher un message « La liste est vide ! »

Voir `pratique2` pour la solution
:::

## Propriété calculée (computed)

Les propriétés calculées `computed` sont des propriétés dans un composant Vue.js qui sont calculées à partir des données existantes (data), plutôt que d'être stockées en tant que valeurs brutes dans le modèle. Elles sont définies comme des fonctions dans le bloc `computed` du composant, et leur valeur est calculée dynamiquement en fonction de la logique définie dans la fonction.

Elles permettent également d'isoler la logique de calcul des données du reste de l'application, ce qui facilite la maintenance et la réutilisation du code.

- Une propriété calculée (computed) permet de manipuler des données réactives (data).
- Le résultat est mis en cache.
- Elle est ré-évaluée seulement si une donnée qu’elle utilise est mise à jour.

- Par opposition, une méthode est ré-évalué dès qu’une donnée change dans le template.

::: info Important :
- Une propriété calculée ne doit jamais modifier une donnée réactive.
- Elle permet de faire des tests sur un données ou de modifier la présentation d’une donnée.
:::

Quand utiliser une méthode ou un propriété calculée ?
- Si on a besoin de **changer les données**, on utilise une *méthode*.
- Si on veut changer la **présentation d’une donnée**, on utilise une propriété **calculée**

[Documentation](https://fr.vuejs.org/guide/essentials/computed.html)

<iframe height="600" style="width: 100%;" scrolling="no" title="vue_demo6-v-model_for_if" src="https://codepen.io/gduquerroy/embed/ZEwLXVe?default-tab=js%2Cresult&editable=true" frameborder="no" loading="lazy" allowtransparency="true" allowfullscreen="true">
  See the Pen <a href="https://codepen.io/gduquerroy/pen/ZEwLXVe">
  vue_demo6-v-model_for_if</a> by Gilles (<a href="https://codepen.io/gduquerroy">@gduquerroy</a>)
  on <a href="https://codepen.io">CodePen</a>.
</iframe>

## Observateur (watch)

[Documentation](https://fr.vuejs.org/guide/essentials/watchers.html) ​​​​​​​
​​​​​​

Un observateur (watcher) est une fonction dans un composant Vue.js qui observe les changements sur une propriété de données spécifique et réagit en conséquence. Il est défini dans le bloc `watch` du composant et permet de surveiller les changements sur une propriété de données (data) et d'exécuter une action lorsque cette propriété change. Il doit porter le même nom que la propriété de données qu'il observe.

- Permet d’exécuter des opérations quand une donnée réactive change.
- L’observateur n’est pas appelé à partir du template, il se contente d’observer une donnée et réagit quand un changement survient.

```html
<div>
  <p>Le compteur est à {{ compteur }}</p>
  <button @click="incrementerCompteur">Incrémenter le compteur</button>
</div>
```

```js
data() {
  return {
    compteur: 0
  }
},
watch: {
  compteur(nouvelleValeur, ancienneValeur) {
    console.log(`La valeur du compteur a changé de ${ancienneValeur} à ${nouvelleValeur}`);
  }
},
methods: {
  incrementerCompteur() {
    this.compteur++;
  }
}
```

<iframe height="350" style="width: 100%;" scrolling="no" title="vue_demo7-computed" src="https://codepen.io/gduquerroy/embed/zYeNPdL?default-tab=js%2Cresult&editable=true" frameborder="no" loading="lazy" allowtransparency="true" allowfullscreen="true">
  See the Pen <a href="https://codepen.io/gduquerroy/pen/zYeNPdL">
  vue_demo7-computed</a> by Gilles (<a href="https://codepen.io/gduquerroy">@gduquerroy</a>)
  on <a href="https://codepen.io">CodePen</a>.
</iframe>


## Pratique 3

::: tip ⏰
Créer deux boutons qui incrémentent un compteur :
- Un bouton qui ajoute 5
- Un bouton qui ajoute 1
Créer un champ texte qui affiche :
- « Pas assez » si le compteur est < 17
- « C’est trop » si le compteur est > 17
- « Gagné ! » si le compteur est égale à 17

Dès que le texte change, le compteur doit revenir à 0 au bout de 5 secondes.

Voir `pratique3` pour la solution
:::

## Styles dynamiques
- Liaison de classe : Vous pouvez utiliser la directive v-bind:class pour lier dynamiquement une ou plusieurs classes ou style CSS à un élément HTML en fonction de la valeur d'une propriété de données dans le modèle.

```html
<div :class="{ 'red': isRed }"></div>
```

```js
data() {
  return {
    isRed: true
  }
}
```
Dans cet exemple, la classe red sera ajoutée à la `div` si la propriété isRed dans le modèle est vraie.


- Liaison d'attribut : vous pouvez utiliser la directive `v-bind` pour lier dynamiquement des attributs HTML tels que le style à un élément HTML en fonction de la valeur d'une propriété de données dans le modèle.

```js
data() {
  return {
    bgColor: 'red'
  }
}
```
```html
<div :style="{ backgroundColor: bgColor }"></div>
```
Il est recommandé d'utiliser la notation camelCase au lieu de la propriété css standard

- Utilisation de méthodes : Vous pouvez également utiliser des méthodes dans le modèle pour calculer dynamiquement les styles CSS en fonction des données existantes dans le modèle. 

```html
<div :style="getStyle()"></div>
```

```js
data() {
  return {
    bgColor: 'red'
  }
},
methods: {
  getStyle() {
    return {
      backgroundColor: this.bgColor,
      color: this.bgColor === 'red' ? 'white' : 'black'
    }
  }
}
```

Dans cet exemple, la méthode `getStyle()` calcule dynamiquement les styles CSS en fonction de la valeur de la propriété `bgColor` dans le modèle.

La couleur de texte sera blanche si la couleur de fond est rouge, sinon elle sera noire.

## Sources

- https://www.vuemastery.com/pdf/Vue-Essentials-Cheat-Sheet.pdf
- https://fr.vuejs.org/api/
- https://fr.vuejs.org/guide/introduction.html