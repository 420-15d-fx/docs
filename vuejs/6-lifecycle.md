# Cycle de vie

Le cycle de vie en Vue.js fait référence aux différentes étapes qu'un composant traverse depuis sa création jusqu'à sa destruction. Chaque étape du cycle de vie permet d'interagir avec le composant et d'effectuer des opérations spécifiques à chaque moment clé de son cycle de vie.
Le cycle de vie d'un composant Vue est divisé en trois grandes étapes :
- La création du composant : cette étape se produit lorsque le composant est créé pour la première fois. Pendant cette étape, Vue instancie le composant et établit ses propriétés initiales. Les hooks de cette étape sont : `beforeCreate`, `created`, `beforeMount` et `mounted`.
- La mise à jour du composant : cette étape se produit lorsque le composant est mis à jour, soit en raison d'un changement de données, soit en raison de la réception de nouvelles propriétés via ses parents. Les hooks de cette étape sont : `beforeUpdate`, `updated`.
- La destruction du composant : cette étape se produit lorsque le composant est détruit, soit parce qu'il n'est plus nécessaire, soit parce que son parent est détruit. Les hooks de cette étape sont : `beforeUnmount` et `unmounted`.

Il est important de comprendre le cycle de vie des composants Vue afin de pouvoir les utiliser efficacement et de les optimiser pour de meilleures performances. Les hooks du cycle de vie peuvent être utilisés pour effectuer des actions spécifiques à chaque étape du cycle de vie.

![cycle de vie](./img/lifecycle1.png){ data-zoomable width=50% }

## Hooks du cycle de vie

Différents « [hooks](https://fr.vuejs.org/api/options-lifecycle.html) » sont disponibles pour exécuter une logique à chaque niveau du cycle de vie.
- `beforeCreate` : Ce hook est appelé avant la création d'une instance de composant. À ce stade, le composant n'est pas encore initialisé, les propriétés et méthodes ne sont pas encore définies.
- `created` : Ce hook est appelé immédiatement après la création de l'instance de composant. Les propriétés et méthodes ont été initialisées, mais le composant n'est pas encore monté sur la page. 
- `beforeMount` : Ce hook est appelé avant le montage du composant sur la page. Le DOM n'est pas encore créé.
- `mounted` : Ce hook est appelé après le montage du composant sur la page. Le composant est visible et accessible dans le DOM.
- `beforeUpdate` : Ce hook est appelé avant la mise à jour du composant. Il est appelé lorsque les données du composant ont changé et que la mise à jour du DOM est imminente.
- `updated` : Ce hook est appelé après la mise à jour du composant. Le DOM a été mis à jour pour refléter les nouvelles données.
- `beforeDestroy` : Ce hook est appelé avant la destruction du composant. C'est l'endroit où vous pouvez nettoyer les ressources que le composant utilise, comme les écouteurs d'événements et les connexions de base de données.
- `destroyed` : Ce hook est appelé après la destruction du composant. À ce stade, le composant a été retiré du DOM et toutes les ressources qu'il utilisait ont été nettoyées.
- `errorCaptured` : Ce hook est appelé lorsque des erreurs se produisent lors du rendu du composant ou lors de la gestion des événements dans le composant.


Voir `demo11-http` pour suivre le cycle de vie dans la console.