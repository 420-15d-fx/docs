# SSL

## HTTP

Le protocole [HTTP](https://developer.mozilla.org/fr/docs/Web/HTTP/Overview) est à la base de tout échange de données sur le Web. Mais les données qui sont échangées ne sont pas encryptées et une personne mal intentionnée peut les récupérer.

![HTTP](./img/ssl1.png){ data-zoomable width=50% }

## HTTPS

Le protocole [HTTPS](https://fr.wikipedia.org/wiki/HyperText_Transfer_Protocol_Secure) (S pour sécurisé) est une version chiffrée du protocole HTTP. Celui-ci utilise le protocole SSL/TLS pour chiffrer les données échangées entre le client et le serveur.

SSL est l’ancêtre de TLS, on trouvera souvent SSL/TLS pour désigner ce protocole.

SSL : Secure Sockets Layer
TLS : Transport Layer Security

![HTTPS](./img/ssl2.png){ data-zoomable width=50% }

- **Clé Publique** :
  - La clé publique est utilisée pour le **chiffrement**.
  - Elle est distribuée librement et peut être partagée avec n'importe qui.
  - Les données chiffrées avec la clé publique ne peuvent être déchiffrées qu'avec la clé **privée** correspondante.

- **Clé Privée** :
  - La clé privée est utilisée pour le **déchiffrement**.
  - Elle doit rester confidentielle et ne doit être connue que du propriétaire de la clé.
  - Les données déchiffrées avec la clé privée peuvent être vérifiées avec la clé publique correspondante.

## Certificat
Pour activer HTTPS sur un site Web, il faut obtenir un certificat (un fichier particulier) à partir d’une autorité de certification (AC ou CA pour Certificate Authority).

Historiquement, ce certificat est payant, mais il existe aujourd’hui des autorités de certification gratuites comme [Let’s Encrypt](https://letsencrypt.org/fr/about/). Les certificats émis ont une validité de 90 jours, il faut les renouveler régulièrement.

### Type de certificats

- **Certificat SSL à domaine unique (single-domain SSL)** : Il s'agit d'un type de certificat SSL/TLS qui est spécifiquement émis pour sécuriser un seul domaine. Par exemple, si vous exploitez un site web avec l'adresse *www.example.com*, vous pouvez utiliser un certificat SSL à domaine unique pour sécuriser les communications entre les utilisateurs qui accèdent à votre site web et votre serveur. Le certificat sera valide uniquement pour le domaine *www.example.com* et ne sera pas applicable à d'autres domaines ou sous-domaines (donc ne fonctionnera pas pour *example.com* par exemple)

- **Certificat Wildcard** : Permet de sécuriser un domaine principal et tous ses sous-domaines. Par exemple, un certificat Wildcard pour le domaine "*example.com*" peut être utilisé pour sécuriser "*www.example.com*", "*mail.example.com*", "*blog.example.com*", etc.

- **Certificat SAN (Subject Alternative Names)** : Permet de sécuriser plusieurs domaines distincts, y compris les sous-domaines. Vous pouvez inclure plusieurs noms de domaine et sous-domaines dans le certificat SAN lors de sa création. Par exemple, un certificat SAN peut être configuré pour sécuriser "*www.example.com*", "blog.example.com" et "*mail.example.com*". Il est également possible d'inclure des noms de domaine complètement différents, tels que "www.example.net" ou "www.example.org", dans le même certificat SAN.


### Type de validations 

- **Certificat à validation de domaine (DV)** : Ce type de certificat vérifie simplement le contrôle du domaine par le demandeur. Il est généralement délivré rapidement et est utilisé pour les sites web personnels, les blogs ou les petites entreprises

- **Certificat à validation d'organisation (OV)** : Ce type de certificat nécessite une vérification plus approfondie de l'identité et de l'organisation du demandeur. Il offre un niveau de confiance supérieur par rapport au certificat DV et est souvent utilisé par les entreprises pour sécuriser leurs sites web.

- **Certificat à validation étendue (EV)** : Les certificats EV sont les plus rigoureux en termes de validation d'identité. Ils affichent également une barre d'adresse verte dans les navigateurs, signalant aux utilisateurs que le site est hautement sécurisé. Les certificats EV sont généralement utilisés par les grandes entreprises et les sites de commerce électronique

- **Certificat de signature de code** : Ce type de certificat est utilisé pour signer numériquement des logiciels ou des applications, garantissant leur authenticité et leur intégrité. Il est utilisé par les développeurs de logiciels pour distribuer des applications en ligne.

Ces certificats sont émis par des autorités de certification (CA) de confiance et sont utilisés pour sécuriser différentes applications en ligne, des sites web aux e-mails en passant par les applications mobiles. Le choix du certificat dépend des besoins spécifiques de sécurité et de l'identité de l'entité qui demande le certificat.

**Let’s Encrypt** délivre seulement des **certificats de validation de domaine**, Il ne peut pas vérifier l’identité d’une organisation ou d’une personne.

## Création du tunnel de communication client / serveur
Une fois le certificat installé sur votre serveur, il va permettre de configurer le site Web pour établir des connexions HTTPS via le port 443.

Voici le fonctionnement:

![HTTPS](./img/ssl3.png)

1. Le client envoie une requête de connexion sécurisée au serveur. Par exemple, un utilisateur peut taper l'URL d'un site web commençant par `https://` dans son navigateur.

1. Le serveur répond en envoyant son certificat SSL au client. Le certificat contient des informations telles que le nom du domaine, la clé publique du serveur et les détails de l'autorité de certification qui a émis le certificat.

1. Le client vérifie la validité du certificat du serveur. Il vérifie si le certificat est émis par une autorité de certification de confiance et si la période de validité est correcte. Le client vérifie également que le nom de domaine du certificat correspond au nom de domaine auquel il tente de se connecter.

1. Si le certificat est considéré comme valide, le client génère une clé de session (clé privée) aléatoire et l'encrypte à l'aide de la clé publique du serveur provenant du certificat. Le client envoie la clé de session encryptée au serveur.

1. Le serveur reçoit la clé de session encryptée du client et utilise sa clé privée pour la décrypter. Le client et le serveur utilisent maintenant la clé de session (clé privée) pour chiffrer et décrypter les données échangées pendant la session. Cela garantit que les informations sensibles, telles que les mots de passe ou les informations de paiement, sont protégées contre l'interception par des tiers.

## Installer un certificat

Sécurisé son site avec le protocole HTTPS est devenu obligatoire aujourd’hui.

- Cela prouve que le domaine vous appartient.
- Les transactions sont davantage sécurisées.
- Vous n’êtes pas pénalisé par les moteurs de recherche.
- Les utilisateurs auront davantage confiances pour faire des achats…

Généralement l’hébergeur que vous avez choisi peut créer et configurer le certificat. Vérifier les spécifications avant de prendre un abonnement. Par exemple, chez vercel.com votre solution est automatiquement hébergée sous HTTPS.

### Installer un certificat auto-signé avec OpenSSL

OpenSSL est une bibliothèque open source largement utilisée pour la mise en œuvre des protocoles SSL/TLS et pour fournir des fonctionnalités de chiffrement et de sécurité dans de nombreuses applications et systèmes d'exploitation. Elle offre une implémentation complète des protocoles SSL (Secure Sockets Layer) et TLS (Transport Layer Security).

- Permet de tester le HTTPS sur une application en local.
- Génère un certificat auto-signé.
- Ce certificat ne sera pas reconnu par le navigateur et il lancera un avertissement de sécurité.
- La connexion sera tout de même encryptée comme pour certificat émanant d’une autorité de certification.
- Disponible sur Linux et Mac.
- Pour Windows, vous devez l’installer. (choisir Win64 OpenSSL v3.0.0).
- Si `git bash` est installé sur votre machine, vous pouvez l’ouvrir et utiliser directement la commande `openssl`.

### Générer un certificat auto-signé avec OpenSSL

- ​​​​​​​Dans le terminal, saisir cette commande pour générer les clés:

```bash
openssl req -nodes -new -x509 -keyout server.key -out server.cert 
```
ou
```bash
openssl.exe req -nodes -new -x509 -keyout server.key -out server.cert 
```

- Répondre aux questions:

![HTTPS](./img/ssl8.png)

- Le certificat et la clé privée sont créés.

![HTTPS](./img/ssl5.png)

- Copier ces fichiers à la racine de votre projet.

## Configuration du projet Node.js

Dans le fichier `app.js`

```js
const fs = require('fs'); // filesystem
const https = require('https'); // import https pour le protocole https
​
// import du certificat et de la clé privée 
// (le certificat est la clé publique)
const certificate = fs.readFileSync('server.cert');
const privateKey = fs.readFileSync('server.key');

// ...


mongoose
  .connect('mongodb://127.0.0.1:27017/blog')
  .then(() => {
    // Utilisation du protocole https, avec les certificats et la clé privée
    https
    .createServer({key: privateKey, cert: certificate}, app)
    .listen(3000, () => {
      console.log('Node.js est à l\'écoute sur le port %s ', PORT);
    });
  })
  ```

## Test dans le navigateur

![HTTPS](./img/ssl6.png){ data-zoomable width=50% }

- Le navigateur ne peut pas vérifier le certificat et lance une alerte.
- On peut accepter et poursuivre, la connexion sera sécurisée par HTTPS.

## Test dans Postman

- Postman détecte également le certificat mais lance une erreur.
- Désactiver la vérification SSL pour pouvoir faire des tests.

![HTTPS](./img/ssl7.png){ data-zoomable width=50% }

## Sources
- https://geekflare.com/fr/tls-101/
- https://www.icodia.com/fr/solutions/certificats-ssl/en-savoir-plus-certificats-ssl.html
