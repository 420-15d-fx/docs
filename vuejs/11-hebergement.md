## Mongo Atlas

### Création de la base de données
Nous allons utiliser le service Mongo Atlas qui permet d’héberger une base de données Mongo gratuitement.
​​​​​​​

- https://www.mongodb.com/atlas/database
- Créer un compte gratuit et connectez-vous.
- Une fois connecté, cliquer"Build a database", sélectionner l'option "M0 FREE" et cliquer sur "Create"

![Mongo Atlas](./img/heber1.png){ data-zoomable width=60% }

![Mongo Atlas](./img/heber2.png){ data-zoomable width=60% }

- Choisissez un Username et Password qui permettront d’accéder à la base de données.

![Mongo Atlas](./img/heber3.png){ data-zoomable width=60% }

- Saisir une adresse ip à partir de laquelle on veut accéder à la base de données.
- Pour le moment on ne connaît pas l’adresse ip de notre API.
- On saisi 0.0.0.0 pour autoriser l’accès à partir de n’importe quel serveur.
- Pour une « vraie » application, il faudrait mettre une vraie adresse ip pour des raisons de sécurité

![Mongo Atlas](./img/heber4.png){ data-zoomable width=60% }

- Une fois que la base de données est prête, on peut aller dans le menu Databases, puis cliquez sur Connect afin de voir les informations de connexion.

![Mongo Atlas](./img/heber5.png){ data-zoomable width=60% }

- Pour obtenir l'url de connexion à votre base de données, cliquer sur "Drivers"

![Mongo Atlas](./img/heber6.png){ data-zoomable width=60% }

- Copier l’adresse, nous allons l’utiliser dans la configuration de notre projet. Il faudra penser à modifier le password par celui choisi lors de la création de la base de données

![Mongo Atlas](./img/heber7.png)

## Héberger l'API sur Vercel.com

Nous allons utiliser les services de [vercel.com](https://vercel.com/) pour mettre notre API en ligne.

Il existe [beaucoup d’entreprise](https://blog.back4app.com/top-10-heroku-alternatives/) qui propose des serveurs afin d’héberger des API, la plupart sont payantes.

Vercel propose un service gratuit pour les applications avec certaines [limites](https://vercel.com/pricing?gad=1&gclid=CjwKCAjwxr2iBhBJEiwAdXECwwIw8tstBGtBSI_o8a2F123Mp7o9ThNZ07fhpbSLDm-aqFFrQBfX6xoCqGYQAvD_BwE).

Nous allons voir comment passer d’un environnement local à un environnement en production.

1. Créer un compte gratuit chez vercel.com et se connecter sur votre dépôt distant (Github ou Gitlab)

![Mongo Atlas](./img/heber8.png){ data-zoomable width=40% }


2. Ajouter un fichier `vercel.json` à la racine de votre API.

Ce fichier sera utilisé par Vercel pour savoir comment déployer votre application.

```json
{
  "builds": [
      {
          "src": "app.js",
          "use": "@vercel/node"
      }
  ],
  "routes": [
      {
        "src": "/(.*)",
        "dest": "app.js"
      }
  ]
}
```	

3. Modifier votre fichier `app.js` afin d'exporter votre application :

```javascript{12}
//...
​
mongoose
    .connect(process.env.DATA_BASE)
    .then(() => {
        app.listen(3000, () => {
            console.log("Node.js est à l'écoute sur http://localhost:%s ", process.env.PORT);
        });
    })
    .catch(err => console.log(err));
​
module.exports = app; //Permet d'exporter l'application pour être utilisée par Vercel
```


4. Sauvegarder et pousser les modification sur votre dépôt distant.

5. Créer un nouveau projet sur Vercel.

![Mongo Atlas](./img/heber9.png){ data-zoomable width=50% }

6. Sélectionner le project correspondant à votre API et cliquer sur Import.

![Mongo Atlas](./img/heber10.png){ data-zoomable width=50% }

7. Inscrire le nom de votre projet

![Mongo Atlas](./img/heber11.png){ data-zoomable width=50% }

8. Ajouter les variables d'environnement contenues dans le fichier `.env` de votre application

![Mongo Atlas](./img/heber12.png){ data-zoomable width=50% }

9) Cliquer sur Deploy

![Mongo Atlas](./img/heber13.png){ data-zoomable width=50% }


`DEPLOYMENT` : indique l'URL de votre API

### Ajouter ou modifier des variable d'environnement

Pour ajouter ou modifier des variables d'environnement du projet, sélectionner le projet et cliquer sur `Setting` et ensuite sur `Environnement variables`

![Mongo Atlas](./img/heber14.png){ data-zoomable width=50% }

