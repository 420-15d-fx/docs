# Variables d'environnements 

## Comment utiliser le fichier .env

1) Dans le *ROOT* de votre projet , créez un nouveau fichier appelé `.env`. Ne placez pas le fichier dans le dossier `src` ou à tout autre endroit.

2) Créer une ou plusieurs variables et ajouter des valeurs

  - Dans Vite , les variables environnementales doivent être préfixées par `VITE_` comme dans `VITE_*variable-name`

```js

VITE_API_URL = "https://monapi.com"

```

3) Utilisez/accédez à la variable dans toute votre application

    - Pour utiliser des variables d'environnement sur l'ensemble de votre application, vous utilisez `import.meta.env` devant le nom de votre variable : 
    
```js

const apiUrl = import.meta.env.VITE_API_URL

```
