# Authentification

## LocalStorage

[Documentation](https://developer.mozilla.org/fr/docs/Web/API/Window/localStorage)

- Permet d’enregistrer des données dans un navigateur Web. 
- Taille entre 5 et 10Mo suivant les navigateurs.
- Nous allons utiliser le localStorage pour sauvegarder le JWT (JSON WEB Token) utilisé pour l’authentification.


::: danger ⚠ Attention ⚠
Le stockage d'un JSON Web Token (JWT) dans le localStorage peut être une pratique courante dans les applications web modernes pour maintenir une session d'utilisateur. Cependant, cela peut présenter des risques de sécurité potentiels.

Le localStorage est vulnérable aux attaques de type Cross-Site Scripting (XSS). Si un attaquant parvient à insérer un script malveillant sur la page web, il peut accéder et récupérer les informations stockées dans le localStorage, y compris les JWT.

Cela peut permettre à l'attaquant d'usurper l'identité de l'utilisateur et d'effectuer des actions malveillantes en son nom.

Pour éviter cela, il est recommandé de stocker les JWT dans des cookies sécurisés, marqués avec l'attribut `HttpOnly`, qui empêchent les scripts côté client d'y accéder, et l'attribut `SameSite`, qui empêche les cookies d'être envoyés avec les requêtes cross-site.

Voir
- https://fr.wikipedia.org/wiki/Cross-site_scripting
- https://cheatsheetseries.owasp.org/cheatsheets/XSS_Filter_Evasion_Cheat_Sheet.html

:::



- Cependant nous allons l’utiliser pour étudier le fonctionnement de l’authentification dans une petite application.
- Par la suite vous pourrez étudier le fonctionnement des bibliothèques Vuex ou Pinia qui permettent de gérer et de centraliser l’état de toutes les données d’une application.

## Authentification
Le gros du travail sur l’authentification grâce au JWT a déjà été réalisé quand nous avons étudié Express avec Node.js.
Dans Vue.js, il faut mettre en place :
- Un formulaire de connexion, et d’inscription.
- Enregistrer le JWT qui est retourné par l’API, et le sauvegarder dans le localStorage.
- Joindre le JWT à chaque requête qui nécessite d’être identifié.
Voici un exemple pour la qui enregistrer le JWT qui est retourné par l’API, et le sauvegarder dans le localStorage

```js
export default {
  data() {
    return {
      email: '',
      password: '',
      errorMessage: ''
    }
  },
  methods: {
    login() {
      fetch('http://monapi.com/login', {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json'
        },
        body: JSON.stringify({
          email: this.email,
          password: this.password
        })
      })
      .then(response => {
        if (response.ok) {
          return response.json()
        } else {
          throw new Error('Impossible de se connecter')
        }
      })
      .then(data => {
        localStorage.setItem('jwt', data.jwt)
        this.$router.push('/')
      })
      .catch(error => {
        this.errorMessage = error.message
      })
    }
  }
}
```

Voici un exemple de code pour passer le JWT dans une requête avec fetch().

```js
getArticles() {
      fetch("http://monapi.com/articles/", {
        method: "GET",
        headers: {
          "Content-Type": "application/json",
          "Authorization": "Bearer " + localStorage.getItem('token'),
        },
      })
      .then(response => response.json())
      .then(data => {
        this.articles = data.posts
      })
    }
​
``` 


Pour supprimer un élément du localStorage vous pouvez utliser la méthode `localStorage.removeItem('item')`;

Voir les `demo13-Auth` et `demo13-api` pour des exemples plus complets.

## Décoder un JWT
Pour décoder le contenu d'une jeton JWT, on peut utiliser le package `jwt-decode` :

```js
import { jwt_decode } from 'jwt-decode';
​
// JWT à décoder
const token = 'votre_jwt';
​
// Décodage du JWT
const decoded = jwt_decode(token);
​
// Récupération des informations contenues dans le payload
const username = decoded.username;
const email = decoded.email;
const exp = decoded.exp; //<== date d'expiration du JWT
```
