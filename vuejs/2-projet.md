# Création d'un projet Vue.js

[Documentation](https://fr.vuejs.org/guide/quick-start.html#creating-a-vue-application)

```bash
npm create vue@latest
```

Cela permet la création du fichier `package.json` avec tous les modules nécessaires pour l'exécution de l'application.

![Création d'un projet Vue.js](./img/projet1.png)

Choisissez les options suivant les besoins de votre projet.

Dans un premier temps, vous pouvez choisir les options `router`, `ESLint` et `Prettier`.

Dans le dossier du projet, faire un `npm install` pour installer les dépendances puis `npm run dev` pour lancer l'application.

Vous devriez voir la page suivante dans votre navigateur :
![Page d'accueil de l'application](./img/projet3.png){ data-zoomable width=50% }

## Structure du projet

![Structure du projet](./img/projet2.png)

Voici une description de la structure de base pour les projets Vue.js :
- `node_modules`: dossier qui contient les dépendances installées via NPM
- `public`: dossier qui contient les fichiers statiques tels que les images, les fichiers CSS et les fichiers JavaScript externes
- `src`: dossier principal qui contient les fichiers sources du projet
  - `assets`: dossier qui contient les ressources telles que les images, les fichiers CSS et les fichiers JavaScript
  - `components`: dossier qui contient les composants Vue.js réutilisables
  - `views`: dossier qui contient les composants Vue.js qui sont spécifiques aux pages
  - `router`: dossier qui contient les fichiers de configuration du routeur pour l'application Vue.js
  - `App.vue`: fichier principal qui contient le composant racine de l'application Vue.js
  - `main.js`: fichier principal qui initialise l'application Vue.js et monte le composant racine
- `index.html` : point d'entrée de l'application dans le navigateur. Il contient le squelette de base de la page HTML et sert de conteneur pour l'application Vue.js.
- `package.json`: fichier qui contient les métadonnées du projet ainsi que les dépendances du projet
- `README.md`: fichier qui contient des informations sur le projet et sa configuration
- `vite.config.js` : fichier de configuration utilisé par l'outil de développement `Vite` pour configurer et personnaliser le comportement de l'application Vue.js pendant le développement et la production.

## Structure des fichiers .vue

```js-vue
<template>
  <!-- Le code HTML de l'application -->
</template>

<script>
export default {
  // Les propriétés de l'application
  data() {
    return {
      // Les données réactives de l'application
    };
  },
  // Les méthodes de l'application
  methods: {
    // Les fonctions de l'application
  },
  // Les propriétés calculées de l'application
  computed: {
    // Les propriétés calculées de l'application
  },
  // Les hooks du cycle de vie de l'application
  mounted() {
    // Le code à exécuter lorsque l'application est montée
  }
};
</script>

<style>
/* Le code CSS de l'application */
</style>
```

## Extension pour navigateur

Pour faciliter le développement, il est possible d'installer une extension pour le navigateur Chrome ou Firefox.

https://devtools.vuejs.org/guide/installation.html