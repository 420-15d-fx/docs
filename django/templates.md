---
prev:
  text: "Première vue et urls"
  link: '/django/premiere_vue'
next:
  text: 'Base de données'
  link: '/django/base_de_donnees'
---

# Template

## Architecture MVT
L’architecture MVT (modèle – Vue – Template) utilisée dans Django est un peu différente du MVC vue précédemment.
- Le modèle, similaire à MVC, agit comme une interface pour vos données.
- La vue exécute la logique métier, interagit avec le modèle et rend le Template. Elle reçoit les requêtes HTTP et renvoie ensuite les réponses HTTP.
- Le template est le composant qui rend MVT différent de MVC. Les templates agissent comme la couche de présentation et sont essentiellement le code HTML qui rend les données. Le contenu de ces fichiers peut être statique ou dynamique.

![Django - Architecture MVT](./img/mvt.png){data-zoomable}

- La View joue donc le rôle du contrôleur
- Le Template joue le rôle de la vue
- (cela n’est pas tout à fait vrai mais permet de faire une analogie avec l’architecture MVC)

## Création d’un Template

Un gabarit (template) contient la partie statique du résultat HTML souhaité ainsi qu’une certaine syntaxe particulière définissant comment insérer le contenu dynamique.

Les templates de chaque applications doivent être dans un sous-dossier templates.

Dans ce dossier templates, il faut créer un autre sous-dossier du nom de l’application. 

Exemple :

`blogue/templates/blogue`

Créer un fichier `index.html` dans ce dossier puis écrire du html dans ce fichier

![Django - Dossier templates/blogue](./img/image1.png){data-zoomable}

## INSTALLED_APPS

Par convention, Django recherche le sous-dossier `templates` dans chaque applications figurant dans `INSTALLED_APPS` du fichier `settings.py` du projet. N’oubliez pas d’ajouter vos applications dans cette liste.

![Django - installed_apps](./img/image2.png){data-zoomable}

## Utiliser le template

Pour utiliser le fichier index.html, il faut modifier la fonction index dans le fichier views.py

```py
def index(request):
    return render(request, "blogue/index.html")
```

On utilise maintenant la fonction `render()` qui prend deux paramètres :

* La requête
* Le chemin vers le template


## Transmettre des informations au template

La fonction `render()` peut prendre un troisième paramètre pour envoyer des variables au template.

```py
  articles = [
        {'title': 'Le cégep Garneau'},
        {'title': 'C\'est la rentrée !!!'}
    ]

def index(request): 
    return render(request, "blogue/index.html", {
        'articles' : articles
    })
```

Dans le template, on utilise les doubles accolades pour appeler une variable 
```
{{ variable à afficher }}
```


Et `{%` pour les déclaration if, for, while… `%}`

```html
<body>
  <h1>Titre</h1>
    <h2>{{ articles.0.title }}</h2>
    {% for article_item in articles %}
      <h3>{{ article_item.title }}</h3>
    {% endfor %}
</body>
```


Voir la [Documentation](https://docs.djangoproject.com/fr/5.0/ref/templates/builtins) pour la liste des balises utilisables dans un template

## transmettre de l'information à tous les templates

Dans Django, un `context_processor` a pour but de fournir des données supplémentaires au contexte de chaque template, sans avoir besoin de les ajouter explicitement dans chaque vue. Cela s'avère particulièrement utile pour les données qui doivent être accessibles à travers de nombreux templates dans une application Django.

Un `context_processor` est une fonction Python qui prend en entrée l'objet `HttpRequest` et retourne un dictionnaire de données à ajouter au contexte global des templates.**Les données retournées par tous les context_processors disponibles sont fusionnées et deviennent accessibles dans tous les templates.**

### Configuration

Pour utiliser un `context_processor`, vous devez l'ajouter à l'option `TEMPLATES` dans votre fichier de configuration `settings.py`. Plus précisément, il doit être inclus dans le dictionnaire `OPTIONS` sous la clé context_processors de la configuration BACKEND de vos templates Django. Voici un exemple :

```py

TEMPLATES = [
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        'DIRS': [],
        'APP_DIRS': True,
        'OPTIONS': {
            'context_processors': [
                'django.template.context_processors.debug',
                'django.template.context_processors.request',
                'django.contrib.auth.context_processors.auth',
                'django.contrib.messages.context_processors.messages',
                # Ajoutez ici votre context_processor personnalisé
                'blogue.context_processors.mon_context_processor',
            ],
        },
    },
]

```

### Création du context_processor

Pour créer un `context_processor` personnalisé, vous devez définir une fonction dans un fichier 
nommé **context_processors.py** au sein de votre application. Cette fonction doit accepter l'objet `HttpRequest` comme paramètre et retourner un dictionnaire contenant les données à ajouter au contexte. Par exemple :

```py
def mon_context_processor(request):
    # Logique pour déterminer les données à ajouter
    return {'ma_cle': 'ma_valeur'}

```
Une fois que vous avez configuré votre context_processor et redémarré votre serveur Django, les données qu'il ajoute au contexte sont disponibles dans tous les templates. Vous pouvez y accéder en utilisant la clé du dictionnaire que vous avez retourné. Reprenant l'exemple ci-dessus, dans un template, vous pourriez faire :

```py
<p>La valeur de ma clé est : {{ ma_cle }}</p>


```


## Passer des paramètres dans l’url

Exemple : `http://monsite.ca/blogue/article/2`

Modifier le fichier `urls.py` pour ajouter les balises

```py
urlpatterns = [
  path('', views.index, name='index'),
  path('article/<int:article_id>', views.article, name='articleDetails'),
  ]
```

 Django va appeler la fonction `article(request, article_id=2)`

On modifie maintenant la fonction `article` afin qu’elle reçoive le `article_id`

```py
def article(request, article_id):
    return render(request, "blogue/article_detail.html", {
        'article_id' : article_id
    })
```

Et on passe la variable au template article_detail.html (que vous pouvez créer!)

> Dans une vraie application, on irait chercher l’article correspondant dans la base de données.


## Type de paramètres

Les paramètres de chemin suivants sont disponibles par défaut :

* **str** - correspond à n’importe quelle chaîne non vide, à l’exception du séparateur de chemin, '/'. C’est ce qui est utilisé par défaut si aucun convertisseur n’est indiqué dans l’expression.
* **int** - correspond à zéro ou un autre nombre entier positif. Renvoie le type int.
* **slug** - correspond à toute chaîne composée de lettres ou chiffres ASCII, du trait d’union ou du caractère soulignement. Par exemple, construire-votre-1er-site-django.
* **uuid** - correspond à un identifiant UUID. Pour empêcher plusieurs URL de correspondre à une même page, les tirets doivent être inclus et les lettres doivent être en minuscules. Par exemple, 075194d3-6885-417e-a8a8-6c931e272f00. Renvoie une instance UUID.
* **path** - correspond à n’importe quelle chaîne non vide, y compris le séparateur de chemin, '/'. Cela permet de correspondre à un chemin d’URL complet au lieu d’un segment de chemin d’URL comme avec str.


## Créer des liens

Ajoutons des ids pour nos articles

```py

 articles = [
        {'title': 'Le cégep Garneau', 'article_id': 1},
        {'title': 'C\'est la rentrée !!!', 'article_id': 2}
    ]

def index(request):
    return render(request, "blogue/index.html", {
        'articles' : articles
    })
```

Pour éviter les problèmes, on utilise donc le champ `name` qui est défini dans les routes.

```html
<a href="{% url 'articleDetails' article_item.article_id %}">
  {{ article_item.title }}
</a>
```

:::tip Pratique
Dans la liste articles, ajoutez dans chaque dictionnaire un champ slug.

Exemple :
```
{'title': 'Le cégep Garneau', 'article_id': 1, 'slug': 'le-cegep-garneau'},
```
Utilisez le slug à la place de l’id pour accéder aux pages `/article/…`

Exemple :

http://localhost:8000/blogue/article/le-cegep-garneau
:::

## Le langage de gabarit de Django

Voir la [documentation](https://docs.djangoproject.com/fr/5.0/ref/templates/language/) afin d'avoir un aperçu du fonctionnement des gabarits.

## Fichiers statiques

Les sites Web ont généralement besoin de servir des fichiers supplémentaires tels que des images, du JavaScript ou du CSS. Dans Django, ces fichiers sont appelés « fichiers statiques ». Django met à disposition django.contrib.staticfiles pour vous assister dans cette gestion.

Pour les fichiers css ou javascript, on procède de la même façon que pour les templates.

Créer un sous-dossier static puis un sous-dossier du nom de l’application.

`blogue/static/blogue`

Créer des sous-dossiers scripts et styles.

![Django - Dossier scripts et styles](./img/image3.png){data-zoomable}

Dans le fichier html, on peut maintenant utiliser les fichiers css ou js comme dans l’exemple en utilisant l'instruction **{% load static %}** dans le haut du gabarit.

```html
{% load static %}
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <link rel="stylesheet" href="{% static 'blogue/styles/bootstrap.min.css' %}">
  <title>Document</title>
</head>
```

### Service des fichiers statiques en développement

Pour servir des fichiers statique en mode `DEBUG`, vous devez ajouter le code suivant dans le urlConf de votre application:

`blogue/urls.py`

```py
from django.conf import settings
from django.conf.urls.static import static

urlpatterns = [
    path('', views.index, name='index'),
    #Autres urls ici ....
]+ static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
```

### Service des fichiers statiques en PRODUCTION

Le serveur Web local de Django ne permet pas servir des fichiers statiques en mode production (DEBUG = False), car ceux-ci sont normalement servis par le server Web de production (Apache, Nginx, etc.).

Si vous désirez faire afficher les fichiers statiques en mode production sur votre server local, alors vous devez démarrer le serveur avec la commande suivante :

```sh
python manage.py runserver --insecure
```

:::tip Pratique
Ajouter le fichier css de bootstrap dans votre application et vérifier le bon fonctionnement
:::


## Héritage de gabarits

L'héritage permet d'éviter la répétition de code dans les pages html. [Documentation](https://docs.djangoproject.com/fr/5.0/ref/templates/language/#template-inheritance)

Il est possible de créer un gabarit de base contenant la structure et les éléments communs à toutes les pages de votre site web (header, nav, footer, etc.) et de définir des sections (block) que les gabarit enfants pourront remplacer.

Exemple de gabarit de base (blogue/base.html)

```html 
<!DOCTYPE html>
<html lang="en">
<head>
    <link rel="stylesheet" href="style.css">
    <title>{% block title %}{% endblock %}</title>
</head>
​
<body>
   <main>
        <div id="content">
            {% block content %}{% endblock %}
        </div>
   </main>
</body>
</html>
```

Dans cet exemple, la balise block définit trois blocs que les gabarits enfants peuvent remplir. La balise block ne fait que signaler au moteur de gabarits qu’un gabarit enfant peut surcharger ces portions du gabarit.

Un gabarit enfant pourrait ressembler à ceci :

```html
{% extends "blogue/base.html" %}
​
{% block title %}Mon super site Web{% endblock %}
​
{% block content %}
  {% for article in articles %}
      <h2>{{ article.title }}</h2>
      <p>{{ article.description }}</p>
  {% endfor %}
{% endblock %}
```

Attention, il est important d'inclure la balise `{% extends "blogue/base.html" %}` au début du gabarit afin de spécifier le gabarit de base utiliser. Il est ainsi possible de définir plusieurs gabarits de base pour une même application.

Voir la [documentation](https://docs.djangoproject.com/fr/5.0/ref/templates/language/#template-inheritance) pour d'autres balises utilisables dans l'héritage d'un template.

:::tip Pratique

Créer le gabarit de base nommé `base.html` dans le dossier `templates/blogue/base.html` contenant les blocs suivants :
- title : permettant de modifier le titre de la page
- content : permettant de modifier le contenu de la page

Ajuster les gabarits index.htm et article_detail.html afin qu'ils utilisent ce gabarit.
:::