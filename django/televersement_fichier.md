---
outline: deep
prev:
  text: "Message flash"
  link: '/django/message_flash'
next:
  text: 'En vrac'
  link: '/django/en_vrac'
---

# Téléversement de fichiers

## Configurations
### Installation de Pillow

Afin d'être en mesure de télécharger des images vous devez installer la librairy `pillow` 

```shell
pip install Pillow
```

### Modification du fichier de configuraiton

Tout d'abord, il faut indiquer au serveur l'endroit où seront stocké nos fichiers dans le fichier `settings.py` du projet

**monsupersite/settings.py**

```py

from os import path
​
...
​
MEDIA_URL = '/media/'
​
MEDIA_ROOT = BASE_DIR / "media"

```

`MEDIA_ROOT` est le chemin sur le serveur où les fichiers sont stockés.

`MEDIA_URL` est l'URL à travers laquelle les fichiers stockés peuvent être accessibles dans les navigateurs web. Si `MEDIA_URL` est défini sur **/media/**, et que vous avez une image téléversée nommée **image.jpg** stockée dans `MEDIA_ROOT`, l'URL pour accéder à cette image serait quelque chose comme http://www.monsite.com/media/image.jpg.


### Ajouter une URL de média aux URLs de Django

Vous devez ajouter l'url suivante aux URls du projet  :

**monsupersite/urls.py**

```py
from django.conf import settings
from django.conf.urls.static import static

urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)

```

Gardez à l'esprit que cela ne fonctionne que lorsque **DEBUG** est défini sur True et que l'URL spécifiée dans les paramètres est locale (c'est-à-dire **/media/**).

Désormais, lorsqu'une image sera est téléchargée en développement, elle est ajoutée à un répertoire multimédia situé dans **monsupersite/media** . 

## Teléversement d'une image à partir d'un formulaire

### Création du template

Créer le template **upload.htm**l dans l'appplication blogue. **Attention** de ne pas oublié de mettre l'attribut **enctype="multipart/form-data"** dans la balise **\<form>**. L'attribut **accept="image/*"** de la balise **​​​​​​​\<input>** permet d'appliquer un filtre dans la sélection du fichier.

```py
<div class="container">
  <h2>Téléversement d'une image</h2>
  <form method="post" enctype="multipart/form-data">
    {% csrf_token %}
    <div class="form-group">
      <input type="file" name="upload" accept="image/*">
    </div>
    <button class="btn btn-primary" type="submit">Soumettre</button>
  </form>
​
​
  <h2 class="my-4">Image téléversée</h2>
  <div class="row">
    <div>
      {% if file_url %}
        <img src="{{ file_url }}" class="img-fluid">
        <br>
      {% else %}
        <p>Aucune image n'a été téléversée</p>
      {% endif %}
    </div>
  </div>
</div>
```


### Création de la vue upload dans le fichier `views.py`

```py
from django.core.files.storage import FileSystemStorage
​
​
def upload(request):
    if request.method == 'POST' and request.FILES['upload']:
        upload = request.FILES['upload']
        fss = FileSystemStorage()
        file = fss.save(upload.name, upload)
        file_url = fss.url(file)
        return render(request, 'blogue/upload.html', {'file_url': file_url})
​
    return render(request, 'blogue/upload.html')
```

**FileSystemStorage()** permettra de sauvegarder le fichier.
    - La propriété location contient l'emplacement où sera copié le fichier (**MEDIA_ROOT** par défaut).
    - La propriété base_url contient l'uri pour accéder au fichier copié (**MEDIA_URL** par défaut).

### Ajout de l'URL aux URLs de l'application

```py
urlpatterns = [
    ...
    path("upload", views.upload, name="upload")
]
```

## Téléversement d'une image à partir d'un ModelForm

### Créer le modèle

Il est possible d'utiliser un ModelForm afin de conserver les informations sur un fichier téléversé dans un modèle. Pour ce faire, vous devez ajouter les champs suivants à votre modèle selon le besoin qu'il s'agisse d'une image ou d'un fichier.

```py

from django.db import models

​
class MonModele(models.Model):
    #Pour un fichier
    fichier = models.FileField(upload_to='documents/', blank=True, null=True)
    #Pour une image
    image = models.ImageField(upload_to='images/', blank=True, null=True)

```

**upload_to** télécharge automatiquement le fichier vers le **MEDIA_ROOT** spécifié dans les paramètres. 

L'emplacement de téléchargement n'est pas important, mais il doit être nommé de manière appropriée (c'est-à-dire les documents téléchargés dans le répertoire **/media/documents/** , les images dans le répertoire **/media/images/** ). 


### Création du template

```py
<div class="container">
  <h2>Téléversement d'une image</h2>
  <form method="post" enctype="multipart/form-data">
    {% csrf_token %}
    {{form}}
    <button class="btn btn-primary" type="submit">Soumettre</button>
  </form>
​
​
  <h2 class="my-4">Image téléversée</h2>
  <div class="row">
    <div>
      {% if modele.image %}
        <img src="{{ modele.image.url }}" class="img-fluid">
        <br>
      {% else %}
        <p>Aucune image n'a été téléversée</p>
      {% endif %}
    </div>
  </div>
</div>
```

### Création de la vue

```py
def upload(request):
    if request.method == "POST":
        form = UploadForm(request.POST, request.FILES)
        if form.is_valid():
            model = form.save()
            return render(request=request, template_name="mon_app/upload.html", context={'form': form, 'model': model})
          
    form = UploadForm()
    
    return render(request=request, template_name="mon_app/upload.html", context={'form': form})
```

:::tip Pratique
- Modifier le modèle **Article** afin de lui ajouter un champs de type **ImageField** nommé **image**
- Modifier le formulaire de création d'un article afin de permettre le téléversement d'image.
- Modifier le template **article_detail.html** afin d'afficher l'image de l'article
:::

