---
outline: deep
next:
  text: "ORM"
  link: '/django/orm'
prev:
  text: 'Base de données'
  link: '/django/base_de_donnees'
---
# Le site d’administration

[Documentation](https://docs.djangoproject.com/fr/5.0/ref/contrib/admin/)

L'administration est configurée automatiquement quand on utilise la commande `django-admin startproject monsupersite` pour démarrer un projet Django.

Dans le fichier `monsupersite/urls.py`, on trouve la route pour accéder à la partie administation


```py
#...
urlpatterns = [
  path('admin/', admin.site.urls),
#  ...

```


Si on se rend à l'adresse `127.0.0.1:8000/admin`, on doit s'identifier...

![Django - Login site admin](./img/screenshot_2021-08-29_at_23-53-48_log_in_django_site_admin.png){data-zoomable width=25%}

## Création du super-utilisateur

```shell
$ python manage.py createsuperuser
```

et répondre aux questions...

Vous pouvez maintenant vous connecter avec les identifiants que vous venez de choisir

![Django - Administration](./img/screenshot_2021-08-29_at_23-58-19_site_administration_django_site_admin.png){data-zoomable width=50%}

- Par défaut, il est possible de créer, supprimer ou modifier des utilisateurs et des groupes.
- Une gestion des permissions est également proposée afin d'autoriser certains utilisateurs à faire certaines actions.
- Il est possible de définir des groupes afin de configurer les permissions pour un ensemble d'utilisateur.

## Ajout des modèles dans l'administration

Afin d'utiliser les modèles de nos applications, il faut les déclarer dans le fichier `admin.py` de chaque application.

Exemple pour ajouter les modèles `Category` et `Article`.

`Fichier /blogue/admin.py`

```py
from django.contrib import admin

# Register your models here.
from .models import Category, Article

admin.site.register(Category)
admin.site.register(Article)
```

![Django - Selectionner une catégorie](./img/screenshot_2021-08-30_at_08-55-05_select_category_to_change_django_site_admin.png){data-zoomable  width=50%}

## Contrôle de l'affichage des champs

Définir `list_display` pour contrôler quels champs sont affichés sur la page de liste pour modification de l’interface d’administration.

Exemple pour afficher le titre et la description d'une catégorie

```py
...
class CategoryAdmin(admin.ModelAdmin):
  list_display = ('title', 'description') 
  
admin.site.register(Category, CategoryAdmin)
...
```


![Django - Affichage des champs](./img/screenshot_2021-08-30_at_08-59-28_select_category_to_change_django_site_admin.png){data-zoomable  width=50%}

Si `list_display` n'est pas défini, le site affichera une seule colonne avec la représentation `__str__()` de chaque objet.

Cette méthode peut être défini dans les modèles.

Exemple fichier models.py :

```py
...
class Category(models.Model):
  title = models.CharField(max_length=255)
  description = models.TextField(blank=True)

  def __str__(self):
        return '%s => %s' % (self.title, self.description)
```

![Django - Affichage des champs](./img/screenshot_2021-08-30_at_09-18-20_select_category_to_change_django_site_admin.png){data-zoomable  width=50% }

Il est également possible d'utiliser `__str__` dans `list_display`

```py
...
class CategoryAdmin(admin.ModelAdmin):
  list_display = ('__str__', 'description')
...
```

### Modifier l'affichage du nom des colonnes

Il est également possible de modifier l'affichage du nom des colonnes. Pour ce faire, il suffit de créer une méthode qui retourne le nom de la colonne et utilser cette méthode dans `list_display` et de définir `short_description` pour cette méthode : 

```py
...
class ArticleAdmin(admin.ModelAdmin):
    list_display = ('display_title', 'slug', 'content', 'is_draft', 'created_at')

    def display_title(self, obj):
        return obj.title

    display_title.short_description = 'Titre'  # Renomme la colonne 'title'
...
```

:::tip Pratique
- Créer un compte super-utilisateur
- Ajouter une catégorie
- Modifier le modèle Category pour lui ajouter la méthode __str__.
- Ajouter un article
- Modifier le fichier admin.py pour faire afficher tous les champs de la table Category.
- Modifier le fichier admin.py pour faire afficher tous les champs sauf categories de la table Article.
:::