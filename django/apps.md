---
prev:
  text: 'Installation de Django'
  link: '/django/install'
next:
  text: "Première vue et urls"
  link: '/django/premiere_vue'
---

# Création d'une application

> Les informations de ce cours proviennent en partie de la documentation de Django

[https://docs.djangoproject.com/fr/5.0/](https://docs.djangoproject.com/fr/5.0/)

Un projet Django est subdivisé en application (ou apps).

Une **application** est une application Web qui fait quelque chose – par exemple un système de blogue, une base de données publique ou une petite application de sondage. 

Un **projet** est un ensemble de réglages et d’applications pour un site Web particulier. Un projet peut contenir plusieurs applications. Une application peut apparaître dans plusieurs projets.

Exemple: un projet de boutique en ligne pourrait contenir plusieurs apps
 * le panier d'achat
 * la présentation des produits
 * le module de paiement
 * le module pour administrer les clients
 * ...

Nous allons créer une application pour faire un blogue par exemple.
Pour créer votre application, **assurez-vous d’être dans le même répertoire que** `manage.py` et saisissez cette commande :

```
$ python manage.py startapp blogue
```

Cela va créer un dossier `blogue` structuré de la façon suivante :

```
blogue/
├── admin.py
├── apps.py
├── __init__.py
├── migrations
│   └── __init__.py
├── models.py
├── tests.py
└── views.py
```

La structure du projet complet doit être maintenant comme ceci:

```
monsupersite/
├── blogue
│   ├── admin.py
│   ├── apps.py
│   ├── __init__.py
│   ├── migrations
│   │   └── __init__.py
│   ├── models.py
│   ├── tests.py
│   └── views.py
├── manage.py
└── monsupersite
    ├── asgi.py
    ├── __init__.py
    ├── __pycache__
    │   ├── __init__.cpython-38.pyc
    │   └── settings.cpython-38.pyc
    ├── settings.py
    ├── urls.py
    └── wsgi.py
```
	
