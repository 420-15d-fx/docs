---
next:
  text: "Le site d'administration"
  link: '/django/le_site_d_administration'
prev:
  text: 'Template'
  link: '/django/templates'
---
# Base de données

Django prend officiellement en charge les bases de données suivantes :

  * PostgreSQL
  * MariaDB
  * MySQL
  * Oracle
  * SQLite

[Documentation](https://docs.djangoproject.com/fr/5.0/ref/databases/)

## Configuration

Par défaut, c'est sqlite qui est configuré dans le fichier `settings.py`

Nous utiliserons cette base pour le cours

```py
DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.sqlite3',
        'NAME': BASE_DIR / 'db.sqlite3',
    }
}
```


## INSTALLED_APPS

Afin d'utiliser toutes les fonctionnalités liées aux bases de données, il faut que l'application soit inscrite dans la liste `INSTALLED_APPS` du fichier `settings.py` du projet. N’oubliez pas d’ajouter vos applications dans cette liste.

 ![Django - installed_app](./img/image2.png){data-zoomable}

## Création des tables de bases de données

3 étapes :

 * Créer un modèle
 * Créer une migration
 * Exécuter la migration

### Créer un modèle

[Documentation](https://docs.djangoproject.com/fr/5.0/topics/db/models/)

Exemple d'un modèle pour des articles de blogue

Fichier `blogue/models.py`

```py
from django.db import models

# Create your models here.
class Article(models.Model):
  title = models.CharField(max_length=255)
  slug = models.SlugField(default='')
  content = models.TextField(blank=True)
  is_draft = models.BooleanField(default=False)
  created_at = models.DateTimeField(auto_now_add=True)
```

Création d'une classe qui hérite de `models.Model`. Cette classe définit une table de la base de données

Définition des champs de la table avec le type et les options. 

  * [Documentation](https://docs.djangoproject.com/fr/5.0/ref/models/fields/#model-field-types) pour les types de champs.

  * Voir aussi ce [tableau](https://python.doctor/page-django-champs-modeles-orm-model) récapitulatif des types de champs disponibles

### Créer une migration

[Documentation](https://docs.djangoproject.com/fr/5.0   /topics/migrations/)

Les fichiers migrations permettent d'enregistrer les caractéristiques des tables de notre base de données. Ils définissent le "plan des tables".

Nous devons donc créer une migration chaque fois que nous apportons un changement à nos modèles.

Ces migrations sont stockées dans le dossier `migrations` de chaque application

exemple: `blogue/migrations`

Pour créer les fichiers de migration, exécuter par exemple :

```shell
$ python manage.py makemigrations 

# ou pour une application en particulier
$ python manage.py makemigrations blogue
```

(Voir le dossier `migrations` pour avoir un aperçu des fichiers créer par la migration)

### Exécuter la migration

```shell
$ python manage.py migrate

# ou pour une application en particulier
$ python manage.py migrate blogue
```

Cela va créer la ou les tables dans la base de données

> Vérifier votre base de données afin de contrôler que la table a été créée correctement.

Si besoin, on peut afficher le code SQL correspondant à une migration. Exemple pour la migration 0001 de la table `blogue`

```shell
python manage.py sqlmigrate blogue 0001
```

## Visualisation la base de donnée dans Pycharm

Pour visualiser la base de données dans PyCharm, vous devez double-cliquer sur le fichier db.sqlite3. La première fois, la fenêtre suivante s'affichera pour la configuration de la source de données. Vous n'avez qu'à cliquer sur le bouton ok :

![Pycharm](./img/pycharm1.png)

Par la suite, pour afficher la base de donnnées sélectionner dans la barre de menu View-->Tools window-->Database. 

![Pycharm](./img/pycharm2.png)

## Visualisation la base de donnée dans VSCode

Pour visualiser la base de données dans VSCode, vous pouvez installer une extension, par exemple: [sqlite-viewer](https://marketplace.visualstudio.com/items?itemName=qwtel.sqlite-viewer)



## Inversion des migrations

Les migrations peuvent être inversées avec migrate en passant le numéro de la migration précédente. Par exemple, pour inverser la migration `blogue.0002`


```shell
$ python manage.py migrate blogue 0002
```

Pour défaire toutes les migrations appliquées d’une application, utilisez le terme `zero` 

```shell
$ python manage.py migrate blogue zero
```

## Relations

### Relations plusieurs-à-un

[Documentation](https://docs.djangoproject.com/fr/5.0/topics/db/models/#many-to-one-relationships)

Exemple: un article peut avoir une catégorie et une catégorie peut appartenir à plusieurs articles

Mise en place d'une clé étrangère dans la table `article`.

```python
class Category(models.Model):
  title = models.CharField(max_length=255)
  description = models.TextField(blank=True)

class Article(models.Model):
  title = models.CharField(max_length=255)
  slug = models.SlugField(default='')
  content = models.TextField(blank=True)
  is_draft = models.BooleanField(default=False)
  created_at = models.DateTimeField(auto_now_add=True)
  category = models.ForeignKey(Category, on_delete=models.PROTECT)
```

[Documentation de `ForeignKey`](https://docs.djangoproject.com/fr/5.0/ref/models/fields/#foreignkey )

### Relations plusieurs-à-plusieurs

[Documentation](https://docs.djangoproject.com/fr/5.0/topics/db/models/#many-to-many-relationships)

Exemple: un article peut avoir plusieurs catégories et une catégorie peut appartenir à plusieurs articles.

Mise en place d'une table de jointure `article_categories`.

```python
class Category(models.Model):
  title = models.CharField(max_length=255)
  description = models.TextField(blank=True)

class Article(models.Model):
  title = models.CharField(max_length=255)
  slug = models.SlugField(default='')
  content = models.TextField(blank=True)
  is_draft = models.BooleanField(default=False)
  created_at = models.DateTimeField(auto_now_add=True)
  categories = models.ManyToManyField(Category)
```

On peut voir que la table `blogue_article_categories` a été créée.


![Django - Base de données](./img/capture.png){data-zoomable}

## Sauvegarde de la BD dans un fichier

[Documentation](https://docs.djangoproject.com/fr/5.0/ref/django-admin/#dumpdata)

```shell
# Enregistre dans un fichier json
python manage.py dumpdata -o mydata.json

# Enregistre dans un fichier json au format compressé
python manage.py dumpdata -o mydata.json.gz
```

## Charger les données dans la BD à partir d'un fichier de sauvegarde

[Documentation](https://docs.djangoproject.com/fr/5.0/ref/django-admin/#loaddata|documentation)


```shell
python manage.py loaddata mydata.json
```
