---
prev:
  text: "Authentification"
  link: '/django/auth'
next:
  text: 'Téléversement de fichiers'
  link: '/django/televersement_fichier'
---

# Message Flash

[Documentation](https://docs.djangoproject.com/fr/5.0/ref/contrib/messages)

L’infrastructure des messages permet de stocker temporairement des messages dans une requête et de les récupérer ensuite dans une prochaine requête (en générale la suivante) pour les afficher. Chaque message est étiqueté avec un niveau spécifique déterminant sa priorité (par ex. info, warning ou error).

## Niveaux de messages

  * DEBUG : 	Messages liés au développement qui seront ignorés (ou supprimés) dans un déploiement en production.
  * INFO : 	Messages d’information pour l’utilisateur.
  * SUCCESS : Une action a réussi, par ex. « votre profil a été mis à jour avec succès ».
  * WARNING : Il n’y a pas eu d’erreur, mais il pourrait y en avoir une sous peu.
  * ERROR : 	Une action n’a pas réussi ou une autre erreur quelconque s’est produite.

## Ajout de message

Par exemple dans une fonction de `views.py` :

```py
from django.contrib import messages
...

messages.add_message(request, messages.INFO, 'Hello world.')
```

Le message est enregistré au niveau de la session et va s'afficher dans le prochain template qui sera appelé.

## Affichage des messages¶

Dans un template :

```html
{% if messages %}
<ul class="messages">
    {% for message in messages %}
    <li{% if message.tags %} class="{{ message.tags }}"{% endif %}>{{ message }}</li>
    {% endfor %}
</ul>
{% endif %}
```

`message.tags` contient la priorité du message (info, success, warning, error) et permet de l'utiliser pour styliser le message.

Exemple avec bootstrap
```html
<div {% if message.tags %} class="alert alert-{{ message.tags }}"{% endif %}>
```

:::tip Pratique
- Supprimer les templates article_confirm.html et l'URL correspondante.
- Modifier la vue article_new afin qu'elle ajoute un message flash de confirmation pour l'ajout d'un article et redirige vers la page d'accueil.
- Modifier le template de la page d'accueil pour qu'il affiche les messages flash.
:::