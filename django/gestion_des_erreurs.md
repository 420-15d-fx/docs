---
prev:
  text: "ORM"
  link: '/django/orm'
next:
  text: 'Les formulaires'
  link: '/django/forms'
---

# Gestion des erreurs

Django fournit des gabarits de base pour afficher les erreurs. Nous verrons comment personnaliser ces gabarits vers la fin de ce cours.

## Configuration fichier `settings.py`

Afin de tester les pages d'erreurs, il faut passer la variable `DEBUG` à `False`

Il faut également définir l'adresse IP sur laquelle tourne le serveur :

`ALLOWED_HOSTS = ['127.0.0.1', '.localhost']`

Sur un serveur en production, il faut mettre le nom de domaine du site.

## Renvoi d’erreurs¶

[Documentation](https://docs.djangoproject.com/fr/5.0/topics/http/views/#returning-errors)

Utilisation des sous-classes de HttpResponse pour tous les codes de statut HTTP les plus courants autres que 200.

La liste complète des sous-classes disponibles se trouve dans la [documentation](https://docs.djangoproject.com/fr/5.0/ref/request-response/#httpresponse-subclasses) des requêtes/réponses.

Renvoyez une instance de l’une de ces sous-classes au lieu d’une réponse HttpResponse normale afin de signaler une erreur. Par exemple :

```py
from django.http import HttpResponse, HttpResponseNotFound

def my_view(request):
    # ...
    if foo:
        return HttpResponseNotFound('<h1>Page not found</h1>')
    else:
        return HttpResponse('<h1>Page was found</h1>')

```


## try ... except Model.DoesNotExist

[Documentation](https://docs.djangoproject.com/fr/5.0/ref/models/class/#doesnotexist)

Quand on fait une recherche dans la base de données et qu'il n'y a pas de résultat, on peut utiliser le `try...except` pour lever une erreur.

```py
from django.http import Http404

def category(request, id):
    try:
        category = Category.objects.get(id=id)
    except Category.DoesNotExist:
        raise Http404("Pas de categorie")

    return render(request, 'blogue/categories.html', {
        'category': category
    })
```

## get_object_or_404()

[documentation](https://docs.djangoproject.com/fr/5.0/topics/http/shortcuts/#get-object-or-404)

`get_object_or_404()` est un raccourci. Voici un exemple :

```py
from django.shortcuts import get_object_or_404

def my_view(request):
    # Trouve l'objet sinon retourne une erreur 404
    article = get_object_or_404(Article, id=id)
    
    return render(request, 'blogue/articles.html', {
        'article': article
    } )
```

Au lieu de 

```py
from django.http import Http404

def article(request, id):
    try:
        article = Article.objects.get(id=id)
    except Article.DoesNotExist: 
        raise Http404("Pas d'articles")
    
    return render(request, 'blogue/articles.html', {
        'article': article
    } )
```
    

## Gabarits personnalisés

Il est possible de créer des gabarits personnalisés nommés avec le numéro de l'erreur. 

Exemple : `404.html`, `500.html`

Les placer au premier niveau de l’arborescence des gabarits. Ces gabarits seront utilisés **lorsque le réglage DEBUG est défini à False**.

![Django - Page 404](./img/404.png){data-zoomable}


Dans les gabarits, il est possible d'afficher le message de l'exception si on le souhaite en utilisant `{{ exception }}`

```html
<div class="error">
  <h1>404</h1>
  <p>Désolé ...</p>
  <p>{{ exception }}</p>
</div>
```

![Django - Page 404](./img/screenshot_2021-09-02_at_21-40-41_document.png){data-zoomable width=25%}


:::tip Pratique
- Créer une page 404 qui utilise votre gabarit de base et qui affiche un message provenant d'une vue.
- Modifier le vue 'article_detail' afin qu'elle affiche la page 404 si aucun article n'est trouvé.
:::