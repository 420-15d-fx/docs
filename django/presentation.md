---
next:
  text: 'Installation de django'
  link: '/django/install'
---

# Présentation de Django

> « Le framework web pour les perfectionnistes sous pression »


- Créé en 2005
- Python
- Framework open-source orienté « Web »
- Modèle de conception MVC
- « Batteries included » : ORM, authentification, bibliothèques HTTP, prise en charge multisite, i18n, administration, moteur de modèles…
- Bonne documentation, en français !
- Populaire : Pinterest, Instagram, Mozilla, Bitbucket, Udemy, Spotify…

https://www.djangoproject.com/

## Environnement de développement
- PyCharm de JetBrains https://www.jetbrains.com/pycharm/
  - Environnement professionnel
  - Payant, mais gratuit pour les étudiants avec votre courriel du cégep
- Vscode de Microsoft https://code.visualstudio.com/
  - Extensions conseillées :
    - Python (Pylance, Linting, auto complétion…)
    - Beaucoup d’extensions Django sur le MarketPlace pour faciliter l’écriture du code (non conseillé pour débuter)
  - Choisir le bon interpréteur Python


## Source & en savoir plus

- https://docs.djangoproject.com/fr/4.2/
- https://medium.com/crowdbotics/when-to-use-django-and-when-not-to-9f62f55f693b
- https://python.doctor/page-django-introduction-python
- http://www.formation-django.fr/generalites/principales-caracteristiques.html

