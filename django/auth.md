---
next:
  text: "Message flash"
  link: '/django/message_flash'
prev:
  text: 'Les formulaires'
  link: '/django/forms'
---
# Authentification

Lors de l'exécution de la première migration, Django a créé plusieurs tables permettant de gérer l'authentification dans notre application. Ces tables sont d'ailleurs accessible via le site d'administration.

![Django - Tables authentification](./img/django-admin1.png)

- **Users**

Contient la liste des utilisateurs de l'application. Celle-ci contient les champs les plus courants pour la gestion d'authentification tel que :
    - Prénom
    - Nom
    - Courriel
    - Mot de passe
    - Date de création
    - Dernière connexion
    - Permissions 

- **Groups**

Les groupes sont un moyen générique de catégoriser les utilisateurs afin que vous puissiez appliquer des permission aux utilisateurs pour l'utilisation du site d'administration. Un utilisateur peut appartenir à n'importe quel nombre de groupes. Un utilisateur d'un groupe dispose automatiquement des autorisations accordées à ce groupe. 

- **Rôles**

Les rôles utilisateur sont comme les *groups*, mais ceux-ci se utilisés du côté de l'application. Supposons une application pour un hôpital. Vous pourriez avoir plusieurs type d'utilisateurs. Comme dans un hôpital, plusieurs rôles existent comme un médecin, une infirmière, un préparateur, etc... alors vous aurez probablement besoin d'un rôle différent pour toutes les utilisateur à catégoriser.

## Comment utiliser les rôles d'utilisateur

Pour utiliser les rôles, vous devez étendre `AbstractUser` (nous y reviendront un peu plus loin) dans votre `models.py` et définir les rôles comme suit :

```py
from django.contrib.auth.models import AbstractUser
from django.db import models 
​
class User(AbstractUser):
      DOCTEUR = 1
      INFIRMIERE = 2
      CHIRURGIEN =3
      
      ROLE_CHOICES = (
          (DOCTEUR, 'Docteur'),
          (INFIRMIERE, 'Infirmière'),
          (CHIRURGIEN, 'Chirurgien'),
      )
      role = models.PositiveSmallIntegerField(choices=ROLE_CHOICES, blank=True, null=True)
```
   
Vous pouvez ensuite définir le rôle d'un utilisateur et le vérifier comme suit :

```py
user = User.objects.first() 
user.role = User.INFIRMIERE 
user.save() 
user.role == User.DOCTEUR 
False 
user.role == User.INFIRMIERE 
True
```

### Utilisation d’un modèle d’utilisateur personnalisé au départ d’un projet¶

[Documentation](https://docs.djangoproject.com/fr/5.0/topics/auth/customizing/#substituting-a-custom-user-model)

Si vous démarrez un nouveau projet, il est fortement recommandé de définir un modèle d’utilisateur personnalisé, même si le modèle `User` par défaut convient à vos besoins. Ce modèle se comportera comme le modèle d’utilisateur par défaut, mais vous serez en mesure de le personnaliser plus tard si le besoin devait se présenter.

**Il faut mettre en place cette configuration au démarrage d'un nouveau projet, avant de faire la première migration.**

```py
from django.contrib.auth.models import AbstractUser

class User(AbstractUser):
    pass
    #Vous pouvez ajouter vos chamnps personnalisé ici.
```


Dans le fichier `settings.py`, définir le modèle à utiliser pour la gestion des utilisateurs. Exemple :
```py
AUTH_USER_MODEL = 'blogue.User'
```

Après avoir créer et exécuter une migration, il y aura maintenant une table `blogue_user` dans la base de données qui sera utilisée par Django pour la gestion des utilisateurs.


![Django - Blogue user](./img/ksnip_20220829-151529.png){data-zoomable}


Pour être en mesure de voir appraître la table sur le site d'administration, vous devez inscrire le modèle dans le fichier admin.py de l'application : :

```py
from django.contrib import admin
from .models import User

admin.site.register(User)
```

la gestion des utilisateurs est maintenant dans la partie Blogue de l'administration.

 ![Django - Gestion utilisateurs](./img/ksnip_20220829-151904.png){data-zoomable}

 :::tip Pratique
- Supprimer toutes migrations contenues dans dossier migrations de votre applicatoin (ne pas supprimer le fichier __inti__.py)
- Supprimer le fichier db.sqlite3
- Créer un modèle personnalité nommé User et lui ajouter le champ Telephone et assurez-vous que la table s'affiche sur le site d'administration.
- Créer et exécuter une migration 
- Créer un super-utilisateur
- Se connecter au site d'administration et vérifier les champs de la table Users.​​​​​​​
:::

## Vues d'authentification

[Documentation](https://docs.djangoproject.com/fr/5.0/topics/auth/default/#module-django.contrib.auth.views)

Django fournit plusieurs vues permettant de gérer la connexion, la déconnexion et le traitement des mots de passe.

Nous aurons seulement à créer les templates, toute le reste de la logique est déjà implémentée par Django. 

Pour les utiliser, il faut ajouter la route `accounts` dans le fichier `urls.py` du projet.

```py
urlpatterns = [
    path('admin/', admin.site.urls),
    path('blogue/', include("blogue.urls")),
    path('accounts/', include('django.contrib.auth.urls')),
]
```

Cela comprendra les modèles d’URL suivants

```py
accounts/login/ [name='login']
accounts/logout/ [name='logout']
accounts/password_change/ [name='password_change']
accounts/password_change/done/ [name='password_change_done']
accounts/password_reset/ [name='password_reset']
accounts/password_reset/done/ [name='password_reset_done']
accounts/reset/<uidb64>/<token>/ [name='password_reset_confirm']
accounts/reset/done/ [name='password_reset_complete']
```


Il faut ensuite créer les `templates` que l'on souhaite utiliser.

Dans le fichier de `settings.py`, ajouter le chemin où vont se trouver les `templates` de login, logout, traitement des mots de passe... 

```py
TEMPLATES = [
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        'DIRS': [BASE_DIR / 'templates'],
        'APP_DIRS': True,
```

Créer les dossiers `templates/registration`directement à la racine du projet.


![Django - Dossier templates](./img/ksnip_20220829-133349.png){data-zoomable}

C'est dans ce dossier que l'on crée les fichiers `login.html`, `logout.html`, `password_change`, etc. 

Le nom des template doit respecter le nom des URL.

Pour le moment, on utilise seulement un fichier `login.html`

La vue Login est accessible avec l'url `http://127.0.0.1:8000/accounts/login/`

Exemple de fichier login.html

```py
<form method="post">
  {% csrf_token %}
  {{ form.as_p }}
  <button class="btn btn-primary" type="submit">
    Soumettre
  </button>
</form>
```

## Modification des urls pour login, logout...

Dans le fichier `settings.py`, vous pouvez changer les valeurs par défaut de quelques constantes :

**`LOGIN_REDIRECT_URL`**¶

Valeur par défaut : `'/accounts/profile/'`

Si un utilisateur se connecte, il est redirigé par défaut sur la page `accounts/profile/`

Si vous n'avez pas de page "profile", vous pouvez le rediriger sur la page d'accueil par exemple

```
LOGIN_REDIRECT_URL = '/'
```

**`LOGIN_URL`**¶

Valeur par défaut : `'/accounts/login/'`

L’URL ou le motif d’URL nommé vers lequel seront redirigées les requêtes pour la connexion.


**`LOGOUT_REDIRECT_URL`**¶

Valeur par défaut : None

L’URL ou le motif d’URL nommé vers lequel seront redirigées les requêtes après la déconnexion. Avec None, aucune redirection n’est effectuée et la vue de déconnexion logout sera produite.

Si vous souhaitez rediriger vers la page de login par exemple :

```
LOGOUT_REDIRECT_URL = '/accounts/login/'
```

:::tip Pratique
- Ajouter une page de connexion à l'application Blogue
- Ajouter un lien dans l'en-tête du site permettant d'accéder au formulaire de connexion. Suite à une connexion, l'utilisateur doit être redirigé vers la page d'accueil
- Ajouter également un lien permettant à un utilisateur de se déconnecter. Suite à une déconnexion, l'utilisateur est redirigé vers la page de connexion.
:::


## Données d’authentification dans les gabarits

[Documentation](https://docs.djangoproject.com/fr/5.0/topics/auth/default/#authentication-data-in-templates)

Dans un gabarit, pour vérifier si un utilisateur est connecté : 

```py
{% if user.is_authenticated %}
    <p>Bienvenue, {{ user.username }}</p>
{% else %}
    <p>Bienvenue, veuillez vous connecter</p>
{% endif %}
```


## Restriction d’accès aux utilisateurs connectés¶

La manière directe de limiter l’accès à certaines pages dans `views.py` est de vérifier `request.user.is_authenticated`et de rediriger l’utilisateur vers la page de connexion le cas échéant :

```py
from django.conf import settings
from django.shortcuts import redirect

def my_view(request):
    if not request.user.is_authenticated:
        return redirect('login')
    # ...
```

…ou d’afficher un message d’erreur :

```py
from django.shortcuts import render

def my_view(request):
    if not request.user.is_authenticated:
        return render(request, 'myapp/login_error.html')
    # ...
```

## Le décorateur login_required

Pour s'implifier la vérification sur l'authentification, Django offre comme raccourci l'utilisation du décorateur login_required() :
from django.contrib.auth.decorators import login_required
​
```py
@login_required
def une_vue(request):
  ...
```

- Si l’utilisateur n’est pas connecté, il redirige vers `LOGIN_URL` spécifié dans le fichier `settings.py` en transmettant le chemin absolu dans la chaîne de requête. Exemple : `/accounts/login/?next=/polls/3/`.
- Si l’utilisateur est connecté, il exécute la vue normalement. 

`login_required()` accepte également un paramètre facultatif `login_url`. Exemple `

```py
from django.contrib.auth.decorators import login_required
​
@login_required(login_url='/accounts/login/')
def une_vue(request):
    ...
```

:::tip Pratique
- Modifier l'en-tête du site afin d'afficher le lien "se connecter" si l'utilisateur n'est pas authentifier ou le lien "se déconnecter" s'il ne l'est pas.
- Modifier le template index.html afin d'afficher les liens permettant la création d'un article et d'une catégorie seulement si l'utilisateur est connecté.
- Modifier les vues article_new et category_new afin que seul un utilisateur connecté puisse accéder à ces vues.
:::

## Création d'un compte utilisateur

Django est livré avec un formulaire d'enregistrement d'utilisateur intégré. Nous pouvons implémenter une inscription simple en utilisant le formulaire d'authentification `UserCreationForm`. Par défaut, celui-ci permet de créer un utilisateur en utilisant uniquement un nom d'utilisateur et un mot de passe. Il est possible de le configurer selon nos besoins en spécifiant d'autres champs dans le fichier `forms.py` :

```py
from django.contrib.auth.forms import UserCreationForm
​
class RegisterForm(UserCreationForm):
   
    class Meta:
        model = User
        fields = ("username", "email", "password1", "password2")
```

Tout comme le formulaire d'authentification, vous devez créer un template `register.html` à la racine du projet dans le dossier `/registration`. Voici un exemple de template:

```py
<form method="post" novalidate>
  {% csrf_token %}
  {{ form.as_div }}
  <button class="btn btn-primary" type="submit">Soumettre</button>
</form>
```

Il faut ensuite créer la vue permettant de créer l'utilisateur :

```py
from .forms import RegisterForm
​
def user_new(request):
    form = RegisterForm(request.POST)
    if request.method == 'POST':
        if form.is_valid():
            form.save()
            return redirect('login')
    else:
        form = RegisterForm()
​
    return render(request, 'registration/register.html', {'form': form})
```

Finalement, on doit ajouter l'URL dans le fichier `urls.py` du projet 
`/monsupersite/urls.py`

```py
from blogue import  views
#...
​
urlpatterns = [
    # ...
    path('accounts/register/',  views.user_new, name='user_new'),
]
```

:::tip Pratique
- Ajouter une page d'inscription à l'application Blogue
- Pour s'inscrire, un utilisateur doit fournir les informations suivantes : nom d'utilisateur, courriel et mot de passe.
- Ajouter un lien dans le template login.thml permettant d'accéder au formulaire d'inscription. Suite à une inscription, l'utilisateur doit être redirigé vers la page de connexion.
:::




