---
prev:
  text: "Authentification"
  link: '/django/auth'
next : false
---


# En vrac

## Convention de nommage

Voici la convention de nommage des URLs, Vue et Template que nous utiliserons pour le cours :

### URLs
- **Liste** :  /employes/
- **Création** : / employe/new/
- **Détail** :  / employe/1/
- **Mise à jour** : / employe/1/edit/
- **Suppression** : / employe/1/delete/
- **N'importe quelles méthodes qui ne dépendant pas d'un model** : / employe /action/
- **N'importe quelles méthodes sur un modèle** : / employe /1/ action /

### Views

- **Liste** :  employe_list
- **Création** : employe_new
- **Détail** :  employe_detail
- **Mise à jour** : employe_edit
- **Suppression** : employe_delete
- **N'importe quelles méthodes sur un modèle** : modele_action

### Template

- **Liste** :  employe_list.html
- **Création** : employe_new.html
- **Détail** :  employe_detail.html
- **Mise à jour** : employe_edit.html
- **Suppression** : employe_delete.html
- **N'importe quelles méthodes sur un modèle** : modele_action.html

## Variables de configuration
Vous pouvez créer vos propres variables de configuration dans le fichier settings.py et les utiliser dans votre code :

- Fichier settings.py​​​​​​​

```py
API_KEY = "UinVRGhVDWLoo0nn0497giMnyA"
```
Exemple d'utlisation dans une vue
```py
from monsupersite import settings
​
api_key = settings.API_URL
```

## I18n (Internationalisation)
[Documentation](https://docs.djangoproject.com/fr/4.2/topics/i18n/)

Pour avoir le formatage des dates et la partie admin en français :

Dans le fichier `settings.py`

```
LANGUAGE_CODE = 'fr-ca'
```


## Time Zone

il est possible de changer la "time zone" qui est configurée sur UTC par défaut

[Documentation](https://docs.djangoproject.com/fr/4.2/ref/settings/#time-zone)

```py
TIME_ZONE = 'UTC'
```

Exemples de time zone:
```py
'Europe/London'
'Europe/Paris'
'America/New_York'
```

[Liste des time zones](https://en.wikipedia.org/wiki/List_of_tz_database_time_zones#WARSAW)

## Utilisation d'une API avec le format JSON

Pour cette exemple on utilise l'api https://jsonplaceholder.typicode.com qui permet d'avoir des données de tests au format JSON.

  * Le module [urlib.request](https://docs.python.org/3/library/urllib.request.html) permet de créer des requêtes.
  * Le module [Json](https://docs.python.org/3/library/json.html) permet de travailler avec le format JSON.


```py
import urllib.request 
import json

# requête pour récupèrer un utilisateur
res = urllib.request.urlopen('https://jsonplaceholder.typicode.com/users/1')

# désérialise le JSON pour pouvoir l'utiliser comme un objet
json_data = json.load(res)

# récupère le champ username
username = str(json_data['username'])
print(username )

```

## Utilisation de litera_eval
`literal_eval` du module `ast` (Abstract Syntax Trees) en Python est une fonction très utile pour convertir des données sous forme de chaînes de caractères en leurs types de données Python correspondants, à condition que ces données soient exprimées sous forme de littéraux Python valides. Ces littéraux incluent des structures de données comme des listes, des dictionnaires, des tuples, des ensembles, ainsi que des types de base tels que des nombres, des chaînes de caractères et des valeurs booléennes.

### Exemples d'utilisation de 'literal_eval'

**Conversion d'une chaîne de caractères en liste :**

Si vous avez une chaîne de caractères qui représente une liste, literal_eval peut la convertir en une liste Python.

```py
import ast

s = "[1, 2, 3, 4, 5]"
result = ast.literal_eval(s)
print(result)  # Sortie: [1, 2, 3, 4, 5]
print(type(result))  # Sortie: <class 'list'>

```

**Conversion d'une chaîne de caractères en tuple :**

De manière similaire, une chaîne qui représente un tuple peut être convertie en un tuple Python.

```py
s = "(1, 'a', 3.14)"
result = ast.literal_eval(s)
print(result)  # Sortie: (1, 'a', 3.14)
print(type(result))  # Sortie: <class 'tuple'>
```

**Conversion d'une chaîne de caractère en un ensemble (set) :**

```py
s = "{1, 2, 3}"
result = ast.literal_eval(s)
print(result)  # Sortie: {1, 2, 3}
print(type(result))  # Sortie: <class 'set'>
```


**Conversion d'une chaîne de caractères en dictionnaire :**

```py
s = "{'key1': 'value1', 'key2': 100, 'key3': [1, 2, 3]}"
result = ast.literal_eval(s)
print(result)  # Sortie: {'key1': 'value1', 'key2': 100, 'key3': [1, 2, 3]}
print(type(result))  # Sortie: <class 'dict'>
```

**Conversion de valeurs numériques et booléennes :**

```py
s_num = "3.14159"
s_bool = "True"

result_num = ast.literal_eval(s_num)
result_bool = ast.literal_eval(s_bool)

print(result_num)  # Sortie: 3.14159
print(type(result_num))  # Sortie: <class 'float'>

print(result_bool)  # Sortie: True
print(type(result_bool))  # Sortie: <class 'bool'>
```


