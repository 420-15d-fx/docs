---
next:
  text: "Authentification"
  link: '/django/auth'
prev:
  text: 'Gestion des erreurs'
  link: '/django/gestion_des_erreurs'
---

# Formulaires

## Méthode avec `ModelForm`

[Documentation](https://docs.djangoproject.com/fr/5.0/topics/forms/modelforms/#modelform)

Création d'un fichier `forms.py` dans le dossier de l'application, et ajout d'une class qui hérite de `ModelForm`

:::tip Info
 Cette classe pourrait aussi être créée dans le fichier `models.py` mais par convention, on met le code des formulaires dans le fichier `forms.py`
:::

```py
from django import forms
from .models import Article

#...

class ArticleForm(forms.ModelForm):
    class Meta:
        model = Article
        fields = ['title', 'slug', 'content', 'is_draft', 'categories']
        labels = { 'title': 'Titre' }
        help_texts = { 'title': 'Veuillez saisir un titre' } 
```

La plupart du temps, les formulaires correspondent étroitement avec les modèles Django. Les champs étant déjà définis au niveau du modèle, il est inutile de les redéfinir pour créer le formulaire.

`model` indique quel modèle sera utilisé pour générer le formulaire.

`fields` permet de choisir les champs que l'on veut afficher dans le formulaire.

`labels` et `help_texts` permettent de surcharger les labels et d'ajouter un texte d'aide si besoin.

### Affichage du formulaire dans le gabarit

Exemple de fichier `add_article.html`

```py
<form action="" method="post">
  {% csrf_token %}
  {{ form }}
  <input type="submit" value="Submit">
</form>
```

La balise `form` permet d'afficher le formulaire qui est passé au gabarit. Cela va créer automatiquement les balises `label` et `input`.

`form` peut prendre trois options afin de configurer l'affichage que l'on souhaite :

- `form.as_table` affiche les composants sous forme de cellules de tableau à l’intérieur de balises `<tr>`
- `form.as_p` affiche les composants dans des balises `<p>`
- `form.as_ul` affiche les composants dans des balises `<li>`



`{% csrf_token %}` permet de protéger le formulaire contre la faille CSRF (Cross-Site Request Forgery). Voir cet [article](https://www.leblogduhacker.fr/faille-csrf-explications-contre-mesures) pour davantage d'informations.


Il est également possible d'afficher manuellement les champs, voir la documentation [Affichage manuel des champs](https://docs.djangoproject.com/fr/5.0/topics/forms/#rendering-fields-manually)


### La vue

Les données de formulaire renvoyés sont traitées par une vue (`views.py`), en principe la même qui a servi à produire le formulaire. Cela permet de réutiliser une partie de la même logique.

On utilise `request.method` pour savoir si la requête a été faire en `GET` ou en `POST`

```py
from django.shortcuts import render, redirect
from .models import Article
from .forms import ArticleForm

#...

def add_article(request):
    # Si requête POST, le formulaire a été soumis
    if request.method == 'POST':
        form = ArticleForm(request.POST)

        # Validation des données
        if form.is_valid():
            form.save()
            # Redirige vers une page de confirmation par exemple
            return redirect('confirm_article')
            
    # Si requête, on crée un formulaire vierge
    else:
        form = ArticleForm()

    return render(request, 'blogue/add_article.html', {'form': form})


def confirm_article(request):
    return render(request, 'blogue/confirm_article.html')
```

Vous devez créer un fichier `confirm_article.html` avec un message de confirmation par exemple...

Si `form.is_valid()` est `False`, l'objet `form` est renvoyé au gabarit avec les erreurs afin de les afficher automatiquement.

### Route

Pour finir, il faut définir les routes correspondantes dans le fichier `urls.py`

```py
from django.urls import path
from . import views

urlpatterns = [
  #...
  path('article/new/', views.article_new,name= 'article_new'),
  path('article/confirm/', views.article_confirm, 'article_confirm')
]
```

:::tip Pratique
- Créer un fomulaire pour l'ajout d'un article.
- Créer une vue pour l'affichage du formulaire pour l'ajout d'un article.
- Créer une vue pour la confirmation de l'ajout d'un article
- Ajoute les URLs nécessaire pour permettre l'ajout d'un article
- Ajouter un lien en page d'accueil permettant d'afficher le formulaire pour la création d'un nouvel article et tester la création.
:::
----


## Autre méthode sans ModelForm mais avec Form

[Documentation](https://docs.djangoproject.com/fr/5.0/topics/forms/#the-form-class)

Reprenons le fichier `forms.py`. 

```py
from django import forms
#...
class CategoryForm(forms.Form):
    title = forms.CharField(max_length=255, label = "Titre")
    description = forms.CharField(widget=forms.Textarea, help_text = "Saisir une description")
```

[Documentation](https://docs.djangoproject.com/fr/5.0/topics/forms/modelforms/#field-types) des types de champs.

En utilisant `Form` à la place de `ModelForm`, on doit saisir chaque champ de formulaire.

### La vue

Dans `views.py`, il faut enregistrer l'objet grâce à la méthode `objects.create`.

Avant d'enregistrer, il faut utiliser la méthode `.cleaned_data` afin de "nettoyer" les données. Django va par exemple convertir les types pour les enregistrer correctement dans la base de données.

```py
def category_new(request):
    if request.method == 'POST':
        form = CategoryForm(request.POST)
        if form.is_valid():
            #On doit nettoyer les données avant l'enregistrement
            title = form.cleaned_data['title']
            description = form.cleaned_data['description']
            Category.objects.create(title=title, description=description)
            return redirect('category_confirm')
    # Si requète GET, on crée un formulaire vierge
    else:
        form = CategoryForm()

    return render(request, 'blogue/add_category.html', {'form': form})

def catgegory_confirm(request):
    return render(request, 'blogue/category_confirm.html')
```

:::tip Pratique
- Créer un fomulaire pour l'ajout d'une catégorie.
- Créer une vue pour l'affichage du formulaire pour l'ajout d'une catégorie.
- Créer une vue pour la confirmation de l'ajout d'une catégorie
- Ajoute les URLs nécessaire pour permettre l'ajout d'une catégorie
- Ajouter un lien en page d'accueil permettant d'afficher le formulaire pour la création d'une nouvelle catégorie et tester la création.
:::

## Modification d'un modèle existant à partir d'un formulaire

Pour afficher les données du modèle dans le formulaire, nous devons utiliser le paramètre `instance` lors de l'instanciation du formulaire.

En utilisant un `ModelForm` :

```py
def category_edit(request, category_id):
    # Récupérer l'instance de Category ou retourner une erreur 404 si non trouvée
    category = get_object_or_404(Category, pk=category_id)

    if request.method == 'POST':
        # Le formulaire a été soumis, le traiter avec les données POST
        form = CategoryForm(request.POST, instance=category)
        if form.is_valid():
            # Le formulaire est valide, sauvegarder les modifications de la catégorie
            form.save()
            return redirect('some_success_url')  # Rediriger vers une URL de succès après la modification
    else:
        # Si la requête n'est pas POST, afficher le formulaire pré-rempli avec les données de la catégorie
        form = CategoryForm(instance=category)

    return render(request, 'category_edit_template.html', {'form': form})
```


En utilisant `Form` à la place de `ModelForm`:

```py
def category_edit(request, category_id):
     # Récupérer l'instance de Category ou retourner une erreur 404 si non trouvée
    category = get_object_or_404(Category, pk=category_id)

    if request.method == 'POST':
         # Le formulaire a été soumis, le traiter avec les données POST
        form = CategoryEditForm(request.POST)
        
        if form.is_valid():
            # Mettre à jour l'instance de Category avec les données validées
            category.title = form.cleaned_data['title']
            category.description = form.cleaned_data['description']
            category.save()
            return redirect('some_success_url')  # Rediriger vers une URL de succès après la modification
    else:
        # Initialiser le formulaire avec les données existantes de la catégorie
        form_data = {'title': category.title, 'description': category.description}
        form = CategoryEditForm(initial=form_data)

    return render(request, 'category_edit_template.html', {'form': form})
```


## Ajouter des attributs aux champs d'un formulaire
Lors de la création d'un formulaire, il est possible d'ajouter d'ajouter des attributs.

```py
class CategoryForm(forms.Form):
    title = forms.CharField(
        widget=forms.TextInput(attrs={'class': 'form-control'}),
        label='Titre',
        required=True
    )
    description = forms.CharField(
        widget=forms.Textarea(attrs={'class': 'form-control'}),
        required=True
    )
```

Ou de la manière suivante lorsqu'on utilise `ModelForm`

```py
class ArticleForm(forms.ModelForm):
    class Meta:
        model = Article
        fields = ['title', 'slug', 'content', 'is_draft', 'categories']
        labels= {'title' : 'titre'}
        help_texts = {'title': 'Veuillez saisir un titre'}
        widgets = {
            'title': forms.TextInput(
                attrs={
                    'class':'form-control', 
                    'placeholder': 'title'
                }
            )
        }
```

::: warning
Les champs **password1** et **password2** sont des champs personalisé du **UserCreationForm**. Étant donné que ceux-ci ne proviennent pas du modèle **User**, les widgets ne fonctionnent pas pour ces champs. Vous devez les redéfinir ansi que leur widget afin de les personnaliser :

```py
class RegisterForm(UserCreationForm):
    password1 = forms.CharField(
        label='Mode de passe',
        widget=forms.PasswordInput(attrs={'placeholder': 'Mot de passe', 'type': 'password', 'class': 'form-input'})
    )

    password2 = forms.CharField(
        label='Confirmation du mot de passe',
        widget=forms.PasswordInput(
            attrs={'placeholder': 'Confirmation du mot de passe', 'type': 'password', 'class': 'form-input'})
    )

    class Meta:
        model = User
        fields = (
            'email',
            'password1',
            'password2',
        )

```
:::

## Ajout d'une classe css pour les erreurs

```py
class ArticleForm(forms.ModelForm):
    error_css_class = 'alert alert-danger'
    ...
```

Ajoute les classes `alert alert-danger` aux champs qui contiennent des erreurs.

---


:::tip Pratique
- Modifier le formulaire ArticleForm afin d'ajouter la classe de Bootstrap form-control aux champs title, slug, content et categories. 
- Ajouter l'attribut `novalidate` à la balise form 
- Assurez-vous que le style s'applique correctement et vérifiez ce qui se passe lors de l'envoi d'un formulaire vide.
:::

## Modifier la langue d'affichage des messages d'erreurs automatisés

Pour modifier la langue par défaut de votre application, vous devez modifier la valeur de LANGUAGE_CODE dans le fichier settings.py.
```py
LANGUAGE_CODE = 'fr-ca'
```

## Validation

En plus de la validation automatique, on peut ajouter nos propres validations dans `forms.py`

Exemple :

```py
class CategoryForm(forms.Form):
    title = forms.CharField(max_length=255, label = "Titre")
    description = forms.CharField(widget=forms.Textarea, help_text = "Saisir une description")

    def clean_title(self):
        data = self.cleaned_data['title']
        if data == "chat":
            raise forms.ValidationError("Il y a déjà trop de chats sur Internet !")
        return data
```

:::tip Pratique
- Modifier le formulaire CategoryForm afin de prévenir l'ajout de nom de catégorie déjà existante.
:::

## Affichage manuel des champs

Il se peut que malgré les possibilité offertes par Django pour créer les formulaires vous ne soyez pas satisfait et que vous préféreriez avoir le contrôle total sur l'affichage des champs d'un formulaire.  Chaque champ est accessible en tant qu’attribut du formulaire avec la syntaxe `{ { form.nom_du_champ } }`​​​​​​​.

Voici un exemple :

```py
<form method="post" novalidate>
   {% csrf_token %}
   <div class="form-group">
     <div class="text-danger"> {{ form.title.errors }}</div>
     <label for="{{ form.title.id_for_label }}">{{ form.title.label_tag }}</label>
     {{ form.title }}
   </div>
   <input type="submit" value="Envoyer">
</form>
```

Dans cet exemple, `{ { form.title.errors } }` contient la liste des erreurs pour le champ title.

## Boucler sur les champs d'un formulaire

Il est possible de boucler à travers les champs d'un formulaire de la manières suivante.

```py
 <form method="post" novalidate>
   {% csrf_token %}
   {% for field in form.visible_fields %}
    <div class="form-group">
        <div class="text-danger">{{ field.errors }}</div>
        <label class="text-dark" for="{{ field.id_for_label }}">{{ field.label }}</label>
        {{ field }}
    </div>
   {% endfor %}
​
   <input type="submit" value="Envoyer">
</form>
```

## Affichage de tous les messages d'erreur

Il se peut que certains messages d'erreur ne soient pas associés à un champ en particulier. Ceux-ci sont donc associés au formulaire.  Voci comment afficher toutes les erreurs d'un formulaire.

```py
 {% if form.errors %}
    <div class="alert alert-danger">
        {{ form.errors }}
    </div>
{% endif %}
```


## Voici quelques propriétés de formulaire utiles :

`Form.errors`

Permet d'obtenir un dictionnaire des messages d'erreurs du formulaire. Dans ce dictionnaire, les clés correspondent aux noms de champs et les valeurs à des listes de chaînes représentant les messages d’erreur. Ceux-ci sont stockés dans des listes car un champ peut générer plusieurs messages d’erreur.

`Form.has_error(field, code=None)`

Cette méthode renvoie une valeur booléenne indiquant si un champ contient une erreur avec un code d’erreur spécifique. Si `code` vaut `None`, la méthode renvoie `True` si le champ contient n’importe quelle erreur.
Pour vérifier la présence d’erreurs non liées aux champs, indiquez `NON_FIELD_ERRORS` dans le paramètre `field`.

`Form.non_field_errors()`

Cette méthode renvoie la liste des erreurs dans `Form.errors` qui ne sont pas associées à un champ particulier. Cela comprend les erreurs `ValidationError` qui sont générées dans `Form.clean()`.

## Voici quelques attributs de champ :

`{ { field.errors } }`

Affiche une liste `<ul class="errorlist">` contenant toute erreur de validation correspondant à ce champ. Vous pouvez personnaliser la présentation des erreurs avec une boucle `{% for error in field.errors %}`. Dans ce cas, chaque objet de la boucle est une chaîne de caractères contenant le message d’erreur.

`{ { field.field } }`

L’instance Field de la classe de formulaire que cet objet BoundField adapte. Vous pouvez l’utiliser pour accéder aux attributs de Field, par exemple `{ { char_field.field.max_length } }`.

`{ { field.help_text } }`

Tout texte d’aide associé au champ.

`{ { field.html_name } }`

Le nom du champ tel qu’il sera utilisé dans le nom de champ de la balise input. Il prend en compte le préfixe de formulaire, si celui-ci est défini.

`{ { field.id_for_label } }`

L’attribut id utilisé pour ce champ. Si vous construisez vous-même l’étiquette du champ, il peut être avantageux d’utiliser ceci à la place de label_tag. C’est aussi utile par exemple si vous avez du code JavaScript en ligne et que vous vouliez éviter de figer l’identifiant du champ.

`{ { field.is_hidden } }`

Cet attribut vaut True si le champ de formulaire est un champ masqué, sinon il vaut False.

`{ { field.label } }`

L’étiquette du champ, par exemple Adresse de courriel.

`{ { field.label_tag } }`

L’intitulé du champ placé dans la balise HTML `<label>` appropriée. 

## Filtrer des données à afficher dans une liste d'un formulaire.

### Avec Forms.Form

 Voici un exemple où nous filtrons les choix d'un champ `ModelChoiceField` basé sur un modèle. Imaginons que nous ayons un modèle Livre et un modèle Auteur, et nous voulons afficher dans le formulaire uniquement les livres d'un auteur spécifique.

Supposons que nous avons les modèles suivants :



```py
from django.db import models

class Auteur(models.Model):
    nom = models.CharField(max_length=100)

    def __str__(self):
        return self.nom

class Livre(models.Model):
    titre = models.CharField(max_length=100)
    auteur = models.ForeignKey(Auteur, on_delete=models.CASCADE)

    def __str__(self):
        return self.titre

```

Voici le formulaire :


```py
from django import forms
from .models import Livre, Auteur

class LivreForm(forms.Form):
    livre = forms.ModelChoiceField(queryset=Livre.objects.all())

   
```
Dans cet exemple, le formulaire LivreForm a un champ livre qui est un ModelChoiceField. Pour lequel les choix de livres par défaut (`queryset`) correspondent à tous les livres.

Dans votre vue, ajustez le `queryset` du champ livre selon le besoin avant de créer une instance du formulaire :

```py

from django.shortcuts import render
from .forms import LivreForm
from .models import Livre

def auteur_detail(request, auteur_id):
    form = LivreForm()
    form.fields['livre'].queryset = Livre.objects.filter(auteur=auteur_id)
    return render(request, 'auteur_detail.html', {'form': form})

```

Dans cet exemple, le formulaire est d'abord instancié normalement. Ensuite, avant de passer le formulaire au contexte du template, le `queryset` du champ livre est ajusté pour inclure uniquement les livres de l'auteur spécifié par auteur_id. Cela permet de filtrer dynamiquement les choix disponibles pour le champ livre sans nécessiter de logique supplémentaire dans la définition du formulaire lui-même.


### Avec un ModelForm

Il s'agit de la même manière pour le filtrage dans la vue que dans l'exemple précédent.


## Modifier le type d'affichage d'une liste.

Pour modifier le type d'affichage d'une liste dans un formulaire Django, que ce soit avec `forms.Form` ou `forms.ModelForm`, vous pouvez utiliser l'argument widget pour spécifier un type d'affichage différent pour les champs de formulaire. Django fournit plusieurs widgets intégrés que vous pouvez utiliser pour changer l'apparence et le comportement des champs de formulaire dans le rendu HTML.

Voici comment vous pouvez modifier le widget pour un champ dans les deux types de formulaires :

### Dans un forms.Form

Si vous avez un forms.Form avec un champ de choix, tel qu'un `ChoiceField` ou un `ModelChoiceField`, et que vous souhaitez utiliser un widget différent (par exemple, un groupe de boutons radio au lieu d'une liste déroulante), vous pouvez le faire de la manière suivante :

```py
from django import forms
from .models import Auteur

class MonFormulaire(forms.Form):
    auteur = forms.ModelChoiceField(queryset=Auteur.objects.all())
    auteur.widget = forms.RadioSelect()
```
Dans cet exemple, le champ auteur utilisera un groupe de boutons radio pour afficher les choix au lieu du widget par défaut, qui est une liste déroulante.

### Dans un forms.Modelform

```py
from django import forms
from .models import Livre

class LivreForm(forms.ModelForm):
    class Meta:
        model = Livre
        fields = ['titre', 'auteur']
        widgets = {
            'auteur': forms.RadioSelect(),
        }
```

Dans cet exemple, le champ auteur dans LivreForm utilisera également un groupe de boutons radio pour les choix.

### Widgets Disponibles pour les listes de choix

Les widgets disponibles pour les listes de choix sont les suivants :

- **CheckboxSelectMultiple**: Une liste de cases à cocher pour une sélection multiple.
- **RadioSelect**: Un groupe de boutons radio pour une sélection unique.
- **Select**: Une liste déroulante pour une sélection unique.
- **SelectMultiple**: Une liste déroulante pour une sélection multiple.
