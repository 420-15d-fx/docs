---
prev:
  text: "Création d'une application"
  link: '/django/apps'
next:
  text: 'Template'
  link: '/django/templates'
---
# Écriture d’une première vue

Une vue est une fonction Python acceptant une requête web et renvoyant une réponse web. Cette réponse peut contenir le contenu HTML d’une page web, une redirection, une erreur 404, un document JSON, une image… ou vraiment n’importe quoi d’autre. La vue elle-même contient la logique nécessaire pour renvoyer une réponse. Le code d'une vue est placé dans un fichier nommé `views.py` se trouvant dans un projet ou un répertoire d’application.

Écrivons la première vue. Ouvrez le fichier `blogue/views.py` et placez-y le code Python suivant :

## blogue/views.py

```py
from django.http import HttpResponse

def index(request):
    return HttpResponse("Hello, world")
```

C’est la vue Django la plus simple possible. Pour appeler cette vue, il s’agit de l’associer à une route définie par une URL, et pour cela nous avons besoin d’un URLconf (configuration d'URL). Le code de ce module contient les correspondances entre expressions de chemins d’URL et fonctions Python (vos vues).

Pour créer un URLconf dans le répertoire `blogue`, créez un fichier nommé `urls.py`. Votre répertoire d’application devrait maintenant ressembler à ceci :

```
├── __init__.py
├── admin.py
├── apps.py
├── migrations
│   └── __init__.py
├── models.py
├── tests.py
├── urls.py
└── views.py
```

Dans le fichier `blogue/urls.py`, insérez le code suivant :

## blogue/urls.py

```py
from django.urls import path
# Importe le fichier views depuis le dossier courant
from . import views

urlpatterns = [
    path('', views.index, name='index'),
]
```

La fonction path() reçoit trois paramètres :

## Paramètre de path() : route¶

route est une chaîne contenant un motif d’URL. Ici nous avons `''` qui correspond à la route par défaut `blogue/`

## Paramètre de path() : view¶

Fonction qui doit être appelée, ici c'est la fonction `index` du fichier `views`

## Paramètre de path() : name¶

Le nommage des URL permet de les référencer de manière non ambiguë depuis d’autres portions de code Django, en particulier depuis les gabarits. Cette fonctionnalité permet d’effectuer des changements globaux dans les modèles d’URL de votre projet en ne modifiant qu’un seul fichier.

## Processus de traitement des requêtes par Django¶

Quand un utilisateur accède à une page de votre site Django, voici l’algorithme que le système suit pour déterminer quel code Python doit être exécuté :
  - Django identifie le module URLconf racine à utiliser. Par défaut, c’est la valeur attribuée au réglage `ROOT_URLCONF` (voir settings.py).
  - Django charge ce module Python et cherche la variable urlpatterns. Ce devrait être une séquence d’instances `django.urls.path()` ou `django.urls.re_path()`.
  - Django parcourt chaque motif d’URL dans l’ordre et s’arrête dès la première correspondance de path_info avec l’URL demandée.
  - Une fois qu’un des motifs d’URL correspond, Django importe et appelle la vue correspondante, qui est une fonction Python La vue se voit passer les paramètres suivants :
        * Une instance HttpRequest.
        * Les paramètres contenus dans chemin indiquée
  - Si aucun motif d’URL ne correspond, ou si une exception est levée durant ce processus, Django appelle une vue d’erreur appropriée. 


L’étape suivante est de faire pointer la configuration d'url racine vers le module `blogue.urls`. 

Dans `monsupersite/urls.py`, ajoutez une importation `django.urls.include` et insérez un appel `include()` dans la liste `urlpatterns`, ce qui donnera :

## monsupersite/urls.py

```py
from django.contrib import admin
from django.urls import include, path

urlpatterns = [
    path('blogue/', include('blogue.urls')),
    path('admin/', admin.site.urls),
]
```

Il faut toujours utiliser `include()` lorsque l’on veut inclure d’autres motifs d’URL. `admin.site.urls` est la seule exception à cette règle.

Vous avez maintenant relié une vue `index` dans la configuration d’URL. Vérifiez qu’elle fonctionne avec la commande suivante :


``` bash
$ python manage.py runserver
```


Ouvrez [http://localhost:8000/blogue/](http://localhost:8000/blogue/) dans votre navigateur et vous devriez voir le texte « *Hello, world* » qui a été défini dans la vue `index`.

:::tip Pratique :

Ajouter deux autres vues dans l’application Blogue :
- Une vue article_detail qui est liée à l’url **/blogue/article** et qui affiche « Détail d'un article»
- Une vue category_list qui est liée à l’url **/blogue/categories** et qui affiche « Liste des categories »
:::


