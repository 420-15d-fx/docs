---
prev:
  text: 'Presentation de Django'
  link: '/django/presentation'
next:
  text: "Création d'une application"
  link: '/django/apps'
---

# Installation de Django

> Les informations de ce cours proviennent en partie de la documentation de Django

https://docs.djangoproject.com/fr/5.0/intro/install/

## Pré-requis

```shell
python3
pip
```

## Pré-requis optionnel

```shell
venv
```
Pour utiliser un environnement Python tel que vous l'avez vu dans la session précédente.

## Installation

```shell
  $ python -m pip install django
```


```shell
  $ python -m django --version
```

> Si Django est installé, vous devriez voir apparaître la version de l’installation. Dans le cas contraire, vous obtiendrez une erreur disant « No module named django » (aucun module nommé django).


# Création d’un projet

Depuis un terminal en ligne de commande, ce placer dans le dossier dans lequel vous souhaitez conserver votre code, puis lancez la commande suivante :

```shell
$ django-admin startproject monsupersite
```
ou
```shell
$ python -m django startproject monsupersite
```

Cela va créer un répertoire `monsupersite` dans le répertoire courant.

::: tip Note:
Vous devez éviter de nommer vos projets en utilisant des noms réservés de Python ou des noms de composants de Django. Cela signifie en particulier que vous devez éviter d’utiliser des noms comme `django` (qui entrerait en conflit avec Django lui-même) ou `test` (qui entrerait en conflit avec un composant intégré de Python).
:::

Voyons ce que [`startproject`](https://docs.djangoproject.com/fr/5.0/ref/django-admin/#django-admin-startproject) a créé :

```shell
monsupersite/
    manage.py
    monsupersite/
        __init__.py
        settings.py
        urls.py
        asgi.py
        wsgi.py
```

Ces fichiers sont :

- Le premier répertoire racine `monsupersite/` est un contenant pour votre projet. Son nom n’a pas d’importance pour Django ; vous pouvez le renommer comme vous voulez.
- `manage.py` : un utilitaire en ligne de commande qui vous permet d’interagir avec ce projet Django de différentes façons. Vous trouverez toutes les informations nécessaires sur `manage.py` dans [django-admin et manage.py](https://docs.djangoproject.com/fr/5.0/ref/django-admin/).
- Le sous-répertoire `monsupersite/` correspond au paquet Python effectif de votre projet. C’est le nom du paquet Python que vous devrez utiliser pour importer ce qu’il contient (par ex. `monsupersite.urls`).
- `monsupersite/__init__.py` : un fichier vide qui indique à Python que ce répertoire doit être considéré comme un paquet. Si vous êtes débutant en Python, lisez [les informations sur les paquets](https://docs.python.org/3/tutorial/modules.html#tut-packages "(disponible dans Python v3.9)") (en) dans la documentation officielle de Python.
- `monsupersite/settings.py` : réglages et configuration de ce projet Django. [Les réglages de Django](https://docs.djangoproject.com/fr/5.0/topics/settings/) vous apprendra tout sur le fonctionnement des réglages.
- `monsupersite/urls.py` : les déclarations des URL de ce projet Django, une sorte de « table des matières » de votre site Django. Vous pouvez en lire plus sur les URL dans [Distribution des URL](https://docs.djangoproject.com/fr/5.0/topics/http/urls/).
- `monsupersite/asgi.py` : un point d’entrée pour les serveurs Web compatibles aSGI pour déployer votre projet. Voir [Comment déployer avec ASGI](https://docs.djangoproject.com/fr/5.0/howto/deployment/asgi/) pour plus de détails.
- `monsupersite/wsgi.py` : un point d’entrée pour les serveurs Web compatibles WSGI pour déployer votre projet. Voir [Comment déployer avec WSGI](https://docs.djangoproject.com/fr/5.0/howto/deployment/wsgi/) pour plus de détails.

# Le serveur de développement

Vérifions que votre projet Django fonctionne. Déplacez-vous dans le répertoire `monsupersite` si ce n’est pas déjà fait, et lancez la commande suivante :

```shell
$ python manage.py runserver
```

Vous verrez les messages suivants défiler en ligne de commande :

```shell
Watching for file changes with StatReloader
Performing system checks...

System check identified no issues (0 silenced).

You have 18 unapplied migration(s). Your project may not work properly until you apply the migrations for app(s): admin, auth, contenttypes, sessions.
Run 'python manage.py migrate' to apply them.
August 21, 2023 - 18:09:24
Django version 4.2.4, using settings 'monsupersite.settings'
Starting development server at http://127.0.0.1:8000/
Quit the server with CONTROL-C.

```

:::tip Note:
Ignorez pour l’instant l’avertissement au sujet des migrations de base de données non appliquées ; nous nous occuperons de la base de données plus tard.
:::

Vous avez démarré le serveur de développement de Django, un serveur Web léger entièrement écrit en Python. 

 **Ne jamais utiliser ce serveur** pour l'environnement de production. Il est fait seulement pour tester le travail pendant le développement.

Allez à l’adresse [http://127.0.0.1:8000](http://127.0.0.1:8000) et vérifiez que la page avec le message « Félicitations ! » ainsi qu’une fusée qui décolle s'affiche

Pour stopper le serveur, faire `ctrl + c`

![Félicitations !](./img/homepage.png){data-zoomable width=50%}


## Modification du port

Par défaut, la commande [`runserver`](https://docs.djangoproject.com/fr/5.0/ref/django-admin/#django-admin-runserver) démarre le serveur de développement sur l’IP interne sur le port 8000.

Si vous voulez changer cette valeur, passez-la comme paramètre sur la ligne de commande. Par exemple, cette commande démarre le serveur sur le port 8080 :


```shell
$ python manage.py runserver 8080
```
  
Si vous voulez changer l’IP du serveur, passez-la comme paramètre avec le port. Par exemple, pour écouter toutes les IP publiques disponibles (ce qui est utile si vous exécutez Vagrant ou que souhaitez montrer votre travail à des personnes sur d’autres ordinateurs de votre réseau), faites :

```shell
$ python manage.py runserver 0.0.0.0:8000
```

La documentation complète du serveur de développement se trouve dans la référence de [`runserver`](https://docs.djangoproject.com/fr/5.0/ref/django-admin/#django-admin-runserver).

## Rechargement automatique de `runserver`

Le serveur de développement recharge automatiquement le code Python lors de chaque requête si nécessaire. Vous ne devez pas redémarrer le serveur pour que les changements de code soient pris en compte. Cependant, certaines actions comme l’ajout de fichiers ne provoquent pas de redémarrage, il est donc nécessaire de redémarrer manuellement le serveur dans ces cas.


## Sources

* [https://docs.djangoproject.com/fr/5.0/intro/install/](https://docs.djangoproject.com/fr/4.2/intro/install/)

* [https://docs.djangoproject.com/fr/5.0/intro/tutorial01/](https://docs.djangoproject.com/fr/4.2/intro/tutorial01/)
