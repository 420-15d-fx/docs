---
prev:
  text: "Le site d'administration"
  link: '/django/le_site_d_administration'
next:
  text: 'Gestion des erreurs'
  link: '/django/gestion_des_erreurs'
---

# ORM

[Documentation](https://docs.djangoproject.com/fr/5.0/topics/db/queries/)

ORM => Object Relational Mapping.

C'est une technique de programmation qui permet de manipuler les données de la base de données comme des objets.

  * Tous les objets nécessitant des enregistrements (modèles) sont listés dans un fichier (models.py)
  * Permet une couche d'abstraction pour exploiter la base de données. Plus besoin d'écrire des requêtes suivant le système de gestion de base de données que le projet utilise.

## Créer un objet à partir d'un modèle

Utilisation du shell afin de faire quelques tests.

```shell
$ python manage.py shell
```

Création d'un article de blogue

```shell
Python 3.7.4 (tags/v3.7.4:e09359112e, Jul  8 2019, 19:29:22) [MSC v.1916 32 bit (Intel)] on win32
Type "help", "copyright", "credits" or "license" for more information.
(InteractiveConsole)
>>> 
>>> from blogue.models import *
>>> a = Article()
>>> a.title = "un article"
>>> a.content = "texte de mon article"
>>> a.save()
```

On peut également écrire de cette façon :

```shell
>>> b = Article(title = "2eme article", content = "texte de l'article").save()
```


Avec une clé étrangère `category` par exemple :

```shell
>>> cat = Category(title="Les chiens")
>>> cat.save()
>>> art = Article(title = "article sur les chiens", content = "texte de l'article") 
>>> art.category = cat
>>> art.save()
```


Liaison plusieurs à plusieurs

```shell
>>> cat1 = Category(title="Les chiens")
>>> cat1.save()
>>> cat2 = Category(title="Les chats")
>>> cat2.save()
>>> art = Article(title = "article sur les chiens et les chats", content = "texte de l'article") 
>>> art.save()
>>> art.categories.add(cat1, cat2)
>>> art.save()
```

## Objects, le manager des modèles 

Récupération des enregistrements de la base de données (sous forme d'objet) via le manager que tout modèle possède et qui s'appelle `objects`.

### Afficher toutes les entrées d'un modèle

```shell
>>> Article.objects.all() 
<QuerySet [<Article: Article object (1)>, <Article: Article object (2)>, <Article: Article object (3)>]>
```

### Récupérer un objet 

```shell
>>> Article.objects.get(id=1)      
<Article: Article object (1)>
>>> Article.objects.get(id=1).title    
'Un super article'
```

Si la méthode `__str__()` est défini dans le modèle, l'affichage correspond au `return` de la méthode

```shell
>>> Category.objects.get(id=1)        
<Category: Animaux => Les animaux>
```

### La méthode filter()

La méthode `get` ne retourne qu'un seul objet, `filter` permet de récupérer plusieurs enregistrement.

```shell
>>> Article.objects.filter(is_draft=0)
<QuerySet [<Article: Article object (2)>, <Article: Article object (3)>]>
```

Pour filtrer sur une relation
```shell
>>> catChien = Category.objects.get(id=4)
>>> Article.objects.filter(categories=catChien)
<QuerySet [<Article: Article object (1)>]>
```

On peut également enchaîner les filtres

```shell
>>> Article.objects.filter(is_draft=0).exclude(id=2)
<QuerySet [<Article: Article object (3)>]>
```

### L'opérateur AND
L'opérateur AND dans un filtre est représenté par l'utilisation d'une virgule (,) entre les critères.

```shell
>>>catChien = Category.objects.get(id=4)
>>> Article.objects.filter(categories=catChien, is_draft=False )
<QuerySet [<Article: Article object (1)>]>
```

### L'opérateur OR
L'opérateur OR est représenté par le caractère `|` entre deux filtres

```shell
>>> catChien = Category.objects.get(id=4)
>>> catChat = Category.objects.get(id=5)
>>> Article.objects.filter(categories = catChien) | Article.objects.filter(categories = catChat)
<QuerySet [<Article: Article object (1)>, <Article: Article object (3)>, <Article: Article object (2)>, <Article: Article object (3)>]>
```

### La méthode exclude()
Cet méthode permet d'exclure des objets répondant aux critères du résultat

```shell
Article.objects.exclude(id=2)
<QuerySet [<Article: Article object (1)>, <Article: Article object (3)>]>
```

On peut également enchaîner les méthodes

```shell
>>> Article.objects.filter(is_draft=0).exclude(id=2)
<QuerySet [<Article: Article object (3)>]>
```

### La méthode order_by()
Permet de trier les résultats selon les critères en ordre croissant par défaut.
```shell
>>> Article.objects.all().order_by('created_at')
<QuerySet [<Article: Article object (1)>, <Article: Article object (2)>, <Article: Article object (3)>]>
```

Pour trier en ordre décroissant, il suffit d'utiliser le signe moins (-) comme préfix du nom du champ

```shell
>>> Article.objects.all().order_by('-created_at')
<QuerySet [<Article: Article object (3)>, <Article: Article object (2)>, <Article: Article object (1)>]>
```

### delete()

```shell
>>> art = Article.objects.get(id=6) 
>>> art.delete()
(1, {'blogue.Article': 1})
```

Pour supprimer plusieurs objets
```shell
>>>Article.objects.filter(categories = catChien).delete()
(5, {'blogue.Article_categories': 3, 'blogue.Article': 2})
```


:::tip Pratique
Reprendre le projet blogue et modifier le fichier `views.py` afin d'afficher les articles dans l'ordre décroissant de leur date de création sur la page `index.html`.

Ajouter également un lien vers la page de détail de l'article et afficher le contenu de l'article sur la page `detail.html`.

:::
