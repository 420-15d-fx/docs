# Démonstrations réalisées en classe

## semaine 1

- [Cours 1](https://gitlab.com/420-15d-fx/demo-aspnet/-/blob/main/cours%201/BlogApp.zip)
- [Cours 2](https://gitlab.com/420-15d-fx/demo-aspnet/-/blob/main/cours%202/BlogApp.zip)

## semaine 2

- [Cours 1](https://gitlab.com/420-15d-fx/demo-aspnet/-/blob/main/cours%203/BlogApp.zip)

- [Cours 2](https://gitlab.com/420-15d-fx/demo-aspnet/-/blob/main/cours%204/BlogApp.zip?ref_type=heads)

## semaine 3

- [Cours 1](https://gitlab.com/420-15d-fx/demo-aspnet/-/blob/main/cours%205/BlogApp.zip?ref_type=heads)

- [Cours 2](https://gitlab.com/420-15d-fx/demo-aspnet/-/blob/main/cours%206/BlogApp.zip?ref_type=heads)


## semaine 4

- [Cours 1](https://gitlab.com/420-15d-fx/demo-aspnet/-/blob/main/cours%207/BlogApp.zip?ref_type=heads)