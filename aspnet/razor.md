# Langage utilisé dans les Pages Razor

Le templating dans** ASP.NET Core Razor Pages** repose sur `Razor`, un moteur de rendu de vues qui permet de combiner **HTML** et **C#** pour générer dynamiquement des pages web.

- Les pages Razor sont des fichiers **.cshtml** qui peuvent contenir du **C# embarqué** à l'aide de la syntaxe **@{ }**.
- Il permet de structurer une application avec des layouts, des vues partielles, et des helpers facilitant la réutilisation du code.


## Syntaxe de base en Razor

Les instructions en **C#** peuvent être incluses directement dans les fichiers `.cshtml` en utilisant la syntaxe Razor :

```razor
@{
    var message = "Bienvenue sur mon blog !";
    var date = DateTime.Now;
}

<h1>@message</h1>
<p>Nous sommes le @date.ToString("dddd dd MMMM yyyy")</p>
``` 

## Exemple : Affichage dynamique de la liste des articles avec Razor

**Pages/Create.cshtml.cs**
```c#
using Microsoft.AspNetCore.Mvc.RazorPages;
using System.Collections.Generic;

public class CreateModel : PageModel
{
    public static List<Article> Articles = new List<Article>();

    public void OnGet()
    {
        // Ajout d'exemples d'articles au premier chargement
        if (Articles.Count == 0)
        {
            Articles.Add(new Article { Titre = "Introduction à Razor", Contenu = "Découvrez comment utiliser Razor dans ASP.NET Core.", Categorie = "Tech" });
            Articles.Add(new Article { Titre = "Avantages d'Entity Framework", Contenu = "Comment gérer vos bases de données efficacement.", Categorie = "Développement" });
        }
    }
}

public class Article
{
    public string Titre { get; set; }
    public string Contenu { get; set; }
    public string Categorie { get; set; }
}
```
**Pages/Index.cshtml**
```razor
@page
@model IndexModel

<h1>Liste des articles</h1>

<a asp-page="/Create" class="btn btn-primary mt-3">Ajouter un article</a>

@if (CreateModel.Articles.Count == 0)
{
    <p>Aucun article disponible.</p>
}
else
{
    <ul class="list-group">
        @foreach (var article in CreateModel.Articles)
        {
            <li class="list-group-item">
                <h2>@article.Titre</h2>
                <p><strong>Catégorie :</strong> @article.Categorie</p>
                <p>@article.Contenu</p>
            </li>
        }
    </ul>
}


```

## Les Tag Helpers

Un Tag Helper est une fonctionnalité d’ASP.NET Core qui permet d’enrichir le HTML avec du C#, en utilisant des attributs HTML pour générer dynamiquement du contenu côté serveur.

Voici la liste des principaux Tag Helpers :

|Tag Helper|Description|	Exemple
|----------|-----------|-----------
|asp-for	|Lie un élément HTML à une propriété du modèle|	`<input asp-for="Nom"/>`
|asp-validation-for|	Associe une validation à un champ|	`<span asp-validation-for="Nom"></span>`
|asp-validation-summary|	Affiche les erreurs de validation|	`<div asp-validation-summary="All"></div>`
|asp-action|	Génère un lien vers une action|	`<a asp-action="Index">Accueil</a>`
|asp-controller|	Spécifie le contrôleur pour un lien| `<a asp-controller="Home" asp-action="Contact">Contact</a>`
|asp-route-{nom}|	Ajoute un paramètre de route| `<a asp-controller="Home" asp-action="Details" asp-route-id="5">Voir</a>`
|asp-route|	Définit la route complète|	`<a asp-route="home/details/5">Voir</a>`
|asp-area|	Définit une zone d'application|	`<a asp-area="Admin" asp-controller="Dashboard" asp-action="Index">Admin</a>`
|asp-items|	Remplit une liste déroulante| `<select asp-for="Genre" asp-items="Model.Genres"></select>`
|asp-append-version|	Ajoute une version aux fichiers CSS/JS pour éviter la mise en cache|	`<script src="script.js"  asp-append-version="true"></script>`
|asp-antiforgery|	Active ou désactive le token CSRF|	`<form asp-antiforgery="true"></form>`
|asp-fragment|	Ajoute un ancrage (#) à une URL|	`<a asp-action="Details" asp-fragment="section1">Voir la section</a>`
|environment|	Affiche du contenu en fonction de l'environnement|	`<environment include="Development">Mode Dev</environment>`



