# Utilisation des Areas

L’utilisation des Areas en ASP.NET Core permet de structurer une application en différentes sections indépendantes. C'est particulièrement utile pour créer une **zone d'administration** séparée du reste du site.

## Qu'est-ce qu'une Area

Une Area est un moyen de regrouper des **pages Razor** dans une structure séparée pour une meilleure organisation du projet.

**Dans notre blog, nous allons créer une Area "Admin" pour :**

- Gérer les articles (ajouter, modifier, supprimer).
- Gérer les catégories des articles (ajouter, modifier, supprimer).

## 1) Création de la structure des Areas

```
📂 Areas
 ├── 📂 Admin
 │   ├── 📂 Pages
 │   │   ├── 📂 Articles
 │   │   │   ├── Index.cshtml
 │   │   │   ├── Create.cshtml
 │   │   │   ├── Edit.cshtml
 │   │   │   ├── Delete.cshtml
 │   │   │   ├── Details.cshtml
 │   │   ├── 📂 Categories
 │   │   │   ├── Index.cshtml
 │   │   │   ├── Create.cshtml
 │   │   │   ├── Edit.cshtml
 │   │   │   ├── Delete.cshtml
 │   │   │   ├── Details.cshtml
 │   │   ├── _ViewStart.cshtml
 │   │   ├── _ViewImports.cshtml
 │   │   ├── _Layout.cshtml
 │   │   ├── Index.cshtml

        
```

## 2) Ajouter un Layout spécifique pour l’Admin

Chaque zone (area) peut avoir son propre layout.

**Fichier Areas/Admin/Pages/Shared_Layout.cshtml**

```html
<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="utf-8" />
    <title>Admin - Blog</title>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0/dist/css/bootstrap.min.css">
</head>
<body>

    <nav class="navbar navbar-expand-lg navbar-dark bg-dark">
        <div class="container">
            <a class="navbar-brand" asp-area="Admin" asp-page="/Index">Tableau de Bord</a>
            <div class="collapse navbar-collapse">
                <ul class="navbar-nav ms-auto">
                    <li class="nav-item">
                        <a class="nav-link" asp-area="Admin" asp-page="/Articles/Index">Articles</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" asp-area="Admin" asp-page="/Categories/Index">Catégories</a>
                    </li>
                </ul>
            </div>
        </div>
    </nav>

    <div class="container mt-4">
        @RenderBody()
    </div>

</body>
</html>
```
## 3) Ajouter _ViewStart.cshtml dans l’Area

Le fichier `_ViewStart.cshtml` permet de définir le Layout par défaut pour toutes les pages de l’Area.

**Areas/Admin/Pages/_ViewStart.cshtml**

```razor
@{
    Layout = "_Layout";
}
```

Il est tourjours possible de ne pas utiliser de layout personnalité et d'utiliser celui de base : 


```razor
@{
     Layout = "/Pages/Shared/_Layout.cshtml";
}
```

## 4) Compléter avec _ViewImports.cshtml

Dans chaque Area, il est aussi recommandé d’ajouter un fichier `_ViewImports.cshtml` pour inclure les directives nécessaires. Permet d'utiliser les modèles et espaces de noms dans toutes les pages de l’Area Admin et d'activer les TagHelpers (asp-page, asp-route, etc.).

```razor

@using BlogApp
@using BlogApp.Models
@namespace MyBlog.Areas.Admin.Pages
@addTagHelper *, Microsoft.AspNetCore.Mvc.TagHelpers


```

