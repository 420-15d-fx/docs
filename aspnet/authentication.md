# ASP.NET Identity
ASP.NET Identity est le système de gestion d’authentification et d’autorisation utilisé dans les applications ASP.NET, en particulier dans **ASP.NET Core**.

ASP.NET Identity permet aux développeurs de : 
- Gérer les utilisateurs : Création, mise à jour, suppression d’utilisateurs
- Gérer les rôles et autorisations : Définir des rôles (Admin, Utilisateur, etc.) et attribuer des droits
- Gérer l’authentification : Connexion avec un mot de passe, via un fournisseur externe (Google, Facebook, Microsoft, etc.), ou via OAuth2/OpenID Connect
- Gérer l’authentification à deux facteurs (2FA) : Envoi de codes par SMS, e-mail, ou applications comme Google Authenticator
- Stocker les informations utilisateur : Stockage en base de données avec Entity Framework Core
- Utiliser des tokens JWT : Pour une authentification sécurisée dans les API
- Personnaliser le système : Ajouter des champs personnalisés aux utilisateurs et modifier le modèle de stockage

## Architecture

ASP.NET Identity utilise **Entity Framework Core** pour interagir avec une base de données SQL, où il stocke les informations des utilisateurs et des rôles.

Voici les principales classes utilisées :

- `IdentityUser` : Représente un utilisateur (email, mot de passe hashé, etc.)
- `IdentityRole` : Définit les rôles (Admin, Utilisateur, etc.)
- `UserManager<TUser>` : Fournit des méthodes pour gérer les utilisateurs (création, suppression, authentification, etc.)
- `RoleManager<TRole>` : Gère les rôles
- `SignInManager<TUser>` : Gère le processus de connexion/déconnexion

## Configuration pour un projet existant

Lors de la création d'un nouveau projet ASP.NET Core, vous devez sélectionner l'option :

**Type d'authentification → Comptes individuels**



## Configuration pour un projet existant

### 1. Installer les packages NutGet suivants : 

- dotnet add package Microsoft.AspNetCore.Identity.EntityFrameworkCore
- dotnet add package Microsoft.AspNetCore.Identity.UI
- dotnet add package Microsoft.EntityFrameworkCore.SqlServer (Problement déjà installé si vous utilisé une base de données)

### 2. Configurer Identity dans le DbContext

Modifier `Data/ApplicationDbContext.cs` pour ajouter `IdentityUser` et `IdentityRole` :

```c#
using BlogApp.Models;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;

namespace BlogApp.Data
{
    public class ApplicationDbContext : IdentityDbContext<IdentityUser> //Ajout de l'authentification
    {
        public ApplicationDBContext(DbContextOptions<ApplicationDBContext> options) : base(options)
        {
            
        }

        public  DbSet<Article> Articles { get; set; }
        public DbSet<Categorie> Categories { get; set; }

    }
}
```

### 3. Configurer Identity dans Program.cs

Modifier Program.cs pour ajouter Identity :

```c#
using BlogApp.Data;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;

var builder = WebApplication.CreateBuilder(args);

// Ajout d'Identity
builder.Services.AddDefaultIdentity<IdentityUser>(options =>
{
    options.Password.RequireDigit = true;
    options.Password.RequiredLength = 6;
    options.Password.RequireUppercase = false;
})
.AddRoles<IdentityRole>()  // Permet la gestion des rôles (Admin, Utilisateur)
.AddEntityFrameworkStores<ApplicationDBContext>();


builder.Services.AddRazorPages();

builder.Services.AddDbContext<ApplicationDBContext>(options =>
    options.UseSqlServer(builder.Configuration.GetConnectionString("DefaultConnection")));

var app = builder.Build();

// Configure the HTTP request pipeline.
if (!app.Environment.IsDevelopment())
{
    app.UseExceptionHandler("/Error");
    // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
    app.UseHsts();
}

app.UseHttpsRedirection();

app.UseRouting();

app.UseAuthorization();

app.MapStaticAssets();
app.MapRazorPages()
   .WithStaticAssets();

app.Run();


```

### 4. Appliquer les migrations et mettre à jour la base de données

 Dans la console du Gestionnaire de package, exécuter :

```cmd
Add-Migration AddIdentity
Update-Database

```

### 5.  Générer les pages d'authentification

Dans Visual Studio :

- Clic droit sur `Pages` → Ajouter → Nouvel élément structuré (Scaffolded Item)...
- Sélectionner "**Identity**".
- Choisir "**Ajouter tous les fichiers d'authentification**".
- Sélectionner `ApplicationDbContext` comme contexte de base de données.
- Cliquer sur Ajouter.

Cela va créer le dossier `Areas/Identity` contenant toutes les vues pour la gestion de l'authentification.


## Gestion des utilisateurs

Le `UserManager<TUser>` permet de gérer les utilisateur.


### Création et gestion des utilisateurs

- **CreateAsync(TUser user, string password)**

Crée un nouvel utilisateur avec un mot de passe hashé.

```c#
var user = new IdentityUser { UserName = "testuser", Email = "test@example.com" };
var result = await _userManager.CreateAsync(user, "P@ssword123");
```

- **UpdateAsync(TUser user)**

Met à jour les informations d’un utilisateur.

```c#
user.Email = "newemail@example.com";
var result = await _userManager.UpdateAsync(user);
```

- **DeleteAsync(TUser user)**

Supprimer l'utlisateur

```c#
var result = await _userManager.DeleteAsync(user);
```

### Recherche des utilisateurs

- **FindByIdAsync(string userId)**

Recherche un utilisateur à partir de son Id

```c#
var user = await _userManager.FindByIdAsync("user-guid-id");
```

- **FindByNameAsync(string userName)**

Recherche un utilisateur par son nom d’utilisateur.

```c#
var user = await _userManager.FindByNameAsync("testuser");
```

- **FindByEmailAsync(string email)**

Rechercheun utilsateur par son courriel.

```c#
var user = await _userManager.FindByEmailAsync("test@domaine.com");

```
### Gestion des mots de passe

- **CheckPasswordAsync(TUser user, string password)**

Vérifie si le mot de passe fourni est correct.

```c#
bool estValide = await _userManager.CheckPasswordAsync(user, "M0tDeP@sse!");
```

- **ChangePasswordAsync(TUser user, string oldPassword, string newPassword)**

Change le mot de passe d’un utilisateur.

```c#
var result = await _userManager.ChangePasswordAsync(user, "AncienMotDePasse", "NouveauMotDePasse");
```

- **ResetPasswordAsync(TUser user, string token, string newPassword)**

Réinitialise le mot de passe d’un utilisateur à l’aide d’un token.

```c#
var result = await _userManager.ResetPasswordAsync(user, token, "NewPassword123");
```



## Gestion des rôles

Pour utiliser les rôles utilisateur, vous devez configurer le fichier program.cs comme suit :

```c#
builder.Services.AddIdentity<IdentityUser, IdentityRole>(options =>
{
    options.Password.RequireDigit = true;
    options.Password.RequiredLength = 6;
    options.Password.RequireUppercase = false;
})
.AddEntityFrameworkStores<ApplicationDBContext>()
.AddDefaultTokenProviders();
```
- `IdentityRole` spécifie l'utilisation des rôles par l'application.

Par la suite, le `RoleManager<TRole> `est utilisé pour gérer les rôles.


### Création et suppression de rôles

- **CreateAsync(TRole role)**

Crée un nouveau rôle.

```c#
var result = await _roleManager.CreateAsync(new IdentityRole("Admin"));
```

- **DeleteAsync(TRole role)**

Supprime un rôle.

```c#
var role = await _roleManager.FindByNameAsync("Admin");
var result = await _roleManager.DeleteAsync(role);
```

### Rechercher des rôles

- **FindByIdAsync(string roleId)**

Recherche un rôle par son ID.

```c#
var role = await _roleManager.FindByIdAsync("role-guid-id");

```
- **FindByNameAsync(string roleName)**

Recherche un rôle par son nom.

```c#
var role = await _roleManager.FindByNameAsync("Admin");
```

### Vérification des rôles

- **RoleExistsAsync(string roleName)**
Vérifie si un rôle existe déjà.

```c#
bool exists = await _roleManager.RoleExistsAsync("Admin");
```

- **AddToRoleAsync(TUser user, string role)**

Ajoute un utilisateur à un rôle.

```c#
var result = await _userManager.AddToRoleAsync(user, "Admin");
```

- **RemoveFromRoleAsync(TUser user, string role)**

Retire un utilisateur d’un rôle.

```c#
var result = await _userManager.RemoveFromRoleAsync(user, "Admin");
```

- **GetRolesAsync(TUser user)**

Retourne la liste des rôles d’un utilisateur.

```c#
var roles = await _userManager.GetRolesAsync(user);
```


## Gestion de l’authentification

Le `SignInManager<TUser>` gère le processus de connexion et de déconnexion des utilisateurs.


### Connexion et déconnexion

- **PasswordSignInAsync(string userName, string password, bool isPersistent, bool lockoutOnFailure)**

Authentifie un utilisateur avec son nom d’utilisateur et mot de passe.

```c#
var result = await _signInManager.PasswordSignInAsync("testuser", "P@ssword123", false, false);
if (result.Succeeded)
{
    Console.WriteLine("Connexion réussie !");
}
```

- **SignOutAsync()**

Déconnecte l’utilisateur actuel.

```c#
await _signInManager.SignOutAsync();
```


## Configuration par défaut de IdentityOptions

Par défaut, lorsque qu'on ASP.NET Identity avec :

```c#
builder.Services.AddIdentity<IdentityUser, IdentityRole>()
    .AddEntityFrameworkStores<ApplicationDbContext>()
    .AddDefaultTokenProviders();
```

ASP.NET Identity applique les règles suivantes :

###  Règles de mot de passe (PasswordOptions)

Les contraintes de mot de passe par défaut sont les suivantes :

- Doit contenir au moins 6 caractères
- Doit contenir au moins une lettre en majuscule
- Doit contenir au moins une lettre en minuscule
- Doit contenir au moins un chiffre
- Doit contenir au moins un caractère spécial
- Ne doit pas être trop simple (basé sur une liste de mots de passe interdits)

### Règles de verrouillage (LockoutOptions)

Par défaut, ASP.NET Identity applique un **verrouillage temporaire** après plusieurs tentatives de connexion infructueuses :

- Verrouille le compte après 5 échecs de connexion
- La durée du verrouillage est de 5 minutes
- Le verrouillage est activé par défaut

### Règles sur les noms d'utilisateur et les emails (UserOptions)

Par défaut les règles suivantes sont appliquées:

- Les noms d’utilisateur doivent être uniques
- Les emails doivent être uniques
- Les emails ne sont pas obligatoires pour créer un compte (mais peuvent être requis en configurant RequireUniqueEmail = true)

### Règles d’authentification (SignInOptions)

Par défaut les règles suivantes sont appliquées pour l'authentification :

- Autoriser la connexion avec email confirmé uniquement : **Désactivé par défaut**
- Autoriser la connexion avec un fournisseur externe : **Activé par défaut**
- Authentification à deux facteurs (2FA) facultative : **Désactivé par défaut**

### Personnaliser la configuration

Il est possible de personnaliser les configurations selon le besoin dans le fichier `program.cs`

```c#

builder.Services.Configure<IdentityOptions>(options =>
{
    // Configuration du mot de passe
    options.Password.RequireDigit = true;
    options.Password.RequireLowercase = true;
    options.Password.RequireUppercase = false; // Désactiver l'obligation d'une majuscule
    options.Password.RequireNonAlphanumeric = false; // Désactiver les caractères spéciaux
    options.Password.RequiredLength = 8; // Exiger au moins 8 caractères
    options.Password.RequiredUniqueChars = 2; // Exiger au moins 2 caractères uniques

    // Configuration du verrouillage
    options.Lockout.DefaultLockoutTimeSpan = TimeSpan.FromMinutes(10); // Verrouiller pendant 10 min
    options.Lockout.MaxFailedAccessAttempts = 3; // Verrouiller après 3 tentatives échouées
    options.Lockout.AllowedForNewUsers = true;

    // Configuration de l'utilisateur
    options.User.RequireUniqueEmail = true; // Exiger un email unique

    // Configuration de l'authentification
    options.SignIn.RequireConfirmedEmail = true; // Obliger la confirmation de l'email
});
```

### `IdentityOptions` - Configuration globale
| **Catégorie** | **Propriété** | **Valeur par défaut** | **Description** |
|--------------|--------------|-----------------|-----------------|
| **Mot de passe** | `Password.RequireDigit` | `true` | Doit contenir au moins un chiffre (`0-9`) |
| | `Password.RequireLowercase` | `true` | Doit contenir au moins une lettre minuscule (`a-z`) |
| | `Password.RequireUppercase` | `true` | Doit contenir au moins une lettre majuscule (`A-Z`) |
| | `Password.RequireNonAlphanumeric` | `true` | Doit contenir au moins un caractère spécial (`!@#$%^&*`) |
| | `Password.RequiredLength` | `6` | Nombre minimum de caractères |
| | `Password.RequiredUniqueChars` | `1` | Nombre de caractères uniques requis |
| **Verrouillage** | `Lockout.DefaultLockoutTimeSpan` | `5 minutes` | Durée du verrouillage après trop de tentatives échouées |
| | `Lockout.MaxFailedAccessAttempts` | `5` | Nombre de tentatives échouées avant verrouillage |
| | `Lockout.AllowedForNewUsers` | `true` | Active le verrouillage pour les nouveaux utilisateurs |
| **Utilisateur** | `User.RequireUniqueEmail` | `false` | Exige un email unique pour chaque utilisateur |
| | `User.AllowedUserNameCharacters` | `toutes les lettres et chiffres et -._@+` | Détermine quels caractères sont autorisés dans le nom d’utilisateur |
| **Authentification** | `SignIn.RequireConfirmedEmail` | `false` | L’utilisateur doit confirmer son email avant de se connecter |
| | `SignIn.RequireConfirmedPhoneNumber` | `false` | L’utilisateur doit confirmer son numéro de téléphone avant de se connecter |
| | `SignIn.RequireConfirmedAccount` | `false` | L’utilisateur doit confirmer son compte avant d’accéder à l’application |
| **Token** | `Tokens.AuthenticatorTokenProvider` | `"Authenticator"` | Détermine le fournisseur utilisé pour l’authentification à deux facteurs |
| | `Tokens.EmailConfirmationTokenProvider` | `"Default"` | Détermine le fournisseur utilisé pour générer un token de confirmation d’email |
| | `Tokens.PasswordResetTokenProvider` | `"Default"` | Détermine le fournisseur utilisé pour générer un token de réinitialisation de mot de passe |
| **Validation des cookies** | `Stores.ProtectPersonalData` | `false` | Indique si les données utilisateur doivent être chiffrées dans la base de données |
| | `Stores.MaxLengthForKeys` | `0` | Longueur maximale des clés utilisées pour stocker les utilisateurs et rôles |


## Personnalisation du IdentityUser

Par défaut, ASP.NET Identity utilise la classe `IdentityUser`, qui contient des propriétés de base comme le nom d'utilisateur, l'email, et le mot de passe. Toutefois, si nous désirons ajouter d'autres informations (ex: prénom, date de naissance, adresse, etc.), nous devons créer une classe personnalisée qui hérite de `IdentityUser`.

### Créer une classe personnalisée pour l'utilisateur

fichier Models/Utilisateur.cs
```c#
using Microsoft.AspNetCore.Identity;
using System;

public class Utilisateur : IdentityUser
{
    public string Prenom { get; set; } // Prénom de l'utilisateur
    public string Nom { get; set; }  // Nom de l'utilisateur
    public DateTime DateNaissance { get; set; } // Date de naissance
}

```

### Modifier la configuration d'Identity pour utiliser `Utilisateur`

Dans Program.cs, modifie l'enregistrement d'Identity pour utiliser `Utilisateur` au lieu de `IdentityUser` :

```c#
builder.Services.AddIdentity<Utilisateur, IdentityRole>()
    .AddEntityFrameworkStores<ApplicationDbContext>()
    .AddDefaultTokenProviders();
```

### Modifier le contexte de base de données

```c#
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;

public class ApplicationDbContext : IdentityDbContext<Utilisateur>
{
    public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options)
        : base(options)
    {
    }
}
``` 

### Appliquer une migration pour mettre à jour la base de données

```cmd
add-migrations MiseAJourIdentityUser
update-database
```

::: Danger Attention!

Si vous utilisiez les classes suivantes dans votre code : 

```c#
UserManager<IdentityUser> 
SignInManager<IdentityUser>
```

Vous devez les modifier pour utiliser `Utilisateur`

```c#
UserManager<Utilisateur> 
SignInManager<Utilisateur>
```