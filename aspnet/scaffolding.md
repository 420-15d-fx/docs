# Scaffolding

Le Scaffolding est une génération automatique de code permettant de créer rapidement des CRUD (Create, Read, Update, Delete) pour une entité en ASP.NET Core avec Entity Framework Core.

Il permet de :

- Générer automatiquement les Razor Pages, contrôleurs et vues pour une entité.
- Créer le modèle (Model), le context EF Core (DbContext) et les migrations SQL.
- Gagner du temps dans le développement en évitant d'écrire tout le code à la main.

## Générer automatiquement les CRUD avec le Scaffolding

### Pré-requis
Avant d'être en mesure de génére les CRUD d'un modèle avec le scaffolding, vous devez vous assurez que les étapes suivantes ont été réalsées préalablement : 

- Installation **Enttity Framwork**
- Configurer le `DbContext` pour la base de données
- Configuration de la** chaîne de connexion** dans le `app.config`


### Génération des fichiers

1) Dans le dossier Pages créer un sous-dossier correspondant au modèle pour lequel vous désirez générler les fichiers CRUD : **/pages/Article**

2) Dans l'Explorateur de solutions, clic droit sur le dossier sous-dossier, ex: Pages/Articles.

3) Sélectionner : Ajouter → Nouvel élément généré automatiquement ...

4) Dans la fenêtre qui s'ouvre, sélectionné dans le menu de gauche l'option **Page Razor** est sélectionné : **Page Razor avec Entity Framework (CRUD)**.

5) Dans la fenêtre qui s'ouvre, sélectionner le **modèle** (Article) et le **DbContext** (ApplicationDbContext).

6) Cliquer sur **Ajouter**

Visual Studio génère automatiquement :

- **Index.cshtml / Index.cshtml.cs** (Liste des articles)
- **Create.cshtml / Create.cshtml.cs** (Ajouter un article)
- **Edit.cshtml / Edit.cshtml.cs** (Modifier un article)
- **Details.cshtml / Details.cshtml.cs** (Voir un article)
- **Delete.cshtml / Delete.cshtml.cs** (Supprimer un article)


