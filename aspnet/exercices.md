# Exercices sur ASP.NET Core Razor pages

## Semaine 1
::: details Exercice 1

### Exercice - Création d'un projet ASP.NET Core Razor pages

1) Créer un nouveau projet Razor Pages nommé `BiblioApp`
2) Modifier le layout global (_Layout.cshtml) pour ressembler à un vrai blog : 
    - Title : Bibliothèque
    - h1 : Bievenue sur le site de la bibliothèque
    - nav : 
        - lien vers index
        - lien vers catalogue
    - footer : @ 2025 - Biliothèqe en ligne
3)  Modifier la page d’accueil (Index.cshtml)
4)  Créer une page "Catalogue" (Catalogue.cshtml).
    - Ajouter les livres suivant dans le html : 
        - Le Petit Prince
        - 1984 - George Orwell
        - Harry Potter - J.K. Rowling
        - Les Misérables - Victor Hugo
5)  Ajouter les vues partielles suivantes : 
    - _Header : Contenant l'en-tête du site
    - _Footer : Contenant le pied de page du site

5)  Tester l’application
    - Vérifier la navigation.
    - Vérifier que les pages index et catalogue s'affichent.
:::





:::details Exercice 2

### Exercice - Navigation et formulaire 

#### Objectifs
- Créer un catalogue de livres accessible via le menu de navigation.
- Afficher la liste des livres disponibles dans la bibliothèque.
- Consulter les détails d’un livre via une page dédiée.
- Ajouter un nouveau livre au catalogue.
- Modifier un livre existant.

#### Instructions

À partir de l'exercice précédent, vous devez créer les pages permettant la gestion d'une liste de livres en mémoire (sans base de données). La liste des livres doit être partagée entre toutes les pages et mise à jour dynamiquement.

1) Créer un dossier `Models` à la racine de projet et y ajouter la classe` Livre.cs` :
    - Id (Guid)
    - Titre
    - Auteur
    - Categorie
    - AnnePublication
2) Créer le dossier `/Pages/Catalogue`
3) Créer la page `/Catalogue/Index` pour afficher la liste des livres
    - Cette page contient la **liste static de livres**.
    - Affiche sous forme de carte Bootstrap l'information de chaque livre (Titre,Auteur et catégorie) ainsi qu'un bouton permettant d'accéder aux détails d'un livre.
4) Créer la page `/Cataglogue/Create` permettant la création d'un nouveau livre. 
    - Le livre doit être ajouté à la liste de livres.
    - Une fois le livre ajouté, vous devez rediriger vers la liste des livres et afficher un message de confirmation.
5) Créer la page `/Catalogue/Details` permettant d'afficher le détails d'un livre. **L'id doit être un paramètre de route**.
6) Modifier le menu afin d'ajouter un lien vers le catalogue.

::: 


## Semaine 2


::: details Exercice 1

### Exercice - Utilisation d'Entity Framwork

#### Objectifs
Dans cet exercice, vous allez améliorer l’application précédente en :

- Persistant les données des livres dans une base de données avec Entity Framework Core.
- Ajoutant la gestion complète des livres (CRUD : Create, Read, Update, Delete).



#### Instructions

À partir de l'exercice précédent, effectuez les améliorations suivantes :

1) Ajouter Entity Framework Core en installant les packages **Microsoft.EntityFrameworkCore.SqlServer** et **Microsoft.EntityFrameworkCore.Tools**

2) Créer le Modèle `Categorie`
    - Id (int)
    - Nom (string)

3) Modifier le modèle `Livre` afin d'utiliser l'objet `Categorie` comme propriété.
4) Créer le `DbContext`
    - Créer une la classe `ApplicationDbContext.cs` dans le dossier `Data`
    - Ajouter les `DBSet` **Livres** et **Categories**
5) Configurer la chaîne de connection dans le fichier `appsettings.json`
6) Modifier le fichier `programm.cs` afin d'ajouter le `DbContext`
6) Appliquer les migrations et mettre à jour la base de données.
7) Ajouter manuellement des catégories dans la base de données.
8) Modifier les pages Index, Create et Details afin d'utiliser la base de données.
9) Ajouter une page Edit pour permettre la modification d'un article
10) Ajouter une page Delete pour permettre la suppressin d'un article

:::


::: details Exercice 2

### Exercice - Utilisation des DataAnnotations, Scaffolding et Areas

#### Objectifs

- Créer une Area "**Admin**" pour gérer les livres et catégories.
- Utiliser les **DataAnnotations** pour valider les champs du modèle.
- Utiliser le **Scaffolding** pour générer automatiquement les pages CRUD.
- Configurer correctement les routes et layouts dans une Area.


**À parti de l'exercice précédent:**

1) Modifier le modèle **Livre** afin de respecter les critèrs suivants :
    - Titre (obligatoire, max 100 caractères)
    - Auteur (obligatoire)
    - CategorieId (clé étrangère)
    - DatePublication (Obligatoire)
2) Modifier le modèle **Categorie** afin de respecter les critèrs suivants :
    - Nom (obligatoire, max 50 caractères)
3) Mettre à jour la base de données
4) Créer la zone (area) Admin :
    - Ajouter un dossier `Areas/Admin/Pages/`.
    - Ajouter les sous-dossiers `Livres` et `Categories`.
    - Ajouter un fichier `_ViewStart.cshtml` dans `Areas/Admin/Pages/`.
    - Configurer `_ViewStart.cshtml` pour utiliser le Layout global.
    - Ajouter un fichier `_ViewImport.cshtml` dans `Areas/Admin/Pages/`.
5) Généré les pages CRUD avec le scaffolding
    - Générer les Razor Pages CRUD pour `Livre` dans` Areas/Admin/Pages/Livres`.
    - Générer les Razor Pages CRUD pour `Categorie` dans `Areas/Admin/Pages/Categories`
    - Modifier les pages html pour qu'elles soient en français.
    - Ajouter un message de confirmation lors de l'ajout, la modification et la suppression.
6) Validation et Affichage des Erreurs
    - Ajouter asp-validation-summary="ModelOnly" dans les pages de création et modification.
    - Ajouter une validation pour que `AnnePublication` ne soit pas antérieur à l'année actuelle.
6) Supprimer les page create, edit et delete du catalogue
7) Supprimer le lien `Ajouter un livre `de la page `/Catalogue/Index`.
8) Supprimer les liens `Modifier` et `Supprimer` de la carte d'un livre dans la page `/Cataglogue/Index`.
7) Tester l'interface d'administration (/Admin/Livres et /Admin/Categories).

:::

## Semaine 3


::: details Exercice 1

#### Exercice - Implémentation de l’Authentification et Gestion des Rôles avec Identity

#### Objectifs
Dans cet exercice, vous allez :
- **Configurer l’authentification avec Identity** pour permettre la création de compte, la connexion et la déconnexion.
- **Créer une classe `Utilisateur` dérivée de `IdentityUser`** avec des champs personnalisés.
- **Créer les pages `Register`, `Login` et `Logout`** pour permettre aux utilisateurs de s'inscrire et de se connecter.
- **Mettre en place un système de rôles (`Utilisateur` et `Administrateur`)**.
- **Restreindre l'accès à la section d'administration** uniquement aux administrateurs.

---


#### Instructions

1) Configuration d'Identity
    - Ajouter **les packages NuGet nécessaires** pour ASP.NET Core Identity.
2) Création de la classe `Utilisateur`
    - Créer un modèle `Utilisateur` qui **hérite de `IdentityUser`**.
    - Ajouter les **champs supplémentaires** suivants :
        - `Nom` (obligatoire, max 50 caractères)
        - `Prenom` (obligatoire, max 50 caractères)
        - `DateNaissance` (optionnel)
    - Mettre à jour la base de données avec `Migrations`.
    - Configurer **Identity** dans `Program.cs` pour gérer les utilisateurs et les rôles.
    - Modifier le contexte de base de données **`AppliationDbContext`** pour intégrer `Identity`.

3) Création des Pages d'Authentification
    -  Ajouter un **dossier `Pages/Account/`** pour les pages d'authentification.
    -  Créer les **pages suivantes** :
        - **`Register.cshtml`** : Permet à un utilisateur de s'inscrire.
        - **`Login.cshtml`** : Permet à un utilisateur de se connecter.
        - **`Logout.cshtml`** : Permet à un utilisateur de se déconnecter.
    -  Modifier le layout (`_Layout.cshtml`) pour **ajouter un menu de connexion/déconnexion**.
    - Lorsque l'utilisateur est connecté, on doit afficher son nom d'utilisateur dans le menu.
    -   Modifier le layout (`_Layout.cshtml`) pour **ajouter un menu de connexion/déconnexion** pour ajouter un lien vers `/Areas/Admin` lorsqu'un administrateur est connecté.
    - Modifier la page /Areas/Admin/Index pour ajouter deux liens vers `/Areas/Admin/Pages/Categories` et `/Areas/Admin/Pages/Livres`

4) Gestion des rôles
    -  Ajouter les **rôles `Utilisateur` et `Administrateur`** au démarrage de l'application.
    -  Modifier la page `Register.cshtml.cs` pour que :
        - Par défaut, un nouvel utilisateur soit assigné au rôle `Utilisateur`.
    -  Ajouter un administrateur manuellement via la base de données.



5. Restriction de l'accès à l'administration
    - **Restreindre l'accès** à **`Areas/Admin`** qu'aux administrateurs.

6. Tests et Validation
    - **Créer un compte utilisateur** et tester la connexion.
    - **Créer un administrateur** et tester l'accès à la section `Admin`.
    - Vérifier que **les utilisateurs sans le rôle `Administrateur` ne peuvent pas accéder à l’administration**.






:::

::: details Exercice 2

#### Exercice : Ajout d’une image à un livre dans l’application Bibliothèque

#### Objectifs
Dans cet exercice, vous devez :

- Permettre le téléversement d'une image pour un livre ainsi que son affichage.

---

#### Instructions

**1. Modifier le modèle `Livre`**
- Ajouter une propriété permettant de stocker le **nom du fichier image** dans la base de données.
- Ne pas stocker le chemin d’accès complet dans la base de données. Seulement le non de l'image.

**2. Configurer le chemin d’enregistrement des images**
- Ajouter une entrée dans `appsettings.json` pour définir **le dossier où seront stockées les images**.

**3. Modifier les pages de création et modification de livres**
- Ajouter un champ permettant **de téléverser une image** lors de l’ajout et la modification d’un livre.
- Vérifier que l’image est bien enregistrée **dans le dossier défini dans `appsettings.json`**.
- Générer un **nom unique pour chaque image** afin d’éviter les conflits.

**4. Mettre à jour la base de données**
- Générer une migration et **mettre à jour la base de données**.

**5. Modifier l’affichage des livres**
- Dans la page `/Catalogue/Index`, **afficher l’image de chaque livre** à côté de son titre et de son auteur.
- Dans la page `/Catalogue/Details`, **afficher l’image en plus des informations du livre**.

**6. Styliser l’affichage des images**
- S’assurer que toutes les images des livres aient **une dimension fixe** pour un affichage uniforme.

---

:::