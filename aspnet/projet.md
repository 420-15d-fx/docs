
# Introduction à ASP.NET Core Razor Pages

## Création d'un projet de type Razor Pages
Dans Visual Studio :

1) Créer un nouveau projet
2) Sélectionner : "Application  Web ASP.NET Core (Razor Pages)"
3) Choisir .NET 8 ou 9 comme version cible
4) Configurer HTTPS
5) Lancer l’application 

## Structure d’un projet ASP.NET Core Razor Pages

Un projet Razor Pages contient plusieurs dossiers et fichiers clés :

```
MonApplication/
│── Pages/
│   ├── Index.cshtml
│   ├── Index.cshtml.cs
│   ├── About.cshtml
│   ├── About.cshtml.cs
│   ├── Contact.cshtml
│   ├── Contact.cshtml.cs
│   ├── Shared/
│   │   ├── _Layout.cshtml
│   │   ├── _ViewImports.cshtml
│   │   ├── _ViewStart.cshtml
│   ├── Error.cshtml
│   ├── Error.cshtml.cs
│── wwwroot/
│   ├── css/
│   │   ├── site.css
│   ├── js/
│   │   ├── site.js
│   ├── lib/
│── appsettings.json
│── Program.cs
│── MyRazorApp.csproj

```
### Explication des dossiers et fichiers :

- **Pages/** :  Contient toutes les pages Razor (.cshtml) et leur code-behind (.cshtml.cs).
- **Shared/** : Contient les fichiers partagés entre toutes les pages.
    - **_Layout.cshtml** :  Modèle de mise en page global.
    - **_ViewImports.cshtml** : Définit les espaces de noms communs.
    - **_ViewStart.cshtml** : Définit le fichier de mise en page par défaut.
- **Error.cshtml** : Page d'erreur standard.
- **wwwroot/** :  Contient les fichiers statiques accessibles par le client.
    - **css/** : Fichiers de style CSS.
    - **js/** : Scripts JavaScript personnalisés.
    - **lib/** : Bibliothèques front-end comme Bootstrap.
- **appsettings.json** :  Configuration de l'application (connexion BD, logs, etc.).
- **Program.cs** : Point d'entrée de l'application.
- **MonApplication**.csproj : Fichier de projet .NET.



### Fichier `Program.cs`

```c#

var builder = WebApplication.CreateBuilder(args);

// Add services to the container.
builder.Services.AddRazorPages();

var app = builder.Build();

// Configure the HTTP request pipeline.
if (!app.Environment.IsDevelopment())
{
    app.UseExceptionHandler("/Error");
    // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
    app.UseHsts();
}

app.UseHttpsRedirection();

app.UseRouting();

app.UseAuthorization();

app.MapStaticAssets();
app.MapRazorPages()
   .WithStaticAssets();

app.Run();

```

Explication des éléments importants :

- `WebApplication.CreateBuilder(args)` : Initialise l’application.
- `builder.Services.AddRazorPages();` : Active Razor Pages.
- `app.UseStaticFiles();` :  Permet l’utilisation de fichiers CSS, JS.
- `app.UseRouting();` :  Active le routage basé sur les pages.
- `app.MapRazorPages();` :  Définit l’entrée principale de l’application.
- `app.Run();` : Démarre l’application.


