

# Entity Framework (EF)

La gestion des données dans un projet ASP.NET Core razor pages se fait à l'aide d'Entity Framework.

Entity Framework (EF) est un **ORM (Object-Relational Mapper)** qui simplifie l’accès aux bases de données en .NET en permettant de manipuler des objets C# plutôt que d’écrire des requêtes SQL directement. Il se charge de la traduction des opérations effectuées en C# vers des commandes SQL pour interagir avec la base de données.

Il permet ainsi de :

- Travailler avec des objets au lieu des tables et requêtes SQL brutes.
- Gérer automatiquement les relations entre les entités (ex. clé étrangère).
- Appliquer des migrations pour modifier la structure de la base de données de manière contrôlée.
- EF Core est la version moderne et multiplateforme d'Entity Framework, compatible avec ASP.NET Core, ce qui le rend parfaitement adapté aux projets Razor Pages.

EF Core prend en charge plusieurs bases de données :

- SQL Server (Par défaut et optimisé pour .NET)
- SQLite 
- PostgreSQL 
- MySQL / MariaDB 
- Oracle / IBM Db2 
- Azure Cosmos DB

## Installation de SQL Server Express

Installer Microsoft SQL Server Express:
- https://www.microsoft.com/fr-fr/download/details.aspx?id=101064



## Utilisation d’Entity Framework Core dans ASP.NET Core Razor Pages

### 1) Installation d’Entity Framework Core

Avant de l’utiliser, il faut installer les packages nécessaires via NuGet :

!["Microsoft.EntityFrameworkCore.SqlServer"](./img/aspnet_EF_1.png)

!["Microsoft.EntityFrameworkCore.SqlServer"](./img/aspnet_EF_2.png)


## 2) Création du DBContext

Afin de pouvoir interagir avec la base de données, nous devons créer le **DBContext**.

**Fichier Data/ApplicationDbContext.cs**

```c#
using Microsoft.EntityFrameworkCore;

public class ApplicationDbContext : DbContext
{
    public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options) : base(options)
    {
    }

    public DbSet<Acticle> Articles { get; set; } // Représente la table "Articles"
}

```
- **`DbContextOptions<ApplicationDbContext> options`** :

    - `DbContextOptions<T>` est un objet de configuration qui contient les paramètres nécessaires pour se connecter à la base de données (chaîne de connexion, type de base de données, etc.).
    - ApplicationDbContext est la classe dérivée de DbContext, donc `DbContextOptions<ApplicationDbContext>` signifie que ces options sont spécifiques à ApplicationDbContext.
    - : base(options) : base(options) signifie que le constructeur de la classe parente (DbContext) est appelé avec options. Cela permet à DbContext d'utiliser les options passées pour configurer l'accès à la base de données.

- **`DbSet<Acticle> Articles`** Représente la table SQL contenant les articles.

## 3) Configuration de la Connexion à la Base de Données

Dans **appsettings.json**, on définit la chaîne de connexion :

```json
"ConnectionStrings": {
  "DefaultConnection": "Server=(LocalDB)\\MSSQLLocalDB;Database=BlogDB;Trusted_Connection=True;"
}
```

- **Server** : Nom du serveur de base de données.
- **Database** : Nom de la base de données.
- **Trusted_Connection** : Indique une connexion locale sans mot de passe.


Puis, on configure EF Core dans **Program.cs** :
```c#
builder.Services.AddDbContext<ApplicationDbContext>(options =>
    options.UseSqlServer(builder.Configuration.GetConnectionString("DefaultConnection")));

```

## 4) Appliquer les migrations et créer la base de données

En **Entity Framework Core (EF Core)**, une **migration** est un mécanisme qui permet de créer, modifier et gérer la structure de la base de données en fonction des changements apportés aux modèles C# (**classes entités**).

Lorsqu'on utilise EF Core avec une **approche Code-First**, la base de données est générée à partir des classes C#. Si on ajoute ou modifie une propriété dans un modèle, une migration permet d'appliquer ces changements à la base de données sans la recréer.

La gestion des migrations se fait à partir de la **Console du Gestionnaire de package**. Pour l'afficher : **Affichage -> Autres fenêtres -> Console du Gestionnaire de package**.

### Création d'une  migration

```cmd

add-migration AddModelArticle

```

Cette commande va créé un fichier contenant le code de la migration :

**Fichier /migrations/20250127194643_AddModelArticle.ca**

```c#
using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace BlogApp.Migrations
{
    /// <inheritdoc />
    public partial class AddModelArticle : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Articles",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    Titre = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    Contenu = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    Categorie = table.Column<string>(type: "nvarchar(max)", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Articles", x => x.Id);
                });
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Articles");
        }
    }
}


```

### Appliquer la migration à la base de données

```cmd
update-database
```


### pour supprimer une migration 

Si la dernière migration n'a pas encore été appliquée :

```cmd
Remove-Migration
```

Si la migration a déjà été appliquée alors il faut revenir à la migration précédente avec :

```cmd
Update-Database NomDeLaMigrationPrecedente
```

Après avoir annulé la migration dans la base de données, on doit supprimer manuellement le fichier `.cs` de migration et son fichier .`Designer.cs` dans le dossier `/Migrations`


## Création d'une relation 1:N

### Définition des classes Article et Categorie

**Modèle Categorie**

```c#
public class Categorie
{
    public int Id { get; set; } // Clé primaire
    public string Nom { get; set; }

    // Relation 1:N : Une catégorie peut avoir plusieurs articles
    public List<Article> Articles { get; set; }
}
```

- Par convention, une propriété nommée **Id** détermine la clé primaire.

**Modèle Article**
Un article appartient à **une seule catégorie**, donc il contient une clé étrangère **CategorieId** et une navigation vers la catégorie.

```c#
public class Article
{
    public int Id { get; set; } // Clé primaire
    public string Titre { get; set; }
    public string Contenu { get; set; }
    public DateTime DatePublication { get; set; }

    // Clé étrangère pour la relation 1:N
    public int CategorieId { get; set; }
    
    // Propriété de navigation vers la catégorie
    public Categorie Categorie { get; set; } 
}

```

- Par convetion, une clé étrangère est identifié par le **nom du modèle (classe) suivi de Id**.

### Définition du DbContext

Le **DbContext** doit inclure les **DbSet** pour les deux entités.

```c#
public class ApplicationDbContext : DbContext
{
    public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options) : base(options)
    {

    }

    public DbSet<Article> Articles { get; set; }
    public DbSet<Categorie> Categories { get; set; }

}
```

Une fois la migration créée et la base de données mise à jour, le modèle de donné sera le suivant :

!["Relation 1:N"](./img/aspnet_EF_3.png)


## Création d'une relation N:N

### Définition des classes Article et Categorie

**Modèle Categorie**
Une catégorie peut contenir plusieurs articles.

```c#
public class Categorie
{
    public int Id { get; set; } // Clé primaire
    public string Nom { get; set; }

    //Une catégorie peut avoir plusieurs articles
    public List<Article> Articles { get; set; }
}
```
**Modèle Article>**

Un article peut appartenir à plusieurs catégories.

```c#
public class Article
{
    public int Id { get; set; } // Clé primaire
    public string Titre { get; set; }
    public string Contenu { get; set; }
    public DateTime DatePublication { get; set; }

    // Collection pour la relation N:N avec ArticleCategorie
    public List<ArticleCategorie> ArticleCategories { get; set; } = new();
}
```

Une fois la migration créée et la base de données mise à jour, le modèle de donné sera le suivant :

!["Relation 1:N"](./img/aspnet_EF_4.png)

