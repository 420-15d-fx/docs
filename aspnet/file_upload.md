# Téléversement de fichier en ASP.NET Core Razor Pages

L’upload de fichier en ASP.NET Core Razor Pages se fait en plusieurs étapes :

- Ajouter un champ de téléversement de fichier dans le formulaire.
- Gérer l’envoi et le stockage du fichier dans OnPostAsync().


Dans cet exemple, **seul le nom du fichier image** est stocké dans la base de données, tandis que le chemin d'accès au dossier des images est défini dans `appsettings.json`.

## Modifier le modèle Article
Fichier Models/Article.cs :

```c#
using System;
using System.ComponentModel.DataAnnotations;

public class Article
{
    [Key]
    public int Id { get; set; }

    [Required(ErrorMessage = "Le titre est obligatoire.")]
    [StringLength(100, ErrorMessage = "Le titre ne peut pas dépasser 100 caractères.")]
    public string Titre { get; set; }

    [Required(ErrorMessage = "Le contenu est obligatoire.")]
    public string Contenu { get; set; }

    public string Auteur { get; set; }

    [DataType(DataType.Date)]
    public DateTime DatePublication { get; set; }

    public string? Image { get; set; } // Stocke uniquement le nom du fichier // [!code focus]
}
```

## Ajouter le chemin de stockage dans `appsettings.json`

`appsettings.json`

```c#

{
  "Logging": {
    "LogLevel": {
      "Default": "Information",
      "Microsoft.AspNetCore": "Warning"
    }
  },
  "ImageUploadSettings": {  // [!code focus]
    "UploadPath": "wwwroot/uploads/articles"// [!code focus]
  }, // [!code focus]
  "AllowedHosts": "*"
}

```

## Modifier la page de création d’un article

Pour permettre le téléversement de fichier, nous devons ajouter un champ de type file.

`Pages/Articles/Create.cshtml`

```razor
@page
@model MyBlog.Pages.Articles.CreateModel
@{
    ViewData["Title"] = "Créer un Article";
}

<h1>Créer un Article</h1>

<form method="post" enctype="multipart/form-data"> // [!code focus]
    <div asp-validation-summary="ModelOnly" class="alert alert-danger"></div>

    <div class="mb-3">
        <label asp-for="Article.Titre" class="form-label"></label>
        <input asp-for="Article.Titre" class="form-control" />
        <span asp-validation-for="Article.Titre" class="text-danger"></span>
    </div>

    <div class="mb-3">
        <label asp-for="Article.Contenu" class="form-label"></label>
        <textarea asp-for="Article.Contenu" class="form-control"></textarea>
        <span asp-validation-for="Article.Contenu" class="text-danger"></span>
    </div>

    <div class="mb-3">
        <label asp-for="ImageUpload" class="form-label">Image de l'article</label>
        <input type="file" asp-for="ImageUpload" class="form-control" /> // [!code focus]
        <span asp-validation-for="ImageUpload" class="text-danger"></span>
    </div>

    <button type="submit" class="btn btn-primary">Publier</button>
</form>

<a asp-page="Index">Retour à la liste</a>
```
-  `enctype="multipart/form-data"` permet l'envoi de fichier par le formulaire.
-  `type="file"` permet la sélection du fichier à téléverser.

Fichier `Pages/Articles/Create.cshtml.cs`

```c#

using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.AspNetCore.Mvc.Rendering;
using BlogApp.Models;
using Microsoft.AspNetCore.Authorization;


namespace BlogApp.Areas.Admin.Pages.Articles
{
    [Authorize]
    public class CreateModel : PageModel
    {
        private readonly BlogApp.Data.ApplicationDBContext _context;
        private readonly IConfiguration _configuration;

        public CreateModel(BlogApp.Data.ApplicationDBContext context, IConfiguration configuration)
        {
            _context = context;
            _configuration = configuration;
        }

        public IActionResult OnGet()
        {
            ViewData["CategorieId"] = new SelectList(_context.Categories, "Id", "Nom");
            return Page();
        }

        [BindProperty]
        public Article Article { get; set; } = default!;

        [BindProperty]
        public IFormFile ImageUpload { get; set; } //Contient l'image téléversée  // [!code focus]
        

        // For more information, see https://aka.ms/RazorPagesCRUD.
        public async Task<IActionResult> OnPostAsync()
        {
            Article.DatePublication = DateTime.Now;

            if (!ModelState.IsValid)
            {
                ViewData["CategorieId"] = new SelectList(_context.Categories, "Id", "Nom");
                return Page();
            }

            if (ImageUpload != null) // [!code focus]
            { // [!code focus] 
                var uploadPath = _configuration["Images:UploadPath"]; //Obtient le chemin d'accès dans le appsettings.json // [!code focus]

                if (string.IsNullOrEmpty(uploadPath)) // [!code focus]
                {
                    ModelState.AddModelError("", "Erreur interne : le chemin pour le téléversement est mal configuré dans le appsettings.json."); // [!code focus]
                    return Page(); // [!code focus]
                } // [!code focus]

                
                var fullPath = Path.Combine(Directory.GetCurrentDirectory(), uploadPath); // [!code focus]


                if (!Directory.Exists(fullPath)) // [!code focus]
                { // [!code focus]
                    Directory.CreateDirectory(fullPath); // [!code focus]
                } // [!code focus]

                var fileName = Guid.NewGuid().ToString() + Path.GetExtension(ImageUpload.FileName); // [!code focus]
                var filePath = Path.Combine(fullPath, fileName); // [!code focus]

                using (var stream = new FileStream(filePath, FileMode.Create)) // [!code focus]
                {
                    await ImageUpload.CopyToAsync(stream); // [!code focus]
                }

                Article.ImageFileName = fileName; // [!code focus]
            } // [!code focus]

            _context.Articles.Add(Article); 
            await _context.SaveChangesAsync(); 

            return RedirectToPage("./Index");
        }
    }
}

```




## Afficher l’image dans la page d’affichage des articles

`/Pages/Articles/Index.cshtml.cs`

```c#
using BlogApp.Data;
using BlogApp.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.Build.Framework;
using Microsoft.EntityFrameworkCore;
using System.Collections.Immutable;

namespace BlogApp.Pages.Articles
{
    public class IndexModel : PageModel
    {
        private readonly ApplicationDbContext _context;
        private readonly IConfiguration _configuration;  // [!code focus]

        public static List<Article> Articles = new();

        public string ImagePath { get; set; } //Contient le chemin d'accès aux images  // [!code focus]

        public IndexModel(ApplicationDbContext context, IConfiguration configuration)  // [!code focus]
        {
            _context = context; 
            _configuration = configuration;  // [!code focus]
        }
        public async Task OnGet()
        {
            
            Articles = await _context.Articles.Include(c => c.Categorie).ToListAsync();

            ImagePath = _configuration["Images:PublicUrl"]; //Lecture du fichier appsetting.json  // [!code focus]
        }
    }
}


```

`/Pages/Articles/Index.cshtml`

```razor

@page
@model BlogApp.Pages.Articles.IndexModel

<h1 class="text-center my-4">Liste des articles</h1>

@if (TempData["Message"] != null)
{
    <div class="alert alert-success" role="alert">
        @TempData["Message"]
    </div>
}

@if (Model.Articles.Count == 0)
{
    <p class="text-center text-muted">Aucun article disponible.</p>
}
else
{
    <div class="container">
        <div class="row">
            @foreach (var article in Model.Articles)
            {
                <div class="col-md-6">
                    <div class="card mb-4 shadow-sm">
                        <div class="card-body">
                            <div>
                                @if (!string.IsNullOrEmpty(article.ImageFileName))
                                {
                                    <img class="article-image" src="@Model.ImagePath@article.ImageFileName" /> // [!code focus]
                                }
                            </div>
                            <h2 class="card-title">@article.Titre</h2>
                            <p class="text-muted"><strong>Catégorie :</strong> @article.Categorie.Nom</p>
                            <p class="card-text">@article.Contenu</p>
                            <a asp-page="/Articles/Details" asp-route-id="@article.Id" class="btn btn-info mt-2">Voir Détails</a>
                        </div>
                    </div>
                </div>
            }
        </div>
    </div>
}

```

