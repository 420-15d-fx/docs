# Jeux d'enregistrements

## Qu'est-ce que le "Seeding" (Jeux d'enregistrements) ?

Le seeding consiste à insérer des données initiales dans la base de données lors du démarrage de l’application.
Ces données sont appelées jeux d'enregistrements (Seeds) et sont utiles pour : 
- Remplir une base de données vide avec des données de test.
- Ajouter des catégories, utilisateurs, rôles ou d'autres informations indispensables à l'application.
- Faciliter le développement en ayant des données prêtes dès le lancement.

## Exemple d'utilisation des seeds

### 1. Création d'une classe statique


```c#

using BlogApp.Models;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using NuGet.LibraryModel;
using SQLitePCL;
using System.Data;

namespace BlogApp.Data
{
    public static class SeedData
    {
        private static UserManager<IdentityUser> _userManager;

        public static async Task Initialize(ApplicationDBContext context, UserManager<Utilisateur> userManager, RoleManager<IdentityRole> roleManager)
        {
           
            //Création des rôles
            if (!await roleManager.Roles.AnyAsync())
            {
                await roleManager.CreateAsync(new IdentityRole("Administrateur"));
                await roleManager.CreateAsync(new IdentityRole("Utilisateur"));
            }

            //Création des utilisateurs
            if (!await userManager.Users.AnyAsync()) {
            
                //Création de l'administrateur
                var user = new Utilisateur { UserName = "admin", Email = "admin@admin.com", Prenom = "Admin", Nom = "Admin" };
                await userManager.CreateAsync(user, "Password123!");
                await userManager.AddToRoleAsync(user, "Administrateur");
            
            }

            //Création des catégories
            if (!context.Categories.Any())
            {

                context.Categories.AddRange(
                    new Categorie { Nom = "ASP.NET Core Razor Pages" },
                    new Categorie { Nom = "Entity Framework" }
                );

               
                context.SaveChanges();



            }

            var categorieIds = context.Categories.Select(c => c.Id).ToList(); // Récupérer tous les IDs de catégories

            Random rnd = new Random();

            //Ajout des articles
            if (!context.Articles.Any())
            {
                context.Articles.AddRange(

                      new Article
                      {
                          Titre = "Premier article de blog",
                          Contenu = "Ceci est un article généré automatiquement.",
                          DatePublication = DateTime.Now,
                          ImageFileName = "image1.jpg",
                          CategorieId = categorieIds[rnd.Next(categorieIds.Count)], //Obtient une catérorie aléatoire
                      },
                    new Article
                    {
                        Titre = "Un autre article intéressant",
                        Contenu = "Cet article a été ajouté via le seeding.",
                        DatePublication = DateTime.Now,
                        ImageFileName = "image2.jpg",
                        CategorieId = categorieIds[rnd.Next(categorieIds.Count)],
                    }
                 );

                context.SaveChanges() ;
            }

        }

    }
}


```


### 2. Exécuter SeedData.Initialize() au démarrage de l’application

`Fichier program.cs`
```c#
using BlogApp.Data;
using BlogApp.Models;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;

var builder = WebApplication.CreateBuilder(args);

builder.Services.AddIdentity<Utilisateur, IdentityRole>()
.AddEntityFrameworkStores<ApplicationDBContext>()
.AddDefaultTokenProviders();

builder.Services.AddDbContext<ApplicationDBContext>(options =>
    options.UseSqlServer(builder.Configuration.GetConnectionString("DefaultConnection")));

var app = builder.Build();

//Création des seeds au démarrage de l'application // [!code focus]
using (var scope = app.Services.CreateScope()) // [!code focus]
{ // [!code focus]
    var services = scope.ServiceProvider; // [!code focus]

    //On obtient les services nécessaires  // [!code focus]
    var context = services.GetRequiredService<ApplicationDBContext>(); // [!code focus]
    var userManager = services.GetRequiredService<UserManager<Utilisateur>>(); // [!code focus]
    var roleManager = services.GetRequiredService<RoleManager<IdentityRole>>(); // [!code focus]

    await SeedData.Initialize(context, userManager, roleManager); // [!code focus]

} // [!code focus]

// Configure the HTTP request pipeline.
if (!app.Environment.IsDevelopment())
{
    app.UseExceptionHandler("/Error");
    // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
    app.UseHsts();
}

app.UseHttpsRedirection();

app.UseRouting();

app.UseAuthorization();

app.MapStaticAssets();
app.MapRazorPages()
   .WithStaticAssets();

app.Run();


```