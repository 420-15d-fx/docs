# Razor Pages, gestion des routes et passage de données 

Dans ASP.NET Core Razor Pages, la route par défaut est basée sur la structure des fichiers `.cshtml` dans le dossier `Pages`. Elle suit ce format :

```
/{nom-de-la-page}
```
Exemple : Si vous avez un fichier About.cshtml situé dans Pages :

```
Pages/
│── About.cshtml
```

Alors, l’URL de cette page sera :

```
https://localhost:7022/about
```

## Format de la Route Par Défaut

Par défaut, ASP.NET Core utilise ce modèle de route :

```
/{page}/{handler?}
```

- `{page}` : Le nom de la page Razor (ex. Index, About).
- `{handler?}` : Un gestionnaire (OnGet, OnPost, etc.), qui est optionnel dans l’URL. Celui-ci est déterminé par défaut par la méthode HTTP utilisée (GET, POST).


## Page dans un Sous-Dossier

Si votre fichier est dans un sous-dossier, son chemin influence la route.

```
Pages/
│── Produits/
│   ├── Liste.cshtml
```

URL correspondante 
```
https://localhost:7022/Produits/Liste

```

## URL avec Query String

Par défaut, Razor Pages utilise la méthode avec query string, c'est-à-dire :

**Exemple** : https://localhost:5001/Produits/Details?id=5

- L’ID est passé comme un paramètre **GET** (id=5).
- Dans la page Details.cshtml.cs, on peut le récupérer ainsi :


**Page Details.cshtml.cs dans /Pages/Produits**

```c#
public class DetailsModel : PageModel
{
    public int Id { get; private set; }

    public void OnGet(int id)
    {
        Id = id; // Stocke la valeur pour affichage
    }
}
```

Voici comment créer vers cette page en utilisant le query string :

```html
<a asp-page="/Produits/Details" asp-route-id="5">Voir le produit</a>
```

- **asp-page="/Produits/Details"**: Spécifie la page cible.
- **asp-route-id="5"** : Génère une query string ?id=5.

## URL avec Paramètre dans la Route (/Details/5)

Si vous voulez utiliser l’URL https://localhost:5001/Produits/Details/5, vous devez modifier la route dans Razor Pages.

Pour ce faire, vous devez ajout du paramètre dans la page Razor. 


**Page Details.cshtml dans /Pages/Produits**
```razor
@page "{id:int}"
@model DetailsModel
<h1>Détails du produit @Model.Id</h1>
```

**Page Details.cshtml.cs dans /Pages/Produits**

```c#
public class DetailsModel : PageModel
{
    public int Id { get; private set; }

    public void OnGet(int id)
    {
        Id = id; // Stocke la valeur pour affichage
    }
}
```

Voici comment créer vers cette page en utilisant un paramètre de route :

```html
<a href="@Url.Page("/Produits/Details", new { id = 5 })">Voir le produit</a>
```

## Page avec un Gestionnaire (Handler)
Dans Razor Pages, vous pouvez utiliser des gestionnaires pour distinguer plusieurs actions.

```c#
public IActionResult OnGetEdit(int id)
{
    // Code pour modifier le produit
    return Page();
}

``` 
**URL correspondante :**

```html

https://localhost:5001/Produits/Details?handler=Edit&id=5

```
pour crée un lien vers un gestionnaire : 

```html

<a asp-page="/Produits/Details" asp-page-handler="Edit" asp-route-id="5">Supprimer le produit</a>

```

## Passer de l'information à la vue

Il existe plusieurs manières de transmettre des informations à une vue (.cshtml) :

- **ViewData** :  Stocke des données temporaires accessibles uniquement pendant la requête en cours.
- **TempData** : Persiste les données entre plusieurs requêtes (utile après une redirection).
- **BindProperty** : Lie automatiquement une propriété du modèle au formulaire HTML.

### Utilisation de ViewData

- `ViewData` est un **dictionnaire** (Dictionary<string, object>) qui permet de stocker des valeurs temporairement pendant une seule requête.
- Il est accessible **uniquement** dans la même page où il est défini.

**Code dans Index.cshtml.cs (code-behind) :**
```c#
public class IndexModel : PageModel
{
    public void OnGet()
    {
        ViewData["Message"] = "Bienvenue sur mon blog !";
        ViewData["Date"] = DateTime.Now.ToString("dddd dd MMMM yyyy");
    }
}
```

**Code dans Index.cshtml :**

```razor
@page
@model IndexModel
@{
    ViewData["Title"] = "Accueil";
}

<h1>@ViewData["Message"]</h1>
<p>Nous sommes le @ViewData["Date"]</p>
```

::: warning Attention!
Les données sont perdues après le rechargement ou la redirection !
:::

## Utilisation de TempData

- `TempData` fonctionne comme `ViewData`, mais les données restent disponibles après une redirection.
- Il utilise les cookies ou la session pour stocker temporairement les données.

### Afficher un message après une redirection

**Page qui enregistre un article (Create.cshtml.cs) :**

```c#
public class CreateModel : PageModel
{
    public IActionResult OnPost()
    {
        TempData["SuccessMessage"] = "Article ajouté avec succès !";
        return RedirectToPage("Index");
    }
}
```
- `IActionResult` est un type de retour qui représente une réponse HTTP.
    - Il permet de retourner différents types de réponses, comme une vue (`Page())`, une redirection (`Redirect()`), ou un JSON (`Json()`).
- `RedirectToPage("Index")` effectue une redirection vers Index.cshtml. 

**Page d’accueil qui affiche le message (Index.cshtml) :**

```razor
@page
@model IndexModel
@{
    ViewData["Title"] = "Accueil";
}

@if (TempData["SuccessMessage"] != null)
{
    <p lass="alert alert-success" role="alert" >@TempData["SuccessMessage"]</p>
}
```


- L'affichage du message persiste après la redirection.
- Les données sont effacées après la première utilisation.

Si on veut que `TempData` persiste plus longtemps, on peut utiliser **Keep()** :


```c#
public class CreateModel : PageModel
{
    public IActionResult OnPost()
    {
        TempData["SuccessMessage"] = "Article ajouté avec succès !";
        TempData.Keep("SuccessMessage"); // Conserve la donnée après la requête suivante
        return RedirectToPage("Index");
    }
}
```

## Utilisation de propriétés

Cette méthode permet d'obtenir l'information directement à partir des propriétés d'un modèle.
- Fonctionne sans passer par `ViewData` ou `TempData`.

**Code dans Index.cshtml.cs (code-behind) :**

```c#
public class IndexModel : PageModel
{
    public string Message { get; set; }

    public DateTime Date { get; set; }

    public void OnGet()
    {
        Message = "Bienvenue sur mon blog !";
        Date = DateTime.Now.ToString("dddd dd MMMM yyyy");
    }
}
```
**Page dans Index.cshtml  :**
```razor
@page
@model IndexModel
@{
    ViewData["Title"] = "Accueil";
}

<div class="text-center">

    <h1>@Model.Message</h1>
    <p>Nous sommes le @Model.Date.ToLongDateString()</p>

</div>
```
- `@Model` permet d'accéder au propriété déclarées dans le code-behing.