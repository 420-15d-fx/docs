# Personnalisation des Pages d'Erreurs (404, 403, etc.)

il est possible de personnaliser les pages d'erreurs comme :

- **404** - Page non trouvée
- **403** - Accès interdit
- **500** - Erreur interne du serveur

Il existe deux manière de le faire

## Méthode 1 : Gestion par code d'erreur.

Il s'agit dans ce cas de créer une page d'erreur ayant le code d'erreur comme nom.

### 1. Activer la gestion des erreurs dans Program.cs

`Fichier program.cs`

```c#
var builder = WebApplication.CreateBuilder(args);

builder.Services.AddRazorPages();

var app = builder.Build();

// Activer la gestion des erreurs
if (!app.Environment.IsDevelopment())
{
    app.UseExceptionHandler("/Error/500"); // Redirige les erreurs 500   // [!code focus]
    app.UseStatusCodePagesWithReExecute("/Error/{0}"); // Redirige les erreurs 404, 403, etc.  // [!code focus]
}

app.UseStaticFiles();
app.UseRouting();
app.UseAuthorization();
app.MapRazorPages();
app.Run();
```
- `app.UseExceptionHandler("/Error/500");` : Redirige les erreurs internes (500) vers /Error/500.
- `app.UseStatusCodePagesWithReExecute("/Error/{0}");` :  Redirige les erreurs 404, 403, etc. vers /Error/{Code}.

### 2. Créer un dossier Pages/Error/ pour stocker les pages d'erreurs

Créer un dossier dédié aux pages d'erreurs.

```text
/Pages
  /Error
    404.cshtml
    403.cshtml
    500.cshtml
```

### 3.  Créer une page d’erreur 404 (Page non trouvée)

Fichier `Pages/Error/404.cshtml` :

```razor
@page
@model MyBlog.Pages.Error.NotFoundModel
@{
    ViewData["Title"] = "Page non trouvée";
}

<h1>Erreur 404 - Page non trouvée</h1>
<p>La page que vous recherchez n'existe pas ou a été déplacée.</p>
<a asp-page="/Index" class="btn btn-primary">Retour à l'accueil</a>
```

## Méthode 2 : Gestion par une page unique

Il s'agit dans ce cas-ci de créer une seule page qui affichera un message différent selon le code d'erreur 



### 1. Activer la gestion des erreurs dans Program.cs

`Fichier program.cs`

```c#
var builder = WebApplication.CreateBuilder(args);

builder.Services.AddRazorPages();

var app = builder.Build();

// Activer la gestion des erreurs
if (!app.Environment.IsDevelopment())
{
    app.UseExceptionHandler("/Error"); // Redirige les erreurs 500   // [!code focus]
    app.UseStatusCodePagesWithReExecute("/Error","?Code={0}"); // Redirige les erreurs 404, 403, etc.  // [!code focus]
}

app.UseStaticFiles();
app.UseRouting();
app.UseAuthorization();
app.MapRazorPages();
app.Run();
```
- `app.UseExceptionHandler("/Error")` : Redirige les erreur 500 vers la page /Pages/Error
- `app.UseStatusCodePagesWithReExecute("/Error","?Code={0}")` : Redirige les erreurs 404, 403, etc. vers la page /Page/Error en ayant comme `query string` le code d'erreur.

### 3.  Créer une page d’erreur générique

Fichier `/Pages/Erreur.cshtml`

```razor
@page "{code:int?}"
@model BlogApp.Pages.Error.IndexModel
@{
}
@{
    ViewData["Title"] = $"Erreur {Model.StatusCode}";
}

<h1>Erreur @Model.StatusCode</h1>

@if (Model.StatusCode == 404)
{
    <p>La page demandée est introuvable.</p>
}
else if (Model.StatusCode == 403)
{
    <p>Vous n'avez pas la permission d'accéder à cette page.</p>
}
else
{
    <h2>Oups ! Une erreur est survenue.</h2>
    <p>Nous avons rencontré une erreur inattendue.</p>
}

<a asp-page="/Index" class="btn btn-secondary">Retour à l'accueil</a>

```

Fichier `/Pages/Error.cshtml.cs`

```c#
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;

namespace BlogApp.Pages.Error
{
    public class IndexModel : PageModel
    {
        public int StatusCode { get; set; }

        public void OnGet(int? code)
        {
            StatusCode = code ?? 500; // Défaut à 500 si aucun code n'est passé
        }
    }
}
```