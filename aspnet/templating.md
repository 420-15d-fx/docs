# Gabarit de page

Dans une application **Razor Pages**, on utilise un **layout global** (_Layout.cshtml) pour définir la structure commune à toutes les pages.


## Le layout global (_Layout.cshtml)

``` html
<!DOCTYPE html>
<html>
<head>
    <title>@ViewData["Title"] - Mon Application</title>
    <link rel="stylesheet" href="~/css/site.css" />

    @RenderSection("Styles", required: false)
</head>
<body>
    <header>
        <h1>Mon Blog</h1>
    </header>

    <main>
        @RenderBody()
    </main>

    <footer>
        <p>© 2024 - Mon Application</p>
    </footer>

    @await RenderSectionAsync("Scripts", required: false)
</body>
</html>
```
- `@RenderSection("Styles", required: false)` : Permet d'inclure du style spécifique à une page. `False` indique que cette section n'est pas obligatoire dans la page.
- `@RenderBody()` : Remplacé par le contenu de chaque page Razor.
- `@await RenderSectionAsync("Scripts", required: false)` : Permet d’inclure du JavaScript spécifique à certaines pages. `False` indique que cette section n'est pas obligatoire dans la page.
- `@ViewData["Title"]` : Permet de définir un titre dynamique pour chaque page.

## Utilisation d’un Layout dans une Page Razor

Dans la page `Index.cshtml`, on définit le layout global avec `_ViewStart.cshtml.`


**Pages/_ViewStart.cshtml :**

```razor
@{
    Layout = "/Pages/Shared/_Layout.cshtml";
}

```
**Pages/Index.cshtml**

```html
@page
@model IndexModel
@{
    ViewData["Title"] = "Accueil";
}

<h1>Bienvenue sur mon Blog</h1>
<p>ASP.NET Core Razor Pages est un framework puissant et moderne.</p>

@section Scripts {
    <script>
        console.log("Script exécuté sur la page d’accueil !");
    </script>
}

```
- `@page` :  Indique que ce fichier est une page Razor.
- `@model IndexModel` : Associe un fichier .cshtml.cs (code-behind).
- `@section Scripts {}` : Ajoute un script spécifique uniquement pour cette page.



## Vues partielles : Réutiliser des éléments

Les vues partielles permettent d’extraire du code HTML réutilisable dans plusieurs pages.

### Création d’une vue partielle _Header.cshtml

1) **Naviguez vers le dossier souhaité dans l'Explorateur de solutions** :

    - Par convention, les vues partielles sont souvent placées dans le dossier Pages/Shared.
2) **Cliquez droit sur le dossier → Ajouter → Nouvel élément**.

3) **Choisissez "Fichier Vue Razor"** :

    - Dans la boîte de dialogue **Ajouter un nouvel élément**, recherchez "**Vue Razor**" dans la barre de recherche.
    - Sélectionnez "**Vue Razor - Vide**" et donnez-lui un nom commençant par un underscore (_), comme _Header.cshtml.

4) Cliquez sur "Ajouter" pour créer la vue partielle.

**Pages/Shared/_Header.cshtml**

```html
<header>
    <h1>Mon Blog</h1>
    <nav>
        <a asp-page="/Index">Accueil</a> |
    </nav>
</header>
```

**Utilisation dans _Layout.cshtml**

```html
<body>

    <partial name="_Header" />

    <div class="container">
        <main role="main" class="pb-3">
            @RenderBody()
        </main>
    </div>

    <footer class="border-top footer text-muted">
        <div class="container">
            &copy; 2025 - MonBlogue - <a asp-area="" asp-page="/Privacy">Privacy</a>
        </div>
    </footer>
</body>
```

### Passer des données à une vue partielle

Il est possible de passser des données à une vue partielle.

Par exemple, si vous avez une liste d'articles que vous souhaitez afficher dans une vue partielle :

**1) Création de la vue partielle**

Vue partielle : `Pages/Shared/_ListeArticles.cshtml`

```razor
@model List<Article>

<ul class="list-group">
    @foreach (var article in Model)
    {
        <li class="list-group-item">
            <h4>@article.Titre</h4>
            <p>@article.Contenu</p>
        </li>
    }
</ul>
```

-`@model List<Article>` définit que cette vue partielle attend une liste d'articles.

**2) Passer des données à la vue partielle dans la page principale**

Fichier `/Pages/Articles/Index.cshtml`
```razor

@page
@model MyBlog.Pages.Articles.IndexModel
@{
    ViewData["Title"] = "Liste des articles";
}

<h1>Articles du blog</h1>

@await Html.PartialAsync("_ListeArticles", Model.Articles)

```

Fichier `/Pages/Article/Index.cshtml.cs`

```c#
using Microsoft.AspNetCore.Mvc.RazorPages;
using System.Collections.Generic;

namespace MyBlog.Pages.Articles
{
    public class IndexModel : PageModel
    {
        public List<Article> Articles { get; set; } = new List<Article>
        {
            new Article { Id = 1, Titre = "Premier article", Contenu = "Ceci est le premier article." },
            new Article { Id = 2, Titre = "Deuxième article", Contenu = "Ceci est un autre article intéressant." }
        };

        public void OnGet() { }
    }
}
```


