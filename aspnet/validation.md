# Validation des données

Les DataAnnotations sont des attributs utilisés en ASP.NET Core pour valider automatiquement les entrées utilisateur dans les modèles liés aux formulaires.

Par exemple, dans un Blog, nous voulons nous assurer que :
- Le titre est obligatoire et ne dépasse pas 100 caractères.
- Le contenu est obligatoire et doit comporter au moins 50 caractères.
- La catégorie doit être sélectionnée obligatoirement.
- La date de publication doit être postérieure à une date donnée.

## Utilisation des DatatAnnotations

### 1) Déclaration des règles de validation dans le modèle**

Les règles de validation sont définies dans modèle à l’aide des `DataAnnotations`.

```c#
using System;
using System.ComponentModel.DataAnnotations;

public class Article
{
    public Guid Id { get; set; } = Guid.NewGuid();

    [Required(ErrorMessage = "Le titre est requis.")]
    [StringLength(50, ErrorMessage = "Le titre ne peut pas dépasser 50 caractères.")]
    public string Titre { get; set; }

    [Required(ErrorMessage = "Le contenu est requis.")]
    [MinLength(200, ErrorMessage = "Le contenu doit contenir au moins 200 caractères.")]
    public string Contenu { get; set; }

    [Required(ErrorMessage = "Veuillez sélectionner une catégorie.")]
    public string Categorie { get; set; }

    [Required(ErrorMessage = "La date de publication est requise.")]
    [DataType(DataType.Date)]
    [Range(typeof(DateTime), "2023-01-01", "2100-12-31", ErrorMessage = "La date de publication doit être postérieure au 1er janvier 2023.")]
    public DateTime DatePublication { get; set; }
}
```

### 2) Appliquer la validation dans la page Create.cshtml.cs

```c#
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;

public class ArticlesCreateModel : PageModel
{
    [BindProperty]
    public Article NouvelArticle { get; set; } = new Article();

    public List<string> Categories { get; set; } = new List<string> { "Tech", "Science", "Sport", "Culture" };

    public void OnGet()
    {
    }

    public IActionResult OnPost()
    {
        if (!ModelState.IsValid)
        {
            return Page(); // Reste sur la page si le modèle est invalide
        }

        ArticlesIndexModel.Articles.Add(NouvelArticle);
        TempData["Message"] = $"L'article '{NouvelArticle.Titre}' a été ajouté avec succès !";
        return RedirectToPage("Index");
    }
}
```

`ModelState.IsValid` Vérifie si toutes les contraintes de validation sont respectées. Si le formulaire est invalide (`!ModelState.IsValid`), la page reste sur Create.cshtml pour afficher les erreurs. Si tout est valide, l'article est ajouté et redirige vers Index.

### 3) Ajouter les messages d’erreur dans Create.cshtml

```html
@page
@model ArticlesCreateModel

<h1 class="text-center my-4">Créer un article</h1>

<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-6">
            <form method="post" class="border p-4 shadow rounded bg-light">
                <div class="mb-3">
                    <label class="form-label">Titre :</label>
                    <input asp-for="NouvelArticle.Titre" class="form-control" placeholder="Entrez le titre de l'article" />
                    <span asp-validation-for="NouvelArticle.Titre" class="text-danger"></span>
                </div>

                <div class="mb-3">
                    <label class="form-label">Contenu :</label>
                    <textarea asp-for="NouvelArticle.Contenu" class="form-control" rows="5"></textarea>
                    <span asp-validation-for="NouvelArticle.Contenu" class="text-danger"></span>
                </div>

                <div class="mb-3">
                    <label class="form-label">Catégorie :</label>
                    <select asp-for="NouvelArticle.Categorie" class="form-select">
                        <option value="">-- Sélectionnez une catégorie --</option>
                        @foreach (var cat in Model.Categories)
                        {
                            <option value="@cat">@cat</option>
                        }
                    </select>
                    <span asp-validation-for="NouvelArticle.Categorie" class="text-danger"></span>
                </div>

                <div class="mb-3">
                    <label class="form-label">Date de publication :</label>
                    <input asp-for="NouvelArticle.DatePublication" type="date" class="form-control" />
                    <span asp-validation-for="NouvelArticle.DatePublication" class="text-danger"></span>
                </div>

                <button type="submit" class="btn btn-primary w-100">Publier</button>
            </form>
        </div>
    </div>
</div>
```

`asp-validation-for="NouvelArticle.Titre"` affiche un message d’erreur sous le champ en cas d’invalidité. La `class="text-danger"` affuce les messages d’erreur en rouge. Les erreurs sont générées automatiquement par ASP.NET Core en fonction des `DataAnnotations`.

### Ajout d’un Message d’Erreur Global en Utilisant ModelState.AddModelError

Il est possible d'ajouter des messages personnalisés en utilisant le ModelState.AddModelError:

Fichier Pages/Articles/Create.cshtml.cs

```c#
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System;
using System.Linq;
using System.ComponentModel.DataAnnotations;

public class ArticlesCreateModel : PageModel
{
    [BindProperty]
    public Article NouvelArticle { get; set; } = new Article();

    public List<string> Categories { get; set; } = new List<string> { "Tech", "Science", "Sport", "Culture" };

    private readonly List<string> MotsInterdits = new List<string> { "pirate", "hack", "illégal" }; // Liste de mots interdits

    public void OnGet()
    {
    }

    public IActionResult OnPost()
    {
        if (!ModelState.IsValid)
        {
            ModelState.AddModelError("", "Veuillez corriger les erreurs ci-dessous."); // Message d'erreur global
            return Page();
        }

        // Vérification des mots interdits dans le titre ou le contenu
        if (MotsInterdits.Any(mot => NouvelArticle.Titre.Contains(mot, StringComparison.OrdinalIgnoreCase) ||
                                      NouvelArticle.Contenu.Contains(mot, StringComparison.OrdinalIgnoreCase)))
        {
            ModelState.AddModelError("", "Votre article contient des mots interdits. Veuillez les supprimer.");
            return Page();
        }

        ArticlesIndexModel.Articles.Add(NouvelArticle);
        TempData["Message"] = $"L'article '{NouvelArticle.Titre}' a été ajouté avec succès !";
        return RedirectToPage("Index");
    }
}
```

`ModelState.AddModelError("", "Message")` ajoute un message d’erreur global. Celui-ci sera affiché si le modèle est invalide.


 Fichier Pages/Articles/Create.cshtml

 ```html
 @page
@model ArticlesCreateModel

<h1 class="text-center my-4">Créer un article</h1>

<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-6">

            <form method="post" class="border p-4 shadow rounded bg-light">
                <div asp-validation-summary="ModelOnly" class="text-danger"></div>
                <div class="mb-3">
                    <label class="form-label">Titre :</label>
                    <input asp-for="NouvelArticle.Titre" class="form-control" placeholder="Entrez le titre de l'article" />
                    <span asp-validation-for="NouvelArticle.Titre" class="text-danger"></span>
                </div>

                <div class="mb-3">
                    <label class="form-label">Contenu :</label>
                    <textarea asp-for="NouvelArticle.Contenu" class="form-control" rows="5"></textarea>
                    <span asp-validation-for="NouvelArticle.Contenu" class="text-danger"></span>
                </div>

                <div class="mb-3">
                    <label class="form-label">Catégorie :</label>
                    <select asp-for="NouvelArticle.Categorie" class="form-select">
                        <option value="">-- Sélectionnez une catégorie --</option>
                        @foreach (var cat in Model.Categories)
                        {
                            <option value="@cat">@cat</option>
                        }
                    </select>
                    <span asp-validation-for="NouvelArticle.Categorie" class="text-danger"></span>
                </div>

                <button type="submit" class="btn btn-primary w-100">Publier</button>
            </form>
        </div>
    </div>
</div>
```
L’attribut `asp-validation-summary` est utilisé pour afficher les erreurs de validation générées par le modèle dans un formulaire. Il peut être configuré pour afficher différents types d'erreurs.

**Options disponibles pour asp-validation-summary**
- **None** : N'affiche aucun message de validation
- **ModelOnly** : Affiche uniquement les erreurs globales (ModelState.AddModelError("", "...")) 
- **All** : Affiche toutes les erreurs (modèle + champs spécificiques)

### ModelState.AddModelError(string key, string errorMessage);

- **Key** : Nom du champ auquel l’erreur est associée, ou "" pour une erreur générale.
- **errorMessage** : Message d'erreur personnalisé affiché à l’utilisateur.

Si l'on veut afficher un message d'erreur général, on passe une chaîne vide ("") comme clé.

```c#
ModelState.AddModelError("", "Une erreur est survenue. Veuillez réessayer.");
```

Si l’on veut associer une ou plusieurs erreurs à un champ particulier, on met le nom de la propriété comme clé.

```c#
ModelState.AddModelError("Titre", "Le titre ne peut pas contenir le mot 'pirate'.");
ModelState.AddModelError("Titre", "Le titre doit commencer par une majuscule.");
```

## Liste des DataAnnotations

Voici un liste des différent `DataAnnotations`:

### Validation de base

**[Required] - Champ obligatoire**

```c#
[Required(ErrorMessage = "Le titre est obligatoire.")]
public string Titre { get; set; }

```

**[StringLength] - Limite la longueur d’un texte**

```c#
[StringLength(100, ErrorMessage = "Le titre ne peut pas dépasser 100 caractères.")]
public string Titre { get; set; }
```

**[MinLength] - Longueur minimale du texte**

```c#
[MinLength(10, ErrorMessage = "Le contenu doit contenir au moins 10 caractères.")]
public string Contenu { get; set; }
```

**[MaxLength] - Longueur maximale du texte**

```c#
[MaxLength(500, ErrorMessage = "Le contenu ne peut pas dépasser 500 caractères.")]
public string Contenu { get; set; }

```

### Validation des nombres

**[Range] - Plage de valeurs acceptées**

```c#
[Range(1, 100, ErrorMessage = "Le stock doit être entre 1 et 100.")]
public int Stock { get; set; }
```

### Validation des dates

**[DataType(DataType.Date)] - Validation d’une date**

```c#
[DataType(DataType.Date, ErrorMessage = "Format de date invalide.")]
public DateTime DatePublication { get; set; }
```

**[Range] - Définir une plage de dates**

```c#
[Range(typeof(DateTime), "2000-01-01", "2100-12-31", ErrorMessage = "La date doit être entre 2000 et 2100.")]
public DateTime DatePublication { get; set; }
```

### Validation des formats

**[DataType(DataType.EmailAddress)] - Email valide**

```c#
[DataType(DataType.EmailAddress, ErrorMessage = "L'adresse courriel est invalide.")]
public string Email { get; set; }
```

**[EmailAddress] - Vérification stricte d’email**

```c#
[EmailAddress(ErrorMessage = "Veuillez entrer une adresse email valide.")]
public string Email { get; set; }
```

**[Url] - URL valide**

```c#
[Url(ErrorMessage = "L'URL fournie n'est pas valide.")]
public string SiteWeb { get; set; }

```

**[Phone] - Numéro de téléphone valide**

```
[Phone(ErrorMessage = "Le numéro de téléphone n'est pas valide.")]
public string Telephone { get; set; }
```

### Validation de motifs personnalisés

**[RegularExpression] - Utilisation d’une expression régulière**

```c#
[RegularExpression(@"^[A-Za-z ]+$", ErrorMessage = "Le nom ne doit contenir que des lettres.")]
public string Nom { get; set; }
```

### Validation avancée

**[Compare] - Comparaison avec un autre champ**

```c#
[Compare("MotDePasse", ErrorMessage = "Les mots de passe ne correspondent pas.")]
public string ConfirmationMotDePasse { get; set; }
```

**[CreditCard] - Vérifier un numéro de carte bancaire**

```c#
[CreditCard(ErrorMessage = "Numéro de carte bancaire invalide.")]
public string NumeroCarte { get; set; }
```

**[FileExtensions] - Vérifier les extensions de fichiers**

```c#
[FileExtensions(Extensions = "jpg,jpeg,png", ErrorMessage = "Seuls les fichiers JPG et PNG sont autorisés.")]
public string FichierImage { get; set; }
```

### Validation personnalisée avec `CustomValidation`

On peut créer une validation personnalisée avec `CustomValidation`.

```c#
public class DateValidation
{
    public static ValidationResult DateFutur(DateTime date, ValidationContext context)
    {
        if (date < DateTime.Today)
        {
            return new ValidationResult("La date doit être dans le futur.");
        }
        return ValidationResult.Success;
    }
}
```

Utilisation de la validation personnalisée

```c#
[CustomValidation(typeof(DateValidation), "DateFutur")]
public DateTime DateEvenement { get; set; }
```

## Afficher un nom personnalisé dans les vues Razor à partir du modèle

**- [Display(Name = "...")] - remplace le nom par défaut de la propriété**

```c#
[Required]
[Display(Name = "Contenu détaillé")]
public string Contenu { get; set; }
```


## Utilisation des paramètres dans les messages d'erreur

Certains DataAnnotations prennent en charge les paramètres dynamiques, comme `[StringLength]`,` [Range]`, `[Compare]`, etc.

**[StringLength] avec paramètres**

```c#
[StringLength(100, MinimumLength = 5, ErrorMessage = "Le titre doit avoir entre {2} et {1} caractères.")]
public string Titre { get; set; }

```
- {1} : La valeur maximale (100)
- {2} : La valeur minimale (5)


**[Range] avec paramètres**

```c#
[Range(1, 10, ErrorMessage = "La note doit être comprise entre {1} et {2}.")]
public int Note { get; set; }
```
- {1} : La valeur minimale (1)
- {2} : La valeur maximale (10)

**[RegularExpression] avec paramètres**

```c#
[RegularExpression(@"^\d{4}-\d{4}$", ErrorMessage = "Le format doit être {0}.")]
public string CodeSecurite { get; set; }
```
- {0} : Le format attendu (ex: 1234-5678)

## DataAnnotations pour la Création de la Base de Données

### [Key] - Définir une Clé Primaire

```c#
[Key]
public int Id { get; set; } // Clé primaire
```
Si aucune clé n'est spécifiée, EF Core cherche une propriété **Id** ou **ClassNameId** par défaut.

### [DatabaseGenerated] - Auto-incrémentation

```c#
[Key]
[DatabaseGenerated(DatabaseGeneratedOption.Identity)]
public int Id { get; set; } // Auto-incrémenté
```

### [ForeignKey] - Définir une Clé Étrangère 

```c#
[ForeignKey("Auteur")]
public int AuteurId { get; set; } // Clé étrangère

public Auteur Auteur { get; set; }
```

- `AuteurId` devient une clé étrangère qui référence la table Auteurs.
- La propriété `Auteur` permet la navigation entre les entités.

### [DefaultValue] - Définir une valeur par Défaut

```c#
[DefaultValue("Non spécifié")]
public string Categorie { get; set; }

```

### [NotMapped] - indique qu’un champ ne doit pas être stocké en base.

```c#
[NotMapped]
public string Resume { get; set; } // Ce champ ne sera pas dans la table
```

### **Nommer une Table ou une Colonne**

L’attribut [Table] permet de spécifier un nom personnalisé pour une table et [Column] pour une colonne.

```c#
[Table("MesArticles")]
public class Article
{
    [Key]
    [Column("ArticleID")]
    public int Id { get; set; }

    public string Titre { get; set; }
}
```













