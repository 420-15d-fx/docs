# Utilisation d'un API externe

## Qu'est-ce qu'une API 

Une API (**Application Programming Interface**) est un **ensemble de règles et d'outils** permettant à **deux applications de communiquer entre elles**.
Elle définit comment une application peut demander des données ou des services à une autre application.

Les API sont utilisées pour : 

- Récupérer des données externes (ex: météo, articles, taux de change).
- Envoyer des données à un service (ex: formulaire d'inscription, paiement en ligne).
- Interagir avec des applications tierces (ex: connexion avec Google ou Facebook).


## Comment obtenir des données d'une API

### 1. Ajouter le service HttpClient dans Program.cs

`Fichier Program.cs`

```c#
using Microsoft.AspNetCore.Builder;
using Microsoft.Extensions.DependencyInjection;

var builder = WebApplication.CreateBuilder(args);

// Ajout du service HttpClient pour les requêtes API
builder.Services.AddHttpClient();   // [!code focus]

builder.Services.AddRazorPages();

var app = builder.Build();

app.UseStaticFiles();
app.UseRouting();
app.UseAuthorization();
app.MapRazorPages();

app.Run();
```

### 2. Créer un modèle pour représenter les donnnées récupérés depuis l'API

Nous allons créer une classe ExternalArticle pour stocker les données récupérées.

`Fichier Models/ApiArticle.cs`

```c#
public class ApiArticle
{
    public int Id { get; set; } 

    public string Title { get; set; }

    public string Body { get; set; }
}

``` 


### 3. Récupérer les données depuis l'API dans

`Fichier Pages/Articles/ApiArticles`

```c#
using BlogApp.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;

namespace BlogApp.Pages.Articles
{
   


    public class ApiArticlesModel : PageModel
    {

        private readonly HttpClient _httpClient; 

        public List<ApiArticle> Articles { get; set; } = new();

        public ApiArticlesModel(HttpClient httpClient)
        {
            _httpClient = httpClient;
        }
        public async Task OnGet()
        {

            string apiUrl = "https://jsonplaceholder.typicode.com/posts";

            List<ApiArticle> response = await _httpClient.GetFromJsonAsync<List<ApiArticle>>(apiUrl);

            if (response is not null)
            {
                Articles = response;
            }
        }
    }
}
```


