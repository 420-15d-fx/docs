# Formulaires

Voici comment utilser les formulaires pour transmettre de l'information au code-behind :

**Fichier Pages/Create.cshtml**

```html
@page
@model CreateModel

<h1>Créer un article</h1>

<form method="post" class="border p-4 shadow rounded bg-light">
                
    <div class="mb-3">
        <label for="titre" class="form-label">Titre :</label>
        <input asp-for="Titre" class="form-control" placeholder="Entrez le titre de l'article" />
    </div>

    <div class="mb-3">
        <label for="contenu" class="form-label">Contenu :</label>
        <textarea asp-for="Contenu" class="form-control" rows="5" placeholder="Rédigez votre article ici..."></textarea>
    </div>

    <div class="mb-3">
        <label for="categorie" class="form-label">Catégorie :</label>
        <select asp-for="CategorieSelectionnee" asp-items="Model.Categories" class="form-select">
            <option value="">-- Sélectionnez une catégorie --</option>
        </select>
    </div>

    <button type="submit" class="btn btn-primary w-100">Publier</button>
</form>

@if (Model.Message != null)
{
    <p style="color: green;">@Model.Message</p>
}

<a asp-page="/Index">Retour à l'accueil</a>

```

L'utilsation de TagHelper permet facilement de générer et lier les contrôle du formulaires aux propriétés du modèle du code-behind 

- `<input asp-for="Titre" />` et `<textarea asp-for="Contenu" />` lient automatiquement les champs HTML aux propriétés du modèle.
- `<select asp-for="CategorieSelectionnee" asp-items="Model.Categories">` Lie la liste des catégories (Categories) au select.
- Le message `Model.Message` est affiché après la soumission.


**Fichier Pages/Create.cshtml.cs (code-behind)**

```c#
using System.ComponentModel.DataAnnotations;
using Microsoft.AspNetCore.Mvc.RazorPages;

public class CreateModel : PageModel
{
    [BindProperty]
    public string Titre { get; set; }

    [BindProperty]
    public string Contenu { get; set; }

    [BindProperty]
    public string CategorieSelectionnee { get; set; }

    public List<SelectListItem> Categories { get; set; }

    public string Message { get; set; }

    public static List<Article> Articles = new List<Article>();

    public void OnGet()
    {
        //Chargement de la liste des catégories
        Categories = new List<SelectListItem>
        {
            new SelectListItem { Value = "Tech", Text = "Technologie" },
            new SelectListItem { Value = "Science", Text = "Science" },
            new SelectListItem { Value = "Sport", Text = "Sport" },
            new SelectListItem { Value = "Culture", Text = "Culture" }
        };
    }

    public void OnPost()
    {
        
        Articles.Add(new Article
        {
            Titre = Titre,
            Contenu = Contenu,
            Categorie = CategorieSelectionnee
        });

        Message = $"Article '{Titre}' ajouté avec succès !";
    }
}

//Classe représentant un article
public class Article
{
    public string Titre { get; set; }
    public string Contenu { get; set; }
    public string Categorie { get; set; }
}
```
- Par défaut, lors de l'envoi du formulaire la méthode `OnPost()` est exécutée.
- `[BindProperty] `: Permet d'associer les champs Titre, Contenu et Categorie au formulaire HTML.







