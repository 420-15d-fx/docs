# Architecture MVC

Le modèle architectural MVC (Model-View-Controller) sépare une application en trois composants principaux : le modèle, la vue et le contrôleur.

![Modification de visual studio](https://learn.microsoft.com/fr-ca/aspnet/mvc/overview/older-versions-1/overview/asp-net-mvc-overview/_static/image1.jpg)

- **Model** : Les objets de modèle sont les parties de l’application qui implémentent la logique pour le domaine de données de l’application. Souvent, ils récupèrent l'état du modèle et le stockent dans une base de données.

- **View** : Les vues sont les composants qui affichent l’interface utilisateur de l’application. En général, cette interface utilisateur est créée à partir des données du modèle. 

- **Controller** :  Les contrôleurs sont les composants qui gèrent les interventions de l'utilisateur, exploitent le modèle et finalement sélectionnent une vue permettant d'afficher  l'interface utilisateur.

Dans une application MVC, la vue affiche uniquement des informations ; le contrôleur gère les entrées et interactions des utilisateurs, et y répond. 

Le modèle MVC vous aide à créer des applications qui séparent les différents aspects de l'application (logique d'entrée, logique métier et logique de l'interface utilisateur) en assurant un couplage lâche entre ces éléments. 


## ASP.NET MVC vs ASP.Net Razor pages

ASP.NET Core offre la possibilité de développer différents types d’applications Web, notamment **ASP.NET MVC** et **ASP.NET Razor Pages**. Ces deux approches se ressemblent beaucoup, mais diffèrent sur quelques points.

### L'architecture ASP.NET MVC suit le modèle MVC (Model-View-Controller)

- Model : représente la logique métier et l’accès aux données.
- View : représente l’interface utilisateur (HTML + Razor).
- Controller : gère les requêtes HTTP, interagit avec le modèle et retourne une vue.

### ASP.NET Razor Pages

Razor Pages est une alternative plus simple et plus intuitive à ASP.NET MVC. Il s’agit d’un modèle basé sur les pages, où chaque page .cshtml est associée à un fichier code-behind .cshtml.cs pour gérer la logique. 

 - Approche "page-centric" : Chaque page est autonome et gère ses propres requêtes.
 - Code-behind simplifié : La logique métier se trouve dans un fichier .cshtml.cs attaché à chaque page.
 - Moins de complexité : Pas besoin de contrôleurs séparés, ce qui facilite l'organisation du code.


|Critère	|ASP.NET Core MVC|	ASP.NET Core Razor Pages
|-----------|----------------|---------------------------|
|Structure	|Séparé en Modèle, Vue et Contrôleur	|Fichiers .cshtml combinant la vue et la logique
|Complexité	|Plus complexe, adapté aux grandes applications	|Plus simple et plus rapide à mettre en place
|Idéal pour	|Applications complexes, API REST	|Applications CRUD, sites web classiques
|Gestion des routes|	Basé sur Controllers et Actions	|Basé sur la structure des fichiers .cshtml


## Quelle approche choisir ?
 - Si vous développez une application avec une structure complexe, où la séparation des responsabilités est essentielle alors **ASP.NET MVC** est recommandé.
- Si vous créez une application CRUD ou avec des pages indépendantes, où la rapidité et la simplicité sont prioritaires alors **ASP.NET Razor Pages** est un meilleur choix.

## Source & en savoir plus

- https://learn.microsoft.com/fr-ca/aspnet/mvc/overview/older-versions-1/overview/asp-net-mvc-overview
