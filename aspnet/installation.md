

# Installation

1) Ouvrir Visual Studio Installer
2) Cliquer sur le boution Modifier 

![Modification de visual studio](./img/aspnet_installation_1.png)

3) Dans l'onglet Charges de travail, sélectionner : Développement web et ASP.NET


![Modification de visual studio](./img/aspnet_installation_2.png)


4) Installer SQL Server Management Studio

https://learn.microsoft.com/fr-ca/sql/ssms/download-sql-server-management-studio-ssms?view=sql-server-ver16


