# LINQ 

LINQ (Language Integrated Query) est un ensemble d'opérateurs de requêtes intégrés au langage C# qui permettent de manipuler et interroger des collections de données de manière déclarative, similaire à SQL.

LINQ permet de travailler avec :

- Collections en mémoire (ex:` List<T>`, Array, Dictionary)
- Bases de données via Entity Framework Core
- XML, JSON, et autres sources de données

Avec EF Core, LINQ permet d’écrire des requêtes sur la base de données en utilisant du code C#, au lieu du SQL brut.

## Fonctionnement de LINQ avec Entity Framework Core

### LINQ et` DbSet<T>`

Dans EF Core, les entités sont représentées par des` DbSet<T>` qui correspondent aux tables de la base de données.

```c#
public class ApplicationDbContext : DbContext
{
    public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options) : base(options)
    {

    }

    public DbSet<Article> Articles { get; set; }
    public DbSet<Categorie> Categories { get; set; }

}
```
Avec LINQ, on peut écrire des requêtes comme :

```c#

var articles = context.Articles.Where(a => a.Titre.Contains("ASP.NET")).ToList();

```

- `context` est une instance de la classe `ApplicationDbContext` qui hérite de `DbContext` et qui représente la base de données.


### Principe d'utilisation de context

Dans ASP.NET Core, `context` (instance de DbContext) est injecté via le constructeur grâce à l’injection de dépendances.
Il est ensuite utilisé dans les méthodes comme `OnGet()`, `OnPost()` pour récupérer, ajouter, modifier ou supprimer des données.


**Exemple : Pour afficher la liste des catégories (OnGet)**

**Fichier Index.cshtml.cs**

```c#
 public class IndexModel : PageModel
 {
     private readonly ApplicationDbContext _context;

     public IndexModel(ApplicationDbContext context)
     {
             _context = context;
     }

     public List<Categorie> Categories { get; set; } = new();

     public async Task OnGet()
     {
         Categories = await _context.Categories.ToListAsync();
     }
 }
 ```

- `async Task` est utilisé pour exécuter des opérations asynchrones sans bloquer le thread principal.

    - **async** : Indique que la méthode est asynchrone.
    - **Task** : Représente une opération qui s'exécute en arrière-plan et retourne un résultat ou rien.

    L'objectif principal est d'améliorer la réactivité et les performances en évitant de bloquer le thread principal.

**Fichier Pages/Categories/Index.cshtml**

```razor
@page
@model IndexModel
@{
    ViewData["Title"] = "Liste des Catégories";
}

<h2>Liste des Catégories</h2>

<table>
    <tr>
        <th>Nom</th>
    </tr>
    @foreach (var categorie in Model.Categories)
    {
        <tr>
            <td>@categorie.Nom</td>
        </tr>
    }
</table>

<a asp-page="Create">Créer une nouvelle catégorie</a>
```

### Principaux opérateurs LINQ avec EF Core

#### Sélectionner toutes les données (ToList)
```c#
var articles = context.Articles.ToList();
```

#### Filtrer les données (Where)

```c#
var articles = context.Articles.Where(a => a.Titre.Contains("ASP.NET")).ToList();

```


#### Trier les résultats (OrderBy, OrderByDescending)

```c#
var articles = context.Articles.OrderBy(a => a.DatePublication).ToList();
```

#### Récupérer un élément spécifique (First, FirstOrDefault)

```c#
var article = context.Articles.First(a => a.Titre == "Introduction à ASP.NET Core");

```

Différences entre First() et FirstOrDefault() :

- First() : Renvoie le premier élément, lève une exception si aucun élément n'est trouvé.
- FirstOrDefault() : Renvoie null si aucun élément n'est trouvé.


#### Récupérer un élément par son ID (Find)

Si l'entité a une clé primaire, `Find` est plus performant car il utilise le cache EF Core.

```c#
var article = context.Articles.Find(1);
```

#### Joindre des entités (Include, ThenInclude)

Dans une relation **1:N** (Article → Categorie), on peut charger les catégories liées.

```c#
var articlesAvecCategories = context.Articles.Include(a => a.Categories).ToList();
```
- Il sera ainsi possible d'accéder au détails de la catégorie associée à l'article.

Si les catégories ont d'autres relations (ex: Auteur), on utilise ThenInclude :

```c#
var articles = context.Articles
    .Include(a => a.Categories)
        .ThenInclude(c => c.Auteur)
    .ToList();
```

#### Compter des éléments (Count)

```c#
int nombreArticles = context.Articles.Count();
```

####  Vérifier l'existence d'un élément (Any)

```c#
bool existe = context.Articles.Any(a => a.Titre == "Entity Framework Core");
```

#### Projection de données (Select)
```c#
var titres = context.Articles.Select(a => a.Titre).ToList();
```

## Instructions INSERT, UPDATE et DELETE en Entity Framework Core

### INSERT - Ajouter une nouvelle entrée

Exemple : Ajouter une nouvelle catégorie

```c#
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using System.Threading.Tasks;

public class CreateModel : PageModel
{
    private readonly ApplicationDbContext _context;

    public CreateModel(ApplicationDbContext context)
    {
        _context = context;
    }

    [BindProperty]
    public Categorie Categorie { get; set; } = new();

    public async Task<IActionResult> OnPostAsync()
    {
       
        _context.Categories.Add(Categorie); // Prépare l'ajout
        await _context.SaveChangesAsync();  // Exécute l'INSERT

        return RedirectToPage("./Index");
    }
}

```
### UPDATE - Modification d’une catégorie

Exemple : Modifier une catégorie

```c#
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;

public class EditModel : PageModel
{
    private readonly ApplicationDbContext _context;

    public EditModel(ApplicationDbContext context)
    {
        _context = context;
    }

    [BindProperty]
    public Categorie Categorie { get; set; } = null!;

    public async Task<IActionResult> OnGetAsync(int id)
    {
        Categorie = await _context.Categories.FindAsync(id);

        if (Categorie == null)
        {
            return NotFound();
        }

        return Page();
    }

    public async Task<IActionResult> OnPostAsync()
    {
       
        _context.Attach(Categorie).State = EntityState.Modified;
        await _context.SaveChangesAsync();

        return RedirectToPage("./Index");
    }
}

```

**Attach et EntityState.Modified**

Lorsque vous mettez à jour un objet en Entity Framework Core (EF Core), il arrive que l'objet ne soit pas suivi (detached) par le `DbContext`. Pour indiquer à EF Core que l'objet a été modifié, on utilise :

```c#
_context.Attach(Categorie).State = EntityState.Modified;

```
Cela informe EF Core que toutes les propriétés de l’objet Categorie doivent être considérées comme modifiées et enregistrées dans la base de données.

Lorsque vous récupérez une entité via `FindAsync()` ou `FirstOrDefaultAsync()`, elle est automatiquement suivie par `DbContext`.
Cependant, si vous passez un objet modifié manuellement (ex: via un formulaire HTML), EF Core ne sait pas s'il provient de la base.



### DELETE - Modification d’une catégorie

Exemple : Suppression d'une catégorie

```c#
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;

public class DeleteModel : PageModel
{
    private readonly ApplicationDbContext _context;

    public DeleteModel(ApplicationDbContext context)
    {
        _context = context;
    }

    [BindProperty]
    public Categorie Categorie { get; set; } = null!;

    public async Task<IActionResult> OnGetAsync(int id)
    {
        Categorie = await _context.Categories.FindAsync(id);

        if (Categorie == null)
        {
            return NotFound();
        }

        return Page();
    }

    public async Task<IActionResult> OnPostAsync(int id)
    {
        var categorie = await _context.Categories.FindAsync(id);

        if (categorie != null)
        {
            _context.Categories.Remove(categorie); //Prépare la suppression
            await _context.SaveChangesAsync(); // Exécute le DELETE
        }

        return RedirectToPage("./Index");
    }
}

```
