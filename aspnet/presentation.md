

# Présentation de ASP.Net Core

ASP.NET Core est un `framework` open-source et multiplateforme pour développer des applications web modernes et performantes. Il fait partie de l'écosystème .NET et permet de créer :

- Applications web (avec MVC, Razor Pages, Blazor)
- APIs REST
- Applications temps réel (avec SignalR)
- Microservices et applications cloud

https://dotnet.microsoft.com/fr-fr/apps/aspnet

## Environnement de développement

- Langages : C#, VB.NET, F#
- Visual Studio
- .Net SDK 6.0
- Mapping objet-relationnel (ORM) : Entity framwork avec SQL Server.

## Pourquoi utiliser ASP.NET Core ?

- **Performance et optimisation** :  ASP.NET Core est l’un des frameworks web les plus rapides.
- **Supporte l’injection de dépendances (DI)** : facilite la gestion des services.
- **Sécurité avancée** : supporte l’authentification, l’autorisation et la protection contre CSRF/XSS.
- **Architecture modulaire** : permet de n’inclure que les composants nécessaires.
- **Intégration avec les technologies modernes** : compatible avec Docker, Kubernetes, Blazor et les bases de données SQL/NoSQL.
- **Écosystème riche** :  Large communauté, support de Visual Studio, et bibliothèques nombreuses pour simplifier le développement.
