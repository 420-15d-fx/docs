# Autorisation

En ASP.NET Core Razor Pages,il est possible sécuriser l'accès aux pages et aux zones (Areas) en utilisant plusieurs approches basées sur l'authentification et l'autorisation. Voici les différentes manières de gérer l'accès en fonction de si l'utilisateur est connecté ou s'il appartient à un rôle spécifique.



## Protéger une Page Individuelle

Il est possible restreindre l'accès à une page Razor spécifique en ajoutant `[Authorize]` au niveau du fichier PageModel :

```c#
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc.RazorPages;

[Authorize]
public class CreateModel : PageModel
{
    public void OnGet()
    {
    }
}
``` 

Seuls les utilisateurs authentifiés peuvent accéder à cette page. Les utilisateurs non connectés seront redirigés vers la page de connexion.

## Protéger une Page pour un Rôle Spécifique

Il est également possible de restreindre l'accès à seulement certaines catégories d’utilisateurs (ex: Admin) en ajoutant l’attribut `[Authorize(Roles = "Admin")]` :

```c#
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc.RazorPages;

[Authorize(Roles = "Admin")]
public class CreateModel : PageModel
{
    public void OnGet()
    {
    }
}
``` 

## Protéger un Dossier Complet (Pages & Areas)


Pour protéger un dossier entier contenant plusieurs pages, il est possible ajouter une politique de sécurité dans le fichier `Program.cs` en appliquant [Authorize] à toutes les pages d'un répertoire.


### Ajouter une règle de sécurité via Program.cs

```c#
builder.Services.AddRazorPages(options =>
{
    options.Conventions.AuthorizeFolder("/Admin"); // Protège toutes les pages dans /Pages/Admin
});
```

- toutes les pages dans `/Pages/Admin` sont accessibles uniquement aux utilisateurs **authentifiés**.


### Ajouter une règle de sécurié à partir d'une politique d'authorisation

Il est possible définir une politique d’autorisation et l’appliquer aux pages ou Areas. 

1) Créer la Politique d’Autorisation

Pôur restreindre l'accès à un **rôle spécifique**, on doit créer une politique avec `RequireRole()` dans `Program.cs` :

```c#
builder.Services.AddAuthorization(options =>
{
    options.AddPolicy("AdminOnly", policy =>
        policy.RequireRole("Admin")); // Seuls les utilisateurs avec le rôle "Admin" peuvent accéder
});
```

2) Restreindre l'accès à un dossier pour un rôle spécifique


```c#
builder.Services.AddRazorPages(options =>
{
    options.Conventions.AuthorizeFolder("/Admin", "AdminOnly");
});
```

3) Restreindre un `Area` complet à un rôle spécifique

```c#
builder.Services.AddRazorPages(options =>
{
    options.Conventions.AuthorizeAreaFolder("Admin", "/", "AdminOnly");
});
```

## Gérer les Utilisateurs Non Authentifiés


Si un utilisateur non connecté essaie d’accéder à une page restreinte, il sera automatiquement redirigé vers la page de connexion.

Le fichier de configuration appsettings.json contient cette route par défaut :

```c#
"Authentication": {
    "LoginPath": "/Account/Login"
}
```

On peut également spécifier la redirection dans `Program.cs` :

```c#
builder.Services.ConfigureApplicationCookie(options =>
{
    options.LoginPath = "/Utilisateur/Connexion"; // Redirige vers /Utilisateur/Connexion au lieu de /Account/Login
    options.AccessDeniedPath = "/Utilisateur/AccesRefuse"; // Page d'erreur si accès refusé
});
```

### Gérer l’Accès Dynamique dans les Pages

Pour contrôler l’accès directement dans une page sans bloquer l’accès immédiatement, on peut utiliser `User.Identity.IsAuthenticated` et `User.IsInRole()` :

```razor
@if (User.Identity.IsAuthenticated)
{
    <p>Bienvenue, @User.Identity.Name !</p>
}
else
{
    <p>Veuillez vous <a asp-page="/Account/Login">connecter</a>.</p>
}

@if (User.IsInRole("Admin"))
{
    <p>Bienvenue sur le tableau de bord administrateur.</p>
}
```

### Rediriger Manuellement un Utilisateur

Dans le PageModel, tu peux vérifier si l’utilisateur est autorisé et le rediriger manuellement :

```c#
public class CreateModel : PageModel
{
    public IActionResult OnGet()
    {
        if (!User.Identity.IsAuthenticated)
        {
            return RedirectToPage("/Account/Login");
        }
        else if (!User.IsInRole("Admin"))
        {
            return Forbid(); // Retourne une erreur 403 Accès refusé
        }

        return Page();
    }
}
```